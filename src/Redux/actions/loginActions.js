import {
  AUTH_SUCCESS, AUTH_PENDING,
  AUTH_ERROR, DESTROY_SESSION,
} from '../types'
import ClientBFF from '../../common/ClientBFF'
import ClientAuth from '../../common/ClientAuth'
import { removeAuthData, setLocalToken } from '../../Helper'
import { userRoles } from '../../../test/Mocks/userRoles'

export const addAuthSuccesUser = payload => ({
  type: AUTH_SUCCESS,
  payload,
})

export const addAuthPendingUser = payload => ({
  type: AUTH_PENDING,
  payload,
})

// Si se presenta error al guardar
export const addAuthErrorUser = payload => ({
  type: AUTH_ERROR,
  payload,
})

export const authUserCL = (form) => {
  const loginTimeStamp = Date.now().toString().replace(/0\./, '')
  return async (dispatch) => {
    try {
      const clientAuth = new ClientAuth()

      const { token } = await clientAuth.authUser(form)
      setLocalToken(token)
      const user = await clientAuth.getAuth(token)

      removeAuthData()
      dispatch(addAuthSuccesUser({
        loginInfo: user,
        result: 'success',
        loginTimeStamp,
      }))
    } catch (error) {
      // si hay un error, cambiar el state
      dispatch(addAuthErrorUser({
        loginInfo: {},
        result: 'error',
        loginTimeStamp,
      }))
    }
  }
}

export const userValidateAction = (code) => {
  const loginTimeStamp = Date.now().toString().replace(/0\./, '')
  return async (dispatch) => {
    try {
      const authData = await new ClientBFF().auth({ authorizationCode: code })
      const isDev = authData.stageName === 'dev'
      const userInfo = isDev ? userRoles : authData

      // when is dev or the process is after pingFed (url has a code)
      if (code || isDev) {
        removeAuthData()
        dispatch(addAuthSuccesUser({
          loginInfo: userInfo,
          result: 'success',
          loginTimeStamp,
        }))
        return
      }

      // When env is not dev and process is before pingFed (url has no code)
      dispatch(addAuthPendingUser({
        loginInfo: {},
        result: 'pending',
        loginTimeStamp,
      }))
    } catch (error) {
      // si hay un error, cambiar el state
      dispatch(addAuthErrorUser({
        loginInfo: {},
        result: 'error',
        loginTimeStamp,
      }))
    }
  }
}

export const setDestroySession = () => (dispatch) => {
  dispatch({
    type: DESTROY_SESSION,
  })
}
