import { SET_WINDOW_SIZE } from '../types'

const setWindowSize = windowSize => (dispatch) => {
  dispatch({
    type: SET_WINDOW_SIZE,
    payload: {
      windowSize,
    },
  })
}
export default setWindowSize
