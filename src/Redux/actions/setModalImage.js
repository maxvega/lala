import { SET_MODAL_IMAGE_OPEN, SET_MODAL_IMAGE_CLOSE, SET_IMAGE_TO_DISPLAY } from '../types'

export const setModalImageOpen = () => (dispatch) => {
  dispatch({
    type: SET_MODAL_IMAGE_OPEN,
  })
}

export const setModalImageClose = () => (dispatch) => {
  dispatch({
    type: SET_MODAL_IMAGE_CLOSE,
  })
}

export const setModalImageToDisplay = image => (dispatch) => {
  dispatch({
    type: SET_IMAGE_TO_DISPLAY,
    payload: {
      imageToDisplay: image,
    },
  })
}
