import { SET_MODAL_IMAGE_OPEN, SET_MODAL_IMAGE_CLOSE, SET_IMAGE_TO_DISPLAY } from '../types'

const initState = {
  isModalImageOpen: false,
  imageToDisplay: '',
}
export default (state = initState, action) => {
  switch (action.type) {
    case SET_MODAL_IMAGE_OPEN:
      return Object.assign({}, state, { isModalImageOpen: true })
    case SET_MODAL_IMAGE_CLOSE:
      return Object.assign({}, state, { isModalImageOpen: false })
    case SET_IMAGE_TO_DISPLAY:
      return Object.assign({}, state, action.payload)
    default:
      return state
  }
}
