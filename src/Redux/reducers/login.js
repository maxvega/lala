import {
  AUTH_SUCCESS,
  AUTH_PENDING,
  AUTH_ERROR,
  DESTROY_SESSION,
} from '../types'

const initState = {
  loginInfo: undefined,
  result: '',
  message: '',
}

/*
export const ERROR_MESSAGE = 'Credenciales Invalidas'
export const AUTH_SUCCESS = 'login:success'
export const AUTH_ERROR = 'login:error'
*/

export default function (state = initState, action) {
  switch (action.type) {
    case AUTH_SUCCESS:
      return Object.assign({}, state, action.payload)
    case AUTH_PENDING:
      return Object.assign({}, state, action.payload)
    case AUTH_ERROR:
      return Object.assign({}, state, action.payload)
    case DESTROY_SESSION:
      return initState
    default:
      return state
  }
}
