import React from 'react'
import Home from '../Pages/Home'

export default props => (
  <div>
    <Home {...props} />
  </div>
)
