import React from 'react'
import LegalInfo from '../Pages/LegalInfo'

export default props => (
  <div>
    <LegalInfo {...props} />
  </div>
)
