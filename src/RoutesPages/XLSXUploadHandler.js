import React from 'react'
import XLSXUploadHandler from 'Src/components/XLSXUploadHandler'

export default props => (
  <div>
    <XLSXUploadHandler {...props} />
  </div>
)
