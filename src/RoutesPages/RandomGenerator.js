import React from 'react'
import RandomGenerator from '../Pages/RandomGenerator'

export default props => (
  <div>
    <RandomGenerator {...props} />
  </div>
)
