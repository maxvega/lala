import React from 'react'
import Profile from '../Pages/Profile'

export default props => (
  <div>
    <Profile {...props} />
  </div>
)
