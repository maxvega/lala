import React from 'react'
import HotjarCategories from '../Pages/HotjarCategories'

export default props => (
  <div>
    <HotjarCategories {...props} />
  </div>
)
