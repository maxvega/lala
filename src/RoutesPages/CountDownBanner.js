import React from 'react'
import BannersPage from 'Src/components/BannersPage'

export default props => (
  <div>
    <BannersPage {...props} />
  </div>
)
