import React from 'react'
import ProductsSelection from '../Pages/ProductsSelection'

export default props => (
  <div>
    <ProductsSelection {...props} />
  </div>
)
