import React from 'react'
import HeaderAlertMessage from '../Pages/HeaderAlertMessage'

export default props => (
  <div>
    <HeaderAlertMessage {...props} />
  </div>
)
