import React from 'react'
import FreeShippingPromotion from '../Pages/Checkout/FreeShippingPromotion'

export default props => (
  <div>
    <FreeShippingPromotion {...props} />
  </div>
)
