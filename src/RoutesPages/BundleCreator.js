import React from 'react'
import UnitBundle from '../Pages/Products/BundleCreator/UnitBundle'
import BulksBundles from '../Pages/Products/BundleCreator/BulksBundles'

export default (props) => {
  // eslint-disable-next-line no-unused-vars,react/prop-types
  const { type } = props

  return (
    <div>
      {(type && type === 'unit') ? <UnitBundle {...props} /> : <BulksBundles {...props} />}
    </div>
  )
}
