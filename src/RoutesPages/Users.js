import React from 'react'
import Users from '../Pages/Users'

export default props => (
  <div>
    <Users {...props} />
  </div>
)
