import React from 'react'
import Roles from '../Pages/Roles'

export default props => (
  <div>
    <Roles {...props} />
  </div>
)
