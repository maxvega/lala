import React from 'react'
import Product from '../Pages/Reports/Product'

export default props => (
  <div>
    <Product {...props} />
  </div>
)
