import React from 'react'
import Permissions from '../Pages/Permissions'

export default props => (
  <div>
    <Permissions {...props} />
  </div>
)
