import React from 'react'
import Categories from '../Pages/Categories'

export default props => (
  <div>
    <Categories {...props} />
  </div>
)
