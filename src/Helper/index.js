const configData = typeof window.__ENV__ !== 'undefined' ? window.__ENV__ : { }
const stratiBlacklist = ['app/roles', 'app/permissions', 'app/users']

export const getLocalToken = () => localStorage.getItem('token') || ''
export const setLocalToken = token => localStorage.setItem('token', token)
export const removeLocalToken = () => localStorage.removeItem('token')

// eslint-disable-next-line max-len
const formatWithBlacklist = array => array && array.filter(({ eventKey }) => !stratiBlacklist.includes(eventKey))

export const calculateMenuToDisplay = (permissions, navigationList) => {
  const menuToDisplay = []
  const { IS_STRATI = false } = configData
  const permissionNames = permissions.map(permissions => permissions.name)
  navigationList.forEach((navigationItem) => {
    if (IS_STRATI) navigationItem.children = formatWithBlacklist(navigationItem.children)

    if (navigationItem.permission && permissionNames.includes(navigationItem.permission)) {
      menuToDisplay.push(navigationItem)
    } else if (navigationItem.children && navigationItem.children.length > 0) {
      const navItemWithChildren = JSON.parse(JSON.stringify(navigationItem))
      navItemWithChildren.children = []
      navigationItem.children.forEach((navChild) => {
        if (navChild.permission && permissionNames.includes(navChild.permission)) {
          navItemWithChildren.children.push(navChild)
        }
      })
      if (navItemWithChildren.children.length > 0) {
        menuToDisplay.push(navItemWithChildren)
      }
    }
  })

  return menuToDisplay
}
export const formatBytes = (bytes, decimals = 2) => {
  if (bytes === 0) return '0 Bytes'
  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  // eslint-disable-next-line no-restricted-properties
  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}
export const allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i
export const maxBytesAllowed = 400000
export const isExtensionValid = filePath => allowedExtensions.exec(filePath)
export const JSONToFile = (json, fileName) => {
  const stringifiedJSON = JSON.stringify(json)
  const blobJSON = new Blob([stringifiedJSON], { type: 'application/json' })
  return new File([blobJSON], fileName, {
    type: 'application/json',
  })
}
export const groupArrayByKey = (arrayOfObjects, key) => {
  const response = arrayOfObjects.map(object => object[key])
    .filter(object => object)
  return { tab: key, products: response }
}

export const generateChunksOfArray = (array, chunkSize) => {
  const R = []
  for (let i = 0; i < array.length; i += chunkSize) R.push(array.slice(i, i + chunkSize))
  return R
}

export const generateImageFormData = (file, container, keepOriginalName = true) => {
  const formData = new FormData()
  formData.append('file', file)
  formData.append('storageContainer', container)
  formData.append('keepOriginalName', keepOriginalName.toString())
  return formData
}

export const generateJSONFormData = (file, container, keepOriginalName = true) => {
  const formData = new FormData()
  formData.append('file', file)
  formData.append('storageContainer', container)
  formData.append('keepOriginalName', keepOriginalName.toString())
  return formData
}

export const fileNameEnhancer = (file) => {
  const originalName = file.name
  const indexOfDot = originalName.lastIndexOf('.')
  const nameToEnhance = [originalName.slice(0, indexOfDot), originalName.slice(indexOfDot)]
  nameToEnhance[0] += `-${Date.now()}`
  return new File([file], nameToEnhance.join(''), { type: file.type })
}

export const cleanString = (str) => {
  if (str) {
    return str.replace(/"|'/g, '')
  }
  return ''
}

export const saveAuthData = (loginInfo) => {
  const { LOCAL_AUTH_KEY = undefined } = configData
  const user = loginInfo.user || loginInfo
  const userInfo = JSON.parse(localStorage.getItem(LOCAL_AUTH_KEY))
  localStorage.setItem(window.__ENV__.LOCAL_AUTH_KEY, JSON.stringify({ ...userInfo, ...user }))
}

export const removeAuthData = () => {
  localStorage.setItem(window.__ENV__.LOCAL_AUTH_KEY, undefined)
  localStorage.removeItem(window.__ENV__.LOCAL_AUTH_KEY)
}

export const getAuthData = () => {
  const { LOCAL_AUTH_KEY = undefined } = configData
  const loginInfoStr = localStorage.getItem(LOCAL_AUTH_KEY)
  return loginInfoStr ? JSON.parse(loginInfoStr) : undefined
}

export const getAuthDataField = (fieldName) => {
  const loginInfo = getAuthData()
  return loginInfo ? loginInfo[fieldName] : undefined
}

export const cleanUserAndLogout = (history) => {
  removeLocalToken()
  removeAuthData()
  setTimeout(() => {
    const loginPath = '/login'
    if (history) {
      history.replace(loginPath)
    } else {
      window.location = loginPath
    }
  }, 10)
}
