export default [{
  eventKey: 'products',
  icon: 'tv',
  text: 'Productos',
  children: [
    { eventKey: 'app/products/bundle', text: 'Crear Bundle Unitario', permission: 'bundles.create' },
    { eventKey: 'app/products/bundles', text: 'Crear Bundle Masivo', permission: 'bundles.create' },
    {
      eventKey: 'app/products/selection', text: 'Administrar Selección de Productos', permission: 'products-selection.create',
    },
    {
      eventKey: 'app/products/initial-stock', text: 'Administrar Stock Inicial', permission: 'products-initial-stock.create',
    },
  ],
},
{
  eventKey: 'administration',
  icon: 'tv',
  text: 'Administración',
  children: [
    {
      eventKey: 'app/roles', icon: 'assignment-account', text: 'Administrar Roles', permission: 'roles.create',
    },
    {
      eventKey: 'app/permissions', icon: 'labels', text: 'Administrar Permisos', permission: 'permissions.create',
    },
    {
      eventKey: 'app/users', icon: 'accounts', text: 'Administrar Usuarios', permission: 'users.create',
    },
    {
      eventKey: 'app/categories', icon: 'accounts', text: 'Administrar Categorías', permission: 'categories.create',
    },
    {
      eventKey: 'app/categories-sod', icon: 'accounts', text: 'Administrar Categorías Supermercado', permission: 'categoriesSod.create',
    },
    {
      eventKey: 'app/hotjar/categories', icon: 'accounts', text: 'Administrar Categorías Hotjar', permission: 'categories.create',
    },
    {
      eventKey: 'app/walstore/stores', icon: 'accounts', text: 'Administrar Tiendas de Walstore', permission: 'walstore.stores.create',
    },
    {
      eventKey: 'app/stores-status', icon: 'accounts', text: 'Administrar Estado de Tiendas', permission: 'stores.create',
    },
    {
      eventKey: 'app/header-alert-message', icon: 'accounts', text: 'Administrar Huincha de Contingencia', permission: 'users.create',
    },
  ],
},
{
  eventKey: 'banners',
  icon: 'labels',
  text: 'Banners',
  children: [
    { eventKey: 'app/banners/banners-home', text: 'Banners home', permission: 'banners.create' },
    { eventKey: 'app/banners/bci', text: 'Banners Líder BCI', permission: 'banners.create' },
    { eventKey: 'app/banners/grid', text: 'Grilla descuentos imperdibles', permission: 'banners.create' },
    { eventKey: 'app/banners/lower-blocks', text: 'Calugas inferiores', permission: 'banners.create' },
    { eventKey: 'app/banners/line-breakers', text: 'Rompefilas', permission: 'banners.create' },
    { eventKey: 'app/banners/inspirational-blocks', text: 'Destacados mundo lider', permission: 'banners.create' },
    { eventKey: 'app/banners/countdown', text: 'Cuentas regresivas', permission: 'banners.create' },
  ],
},
{
  eventKey: 'reports',
  icon: 'file-text',
  text: 'Reportes',
  children: [{
    eventKey: 'app/reports/product',
    text: 'Reporte de products',
    permission: 'products-report.read',
  }],
},
{
  eventKey: 'app/random',
  icon: 'cake',
  text: 'Random Generator',
  permission: 'users.create',
},
{
  eventKey: 'app/legal-info',
  icon: 'file-text',
  text: 'Información Legal',
  permission: 'legal.create',
},
{
  eventKey: 'app/checkout',
  icon: 'shopping-cart',
  text: 'Checkout',
  children: [{
    eventKey: 'app/checkout/free-shipping-promotion',
    text: 'Promoción despacho gratis',
    permission: 'checkout.write',
  }],
},
]
