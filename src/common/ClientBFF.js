import axios from 'axios'
import axiosRetry from 'axios-retry'

import { getAuthData } from '../Helper'
import logger from './Logger'

const retryConfig = {
  retries: 0,
  shouldResetTimeout: true,
  retryCondition: () => true,
}

export default class ClientBFF {
  constructor(config) {
    const localAuthData = getAuthData()
    this.user = localAuthData && (localAuthData.sAMAccountName || localAuthData.email)
    this.config = typeof window !== 'undefined' ? window.__ENV__ : config
    this.log = () => logger.getLogInstance(this.config)
  }

  bffInstance() {
    const bffInstance = axios.create({
      baseURL: this.config.baseURLBFF,
      timeout: 20000,
      headers: { 'X-username': this.user },
    })
    axiosRetry(bffInstance, retryConfig)
    return bffInstance
  }

  bffStagingInstance() {
    const bffInstance = axios.create({
      baseURL: this.config.baseURLStagingBFF,
      timeout: this.config.timeout,
      headers: { 'X-username': this.user },
    })
    axiosRetry(bffInstance, retryConfig)
    return bffInstance
  }

  landingBffInstance() {
    const bffInstance = axios.create({
      baseURL: this.config.baseLandingURLbff,
      timeout: 20000,
    })
    axiosRetry(bffInstance, retryConfig)
    return bffInstance
  }

  getBanners(data) {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: '/storage/list',
        method: 'post',
        data,
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  upload(data, request = {}) {
    const {
      path = '/storage/upload',
      isProduction = true,
      fromXlsxUploadHandle = false,
      tenant = 'catalogo',
    } = request

    return new Promise((resolve, reject) => {
      const client = isProduction ? this.bffInstance() : this.bffStagingInstance()
      client.request({
        url: encodeURI(path),
        method: 'post',
        headers: {
          'Content-Type': fromXlsxUploadHandle ? '' : 'application/x-www-form-urlencoded',
          tenant,
        },
        data,
      })
        .then(async (response) => {
          // await this.clearBFFCache()
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  download(data, request = {}) {
    const {
      path = '/storage/download',
      method = 'post',
      responseType = '',
      fileName = 'file.xlsx',
      isProduction = true,
      tenant = 'catalogo',
    } = request

    return new Promise((resolve, reject) => {
      const client = isProduction ? this.bffInstance() : this.bffStagingInstance()
      client.request({
        url: encodeURI(path),
        method,
        data,
        responseType,
        headers: {
          tenant,
        },
      })
        .then((response) => {
          if (method.toLowerCase() === 'post') {
            resolve(response.data)
          } else {
            const url = window.URL.createObjectURL(new Blob([response.data]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', fileName) // or any other extension
            document.body.appendChild(link)
            link.click()
            resolve({})
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  delete(data) {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: '/storage/delete',
        method: 'delete',
        data,
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  getBundles() {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: '/product/bundles',
        method: 'get',
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  updateProductsToStaging(data) {
    return new Promise((resolve, reject) => {
      const client = this.bffStagingInstance()
      client.request({
        url: '/product/bundles',
        method: 'post',
        data,
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  updateProductsToProduction(data) {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: '/product/bundles',
        method: 'post',
        data,
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  clearBFFCache() {
    return new Promise((resolve, reject) => {
      const client = this.landingBffInstance()
      client.request({
        url: '/cache/flush',
        method: 'get',
        headers: {
          'x-token': this.config.REDIS_FLUSH_TOKEN,
        },
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  auth(data) {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: '/auth/accessToken',
        method: 'post',
        data,
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  checkoutGetFreeShippingPromotionConfig() {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: 'checkout/promotions/free-shipping',
        method: 'get',
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  checkoutUpdateFreeShippingPromotionConfig(data) {
    return new Promise((resolve, reject) => {
      const client = this.bffInstance()
      client.request({
        url: 'checkout/promotions/free-shipping',
        method: 'post',
        data,
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}
