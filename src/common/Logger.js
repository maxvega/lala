import { createLogger, INFO } from 'browser-bunyan'
import { ServerStream } from '@browser-bunyan/server-stream'

let logInstance

const logger = {
  getLogInstance: (config) => {
    if (typeof logInstance === 'undefined') {
      if (config.logger && typeof window !== 'undefined') {
        logInstance = createLogger({
          name: 'buysmart-content-manager',
          streams: [{
            level: INFO,
            stream: new ServerStream({
              url: `${config.baseURLbff}logs`, // baseURLbff includes the slash
              method: 'POST',
            }),
          }],
        })
      } else {
        return {
          info: () => { },
          error: () => {},
        }
      }
    }
    return logInstance
  },
}

export default logger
