import axios from 'axios'
import axiosRetry from 'axios-retry'

import logger from './Logger'
import { removeLocalToken } from '../Helper'

const retryConfig = {
  retries: 0,
  shouldResetTimeout: true,
  retryCondition: () => true,
}

export default class ClientAuth {
  constructor(config) {
    this.config = typeof window !== 'undefined' ? window.__ENV__ : config
    this.log = () => logger.getLogInstance(this.config)
  }

  authInstance() {
    const authInstance = axios.create({
      baseURL: this.config.baseURLAuth,
      timeout: this.config.timeout,
    })
    axiosRetry(authInstance, retryConfig)
    return authInstance
  }

  handleLogOut() {
    removeLocalToken()
    window.location.href = window.location.origin
  }

  // Auth
  getAuth(token) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'auth',
        method: 'get',
        headers: {
          'x-auth-token': token,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  authUser(data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'auth',
        method: 'post',
        timeout: 10000,
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  // Roles
  createRole(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'roles',
        method: 'post',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  getRoles(token) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'roles',
        method: 'get',
        headers: {
          'x-auth-token': token,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  updateRole(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: `roles/${data.id}`,
        method: 'put',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  deleteRole(token, roleId) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: `roles/${roleId}`,
        method: 'delete',
        headers: {
          'x-auth-token': token,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  // Permissions
  getPermissions(token) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'permissions',
        method: 'get',
        headers: {
          'x-auth-token': token,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  updatePermission(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: `permissions/${data.id}`,
        method: 'put',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  createPermission(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'permissions',
        method: 'post',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  deletePermission(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: `permissions/${data.id}`,
        method: 'delete',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  // Users
  createUser(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'users',
        method: 'post',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  updateUser(token, data) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: `users/${data.id}`,
        method: 'put',
        headers: {
          'x-auth-token': token,
        },
        data,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  getUsers(token) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: 'users/',
        method: 'get',
        headers: {
          'x-auth-token': token,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }

  deleteUser(token, userId) {
    return new Promise((resolve, reject) => {
      const client = this.authInstance()
      client.request({
        url: `users/${userId}`,
        method: 'delete',
        headers: {
          'x-auth-token': token,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        if (error.response.status === 401) {
          this.handleLogOut()
        }
        reject(error)
      })
    })
  }
}
