import axios from 'axios'
import axiosRetry from 'axios-retry'

import logger from './Logger'

const retryConfig = {
  retries: 2,
  shouldResetTimeout: true,
  retryCondition: () => true,
}

export default class ClientAlgolia {
  constructor(config) {
    this.config = typeof window !== 'undefined' ? window.__ENV__ : config
    this.log = () => logger.getLogInstance(this.config)
  }

  algoliaInstance() {
    const algoliaInstance = axios.create({
      baseURL: 'https://analytics.us.algolia.com/2/',
      timeout: this.config.timeout,
    })
    axiosRetry(algoliaInstance, retryConfig)
    return algoliaInstance
  }

  getTopSearches(startDate, endDate, limit) {
    return new Promise((resolve, reject) => {
      const client = this.algoliaInstance()
      client.request({
        url: `searches?index=campaigns_production&startDate=${startDate}&endDate=${endDate}&limit=${limit}&tags=`,
        method: 'get',
        headers: {
          'x-algolia-application-id': this.config.appId,
          'x-algolia-api-key': this.config.analyticsApiKey,
        },
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }
}
