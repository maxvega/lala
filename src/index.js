import React from 'react'
import ReactDOM from 'react-dom'
import uuidv4 from 'uuid/v4'
import App from './App'

const main = () => {
  if (typeof localStorage !== 'undefined' && !localStorage.getItem('XFlowId')) {
    localStorage.setItem('XFlowId', uuidv4())
  }
  ReactDOM.render(<App />, document.getElementById('root'))
}

main()
