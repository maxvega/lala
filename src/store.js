import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import modalImageReducer from './Redux/reducers/modalImage'
import windowSizeReducer from './Redux/reducers/windowSize'
import loginReducer from './Redux/reducers/login'

const reducer = combineReducers({
  modalImageReducer,
  windowSizeReducer,
  loginReducer,
})


const middleware = applyMiddleware(thunk)
const store = createStore(
  reducer,
  middleware,
)
export default store
