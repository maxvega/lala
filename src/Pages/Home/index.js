import React, { Component } from 'react'
import { format, subDays } from 'date-fns'
import DynamicTable from '../../components/DynamicTable'
import Card from '../../components/Card'
import ClientAlgolia from '../../common/ClientAlgolia'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      algoliaSearches: [],
      columns: [],
      showTableError: false,
    }
    this.clientAlgolia = new ClientAlgolia()
  }

  componentDidMount() {
    this.getSearches()
  }

  getSearches= async () => {
    const columns = [
      { name: 'Búsqueda', selector: 'search', sortable: true },
      { name: 'Cantidad', selector: 'count', sortable: true },
      { name: 'Resultados', selector: 'nbHits', sortable: true }]

    const today = format(new Date(), 'yyyy-MM-dd')
    const lastSevenDays = format(subDays(new Date(), 7), 'yyyy-MM-dd')
    const limit = 100

    const algoliaSearches = await this.clientAlgolia.getTopSearches(lastSevenDays, today, limit)
    if (algoliaSearches.searches && algoliaSearches.searches.length > 0) {
      this.setState({ algoliaSearches: algoliaSearches.searches, columns })
    } else {
      this.setState({ showTableError: true })
    }
  }

  render() {
    const { algoliaSearches, columns, showTableError } = this.state
    return (
      <div>
        <Card>
          {!showTableError ? (
            <div className="text-center">
              <DynamicTable columns={columns} data={algoliaSearches} title="Búquedas en los últimos 7 días" />
            </div>
          ) : (<div>Error</div>) }
        </Card>
      </div>
    )
  }
}

export default Home
