import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Scrollbars } from 'react-custom-scrollbars'
import CircularProgress from '@material-ui/core/CircularProgress'
import { AutoSizer } from 'react-virtualized'
import Element from '../../../components/Card/Element'
import TextButton from '../../../components/TextButton'
import Card from '../../../components/Card'
import Label from '../../../components/Label'
import BackArrow from '../../../components/BackArrow'
import EditTitle from '../../../components/EditTitle'
import RoundIconWrapper from '../../../components/RoundIconWrapper'
import Input from '../../../components/Input'
import ClientAuth from '../../../common/ClientAuth'
import { getLocalToken } from '../../../Helper'
import ActionButton from '../../../components/ActionButton'

class EditRoleCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      role: this.props.role,
      roleName: this.props.role.name,
      roleDescription: this.props.role.description,
      permissions: this.permissionsFulfillment(this.props.role.permissions, this.props.permissions),
      showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.role) !== JSON.stringify(nextProps.role)) {
      this.setState({
        role: nextProps.role,
        roleName: nextProps.role.name,
        roleDescription: nextProps.role.description,
        permissions: this.permissionsFulfillment(nextProps.role.permissions, nextProps.permissions),
      })
    }
  }

  permissionsFulfillment = (permissionsInRole, permissions) => {
    const permissionsObject = {}
    permissionsInRole.forEach((permission) => {
      permissionsObject[permission.name] = Object.assign({}, permission, { applied: true })
    })
    permissions.forEach((permission) => {
      if (!permissionsObject[permission.name]) {
        permissionsObject[permission.name] = Object.assign({}, permission, { applied: false })
      }
    })
    return Object.values(permissionsObject)
  }

  togglePermission=(permissionId) => {
    const { permissions } = this.state

    const newPermissions = JSON.parse(JSON.stringify(permissions))
    newPermissions.forEach((permission) => {
      if (permission.id === permissionId) {
        permission.applied = !permission.applied
      }
    })
    this.setState({ permissions: newPermissions })
  }

  renderRolePermissions = () => {
    const { permissions } = this.state
    return (
      <AutoSizer disableWidth>
        {({ height }) => (
          <Scrollbars
            className="rct-scroll"
            autoHeight
            autoHeightMax={height}
            autoHide
          >
            {permissions.map(permission => (
              <Element key={`permission-${permission.name}`} className="d-flex justify-content-between pointer" onClick={() => { this.togglePermission(permission.id) }}>
                <div>
                  <div className="bold">{permission.name}</div>
                  <div>{permission.description}</div>
                </div>
                <div>
                  {permission.applied ? (
                    <RoundIconWrapper>
                      <i className="zmdi zmdi-check" />
                    </RoundIconWrapper>
                  ) : (
                    <div
                      className="pointer"
                      style={{
                        width: '27px', height: '27px', border: '1px solid gray', borderRadius: '50%',
                      }}
                    />
                  )}
                </div>
              </Element>
            ))}
          </Scrollbars>
        )}
      </AutoSizer>
    )
  }

  handleChangeName = (e) => {
    this.setState({ roleName: e.target.value })
  }

  handleChangeDescription = (e) => {
    this.setState({ roleDescription: e.target.value })
  }

  updateRole = () => {
    const { getRolesAndPermissions, handleFeedback, setSelectedRole } = this.props
    const {
      role, permissions, roleName, roleDescription,
    } = this.state
    const token = getLocalToken()
    const newRole = Object.assign({}, role)
    newRole.name = roleName
    newRole.description = roleDescription
    newRole.permissions = permissions.filter(permission => permission.applied)
    newRole.permissions.forEach(permission => delete permission.applied)
    return this.clientAuth.updateRole(token, newRole).then(() => {
      handleFeedback({
        title: 'Rol editado correctamente',
      })
    }).catch((err) => {
      handleFeedback({
        title: 'Error al editar',
        message: `Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: ${err?.response?.data?.message}`,
      })
    }).finally(() => {
      getRolesAndPermissions()
      this.setState({ showLoader: false })
      setSelectedRole(null)
    })
  }

  saveRole = async () => {
    this.setState({ showLoader: true }, this.updateRole)
  }

  render() {
    const { roleName, roleDescription, showLoader } = this.state
    const { showNewPermission, setSelectedRole } = this.props
    return (
      <Card className="col-md-5 col-sm-12 right-card-margin">
        <div className="d-flex justify-content-between mb-10">
          <div>
            <div className="d-flex">
              <BackArrow
                className="text-center"
                onClick={() => { setSelectedRole(null) }}
              />
              <EditTitle>
                {roleName}
              </EditTitle>
            </div>
          </div>
        </div>
        <div className="mb-10">
          <Label>Nombre</Label>
          <Input
            className="bold"
            value={roleName}
            onChange={e => this.handleChangeName(e)}
          />
        </div>
        <div className="mb-10">
          <Label>Descripción</Label>
          <Input
            className="bold"
            value={roleDescription}
            onChange={e => this.handleChangeDescription(e)}
          />
        </div>
        <div className="d-flex justify-content-between mb-5">
          <Label>Permisos de este rol</Label>
          <div onClick={showNewPermission}>
            <div className="d-flex">
              <TextButton>
                <div className="d-flex">
                  nuevo permiso
                  <div>
                    <i style={{ marginLeft: '5px', fontWeight: 'bolder' }} className="zmdi zmdi-plus" />
                  </div>
                </div>
              </TextButton>
            </div>

          </div>
        </div>
        <div className="mb-20" style={{ height: 'calc(100vh - 450px)' }}>
          {this.renderRolePermissions()}
        </div>

        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}
        {!showLoader && (
        <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.saveRole}>
          guardar cambios
        </ActionButton>
        )}
      </Card>
    )
  }
}

EditRoleCard.propTypes = {
  permissions: PropTypes.array.isRequired,
  role: PropTypes.object.isRequired,
  showNewPermission: PropTypes.func.isRequired,
  setSelectedRole: PropTypes.func.isRequired,
  getRolesAndPermissions: PropTypes.func.isRequired,
  handleFeedback: PropTypes.func,
}

EditRoleCard.defaultProps = {
  handleFeedback: () => {},
}

export default EditRoleCard
