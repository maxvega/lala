import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Scrollbars } from 'react-custom-scrollbars'
import { AutoSizer } from 'react-virtualized'
import ClientAuth from '../../common/ClientAuth'
import PageTitle from '../../components/PageTitle'
import InnerTitle from '../../components/InnerTitle'
import Card from '../../components/Card'
import Element from '../../components/Card/Element'
import ActionButton from '../../components/ActionButton'
import RoundIconWrapper from '../../components/RoundIconWrapper'
import { getLocalToken } from '../../Helper'
import EditRoleCard from './EditRoleCard'
import NewRole from './NewRole'
import NewPermission from '../Permissions/NewPermission'
import withFeedbackModal from '../../components/WithFeedbackModal'

export class Roles extends Component {
  constructor(props) {
    super(props)
    this.state = {
      roles: [],
      permissions: [],
      selectedRole: null,
      showNewRole: false,
      showLoader: false,
      showDeleteModal: false,
      roleToDelete: null,
    }
    this.clientAuth = new ClientAuth()
  }

  componentDidMount = () => {
    this.getRolesAndPermissions()
  }

  getRolesAndPermissions = async () => {
    this.setState({ showLoader: true }, async () => {
      const token = getLocalToken()
      const roles = await this.clientAuth.getRoles(token || 'bananas')
      const permissions = await this.clientAuth.getPermissions(token || 'bananas')
      this.setState({ roles, permissions, showLoader: false })
    })
  }

  setSelectedRole = (role) => {
    this.setState({ selectedRole: role })
  }

  showNewRole = () => {
    this.setState({ showNewRole: true })
  }

  showNewPermission = () => {
    this.setState({ showNewPermission: true })
  }

  closeSideNav =() => {
    this.setState({ showNewRole: false, showNewPermission: false })
  }

  onClickRemove = (role) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: `Borrar ${role.name}`,
      message: `¿Estás seguro que quieres borrar a ${role.name} ?`,
      confirmationHandler: this.handleDeleteRole,
      confirmationData: [role],
    })
  }

  renderRoles = roles => (
    <AutoSizer disableWidth>
      {({ height }) => (
        <Scrollbars
          className="rct-scroll"
          autoHeight
          autoHeightMax={height}
          autoHide
        >
          {roles.map(role => (
            <Element key={`role-${role.name}`} className="d-flex justify-content-between">
              <div>{role.name}</div>
              <div className="d-flex">
                <RoundIconWrapper tooltipText="Editar Rol" id={`edit-${role.id}`} style={{ marginRight: '5px' }} onClick={() => { this.setSelectedRole(role) }}>
                  <i className="zmdi zmdi-edit" />
                </RoundIconWrapper>
                <RoundIconWrapper tooltipText="Borrar Rol" id={`delete-${role.id}`} backgroundColor="#eaeaea" color="#000000" onClick={() => { this.onClickRemove(role) }}>
                  <i className="zmdi zmdi-delete" />
                </RoundIconWrapper>
              </div>
            </Element>
          ))}
        </Scrollbars>
      )}
    </AutoSizer>
  )

  handleDeleteRole=async (role) => {
    const token = getLocalToken()
    await this.clientAuth.deleteRole(token, role.id)
    return this.getRolesAndPermissions()
  }

  render() {
    const {
      roles, selectedRole, permissions, showNewRole, showNewPermission, showLoader, showDeleteModal, roleToDelete,
    } = this.state

    const { handleFeedback, title } = this.props

    return (
      <Fragment>
        <PageTitle>{title}</PageTitle>
        <div className="row mb-20">
          <Card className={`${selectedRole ? 'not-display-in-mobile col-md-7 ' : ''}col-sm-12 left-card-margin`}>
            <div className="row mb-10">
              <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
                <InnerTitle>Roles creados</InnerTitle>
              </div>
              <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
                <div className="create-button-container">
                  <ActionButton type="info" onClick={this.showNewRole} className="create-new-button">
                    <div className="d-flex">
                      <div>crear nuevo</div>
                      <div><i style={{ marginLeft: '5px', fontWeight: 'bolder' }} className="zmdi zmdi-plus" /></div>
                    </div>
                  </ActionButton>
                </div>
              </div>
            </div>

            {showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto' }}
              />
            </div>
            )}
            <div style={{ height: 'calc(100vh - 250px)' }}>
              {!showLoader && this.renderRoles(roles)}
            </div>
          </Card>
          {selectedRole && selectedRole.name && (
          <EditRoleCard role={selectedRole} permissions={permissions} setSelectedRole={this.setSelectedRole} showNewPermission={this.showNewPermission} getRolesAndPermissions={this.getRolesAndPermissions} handleFeedback={handleFeedback} />
          ) }

          <div
            className={showNewRole || showNewPermission ? 'overlay-right-modal' : ''}
            onClick={() => { this.closeSideNav(false) }}
          />

          {showNewRole && <NewRole closeSideNav={this.closeSideNav} permissions={permissions} getRolesAndPermissions={this.getRolesAndPermissions} handleFeedback={handleFeedback} />}
          {showNewPermission && (
          <NewPermission
            closeSideNav={() => {
              this.closeSideNav()
              this.setState({ selectedRole: null })
            }}
            roles={roles}
            getPermissionsAndRoles={this.getRolesAndPermissions}
            handleFeedback={handleFeedback}
          />
          )}
          {showDeleteModal && this.renderDeleteModal(showDeleteModal, roleToDelete)}
        </div>
      </Fragment>
    )
  }
}


export default withFeedbackModal(Roles)

Roles.propTypes = {
  handleFeedback: PropTypes.func,
  title: PropTypes.string.isRequired,
}

Roles.defaultProps = {
  handleFeedback: () => {},
}
