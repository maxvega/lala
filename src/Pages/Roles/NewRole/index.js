import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Scrollbars } from 'react-custom-scrollbars'
import { AutoSizer } from 'react-virtualized'
import Element from '../../../components/Card/Element'
import RoundIconWrapper from '../../../components/RoundIconWrapper'
import ActionButton from '../../../components/ActionButton'
import Input from '../../../components/Input'
import Label from '../../../components/Label'
import SideModal from '../../../components/SideModal'
import { getLocalToken } from '../../../Helper'
import ClientAuth from '../../../common/ClientAuth'

class NewRole extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '', description: '', permissions: props.permissions,
    }
    this.clientAuth = new ClientAuth()
  }

  handleChangeName = (e) => {
    this.setState({ name: e.target.value })
  }

  handleChangeDescription = (e) => {
    this.setState({ description: e.target.value })
  }

  togglePermission=(permissionId) => {
    const { permissions } = this.state

    const newPermissions = JSON.parse(JSON.stringify(permissions))
    newPermissions.forEach((permission) => {
      if (permission.id === permissionId) {
        permission.applied = !permission.applied
      }
    })
    this.setState({ permissions: newPermissions })
  }

  renderPermissions = () => {
    const { permissions } = this.state
    return (
      <AutoSizer disableWidth>
        {({ height }) => (
          <Scrollbars
            className="rct-scroll mr-20"
            autoHeight
            autoHeightMax={height}
            autoHide
          >
            {permissions.map(permission => (
              <Element key={`permission-${permission.name}`} className="d-flex justify-content-between pointer" onClick={() => { this.togglePermission(permission.id) }}>
                <div>
                  <div className="bold">{permission.name}</div>
                  <div>{permission.description}</div>
                </div>
                <div>
                  {permission.applied ? (
                    <RoundIconWrapper>
                      <i className="zmdi zmdi-check" />
                    </RoundIconWrapper>
                  ) : (
                    <div
                      className="pointer"
                      style={{
                        width: '27px', height: '27px', border: '1px solid gray', borderRadius: '50%',
                      }}
                    />
                  )}
                </div>
              </Element>
            ))}
          </Scrollbars>
        )}
      </AutoSizer>
    )
  }

  createRole = () => {
    const { name, description, permissions } = this.state
    const { getRolesAndPermissions, closeSideNav, handleFeedback } = this.props
    const token = getLocalToken()
    const newRole = {}
    newRole.name = name
    newRole.description = description
    newRole.permissions = permissions.filter(permission => permission.applied)
    newRole.permissions.forEach(permission => delete permission.applied)
    return this.clientAuth.createRole(token, newRole).then(() => {
      handleFeedback({
        title: 'Creación exitosa',
        message: 'El rol ha sido creado correctamente',
      })
    }).catch((err) => {
      handleFeedback({
        title: 'Error al crear',
        message: `Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: ${err?.response?.data?.message}`,
      })
    }).finally(() => {
      getRolesAndPermissions()
      closeSideNav()
      this.setState({ showLoader: false })
    })
  }

  saveRole = async () => {
    const { name, description } = this.state
    const { handleFeedback } = this.props
    if (name && description) {
      this.setState({ showLoader: true }, this.createRole)
    } else {
      handleFeedback({
        title: 'Faltan Campos',
        message: 'Por favor revisa los campos solicitados',
      })
    }
  }

  render() {
    const {
      name, description, showLoader,
    } = this.state
    const { closeSideNav } = this.props
    return (
      <SideModal>
        <div
          style={{
            position: 'absolute', top: 10, right: 20, cursor: 'pointer',
          }}
          onClick={closeSideNav}
        >
          <i style={{ marginLeft: ' 10px', fontSize: '30px' }} className="zmdi zmdi-close" />
        </div>

        <div className="d-flex justify-content-between mb-10" style={{ marginTop: '20px' }}>
          <div>
            <span style={{ fontSize: '24px' }}>Nuevo Rol</span>
          </div>
        </div>
        <div className="mb-10">
          <Label>Nombre</Label>
          <Input
            className="bold"
            value={name}
            onChange={e => this.handleChangeName(e)}
          />
        </div>
        <div className="mb-10">
          <Label>Descripción</Label>
          <Input
            className="bold"
            value={description}
            onChange={e => this.handleChangeDescription(e)}
          />
        </div>

        <Label className="mb-10">Permisos de este rol</Label>
        <div className="mb-20" style={{ backgroundColor: 'white', height: 'calc(100% - 340px)' }}>{this.renderPermissions()}</div>
        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}

        {!showLoader && (
          <div>
            <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.saveRole}>
              guardar cambios
            </ActionButton>
          </div>
        )}
      </SideModal>
    )
  }
}

NewRole.propTypes = {
  closeSideNav: PropTypes.func.isRequired,
  getRolesAndPermissions: PropTypes.func.isRequired,
  permissions: PropTypes.array.isRequired,
  handleFeedback: PropTypes.func,
}

NewRole.defaultProps = {
  handleFeedback: () => {},
}

export default NewRole
