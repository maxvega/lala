import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '../../components/Card'
import ActionButton from '../../components/ActionButton'
import FileInput from '../../components/FileInput'
import ClientBFF from '../../common/ClientBFF'
import withFeedbackModal from '../../components/WithFeedbackModal'

export class Categories extends Component {
  constructor(props) {
    super(props)
    this.state = {
      file: null,
      uploadedToStaging: false,
      disableUploadToProductionButton: true,
      uploadToStagingLoader: false,
      uploadToProductionLoader: false,
    }
    this.clientBFF = new ClientBFF()
    this.config = typeof window !== 'undefined' ? window.__ENV__ : { }
  }

  getCategoriesXLSX = async () => this.clientBFF.getCategoriesXLSX()

  handleChange = (e) => {
    e.preventDefault()
    const { files } = e.target
    if (files && files[0]) {
      this.setState({ file: files[0] })
    }
  }

  handleUploadToStaging = async () => {
    const { file } = this.state
    this.setState({ uploadToStagingLoader: true }, async () => {
      const formData = new FormData()
      formData.append('file', file)
      await this.clientBFF.uploadCategoriesToStaging(formData)
      this.setState({ uploadToStagingLoader: false, uploadedToStaging: true })
      // window.open('https://buysmart-staging.lider.cl/catalogo', '_blank')
    })
  }

  handleUploadToProduction = async () => {
    const { file } = this.state
    this.setState({ uploadToProductionLoader: true }, async () => {
      const formData = new FormData()
      formData.append('file', file)
      await this.clientBFF.uploadCategories(formData)
      this.setState({ uploadToProductionLoader: false })
      // window.open('https://www.lider.cl/catalogo', '_blank')
    })
  }

  handleError = (message) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: message,
    })
  }

  showLoader = () => (
    <CircularProgress
      className="progress-primary"
      size={26}
      mode="determinate"
      value={75}
      style={{ color: '#fff', margin: 'auto' }}
    />
  )

  render() {
    const {
      uploadedToStaging, disableUploadToProductionButton, file, uploadToStagingLoader, uploadToProductionLoader,
    } = this.state
    return (
      <div className="row">
        <Card className="col-sm-12 left-card-margin">
          <Fragment>
            <div className="row mb-10">
              <div className="col-sm-12">
                <h3 style={{ lineHeight: '40px' }}>Administrador de Categorias</h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 mb-20 text-center" style={{ borderRight: '1px solid black' }}>

                <a href={`${this.config.baseURLBFF}/categories/download`} download>
                  <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }}>
                    Descargar categorías activas
                  </ActionButton>
                </a>
              </div>
              <div className="col-md-6 mb-20 text-center">
                <div className="d-flex text-center mb-20">
                  <FileInput type="file" title="Subir archivo xlsx de categorías" accept=".xlsx" inputHandler={this.handleChange} />
                </div>
                {file && (
                  <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleUploadToStaging}>
                    {uploadToStagingLoader ? (
                      <CircularProgress
                        className="progress-primary"
                        size={26}
                        mode="determinate"
                        value={75}
                        style={{ color: '#fff', margin: 'auto' }}
                      />
                    ) : 'Subir categorías a staging'}
                  </ActionButton>
                )}
              </div>
            </div>
            {uploadedToStaging && (
              <div className="text-center">
                <div className="mb-20">
                  <input type="checkbox" onChange={() => this.setState({ disableUploadToProductionButton: !disableUploadToProductionButton })} />
                  {' He revisado en el sitio de pruebas el funcionamiento del menú que voy a subir a producción.'}
                </div>
                <ActionButton disabled={disableUploadToProductionButton} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleUploadToProduction}>
                  {uploadToProductionLoader ? (
                    this.showLoader()
                  ) : 'Subir categorías a producción'}
                </ActionButton>

              </div>
            )}
          </Fragment>
        </Card>
      </div>
    )
  }
}

Categories.propTypes = {
  handleFeedback: PropTypes.func.isRequired,
}

export default withFeedbackModal(Categories)
