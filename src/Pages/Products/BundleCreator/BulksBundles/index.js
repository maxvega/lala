import React, { Component } from 'react'
import XLSX from 'xlsx'
import * as FileSaver from 'file-saver'
import ClientBFF from '../../../../common/ClientBFF'
import { generateChunksOfArray, cleanString } from '../../../../Helper'
import CustomFileManager from '../../../../components/CustomFileManager'

const ALLOWED_STORES_HD = []
const ALLOWED_STORES_S2S = []

class BulksBundles extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bundlesListDB: [],
      generatedJSON: [],
    }
    this.clientBFF = new ClientBFF()
  }

  componentDidMount = async () => {
    await this.getBundlesProducts()
  }

  getBundlesProducts = async () => {
    const bundlesDB = await this.clientBFF.getBundles()
    this.setState({ bundlesListDB: bundlesDB })
    return bundlesDB
  }

  getXlsxColumnTitles = () => {
    const tabBundles = ['sku', 'itemNumber', 'brand', 'displayName', 'posicion', 'destacado', 'size', 'campaignId', 'categorias', 'keyword', 'filters', 'tags', 'imagesAvailables', 'stockInicial']
    const tabAssociateds = ['sku_bundle', 'sku', 'itemNumber', 'brand', 'displayName', 'quantity', 'BasePriceReference', 'BasePriceSales', 'BasePriceTLMC']
    return { tabBundles, tabAssociateds }
  }

  getCategoriesContent = (bundle) => {
    let categories = ''
    bundle.categorias.forEach((category, index) => {
      if (index === bundle.categorias.length - 1) {
        categories = categories.concat(category)
      } else {
        categories = categories.concat(category).concat('|')
      }
    })

    const splitCategories = categories.split('|')
    const sortCategories = [...new Set(splitCategories)].sort((a, b) => b.length - a.length)

    let result = ''
    sortCategories.forEach((category, index) => {
      if (!result.includes(category)) {
        if (index === sortCategories.length - 1) {
          result = result.concat(category)
        } else {
          result = result.concat(category).concat('|')
        }
      }
    })
    if (result.slice(-1) === '|') {
      result = result.slice(0, -1)
    }
    return result
  }

  getFiltersContent = (bundle) => {
    let filters = ''

    const filtersFlat = bundle.filters ? bundle.filters.map(filter => (Object.entries(filter)).flat()) : []

    filtersFlat.forEach((filter, index) => {
      if (index === filtersFlat.length - 1) {
        filters = filters.concat(filter)
      } else {
        filters = filters.concat(filter).concat('|')
      }
    })

    return filters
  }

  generateXlsxColumnContent = (bundleData, bundleAssociatedsData) => {
    const { bundlesListDB } = this.state

    bundlesListDB.forEach((bundle) => {
      bundleData.push([
        bundle.sku.toString(),
        bundle.itemNumber,
        bundle.brand,
        bundle.displayName,
        bundle.posicion || 9999,
        bundle.destacado ? 'true' : 'false',
        bundle.size,
        bundle.campaignId,
        this.getCategoriesContent(bundle),
        bundle.keyword ? bundle.keyword.toString() : '',
        this.getFiltersContent(bundle),
        bundle.tags ? bundle.tags.toString() : '',
        bundle.imagesAvailables ? bundle.imagesAvailables.toString() : '',
        bundle.stockInicial,
      ])

      bundle.associatedProducts.forEach((associatedProduct) => {
        bundleAssociatedsData.push([
          bundle.sku.toString(),
          associatedProduct.sku,
          associatedProduct.itemNumber,
          associatedProduct.brand,
          associatedProduct.displayName,
          associatedProduct.quantity,
          associatedProduct.price.BasePriceReference,
          associatedProduct.price.BasePriceSales,
          associatedProduct.price.BasePriceTLMC,
        ])
      })
    })
  }

    generateXLSXfromJSON = async () => {
      const bundleData = []
      const bundleAssociatedsData = []
      const { tabBundles, tabAssociateds } = this.getXlsxColumnTitles()
      bundleData.push(tabBundles)
      bundleAssociatedsData.push(tabAssociateds)

      this.generateXlsxColumnContent(bundleData, bundleAssociatedsData)

      const wb = XLSX.utils.book_new()
      const wsBundle = XLSX.utils.aoa_to_sheet(bundleData)
      const wsBundleAssociateds = XLSX.utils.aoa_to_sheet(bundleAssociatedsData)

      wb.SheetNames.push('Bundles')
      wb.Sheets.Bundles = wsBundle
      wb.SheetNames.push('Associateds')
      wb.Sheets.Associateds = wsBundleAssociateds

      return XLSX.write(wb, { type: 'array', bookType: 'xlsx', bookSST: false })
    }


    getCategoriesFromXlsx = (bundle) => {
      const splitCategories = bundle.categorias ? bundle.categorias.split('|') : []
      const resultCategories = []

      splitCategories.forEach((category) => {
        const splitCategory = category.split('/')
        if (splitCategory[0]) {
          resultCategories.push(splitCategory[0])
        }
        if (splitCategory[0] && splitCategory[1]) {
          resultCategories.push(`${splitCategory[0]}/${splitCategory[1]}`)
        }
        if (splitCategory[0] && splitCategory[1] && splitCategory[2]) {
          resultCategories.push(`${splitCategory[0]}/${splitCategory[1]}/${splitCategory[2]}`)
        }
      })

      const setResultCategories = [...new Set(resultCategories)]

      return setResultCategories
    }

    getFiltersFromXlsx = (bundle) => {
      const splitFilters = bundle.filters ? bundle.filters.split('|') : []

      return splitFilters.map((filter) => {
        const splitFilter = filter.split(',')
        return { [splitFilter[0]]: splitFilter[1] }
      })
    }

    validateAssociatedProductsFields = (product) => {
      if (product.sku === undefined || product.itemNumber === undefined
          || product.brand === undefined || product.displayName === undefined || product.quantity === undefined
          || product.BasePriceReference === undefined || product.BasePriceSales === undefined || product.BasePriceTLMC === undefined) {
        return false
      }
      return true
    }

    getAssociatedProductsFromXlsx = (newBundle, dataBundleAssociateds) => {
      const associatedProductsInBundle = dataBundleAssociateds
        .filter(product => (Number(product.sku_bundle) === Number(newBundle.sku)))
        .map(product => ({
          sku: product.sku,
          ID: `PROD_${product.sku}`,
          itemNumber: product.itemNumber,
          price: {
            BasePriceReference: product.BasePriceReference,
            BasePriceSales: product.BasePriceSales,
            BasePriceTLMC: product.BasePriceTLMC,
          },
          brand: product.brand,
          displayName: product.displayName,
          quantity: product.quantity,
        }))

      newBundle.associatedProducts = associatedProductsInBundle
      newBundle.price = {
        BasePriceReference: associatedProductsInBundle.reduce((acc, score) => acc + score.price.BasePriceReference * score.quantity, 0),
        BasePriceSales: associatedProductsInBundle.reduce((acc, score) => acc + score.price.BasePriceSales * score.quantity, 0),
        BasePriceTLMC: associatedProductsInBundle.reduce((acc, score) => acc + score.price.BasePriceTLMC * score.quantity, 0),
      }

      if (newBundle.price.BasePriceReference > 0) {
        newBundle.discount = Math.floor(100 - ((newBundle.price.BasePriceSales * 100) / newBundle.price.BasePriceReference))
      }
    }

  isDestacado = (str) => {
    if (!str || str.length === 0) return false

    return str.toLowerCase().includes('true')
  }

    generateJSONfromXLSX = (dataBundle, dataBundleAssociateds) => {
      const generatedBundles = []
      try {
        dataBundle.forEach((bundle) => {
          const newBundle = {
            sku: Number(bundle.sku),
            itemNumber: bundle.itemNumber.toString(),
            ID: `PROD_${bundle.sku}`,
            brand: cleanString(bundle.brand),
            displayName: cleanString(bundle.displayName),
            associatedProducts: [],
            price: [],
            discount: 0,
            posicion: bundle.posicion,
            stockInicial: bundle.stockInicial || null,
            destacado: this.isDestacado(bundle.destacado),
            size: (bundle.size === undefined || bundle.size === null) ? 100 : bundle.size,
            color: '',
            peso: '',
            max: null,
            available: false,
            makePublic: true,
            vendorId: null,
            leadTime: null,
            appId: 'BuySmart',
            campaignId: bundle.campaignId || 'No campaign',
            gtin13: '',
            longDescription: null,
            categorias: this.getCategoriesFromXlsx(bundle),
            keyword: bundle.keyword ? bundle.keyword.split(',') : ['Campana'],
            imagesAvailables: bundle.imagesAvailables ? bundle.imagesAvailables.split(',') : [],
            allowedStoresHD: ALLOWED_STORES_HD,
            allowedStoresS2S: ALLOWED_STORES_S2S,
            inStore: null,
            stockInStores: null,
            filters: this.getFiltersFromXlsx(bundle),
            specifications: [],
            tags: bundle.tags ? bundle.tags.split(',') : ['exclusivo'],
          }

          this.getAssociatedProductsFromXlsx(newBundle, dataBundleAssociateds)
          if (newBundle.associatedProducts && newBundle.associatedProducts.length > 0) {
            generatedBundles.push(newBundle)
          }
        })
        return generatedBundles
      } catch (e) {
        return []
      }
    }

    handleUploadToStaging = async e => new Promise(async (resolve, reject) => {
      const wb = XLSX.read(e.target.result, { type: 'binary', bookVBA: true })
      const wsBundle = wb.Sheets[wb.SheetNames[0]]
      const wsBundleAssociateds = wb.Sheets[wb.SheetNames[1]]
      const dataBundle = XLSX.utils.sheet_to_json(wsBundle)
      const dataBundleAssociateds = XLSX.utils.sheet_to_json(wsBundleAssociateds)
      const dataBundleAssociatedsValidated = dataBundleAssociateds.filter(product => this.validateAssociatedProductsFields(product))
      try {
        const generatedJSON = generateChunksOfArray(this.generateJSONfromXLSX(dataBundle, dataBundleAssociatedsValidated), 50)
        if (generatedJSON && generatedJSON.length > 0) {
          await this.clientBFF.updateProductsToStaging({ products: generatedJSON.flat(1) })
          // await Promise.all(promises)
          this.setState({ generatedJSON })
          // window.open('https://buysmart-sta˘ging.lider.cl/catalogo', '_blank')
          resolve()
        }
        reject()
      } catch (e) {
        reject()
      }
    })

    handleUploadToProduction = () => new Promise(async (resolve, reject) => {
      const { generatedJSON } = this.state
      if (generatedJSON && generatedJSON.length > 0) {
        try {
          await this.clientBFF.updateProductsToProduction({ products: generatedJSON.flat(1) })
          resolve()
        } catch (e) {
          reject()
        }
      }
      reject()
    })

    handleGenerateXLSXClick = () => new Promise(async (resolve, reject) => {
      const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
      const fileExtension = '.xlsx'
      const { bundlesListDB } = this.state
      try {
        if (bundlesListDB && bundlesListDB.length > 0) {
          const newXLSX = await this.generateXLSXfromJSON(bundlesListDB)
          const data = new Blob([newXLSX], { type: fileType })
          FileSaver.saveAs(data, `bundles${fileExtension}`)
          resolve()
        }
        reject()
      } catch (e) {
        reject()
      }
    })

    render() {
      const titlesConfig = {
        alertSucessTitle: 'Los bundles se han creado correctamente en producción',
        headTitle: 'Carga Masiva de Bundles',
        donwloadButtonText: 'Descargar Bundles',
        uploadStagingButtonText: 'Subir bundles a staging',
        uploadProductionButtonText: 'Subir bundles a producción',
        checkInfoText: ' He revisado en el sitio de pruebas que los bundles posean la información correcta antes de subir a producción.',
      }
      return (
        <CustomFileManager
          handleGenerateXLSXClick={this.handleGenerateXLSXClick}
          handleUploadToStaging={this.handleUploadToStaging}
          handleUploadToProduction={this.handleUploadToProduction}
          titlesConfig={titlesConfig}
        />
      )
    }
}

export default BulksBundles
