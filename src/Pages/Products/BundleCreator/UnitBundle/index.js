import React, { Component, Fragment } from 'react'
import Form from '@rjsf/core'
import './index.css'
import * as FileSaver from 'file-saver'
import PropTypes from 'prop-types'
import PageTitle from '../../../../components/PageTitle'
import Card from '../../../../components/Card'

const originalSchema = {
  title: 'Create bundle',
  type: 'object',
  required: [
    'sku',
    'itemNumber',
    'ID',
    'brand',
    'displayName',
    'price',
    'associatedProducts'],
  properties: {
    sku: {
      type: 'number',
      title: 'sku',
      default: '',
    },
    itemNumber: {
      type: 'string',
      title: 'itemNumber',
      default: '',
      readOnly: true,
    },
    ID: {
      type: 'string',
      title: 'ID',
      readOnly: true,
      default: '',
    },
    brand: {
      type: 'string',
      title: 'brand',
      default: 'Combo',
    },
    displayName: {
      type: 'string',
      title: 'displayName',
      default: '',
    },
    associatedProducts: {
      type: 'array',
      title: 'Associated Products',
      items: {
        type: 'object',
        properties: {
          sku: {
            type: 'number',
            title: 'sku',
            default: '',
          },
          itemNumber: {
            type: 'string',
            title: 'itemNumber',
            default: '',
          },
          ID: {
            type: 'string',
            title: 'ID',
            readOnly: true,
            default: '',
          },
          price: {
            title: 'Price',
            type: 'object',
            properties: {
              BasePriceReference: {
                type: 'number',
                title: 'BasePriceReference',
                default: 0,
              },
              BasePriceSales: {
                type: 'number',
                title: 'BasePriceSales',
                default: 0,
              },
              BasePriceTLMC: {
                type: 'number',
                title: 'BasePriceTLMC',
                default: 0,
                readOnly: true,
              },
            },
          },
          brand: {
            type: 'string',
            title: 'brand',
            default: '',
          },
          displayName: {
            type: 'string',
            title: 'displayName',
            default: '',
          },
          quantity: {
            type: 'number',
            title: 'quantity',
            default: 1,
          },
        },
      },
    },
    price: {
      title: 'Bundle Prices',
      type: 'object',
      properties: {
        BasePriceReference: {
          type: 'number',
          title: 'BasePriceReference',
          default: 0,
          readOnly: true,
        },
        BasePriceSales: {
          type: 'number',
          title: 'BasePriceSales',
          default: 0,
          readOnly: true,
        },
        BasePriceTLMC: {
          type: 'number',
          title: 'BasePriceTLMC',
          default: 0,
        },

      },

    },
    discount: {
      type: 'number',
      title: 'discount',
      default: 0,
      readOnly: true,
    },
    posicion: {
      type: 'number',
      title: 'posicion',
      default: 9999,
    },
    stockInicial: {
      type: 'string',
      title: 'stockInicial',
      default: '',
    },
    destacado: {
      type: 'boolean',
      title: 'destacado',
      default: false,
    },
    /* ---start hidden fields---*/
    size: {
      type: 'number',
      title: 'size',
      default: 100,
    },
    color: {
      type: 'string',
      title: 'color',
      default: '',
    },
    peso: {
      type: 'string',
      title: 'peso',
      default: '',
    },
    max: {
      type: 'string',
      title: 'max',
      default: '',
    },
    available: {
      type: 'boolean',
      title: 'available',
      default: false,
    },
    makePublic: {
      type: 'boolean',
      title: 'makePublic',
      default: true,
    },
    vendorId: {
      type: 'string',
      title: 'vendorId',
      default: '',
    },
    leadTime: {
      type: 'string',
      title: 'leadTime',
      default: '',
    },
    appId: {
      type: 'string',
      title: 'appId',
      default: 'BuySmart',
    },
    campaignId: {
      type: 'string',
      title: 'campaignId',
      default: 'blackcyber',
    },
    /* positionCarousel: { type: 'number', title: 'positionCarousel', default: 0 }, */
    gtin13: {
      type: 'number',
      title: 'gtin13',
      default: 1231231,
    },
    longDescription: {
      type: 'null',
      title: 'longDescription',
    },
    /* ---end hidden fields---*/
    categorias: {
      type: 'array',
      title: 'Categorías (igual al nombre de BCC)',
      items: {
        type: 'object',
        properties: {
          category1: {
            type: 'string',
            title: 'Nivel 1',
            default: '',
          },
          category2: {
            type: 'string',
            title: 'Nivel 2',
            default: '',
          },
          category3: {
            type: 'string',
            title: 'Nivel 3',
            default: '',
          },
        },
      },
    },
    keyword: {
      type: 'array',
      title: 'Keywords',
      default: [
        'Campana',
      ],
      items: {
        type: 'string',
        default: '',
      },
    },
    filters: {
      type: 'array',
      title: 'Filters',
      items: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            title: 'Name',
            default: '',
          },
          description: {
            type: 'string',
            title: 'Description',
            default: '',
          },
        },
      },
    },
    tags: {
      type: 'array',
      title: 'Tags',
      default: [
        'exclusivo',
      ],
      items: {
        type: 'string',
        default: '',
      },
    },
    imagesAvailables: {
      type: 'array',
      title: 'Images Availables',
      default: [
        'a.jpg',
      ],
      items: {
        type: 'string',
        default: 'a.jpg',
      },
    },
  },
}

const originalUISchema = {
  available: { 'ui:widget': 'hidden' },
  color: { 'ui:widget': 'hidden' },
  peso: { 'ui:widget': 'hidden' },
  max: { 'ui:widget': 'hidden' },
  makePublic: { 'ui:widget': 'hidden' },
  vendorId: { 'ui:widget': 'hidden' },
  leadTime: { 'ui:widget': 'hidden' },
  /* positionCarousel: { 'ui:widget': 'hidden' }, */
  gtin13: { 'ui:widget': 'hidden' },
  longDescription: { 'ui:widget': 'hidden' },
}

class BundleCreator extends Component {
  constructor(props) {
    super(props)
    this.state = {
      schema: originalSchema,
    }
  }

  handleOnChange = (changedForm) => {
    const { formData } = changedForm
    const { schema } = this.state
    let bundleSalesPrice = 0
    let bundleReferencePrice = 0
    Object.keys(schema.properties)
      .forEach((key) => {
        schema.properties[key].default = formData[key]
      })

    if (schema.properties.associatedProducts.default && schema.properties.associatedProducts.default.length > 0) {
      schema.properties.associatedProducts.default.forEach((associatedProduct) => {
        const { quantity = 0, price, sku = '' } = associatedProduct
        const { BasePriceReference = 0, BasePriceSales = 0 } = price
        associatedProduct.ID = `PROD_${sku}`
        bundleReferencePrice += BasePriceReference * quantity
        bundleSalesPrice += BasePriceSales * quantity
      })
    }
    const sku = formData.sku || ''
    schema.properties.ID.default = `PROD_${sku}`
    schema.properties.itemNumber.default = sku.toString()
    schema.properties.price.properties.BasePriceReference.default = bundleReferencePrice
    schema.properties.price.properties.BasePriceSales.default = bundleSalesPrice
    if (bundleReferencePrice > 0) {
      schema.properties.discount.default = Math.floor(100 - ((bundleSalesPrice * 100) / bundleReferencePrice))
    }
    this.setState({ schema })
  }

  ArrayFieldTemplate = props => (
    <div
      className=""
      style={{
        border: '2px solid #ececec',
        borderRadius: '4px',
        padding: '12px',
      }}
    >
      <h3>{props.title}</h3>
      <div className="row" style={{ padding: '15px' }}>
        {props.items
        && props.items.map(element => (
          <div key={element.key} style={{ marginRight: '10px' }}>
            <div>{element.children}</div>
            <button
              type="button"
              className="btn btn-danger float-right"
              style={{
                marginRight: '15px',
                marginBottom: '20px',
                color: '#dc3545',
                backgroundColor: '#fff',
                borderColor: '#dc3545',
              }}
              onClick={element.onDropIndexClick(element.index)}
            >
              Delete
            </button>
          </div>
        ))}
      </div>

      {props.canAdd && (
        <div className="d-flex">
          <p className="col-12">
            <button
              className="btn btn-info"
              style={{ width: '100%' }}
              onClick={props.onAddClick}
              type="button"
            >
              {`Add ${props.title}`}
            </button>
          </p>
        </div>
      )}
    </div>
  )

  getFormatFilters = (filters) => {
    const newFilters = []
    filters.map(filter => newFilters.push({ [filter.name]: filter.description }))
    return newFilters
  }

  handleSubmit = (form) => {
    const { formData } = form
    const allowdStoresObject = {
      allowedStoresHD: [],
      allowedStoresS2S: [],
    }

    const enhancedFormData = Object.assign({}, formData, allowdStoresObject)
    const newCategories = []
    if (enhancedFormData.categorias && enhancedFormData.categorias.length > 0) {
      enhancedFormData.categorias.forEach((category) => {
        const firstLevelCategory = category.category1
        const secondLevelCategory = `${firstLevelCategory}/${category.category2}`
        const thirdLevelCategory = `${secondLevelCategory}/${category.category3}`
        newCategories.push(firstLevelCategory, secondLevelCategory, thirdLevelCategory)
      })
    }
    enhancedFormData.categorias = newCategories

    enhancedFormData.filters = enhancedFormData.filters ? this.getFormatFilters(enhancedFormData.filters) : []

    const data = new Blob([JSON.stringify(enhancedFormData)], { type: 'application/JSON;charset=UTF-8' })
    FileSaver.saveAs(data, `bundle${formData.sku}.json`)
  }

  render() {
    const { schema } = this.state
    const { title } = this.props
    return (
      <Fragment>
        <PageTitle>{title}</PageTitle>
        <div className="row mb-20">
          <Card className="col-sm-12 left-card-margin">
            <Form
              schema={schema}
              uiSchema={originalUISchema}
              ArrayFieldTemplate={this.ArrayFieldTemplate}
              onChange={this.handleOnChange}
              onSubmit={this.handleSubmit}
              onError={() => {}}
            />
          </Card>
        </div>
      </Fragment>

    )
  }
}

BundleCreator.propTypes = {
  title: PropTypes.string.isRequired,
}

export default BundleCreator
