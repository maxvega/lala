import React, { Component, Fragment } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import PropTypes from 'prop-types'
import FileInput from '../../../../components/FileInput'
import SideModal from '../../../../components/SideModal'
import Input from '../../../../components/Input'
import Label from '../../../../components/Label'
import ClientBFF from '../../../../common/ClientBFF'
import ActionButton from '../../../../components/ActionButton'
import { isExtensionValid, maxBytesAllowed } from '../../../../Helper'

export class NewBanner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      banner: '',
      url: '',
      showLoader: false,
      imagesToUpload: [],
    }
    this.clientBFF = new ClientBFF()
  }


  inputHandler=(e, type) => {
    const { files } = e.target
    const { imagesToUpload } = this.state
    // const { showSuccessCustomAlert, showErrorCustomAlert } = this.props
    if (files && files[0]) {
      if (isExtensionValid(files[0].name) && files[0].size <= maxBytesAllowed) {
        imagesToUpload.push({ type, file: files[0] })
        this.setState({ imagesToUpload })
        // showSuccessCustomAlert()
      } else {
        alert('error con la imagen')
      }
    }
  }


  renderUploadButton = title => (
    <Fragment>
      <Label className="mb-10">{`Imagen ${title}`}</Label>
      <div className="d-flex justify-content-center">
        <FileInput type={title} inputHandler={this.inputHandler} />
      </div>
      <p style={{
        fontSize: '8px', fontStyle: 'italic', textAlign: 'center', color: '#0071c3',
      }}
      >
        La imagen no debe exceder los 200Kb y debe estar en  formato .jpg .png o .gif
      </p>
    </Fragment>
  )

  uploadImage = async (uploadedImage) => {
    const formData = new FormData()
    formData.append('file', uploadedImage.file)
    formData.append('storageContainer', `landing/banners/liderbci/${uploadedImage.type}`)
    return this.clientBFF.upload(formData)
  }

  uploadImagesHandler = async () => {
    const { imagesToUpload } = this.state

    this.setState({ showLoader: true }, async () => {
      await Promise.all(imagesToUpload.map(uploadPromise => this.uploadImage(uploadPromise)))
      this.setState({ showLoader: false })
    })
  }

  render() {
    const { banner, url, showLoader } = this.state
    const { closeSideNav } = this.props
    return (
      <SideModal>
        <div
          style={{
            position: 'absolute', top: 10, right: 20, cursor: 'pointer',
          }}
          onClick={closeSideNav}
        >
          <i style={{ marginLeft: ' 10px', fontSize: '30px' }} className="zmdi zmdi-close" />
        </div>

        <div className="d-flex justify-content-between mb-10" style={{ marginTop: '20px' }}>
          <div>
            <span style={{ fontSize: '24px' }}>Nuevo Banner</span>
          </div>
        </div>
        <div className="mb-10">
          <Label>Banner</Label>
          <Input
            className="bold"
            value={banner}
            onChange={e => this.handleChangeName(e)}
          />
        </div>
        <div className="mb-10">
          <Label>URL</Label>
          <Input
            className="bold"
            value={url}
            onChange={e => this.handleChangeDescription(e)}
          />
        </div>
        {this.renderUploadButton('desktop')}

        {this.renderUploadButton('mobile')}
        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}

        {!showLoader && (
          <div>
            <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.uploadImagesHandler}>
              guardar cambios
            </ActionButton>
          </div>
        )}
      </SideModal>
    )
  }
}

NewBanner.propTypes = {
  closeSideNav: PropTypes.func.isRequired,
}

export default NewBanner
