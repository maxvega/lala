import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import { connect } from 'react-redux'
import Card from '../../../../components/Card'
import ActionButton from '../../../../components/ActionButton'
import ImageGallery from '../../../../components/ImageGallery'
import FileInput from '../../../../components/FileInput'
import CustomParagraph from '../../../../components/CustomParagraph'
import StyledModal from '../../../../components/StyledModal'
import BackArrow from '../../../../components/BackArrow'
import EditTitle from '../../../../components/EditTitle'
import {
  isExtensionValid, maxBytesAllowed, formatBytes, JSONToFile,
} from '../../../../Helper'
import ClientBFF from '../../../../common/ClientBFF'
import '../index.css'

const configData = window.__ENV__

class EditBciBanner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bciBanners: [],
      showLoader: false,
      uploadedFile: null,
      showModal: false,
      storageContainer: 'landing',
      storageMobileDelimiter: 'banners/liderbci/mobile',
      storageDesktopDelimiter: 'banners/liderbci/desktop',
      selectedImageFromGallery: '',
      deleteImageInfo: null,
      path: '/storage/download-parsed',
    }

    this.clientBFF = new ClientBFF(configData)
  }

  componentDidMount = async () => {
    await this.getBciBanners()
  }

  componentDidUpdate=async (prevProps) => {
    const { deviceToEdit } = this.props
    if (deviceToEdit !== prevProps.deviceToEdit) {
      await this.getBciBanners()
    }
  }

  getBciBanners = async () => {
    const { deviceToEdit } = this.props
    const {
      storageMobileDelimiter, storageDesktopDelimiter, storageContainer,
    } = this.state
    const delimiter = deviceToEdit === 'desktop' ? storageDesktopDelimiter : storageMobileDelimiter
    this.setState({ showLoader: true }, async () => {
      const bciBanners = await this.clientBFF.getBanners({
        storageContainer,
        delimiter,
        prefix: delimiter,
      })
      this.setState({ bciBanners, showLoader: false })
    })
  }

  getBCIBannersSettings = async () => {
    const { path } = this.state
    const bannersSettings = await this.clientBFF.download({
      storageContainer: 'landing/contentSettings',
      resourceName: 'banners_settings.json',
    }, { path })
    return bannersSettings
  }

  uploadBCIBanner = async () => {
    const { showSuccessCustomAlert, showErrorCustomAlert } = this.props
    const { deviceToEdit } = this.props
    const {
      storageMobileDelimiter, storageDesktopDelimiter, storageContainer, uploadedFile,
    } = this.state
    if (uploadedFile && uploadedFile.imageIsValid) {
      const container = deviceToEdit === 'desktop' ? `${storageContainer}/${storageDesktopDelimiter}` : `${storageContainer}/${storageMobileDelimiter}`
      const formData = new FormData()
      formData.append('file', uploadedFile)
      formData.append('storageContainer', container)
      this.setState({ showLoader: true }, async () => {
        this.clientBFF.upload(formData).then(() => {
          showSuccessCustomAlert()
          this.getBciBanners()
          this.toggleModal()
        }).catch(() => {
          showErrorCustomAlert()
          this.toggleModal()
        })
      })
    }
  }

  updateBCIBannersSettings = async (fileName) => {
    const { updateBCIBannersSettings, deviceToEdit, contentToEdit } = this.props
    const { storageMobileDelimiter, storageDesktopDelimiter } = this.state
    const container = deviceToEdit === 'desktop' ? `${storageDesktopDelimiter}` : `${storageMobileDelimiter}`
    const LiderBCIBanners = await this.getBCIBannersSettings()
    const content = LiderBCIBanners[contentToEdit]
    const currentBanner = content[deviceToEdit][0]
    currentBanner.image = fileName.replace(`${container}/`, '')
    currentBanner.url = fileName
    const file = JSONToFile(LiderBCIBanners, 'banners_settings.json')
    const formData = new FormData()
    formData.append('file', file)
    formData.append('storageContainer', 'landing/contentSettings')
    formData.append('keepOriginalName', 'true')
    await this.clientBFF.upload(formData)
    updateBCIBannersSettings()
  }

  removeBCIBanner = async (image) => {
    try {
      const { bciBanners, storageContainer } = this.state
      const newBCIBanners = bciBanners.filter(img => img !== image)
      await this.clientBFF.delete({
        storageContainer,
        fileName: image,
      })
      this.setState({
        showModal: false, deleteImageInfo: null, uploadedFile: null, bciBanners: newBCIBanners,
      })
      this.setState({ bciBanners: newBCIBanners })
    } catch (e) {
      // TODO, to be defined error message
      // eslint-disable-next-line no-console
      console.log(e)
    }
  }

  handleRemoveBanner = async (image) => {
    this.setState({ showModal: true, deleteImageInfo: image, uploadedFile: null })
  }

  inputHandler=(e) => {
    const { showErrorCustomAlert, hideCustomAlerts } = this.props
    const { files } = e.target
    hideCustomAlerts()
    if (files && files[0]) {
      const imageIsValid = isExtensionValid(files[0].name) && files[0].size <= maxBytesAllowed
      files[0].imageIsValid = imageIsValid
      if (!imageIsValid) {
        showErrorCustomAlert()
      }
      this.setState({
        uploadedFile: files[0], showModal: true, deleteImageInfo: null,
      })
    }
  }

  renderUploadImageInput = () => (
    <div className="mb-10" style={{ display: 'flex', justifyContent: 'center' }}>
      <FileInput inputHandler={this.inputHandler} />
    </div>
  )

  toggleModal=() => {
    this.setState({ showModal: false })
  }

  handleImageSelectedFromGallery = (image) => {
    const { selectedImageFromGallery } = this.state
    const newImage = selectedImageFromGallery === image ? '' : image
    this.setState({ selectedImageFromGallery: newImage })
  }

  renderNewImageModalBody = (uploadedFile) => {
    const { imageIsValid, name, size } = uploadedFile
    return (
      <Fragment>
        <div className="container" style={{ padding: '20px' }}>
          <div className="text-center mb-20">
            <h2 className="mb-10" style={{ fontSize: '16px' }}>Subir una imagen nueva</h2>
            <div className="mb-20">
              {imageIsValid ? 'La imagen se ha cargado correctamente.' : '¡Lo sentimos! Esta imagen no es válida..' }
            </div>
            <div className="mb-20">
              <CustomParagraph
                paragraphTitle={`${name} (${formatBytes(size)})`}
                paragraphStyle={{ whiteSpace: 'nowrap', color: imageIsValid ? '#0071c3' : '#cd3434' }}
                iconCustomStyle={{ marginRight: '5px', backgroundColor: imageIsValid ? '#0071c3' : '#cd3434' }}
                parentCustomStyle={{ justifyContent: 'center' }}
                iconTooltipText="editar banner"
                iconTooltipId="edit-banner"
                icon={imageIsValid ? 'zmdi zmdi-check' : 'zmdi zmdi-close'}
              />
              {!imageIsValid && (
              <p style={{
                fontSize: '10px', fontStyle: 'italic', textAlign: 'center', color: imageIsValid === false ? '#cd3434' : '#0071c3',
              }}
              >
                La imagen no debe exceder los 200Kb y debe estar en  formato .jpg .png o .gif
              </p>
              )}
            </div>
            <div className="mb-10">
              {imageIsValid ? (
                <ActionButton type="primary" onClick={() => this.uploadBCIBanner()} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
                  guardar cambios
                </ActionButton>
              ) : (
                <ActionButton type="primary" onClick={() => this.toggleModal()} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
                  intentar nuevamente
                </ActionButton>
              )}
            </div>
            <div className="mb-20">
              <ActionButton
                onClick={() => this.toggleModal()}
                style={{
                  margin: 'auto', backgroundColor: '#f6f6f6', color: '#293152', width: '200px', borderColor: '#f6f6f6',
                }}
              >
                Cancelar
              </ActionButton>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }

  renderDeleteImageModalBody = deleteImageInfo => (
    <Fragment>
      <div className="container" style={{ padding: '20px' }}>
        <div className="text-center mb-20">
          <h2 className="mb-10">Seguro deseas eliminar esta imagen?</h2>
          <div className="mb-20">
            <img style={{ maxHeight: '100%' }} className="img-fluid" alt="logo" src={`${configData.storageBaseUrl}/${deleteImageInfo}`} />
          </div>
          <div className="mb-10">
            <ActionButton type="danger" onClick={() => this.removeBCIBanner(deleteImageInfo)} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
              Borrar
            </ActionButton>
          </div>
          <div className="mb-20">
            <ActionButton
              onClick={() => this.toggleModal()}
              style={{
                margin: 'auto', backgroundColor: '#f6f6f6', color: '#293152', width: '200px', borderColor: '#f6f6f6',
              }}
            >
              Cancelar
            </ActionButton>
          </div>
        </div>
      </div>
    </Fragment>
  )


  renderDeleteImageModalBody = deleteImageInfo => (
    <Fragment>
      <div className="container" style={{ padding: '20px' }}>
        <div className="text-center mb-20">
          <h2 className="mb-10">Seguro deseas eliminar esta imagen?</h2>
          <div className="mb-20">
            <img style={{ maxHeight: '100%' }} className="img-fluid" alt="logo" src={`${configData.storageBaseUrl}/${deleteImageInfo}`} />
          </div>
          <div className="mb-10">
            <ActionButton type="danger" onClick={() => this.removeBCIBanner(deleteImageInfo)} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
              Borrar
            </ActionButton>
          </div>
          <div className="mb-20">
            <ActionButton
              onClick={() => this.toggleModal()}
              style={{
                margin: 'auto', backgroundColor: '#f6f6f6', color: '#293152', width: '200px', borderColor: '#f6f6f6',
              }}
            >
              Cancelar
            </ActionButton>
          </div>
        </div>
      </div>
    </Fragment>
  )

  render() {
    const {
      showLoader, bciBanners, uploadedFile, showModal, selectedImageFromGallery, deleteImageInfo,
    } = this.state
    const { title, setDeviceToEdit, deviceToEdit } = this.props

    return (
      <Card className="col-md-5 col-sm-12 right-card-margin">
        <div className="d-flex justify-content-between mb-20">
          <div>
            <div className="d-flex">
              <BackArrow
                className="text-center"
                onClick={() => { setDeviceToEdit(null) }}
              />
              <EditTitle>
                {`${title} para ${deviceToEdit}`}
              </EditTitle>
            </div>
          </div>
        </div>
        {
          !showLoader && (
            <Fragment>
              {this.renderUploadImageInput()}
              <p style={{
                fontSize: '10px', fontStyle: 'italic', textAlign: 'center', color: '#0071ce',
              }}
              >
                La imagen no debe exceder los 200Kb y debe estar en  formato .jpg .png o .gif
              </p>
            </Fragment>
          )
        }

        {showLoader && (
        <div className="row">
          <CircularProgress
            className="progress-primary"
            size={26}
            mode="determinate"
            value={75}
            style={{ color: '#0071ce', margin: 'auto' }}
          />
        </div>
        )}
        {!showLoader && (
        <Fragment>
          <ImageGallery handleImageToDelete={this.handleRemoveBanner} images={bciBanners} title="Subidas recientes" selectedImageHandler={this.handleImageSelectedFromGallery} selectedImageFromGallery={selectedImageFromGallery} />
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <ActionButton disabled={!selectedImageFromGallery} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={() => this.updateBCIBannersSettings(selectedImageFromGallery)}>
              subir cambios a producción
            </ActionButton>
          </div>
        </Fragment>
        )}
        <StyledModal
          modalIsOpen={showModal}
          closeModalAction={() => {
            this.toggleModal()
          }}
        >
          {uploadedFile && this.renderNewImageModalBody(uploadedFile)}
          {deleteImageInfo && this.renderDeleteImageModalBody(deleteImageInfo)}
        </StyledModal>
      </Card>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.loginReducer,
})

EditBciBanner.propTypes = {
  title: PropTypes.string,
  showSuccessCustomAlert: PropTypes.func.isRequired,
  showErrorCustomAlert: PropTypes.func.isRequired,
  hideCustomAlerts: PropTypes.func.isRequired,
  deviceToEdit: PropTypes.string.isRequired,
  contentToEdit: PropTypes.string.isRequired,
  setDeviceToEdit: PropTypes.func.isRequired,
  updateBCIBannersSettings: PropTypes.func,
}

EditBciBanner.defaultProps = {
  title: '',
  updateBCIBannersSettings: () => {},
}

export default connect(mapStateToProps)(EditBciBanner)
