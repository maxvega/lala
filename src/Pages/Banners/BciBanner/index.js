import React, { Component, Fragment } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Card from '../../../components/Card'
import CustomAlert from '../../../components/CustomAlert'
import EditBciBanner from './EditBciBanner'
import CustomParagraph from '../../../components/CustomParagraph'
import PageTitle from '../../../components/PageTitle'
import Element from '../../../components/Card/Element'
import RoundIconWrapper from '../../../components/RoundIconWrapper'
import ClientBFF from '../../../common/ClientBFF'
import { setModalImageOpen, setModalImageToDisplay } from '../../../Redux/actions/setModalImage'
import './index.css'

const configData = window.__ENV__

class BciBanner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      showErrorCustomAlert: false,
      showSuccessCustomAlert: false,
      showMondayBCIContent: true,
      showLiderBCIContent: false,
      deviceToEdit: null,
      contentToEdit: '',
      bannersSettings: {},
      path: '/storage/download-parsed',
    }
    this.clientBFF = new ClientBFF(configData)
  }

  componentDidMount = async () => {
    this.getBCIBannersSettings()
  }

  getBCIBannersSettings = async () => {
    this.setState({ showLoader: true }, async () => {
      const { path } = this.state
      const bannersSettings = await this.clientBFF.download({
        storageContainer: 'landing/contentSettings',
        resourceName: 'banners_settings.json',
        contentToExtract: ['mondayLiderBCI', 'liderBCI'],
      }, { path })

      this.setState({ bannersSettings, showLoader: false })
    })
  }

  setImageToDisplayInModal = (image) => {
    const { setModalImageToDisplay, setModalImageOpen } = this.props
    setModalImageToDisplay(image)
    setModalImageOpen()
  }

  renderParagraph = ({
    paragraphTitle, imageURL, deviceToEdit, contentToEdit,
  }) => {
    const { windowSize } = this.props
    const isMobile = windowSize.width <= 768
    const iconDimessions = isMobile ? '17px' : '19px'
    return (
      <div className="row mb-20">
        <div className={`${isMobile ? 'col-sm-12' : ''} banner-paragraph`} style={{ paddingLeft: '0px' }}>
          {paragraphTitle}
        </div>
        <div className={`${isMobile ? 'col-sm-12' : ''}`} style={{ display: 'flex', paddingLeft: '0px' }}>
          <div className="banner-paragraph" onClick={(e) => { e.preventDefault(); this.setDeviceToEdit(deviceToEdit, contentToEdit) }}>
            <a href="#" style={{ color: '#0071ce', fontWeight: 'bold' }}>{`editar ${deviceToEdit}`}</a>
          </div>
          <RoundIconWrapper tooltipText="editar banner" id={`edit-${deviceToEdit}-banner`} width={iconDimessions} height={iconDimessions} className="icon-resize-container">
            <i className="zmdi zmdi-edit" onClick={() => this.setDeviceToEdit(deviceToEdit, contentToEdit)} />
          </RoundIconWrapper>
          <div className="banner-paragraph" style={{ marginLeft: '10px' }} onClick={(e) => { e.preventDefault(); this.setImageToDisplayInModal(imageURL) }}>
            <a href="#" style={{ color: '#041e42', fontWeight: 'bold' }}>{`ver ${deviceToEdit}`}</a>
          </div>
          <RoundIconWrapper tooltipText="ver imagen" id={`view-${deviceToEdit}-image`} width={iconDimessions} height={iconDimessions} className="icon-resize-container" backgroundColor="#041e42">
            <i className="zmdi zmdi-search" onClick={() => this.setImageToDisplayInModal(imageURL)} />
          </RoundIconWrapper>
        </div>
      </div>
    )
  }

  renderCampaignBCIContent = () => {
    const { showLiderBCIContent, bannersSettings } = this.state
    const { liderBCI } = bannersSettings
    const mobileBanner = liderBCI.mobile[0]
    const desktopBanner = liderBCI.desktop[0]
    return (
      <div>
        <Element className="d-flex justify-content-between" style={{ flexDirection: 'column' }}>
          <CustomParagraph
            paragraphTitle="Huincha campaña martes a domingo"
            iconCustomStyle={{ right: '0', position: 'absolute' }}
            parentCustomStyle={{ position: 'relative' }}
            iconTooltipText="editar banner"
            iconTooltipId="edit-banner"
            icon="zmdi zmdi-edit"
            clickHandler={() => { this.handleLiderBCIContent() }}
          />
          {showLiderBCIContent && (
          <Fragment>
            {this.renderParagraph({
              paragraphTitle: desktopBanner.image, imageURL: desktopBanner.url, deviceToEdit: 'desktop', contentToEdit: 'liderBCI',
            })}

            {this.renderParagraph({
              paragraphTitle: mobileBanner.image, imageURL: mobileBanner.url, deviceToEdit: 'mobile', contentToEdit: 'liderBCI',
            })}
          </Fragment>
          )}
        </Element>
      </div>
    )
  }

  renderMondayBCIContent = () => {
    const { showMondayBCIContent, bannersSettings } = this.state
    const { mondayLiderBCI } = bannersSettings
    const mobileBanner = mondayLiderBCI.mobile[0]
    const desktopBanner = mondayLiderBCI.desktop[0]

    return (
      <div>
        <div>Huinchas Lider BCI</div>
        <Element className="d-flex justify-content-between" style={{ flexDirection: 'column' }}>
          <CustomParagraph
            paragraphTitle="Huincha 6% lunes"
            iconCustomStyle={{ right: '0', position: 'absolute' }}
            parentCustomStyle={{ position: 'relative' }}
            clickHandler={() => { this.handleMondayBCIContent() }}
            iconTooltipText="editar banner"
            iconTooltipId="edit-banner"
            icon="zmdi zmdi-edit"
          />
          {showMondayBCIContent && (
          <Fragment>
            {this.renderParagraph({
              paragraphTitle: desktopBanner.image, imageURL: desktopBanner.url, deviceToEdit: 'desktop', contentToEdit: 'mondayLiderBCI',
            })}

            {this.renderParagraph({
              paragraphTitle: mobileBanner.image, imageURL: mobileBanner.url, deviceToEdit: 'mobile', contentToEdit: 'mondayLiderBCI',
            })}
          </Fragment>
          )}
        </Element>
      </div>
    )
  }

  handleMondayBCIContent = () => {
    const { showMondayBCIContent } = this.state
    this.setState({ showMondayBCIContent: !showMondayBCIContent })
  }

  handleLiderBCIContent = () => {
    const { showLiderBCIContent } = this.state
    this.setState({ showLiderBCIContent: !showLiderBCIContent })
  }

  setDeviceToEdit = (deviceToEdit, contentToEdit) => {
    this.setState({ deviceToEdit, contentToEdit })
  }

  showSuccessCustomAlert = () => {
    this.setState({ showSuccessCustomAlert: true, showErrorCustomAlert: false }, () => { setTimeout(() => { this.setState({ showSuccessCustomAlert: false }) }, 5000) })
  }

  showErrorCustomAlert = () => {
    this.setState({ showSuccessCustomAlert: false, showErrorCustomAlert: true }, () => { setTimeout(() => { this.setState({ showErrorCustomAlert: false }) }, 5000) })
  }

  hideCustomAlerts = () => {
    this.setState({ showSuccessCustomAlert: false, showErrorCustomAlert: false })
  }

  renderErrorAlert = () => (
    <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
      ¡Lo sentimos! La imagen no es válida. Intenta con otra.
    </CustomAlert>
  )

  renderSuccessAlert = () => (
    <CustomAlert type="success" onClick={this.hideCustomAlerts}>
      La imagen ha cargado correctamente.
    </CustomAlert>
  )

  render() {
    const {
      showLoader, showErrorCustomAlert, showSuccessCustomAlert, deviceToEdit, bannersSettings, contentToEdit,
    } = this.state
    const { title } = this.props
    const editBCIBannerSubTitle = contentToEdit === 'mondayLiderBCI' ? 'huincha 6% lunes' : 'huincha martes a domingo'
    const editBCIBannerTitle = `Editar ${editBCIBannerSubTitle}`
    return (
      <Fragment>
        {showErrorCustomAlert && this.renderErrorAlert()}
        {showSuccessCustomAlert && this.renderSuccessAlert()}
        <div>
          <PageTitle>{title}</PageTitle>
        </div>
        <div className="row">
          <Card className={`${deviceToEdit ? 'not-display-in-mobile col-md-7 ' : ''}col-sm-12 left-card-margin`}>
            <div className="row mb-10">
              <div className="col-md-6 col-sm-12">
                <h3 style={{ lineHeight: '40px' }}>Huinchas Lider BCI</h3>
              </div>
            </div>

            {showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto' }}
              />
            </div>
            )}
            {!showLoader && (
            <div>
              {bannersSettings && Object.keys(bannersSettings).length > 0 && (
              <Fragment>
                {this.renderMondayBCIContent()}
                {this.renderCampaignBCIContent()}
              </Fragment>
              )}
            </div>
            )}
          </Card>
          { deviceToEdit && (
          <EditBciBanner
            deviceToEdit={deviceToEdit}
            showErrorCustomAlert={this.showErrorCustomAlert}
            showSuccessCustomAlert={this.showSuccessCustomAlert}
            hideCustomAlerts={this.hideCustomAlerts}
            setDeviceToEdit={this.setDeviceToEdit}
            updateBCIBannersSettings={this.getBCIBannersSettings}
            contentToEdit={contentToEdit}
            title={editBCIBannerTitle}
          />
          )}
        </div>
      </Fragment>
    )
  }
}


const mapDispatchToProps = dispatch => ({
  setModalImageToDisplay: image => dispatch(setModalImageToDisplay(image)),
  setModalImageOpen: () => dispatch(setModalImageOpen()),
})

const mapStateToProps = state => ({
  windowSize: state.windowSizeReducer.windowSize,
})

BciBanner.propTypes = {
  setModalImageToDisplay: PropTypes.func.isRequired,
  setModalImageOpen: PropTypes.func.isRequired,
  title: PropTypes.string,
  windowSize: PropTypes.object.isRequired,
}

BciBanner.defaultProps = {
  title: '',
}

export default connect(mapStateToProps, mapDispatchToProps)(BciBanner)
