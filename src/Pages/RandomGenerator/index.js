import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import ActionButton from '../../components/ActionButton'
import Card from '../../components/Card'
import Input from '../../components/Input'
import withFeedbackModal from '../../components/WithFeedbackModal'

class RandomGenerator extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      userInput: '',
    }
  }

  handleSaveUser=(e) => {
    const { users, userInput } = this.state
    if (e.charCode === 13 && userInput) {
      users.push(userInput)
      this.setState({ users, userInput: '' })
    }
  }

  onChange=(e) => {
    this.setState({ userInput: e.target.value })
  }

  getRandomWinner=() => {
    const { users } = this.state
    const { handleFeedback } = this.props
    const randomElement = users[Math.floor(Math.random() * users.length)]
    handleFeedback({
      title: randomElement,
    })
  }

  render() {
    const {
      users, userInput,
    } = this.state
    return (
      <div className="row">
        <Card className="col-sm-12 left-card-margin">
          <Fragment>
            <div className="row mb-10">
              <div className="col-sm-12">
                <h3 style={{ lineHeight: '40px' }}>Random Generator</h3>
              </div>
            </div>
            <div>
              <div>
                Nombres:
              </div>
              {users.map((user, ix) => <div key={`key-${user}-${ix}`} className="d-flex">{user}</div>)}
              <div className="d-flex">
                <Input onChange={(e) => { this.onChange(e) }} value={userInput} onKeyPress={(e) => { this.handleSaveUser(e) }} />
              </div>
              {users.length > 0 && (
              <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.getRandomWinner}>
                Random!
              </ActionButton>
              )}
            </div>
          </Fragment>
        </Card>
      </div>
    )
  }
}

RandomGenerator.propTypes = {
  handleFeedback: PropTypes.func.isRequired,
}

export default withFeedbackModal(RandomGenerator)
