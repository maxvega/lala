import React from 'react'

const Loader = () => (
  <div
    style={{
      position: 'absolute', background: '#fff', height: '100%', width: '100%',
    }}
  >
    <div style={{
      position: 'absolute', top: '25%', width: '100%', background: '#fff',
    }}
    >
      <div className="d-flex">
        <div className="col-12">
          <div className="text-center">
            <img style={{ maxWidth: '100px' }} className="img-fluid" alt="BuySmart Content Manager" src="/images/spark-small.gif" />
          </div>
          <div className="text-center">
            <img style={{ maxWidth: '600px' }} className="img-fluid" alt="BuySmart Content Manager" src="/images/buysmart-logo-blue.svg" />
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Loader
