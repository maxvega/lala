import React, { useEffect, useState } from 'react'
import PropTypes, { shape } from 'prop-types'
import './style.css'
import CircularProgress from '@material-ui/core/CircularProgress'
import ActionButton from '../../components/ActionButton'
import Card from '../../components/Card'
import Input from '../../components/Input'
import Label from '../../components/Label'
import { saveAuthData } from '../../Helper'

const Login = ({
  authUserCL, userInfo, history, form, handleChange,
}) => {
  const [showLoader, setShowLoader] = useState(false)
  const [showError, setShowError] = useState(false)

  const { result, loginInfo = {} } = userInfo

  const { email = '', password = '' } = form

  useEffect(() => {
    if (result === 'success') {
      saveAuthData(loginInfo)
      setTimeout(() => {
        history.push('/app')
      }, 1000)
    }

    if (result === 'error') {
      setShowError(true)
      setTimeout(() => {
        setShowLoader(false)
      }, 3000)
    }
  }, [result])

  const onSubmit = async (e) => {
    e.preventDefault()
    authUserCL(form)
  }

  return (
    <div className="login-background">
      <div>
        <div className="text-center mb-10">
          <img width={300} className="img-fluid" alt="logo" src="/images/buysmart-logo.svg" />
        </div>
        <Card className="login-container" style={{ padding: '25px' }}>
          <form onSubmit={onSubmit}>
            <div className="text-center">
              <div className="bold" style={{ fontSize: '20px' }}>Bienvenido</div>
              <div className="mb-10" style={{ fontSize: '16px' }}>inicia sesión para comenzar</div>
            </div>
            <div className="mb-20">
              <Label>Correo</Label>
              <Input
                type="text"
                name="email"
                placeholder="Email"
                value={email}
                onChange={handleChange}
              />
              <Label>Contraseña</Label>
              <Input
                type="password"
                name="password"
                placeholder="Contraseña"
                value={password}
                onChange={e => handleChange(e)}
              />
            </div>

            {showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto' }}
              />
            </div>
            )}

            {!showLoader && (
            <div className="d-flex">
              <ActionButton
                className="text-center"
                type="primary"
                style={{ color: '#ffffff', width: '100%', margin: 'auto' }}
                onClick={onSubmit}
              >
                Iniciar sesión
              </ActionButton>
            </div>
            )}
            {showError && <div className="text-center">Algo salió mal :c</div>}
          </form>

        </Card>
      </div>
    </div>
  )
}

Login.defaultProps = {
  form: {
    email: '',
    password: '',
  },
}

Login.propTypes = {
  authUserCL: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  userInfo: shape({
    result: PropTypes.string.isRequired,
    loginInfo: PropTypes.object,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  form: shape({
    email: PropTypes.string,
    password: PropTypes.string,
  }),
}

export default Login
