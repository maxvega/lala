import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import './style.css'
import { connect } from 'react-redux'
import StratiLogin from './StratiLogin'
import OriginalLogin from './OriginalLogin'
import { authUserCL } from '../../Redux/actions/loginActions'

const Login = (props) => {
  const config = typeof window !== 'undefined' ? window.__ENV__ : { }
  const isStrati = config.IS_STRATI || false
  const history = useHistory()
  const [form, setForm] = useState({})

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target

    setForm(values => ({
      ...values,
      [name]: value,
    }))
  }

  return (
  // eslint-disable-next-line react/prop-types
    isStrati ? <StratiLogin history={history} /> : <OriginalLogin history={history} authUserCL={props.authUserCL} userInfo={props.userInfo} form={form} handleChange={handleChange} />
  )
}

const mapDispatchToProps = dispatch => ({
  authUserCL: code => dispatch(authUserCL(code)),
})

const mapStateToProps = state => ({
  userInfo: state.loginReducer,
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
