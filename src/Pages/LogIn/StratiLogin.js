import React, { useEffect, useState } from 'react'
import './style.css'
import CircularProgress from '@material-ui/core/CircularProgress'
import { connect } from 'react-redux'
import PropTypes, { shape } from 'prop-types'
import { saveAuthData } from '../../Helper'
import ActionButton from '../../components/ActionButton'

import { userValidateAction } from '../../Redux/actions/loginActions'
import Loader from './Loader'

import Card from '../../components/Card'


const StratiLogin = (props) => {
  const config = typeof window !== 'undefined' ? window.__ENV__ : { }
  const [showLoader, saveShowLoader] = useState(true)
  const [showError, showErrorLoader] = useState(false)

  const {
    userValidateAction, userInfo, history,
  } = props

  const { result, loginInfo = {} } = userInfo

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search)
    const code = searchParams.get('code')
    userValidateAction(code)
  }, [window.location])

  useEffect(() => {
    if (result === 'success') {
      saveAuthData(loginInfo)
      setTimeout(() => {
        history.push('/app')
      }, 1000)
    }

    if (result === 'error') {
      showErrorLoader(true)
      setTimeout(() => {
        saveShowLoader(false)
      }, 3000)
    }

    if (result === 'pending') {
      setTimeout(() => {
        saveShowLoader(false)
      }, 1000)
    }
  }, [result])

  return (
    showLoader ? <Loader />
      : (
        <div className="login-background">
          <div>
            <div className="text-center mb-10">
              <img width={300} className="img-fluid" alt="logo" src="/images/buysmart-logo.svg" />
            </div>
            <Card className="login-container" style={{ padding: '25px' }}>
              <div className="text-center">
                <div className="bold" style={{ fontSize: '20px' }}>Bienvenido</div>
                <div className="mb-10" style={{ fontSize: '16px' }}>inicia sesión para comenzar</div>
              </div>

              {showLoader && (
              <div className="row">
                <CircularProgress
                  className="progress-primary"
                  size={26}
                  mode="determinate"
                  value={75}
                  style={{ color: '#0071ce', margin: 'auto' }}
                />
              </div>
              )}

              {!showLoader && (
              <div style={{ textAlign: 'center' }} className="d-flex">
                <a style={{ width: '100%', margin: 'auto' }} href={config.PINGFED}>
                  <ActionButton
                    className="text-center"
                    type="primary"
                    style={{ color: '#ffffff', width: '100%', margin: 'auto' }}
                  >
                    Walmart Login
                  </ActionButton>
                </a>
              </div>
              )}

              {showError && <div id="showError" style={{ color: 'red' }} className="text-center">Algo salió mal :c</div>}

            </Card>
          </div>
        </div>
      )
  )
}

const mapDispatchToProps = dispatch => ({
  userValidateAction: code => dispatch(userValidateAction(code)),
})

const mapStateToProps = state => ({
  userInfo: state.loginReducer,
})

StratiLogin.defaultProps = {
  history: {},
}

StratiLogin.propTypes = {
  userValidateAction: PropTypes.func.isRequired,
  history: PropTypes.object,
  userInfo: shape({
    result: PropTypes.string.isRequired,
    loginInfo: PropTypes.object,
  }).isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(StratiLogin)
