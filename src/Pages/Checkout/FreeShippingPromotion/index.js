import React, { useState, useEffect } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '../../../components/Card'
import ClientBFF from '../../../common/ClientBFF'
import ActionButton from '../../../components/ActionButton'
import Input from '../../../components/Input'
import Label from '../../../components/Label'

const clientBFF = new ClientBFF(window.__ENV__)

const FreeShippingPromotion = () => {
  const [enabled, setIsEnabled] = useState(false)
  const [value, setValue] = useState(undefined)

  const getFreeShippingPromotionConfig = async () => {
    const { freeShippingPromotionConfig } = await clientBFF.checkoutGetFreeShippingPromotionConfig()
    setIsEnabled(freeShippingPromotionConfig.enabled)
    setValue(freeShippingPromotionConfig.value)
  }
  useEffect(() => {
    getFreeShippingPromotionConfig()
  }, [])

  const valueHandler = (e) => {
    if (e.target && e.target.value > -1) {
      const intValue = parseInt(e.target.value, 10)
      setValue(intValue)
    }
  }

  const updatePromotion = async (newConfig) => {
    await clientBFF.checkoutUpdateFreeShippingPromotionConfig(newConfig)
    await clientBFF.clearBFFCache()
    getFreeShippingPromotionConfig()
  }

  if (!value) {
    return (
      <Card>
        <div className="row" data-testid="loader-container">
          <CircularProgress
            className="progress-primary"
            size={26}
            mode="determinate"
            value={75}
            style={{ color: '#0071ce', margin: 'auto' }}
          />
        </div>
      </Card>
    )
  }

  return (
    <Card>
      <h3>Promoción de despacho gratis para todo medio de pagos</h3>

      <div className="mb-4">
        Actualmente está:
        <b>{enabled ? 'Activado' : 'Desactivado'}</b>
      </div>
      <div>
        <Label>Monto mínimo para aplicar la promoción</Label>
        <Input className="col-sm-2" type="number" defaultValue={value} onChange={valueHandler} min="0" data-testid="free-shipping-value" />
      </div>

      <div className="d-flex my-4">
        <ActionButton className="text-center mr-2" type="primary" style={{ width: '100%', color: '#fff' }} onClick={() => updatePromotion({ enabled: true, value })} data-testid="activate-button">
          {enabled ? 'Actualizar monto mínimo' : 'Activar'}
        </ActionButton>

        {enabled && (
          <ActionButton className="text-center" type="danger" style={{ width: '100%', color: '#fff' }} onClick={() => updatePromotion({ enabled: false, value })} data-testid="deactivate-button">
            Desactivar
          </ActionButton>
        )}
      </div>
    </Card>
  )
}

export default FreeShippingPromotion
