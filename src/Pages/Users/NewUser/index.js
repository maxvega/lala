import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import CustomSelect from '../../../components/CustomSelect'
import ActionButton from '../../../components/ActionButton'
import Input from '../../../components/Input'
import Label from '../../../components/Label'
import SideModal from '../../../components/SideModal'
import { getLocalToken } from '../../../Helper'
import ClientAuth from '../../../common/ClientAuth'

class NewUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '', lastName: '', email: '', password: '', selectedRole: null, roles: props.roles, showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }

  handleChangeRole = (role) => {
    this.setState({ selectedRole: role })
  }

  handleChangeFirstName = (e) => {
    this.setState({ firstName: e.target.value })
  }

  handleChangeLastName = (e) => {
    this.setState({ lastName: e.target.value })
  }

  handleChangeEmail = (e) => {
    this.setState({ email: e.target.value })
  }

  handleChangePassword = (e) => {
    this.setState({ password: e.target.value })
  }

  renderRoles=() => {
    const { roles, selectedRole } = this.state
    const options = roles.map(role => ({ value: role.id, label: role.name }))
    return (
      <CustomSelect
        value={selectedRole}
        placeholder="Seleccionar rol"
        onChange={this.handleChangeRole}
        options={options}
      />
    )
  }

  createUser = () => {
    const { getUsersAndRoles, closeSideNav, handleFeedback } = this.props
    const {
      firstName, lastName, email, password, selectedRole,
    } = this.state

    const token = getLocalToken()
    const newUser = {
      firstName, lastName, email, password, roleId: selectedRole.value,
    }
    return this.clientAuth.createUser(token, newUser).then(() => {
      handleFeedback({
        title: 'Usuario creado correctamente',
        message: 'El usuario ha sido creado correctamente',
      })
    }).catch((err) => {
      handleFeedback({
        title: 'Error al crear usuario',
        message: `Por favor enviar la siguiente información a #black-cyber-squad en Slack: ${err?.response?.data?.message}`,
      })
    }).finally(() => {
      closeSideNav()
      this.setState({ showLoader: false })
      getUsersAndRoles()
    })
  }

  saveUser = async () => {
    const {
      firstName, lastName, email, password, selectedRole,
    } = this.state
    const { handleFeedback } = this.props
    if (firstName && lastName && email && password && selectedRole && selectedRole.value) {
      this.setState({ showLoader: true }, this.createUser)
    } else {
      handleFeedback({
        title: 'Campos incompletos',
        message: 'Por favor revisa los campos solicitados',
      })
    }
  }

  render() {
    const {
      firstName, lastName, email, password, showLoader,
    } = this.state
    const { closeSideNav } = this.props
    return (
      <Fragment>
        <SideModal>
          <div
            role="button"
            style={{
              position: 'absolute', top: 10, right: 20, cursor: 'pointer',
            }}
            onClick={closeSideNav}
            tabIndex="-1"
          >
            <i style={{ marginLeft: ' 10px', fontSize: '30px' }} className="zmdi zmdi-close" />
          </div>

          <div className="d-flex justify-content-between mb-10" style={{ marginTop: '20px' }}>
            <div>
              <span style={{ fontSize: '24px' }}>Nuevo Usuario</span>
            </div>
          </div>
          <div className="mb-10">
            <Label>Nombre</Label>
            <Input
              className="bold"
              value={firstName}
              onChange={e => this.handleChangeFirstName(e)}
            />
          </div>
          <div className="mb-10">
            <Label>Apellido</Label>
            <Input
              className="bold"
              value={lastName}
              onChange={e => this.handleChangeLastName(e)}
            />
          </div>
          <div className="mb-10">
            <Label>Email</Label>
            <Input
              className="bold"
              value={email}
              onChange={e => this.handleChangeEmail(e)}
            />
          </div>
          <div className="mb-10">
            <Label>Password</Label>
            <Input
              type="password"
              className="bold"
              value={password}
              onChange={e => this.handleChangePassword(e)}
            />
          </div>
          <Label className="mb-10">Rol</Label>
          <div className="mb-50">{this.renderRoles()}</div>
          {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
          )}

          {!showLoader && (
          <div>
            <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.saveUser}>
              guardar cambios
            </ActionButton>
          </div>
          )}
        </SideModal>
      </Fragment>
    )
  }
}

NewUser.defaultProps = {
  handleFeedback: () => {},
}

NewUser.propTypes = {
  closeSideNav: PropTypes.func.isRequired,
  getUsersAndRoles: PropTypes.func.isRequired,
  roles: PropTypes.array.isRequired,
  handleFeedback: PropTypes.func,
}

export default NewUser
