import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import InnerTitle from '../../components/InnerTitle'
import ClientAuth from '../../common/ClientAuth'
import PageTitle from '../../components/PageTitle'
import Card from '../../components/Card'
import Element from '../../components/Card/Element'
import ActionButton from '../../components/ActionButton'
import RoundIconWrapper from '../../components/RoundIconWrapper'
import { getLocalToken } from '../../Helper'
import EditUserCard from './EditUserCard'
import NewUser from './NewUser'
import withFeedbackModal from '../../components/WithFeedbackModal'

export class Users extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      roles: [],
      selectedUser: null,
      showNewUser: false,
      showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }

  componentDidMount = () => {
    this.getUsersAndRoles()
  }

  getUsersAndRoles = async () => {
    this.setState({ showLoader: true }, async () => {
      const token = getLocalToken()
      const users = await this.clientAuth.getUsers(token || 'bananas')
      const roles = await this.clientAuth.getRoles(token || 'bananas')
      this.setState({ users, roles, showLoader: false })
    })
  }

  setSelectedUser = (user) => {
    this.setState({ selectedUser: user })
  }

  showNewUser = () => {
    this.setState({ showNewUser: true })
  }

  closeSideNav = () => {
    this.setState({ showNewUser: false })
  }

  onClickRemove = (user) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: `Borrar ${user.name}`,
      message: `¿Estás seguro que quieres borrar a ${user.name} ?`,
      confirmationHandler: this.handleDeleteUser,
      confirmationData: [user],
    })
  }

  renderUsers = users => users.map(user => (
    <Element key={`user-${user.id}`} className="d-flex justify-content-between">
      <div>{user.email}</div>
      <div className="d-flex">
        <RoundIconWrapper tooltipText="Editar Usuario" id={`edit-${user.id}`} style={{ marginRight: '5px' }} onClick={() => { this.setSelectedUser(user) }}>
          <i className="zmdi zmdi-edit" />
        </RoundIconWrapper>

        <RoundIconWrapper tooltipText="Borrar Usuario" id={`delete-${user.id}`} backgroundColor="#eaeaea" color="#000000" onClick={() => { this.onClickRemove(user) }}>
          <i className="zmdi zmdi-delete" />
        </RoundIconWrapper>

      </div>
    </Element>
  ))

  handleDeleteUser=async (user) => {
    const token = getLocalToken()
    await this.clientAuth.deleteUser(token, user.id)
    return this.getUsersAndRoles()
  }

  render() {
    const {
      users, selectedUser, showNewUser, roles, showLoader,
    } = this.state
    const { handleFeedback, title } = this.props
    return (
      <Fragment>
        <PageTitle>{title}</PageTitle>
        <div className="row mb-20">
          <Card className={`${selectedUser ? 'not-display-in-mobile col-md-7 ' : ''}col-sm-12 left-card-margin`}>
            <div className="row mb-10">
              <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
                <InnerTitle>Usuarios creados</InnerTitle>
              </div>
              <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
                <div className="create-button-container">
                  <ActionButton type="info" onClick={this.showNewUser} className="create-new-button">
                    <div className="d-flex">
                      <div>crear nuevo</div>
                      <div><i style={{ marginLeft: '5px', fontWeight: 'bolder' }} className="zmdi zmdi-plus" /></div>
                    </div>
                  </ActionButton>
                </div>
              </div>

            </div>
            {showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto' }}
              />
            </div>
            )}
            {!showLoader && this.renderUsers(users)}
          </Card>
          {selectedUser && selectedUser.email && (
          <EditUserCard user={selectedUser} roles={roles} setSelectedUser={this.setSelectedUser} getUsersAndRoles={this.getUsersAndRoles} handleFeedback={handleFeedback} />
          )}
          <div
            className={showNewUser ? 'overlay-right-modal' : ''}
            onClick={() => { this.closeSideNav() }}
          />
          {showNewUser && <NewUser closeSideNav={this.closeSideNav} roles={roles} getUsersAndRoles={this.getUsersAndRoles} handleFeedback={handleFeedback} />}
        </div>
      </Fragment>
    )
  }
}

Users.propTypes = {
  handleFeedback: PropTypes.func,
  title: PropTypes.string.isRequired,
}

Users.defaultProps = {
  handleFeedback: () => {},
}

export default withFeedbackModal(Users)
