import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import EditTitle from '../../../components/EditTitle'
import CustomSelect from '../../../components/CustomSelect'
import Card from '../../../components/Card'
import Label from '../../../components/Label'
import Input from '../../../components/Input'
import BackArrow from '../../../components/BackArrow'
import ClientAuth from '../../../common/ClientAuth'
import { getLocalToken } from '../../../Helper'
import ActionButton from '../../../components/ActionButton'

class EditUserCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: props.user,
      firstName: props.user.firstName,
      lastName: props.user.lastName,
      email: props.user.email,
      roles: props.roles,
      selectedRole: this.selectedRoleBuilder(props.user.role),
      showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.user) !== JSON.stringify(nextProps.user)) {
      this.setState({
        user: nextProps.user,
        email: nextProps.user.email,
        firstName: nextProps.user.firstName,
        lastName: nextProps.user.lastName,
        roles: nextProps.roles,
        selectedRole: this.selectedRoleBuilder(nextProps.user.role),
      })
    }
  }

  selectedRoleBuilder = (role) => {
    if (role && role.id) {
      return { value: role.id, label: role.name }
    }
    return null
  }

  handleChangeFirstName = (e) => {
    this.setState({ firstName: e.target.value })
  }

  handleChangeLastName = (e) => {
    this.setState({ lastName: e.target.value })
  }

  handleChangeEmail = (e) => {
    this.setState({ email: e.target.value })
  }

  handleChangeRole = (role) => {
    this.setState({ selectedRole: role })
  }

  updateUser = () => {
    const {
      user, userEmail, firstName, lastName, selectedRole, roles,
    } = this.state
    const { getUsersAndRoles, handleFeedback, setSelectedUser } = this.props
    const token = getLocalToken()
    const newUser = Object.assign({}, user)
    newUser.firstName = firstName
    newUser.lastName = lastName
    newUser.email = userEmail
    newUser.role = roles.find(role => role.id === selectedRole.value)
    return this.clientAuth.updateUser(token, newUser).then(() => {
      handleFeedback({
        title: 'Usuario editado correctamente',
      })
    }).catch((err) => {
      handleFeedback({
        title: 'Usuario editado correctamente',
        message: `Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: ${err?.response?.data?.message}`,
      })
    }).finally(() => {
      getUsersAndRoles()
      this.setState({ showLoader: false })
      setSelectedUser(null)
    })
  }

  saveUser = async () => {
    this.setState({ showLoader: true }, this.updateUser)
  }

  renderRoles=() => {
    const { roles, selectedRole } = this.state
    const options = roles.map(role => ({ value: role.id, label: role.name }))
    return (
      <CustomSelect
        value={selectedRole}
        placeholder="Seleccionar rol"
        onChange={this.handleChangeRole}
        options={options}
      />
    )
  }

  render() {
    const {
      firstName, lastName, email, showLoader,
    } = this.state
    const { setSelectedUser } = this.props
    return (
      <Card className="col-md-5 col-sm-12 right-card-margin">
        <div className="d-flex justify-content-between mb-20">
          <div>
            <div className="d-flex">
              <BackArrow
                className="text-center"
                onClick={() => { setSelectedUser(null) }}
              />
              <EditTitle>
                {firstName}
              </EditTitle>
              <EditTitle>
                {lastName}
              </EditTitle>
            </div>

          </div>

        </div>

        <div className="mb-10">
          <Label>Nombre</Label>
          <Input
            className="bold"
            value={firstName}
            onChange={e => this.handleChangeFirstName(e)}
          />
        </div>

        <div className="mb-10">
          <Label>Apellido</Label>
          <Input
            className="bold"
            value={lastName}
            onChange={e => this.handleChangeLastName(e)}
          />
        </div>

        <div className="mb-10">
          <Label>Correo</Label>
          <Input
            className="bold"
            value={email}
            onChange={e => this.handleChangeEmail(e)}
          />
        </div>


        <div className="mb-50">
          <Label className="mb-10">Rol</Label>
          {this.renderRoles()}
        </div>

        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}

        {!showLoader && (
        <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.saveUser}>
          guardar cambios
        </ActionButton>
        )}

      </Card>
    )
  }
}

EditUserCard.propTypes = {
  roles: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  setSelectedUser: PropTypes.func.isRequired,
  getUsersAndRoles: PropTypes.func.isRequired,
  handleFeedback: PropTypes.func,
}

EditUserCard.defaultProps = {
  handleFeedback: () => {},
}

export default EditUserCard
