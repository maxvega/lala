import styled from 'styled-components'

export const toolbarModules = {
  toolbar: {
    container: '#toolbar',
  },
}

export const MainQuill = styled.div`
  display: flex; 
  height: 70vh;
  
  @media (max-width: 768px) {
    flex-direction: column;
    height: unset;
    row-gap: 20px;
  }
  
`

export const QuillSectionContent = styled.div`
  display: flex;
  flex-direction: column;
  
  @media (max-width: 768px) {
    padding-right: 0px; 
    padding-left: 0px;
  }
  
`
