import React from 'react'

const Toolbar = () => (
  <div id="toolbar" style={{ backgroundColor: 'gainsboro', borderRadius: '6px 6px 0px 0px' }}>
    <select className="ql-header" defaultValue="" onChange={e => e.persist()}>
      <option value="1" />
      <option value="2" />
      <option selected />
    </select>
    <button type="button" className="ql-bold" />
    <button type="button" className="ql-italic" />
    <button type="button" className="ql-underline" />
    <button type="button" className="ql-strike" />
    <select className="ql-color">
      <option value="red" />
      <option value="green" />
      <option value="blue" />
      <option value="orange" />
      <option value="violet" />
      <option value="#d0d1d2" />
      <option selected />
    </select>
    <button className="ql-list" value="ordered" type="button">
      <svg viewBox="0 0 18 18">
        <line className="ql-stroke" x1="7" x2="15" y1="4" y2="4" />
        <line className="ql-stroke" x1="7" x2="15" y1="9" y2="9" />
        <line className="ql-stroke" x1="7" x2="15" y1="14" y2="14" />
        <line className="ql-stroke ql-thin" x1="2.5" x2="4.5" y1="5.5" y2="5.5" />
        <path
          className="ql-fill"
          d="M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z"
        />
        <path
          className="ql-stroke ql-thin"
          d="M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156"
        />
        <path
          className="ql-stroke ql-thin"
          d="M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109"
        />
      </svg>
    </button>
    <button className="ql-list" value="bullet" type="button">
      <svg viewBox="0 0 18 18">
        <line className="ql-stroke" x1="6" x2="15" y1="4" y2="4" />
        <line className="ql-stroke" x1="6" x2="15" y1="9" y2="9" />
        <line className="ql-stroke" x1="6" x2="15" y1="14" y2="14" />
        <line className="ql-stroke" x1="3" x2="3" y1="4" y2="4" />
        <line className="ql-stroke" x1="3" x2="3" y1="9" y2="9" />
        <line className="ql-stroke" x1="3" x2="3" y1="14" y2="14" />
      </svg>
    </button>
    <button className="ql-link" type="button">
      <svg viewBox="0 0 18 18">
        <line className="ql-stroke" x1="7" x2="11" y1="7" y2="11" />
        <path
          className="ql-even ql-stroke"
          d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"
        />
        <path
          className="ql-even ql-stroke"
          d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"
        />
      </svg>
    </button>
  </div>
)

export default Toolbar
