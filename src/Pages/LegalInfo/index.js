import React, { Component, Fragment } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import CircularProgress from '@material-ui/core/CircularProgress'
import PropTypes from 'prop-types'
import ClientBFF from '../../common/ClientBFF'
import Card from '../../components/Card'
import './index.css'
import PageTitle from '../../components/PageTitle'
import {
  MainQuill, QuillSectionContent, toolbarModules,
} from './styled'
import { Checkbox, ModalContainer, ModalTitle } from '../../components/CustomEditCard/styled'
import ActionButton from '../../components/ActionButton'
import { generateJSONFormData, JSONToFile } from '../../Helper'
import CustomAlert from '../../components/CustomAlert'
import Modal from '../../components/StyledModal'
import Toolbar from './Toolbar'
import InnerTitle from '../../components/InnerTitle'

const storageContainer = 'landing'
const selectedResourceName = 'json/campaignConfig.json'

class LegalInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      legalInfoText: '',
      showLoader: false,
      testedStaging: false,
      showErrorAlert: false,
      showSuccessAlert: false,
      isConfirmationModalOpen: false,
      isProductionChecked: false,
      path: '/storage/download-parsed',
    }
    this.clientBFF = new ClientBFF()
  }

  componentDidMount = async () => {
    await this.getCampaignTextLegal()
  }

  getCampaignTextLegal = async () => {
    const { path } = this.state
    this.setState({ showLoader: true }, async () => {
      const campaignConfig = await this.clientBFF.download({
        resourceName: selectedResourceName,
        storageContainer,
      }, { path })

      this.setState({ legalInfoText: campaignConfig.showCampaignTextLegal, showLoader: false })
    })
  }

  legalInfoTextFormatted = () => {
    const { legalInfoText } = this.state
    return legalInfoText
      .replace(/<\/p><p><br><\/p><p>/g, '<br><br>')
      .replace(/<\/p><p>/g, '<br>')
      .replace(/<p>/g, '')
      .replace(/<\/p>/g, '')
      .replace(/<ul>/g, '<ul style="margin-left: 16px;">')
      .replace(/<\/ul><br><br>/g, '</ul>')
  }

  uploadToStaging = async () => {
    try {
      this.setState({ showLoader: true }, async () => {
        const { path } = this.state
        const elements = await this.clientBFF.download({
          resourceName: selectedResourceName,
          storageContainer,
        }, { path, isProduction: false })

        elements.showCampaignTextLegal = this.legalInfoTextFormatted()
        const file = JSONToFile(elements, selectedResourceName)
        const formData = generateJSONFormData(file, storageContainer)
        await this.clientBFF.upload(formData, { isProduction: false })
        this.setState({ showLoader: false, testedStaging: true })
        // window.open('https://buysmart-staging.lider.cl/catalogo', '_blank')
        return true
      })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e.message)
      this.setState({ showLoader: false, testedStaging: false, showErrorAlert: true })
    }
  }

  uploadToProduction = async () => {
    try {
      this.setState({ showLoader: true }, async () => {
        const { path } = this.state

        const elements = await this.clientBFF.download({
          resourceName: selectedResourceName,
          storageContainer,
        }, { path })

        elements.showCampaignTextLegal = this.legalInfoTextFormatted()
        const file = JSONToFile(elements, selectedResourceName)
        const formData = generateJSONFormData(file, storageContainer)
        await this.clientBFF.upload(formData)

        this.setState({
          showLoader: false,
          isConfirmationModalOpen: false,
          isProductionChecked: false,
          showSuccessAlert: true,
        })
        return true
      })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e.message)
      this.setState({ showLoader: false, testedStaging: false, showErrorAlert: true })
    }
  }

  renderCircularProgress = () => (
    <div className="row">
      <CircularProgress
        className="progress-primary"
        size={26}
        mode="determinate"
        value={75}
        style={{ color: '#0071ce', margin: 'auto' }}
      />
    </div>
  )

  showConfirmationModal = () => {
    this.setState({ isConfirmationModalOpen: true })
  }

  hideConfirmationModal = () => {
    this.setState({ isConfirmationModalOpen: false })
  }

  toggleProductionCheckbox = () => {
    this.setState(({ isProductionChecked }) => ({
      isProductionChecked: !isProductionChecked,
    }))
  }

  hideCustomAlerts=() => {
    this.setState({ showErrorAlert: false, showSuccessAlert: false })
  }

  showErrorCustomAlert=() => {
    this.setState({ showErrorAlert: true }, () => {
      setTimeout(() => { this.hideCustomAlerts() }, 2000)
    })
  }

  render() {
    const { title } = this.props
    const {
      legalInfoText,
      showLoader,
      testedStaging,
      showErrorAlert,
      showSuccessAlert,
      isConfirmationModalOpen,
      isProductionChecked,
    } = this.state

    return (
      <div className="row">
        <div>
          <PageTitle>{title}</PageTitle>
        </div>
        <Card className="col-sm-12 left-card-margin">
          {showErrorAlert && (
          <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
            ¡Lo sentimos! Algo salió mal.
          </CustomAlert>
          )}
          {showSuccessAlert && (
          <CustomAlert type="success" onClick={this.hideCustomAlerts}>
            <b>¡Tus cambios han subido a producción correctamente! </b>
          </CustomAlert>
          )}
          <div style={{ display: 'flex', flexDirection: 'column', rowGap: '20px' }}>
            <div>
              <div style={{ display: 'flex' }} className="row mb-10">
                <div className="col-sm-12">
                  <InnerTitle>Actualizar información</InnerTitle>
                </div>
              </div>
              {showLoader && this.renderCircularProgress()}
              {!showLoader && (
              <MainQuill>
                <QuillSectionContent className="col-lg-12 col-md-12">
                  <Toolbar />
                  <div style={{ overflow: 'scroll', fontFamily: 'boogle' }}>
                    <ReactQuill
                      theme="snow"
                      modules={toolbarModules}
                      value={legalInfoText}
                      onChange={value => this.setState({ legalInfoText: value })}
                    />
                  </div>
                </QuillSectionContent>
              </MainQuill>
              )}

            </div>
            {!showLoader && (
            <div style={{ display: 'flex', alignSelf: 'center', columnGap: '10px' }}>
              <div>
                <ActionButton
                  className="text-center"
                  type="primary"
                  style={{
                    width: '100%', color: '#fff', margin: 'auto', marginBottom: '10px',
                  }}
                  onClick={this.uploadToStaging}
                >
                  probar en staging
                </ActionButton>
              </div>
              <div>
                <ActionButton disabled={!testedStaging} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.showConfirmationModal}>
                  pasar a producción
                </ActionButton>
              </div>
            </div>
            )}
          </div>
          <Modal
            modalIsOpen={isConfirmationModalOpen}
            closeModalAction={this.hideConfirmationModal}
            style={{
              content: {
                width: '470px',
              },
            }}
          >
            <ModalContainer>
              <ModalTitle>¿Listo para pasar a producción?</ModalTitle>
              <div className="d-flex">
                <Checkbox
                  checked={isProductionChecked}
                  onClick={this.toggleProductionCheckbox}
                >
                  {isProductionChecked && <i className="zmdi zmdi-check" />}
                </Checkbox>

                <span>Confirmo que he revisado los cambios realizados en Staging y estoy seguro que no hay errores.</span>
              </div>
              <div className="d-flex flex-column">
                {showLoader && (
                <div className="row">
                  <CircularProgress
                    className="progress-primary"
                    size={26}
                    mode="determinate"
                    value={75}
                    style={{ color: '#0071ce', margin: 'auto', marginTop: '28px' }}
                  />
                </div>
                )}
                {!showLoader && (
                <Fragment>
                  <ActionButton
                    className="text-center"
                    type="primary"
                    style={{
                      width: '100%', color: '#fff', margin: 'auto', marginBottom: '10px', marginTop: '28px',
                    }}
                    disabled={!isProductionChecked}
                    onClick={this.uploadToProduction}
                  >
                    pasar a producción
                  </ActionButton>
                  <ActionButton
                    className="text-center"
                    type="secondary"
                    style={{ width: '100%', color: '#0071c3', margin: 'auto' }}
                    onClick={this.hideConfirmationModal}
                  >
                    cancelar
                  </ActionButton>
                </Fragment>
                )}
              </div>
            </ModalContainer>
          </Modal>
        </Card>

      </div>
    )
  }
}

LegalInfo.propTypes = {
  title: PropTypes.string.isRequired,
}

export default LegalInfo
