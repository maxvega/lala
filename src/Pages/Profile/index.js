import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Card from '../../components/Card'

class Profile extends Component {
  render() {
    const { userInfo } = this.props
    return (
      <div className="row">
        <Card className="col-sm-12">
          <div>
            name:
            {userInfo.name}
          </div>
          <div>
            username:
            {userInfo.username}
          </div>
          <div>
            email:
            {userInfo.email}
          </div>
          <div>
            role:
            {userInfo.role && userInfo.role.name}
          </div>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.loginReducer,
})

export default connect(mapStateToProps, null)(Profile)

Profile.propTypes = {
  userInfo: PropTypes.object.isRequired,
}
