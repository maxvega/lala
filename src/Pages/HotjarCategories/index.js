import React, { Component, Fragment } from 'react'
import Select from 'react-select'
import ActionButton from 'Src/components/ActionButton'
import { JSONToFile } from 'Src/Helper'
import CustomAlert from 'Src/components/CustomAlert'
import CircularProgress from '@material-ui/core/CircularProgress'
import PageTitle from '../../components/PageTitle'
import ClientBFF from '../../common/ClientBFF'
import Card from '../../components/Card'
import InnerTitle from '../../components/InnerTitle'

const storageContainer = 'landing'
const selectedResourceName = 'json/campaignConfig.json'
const categoriesResourceName = 'json/menu_categories.json'

class HotjarCategories extends Component {
  constructor(props) {
    super(props)
    this.state = {
      preselectedCategories: [],
      categories: [],
      selectedCategories: [],
      showErrorAlert: false,
      showSuccessAlert: false,
      showLoader: true,
      path: '/storage/download-parsed',
    }
    this.clientBFF = new ClientBFF()
  }

    componentDidMount = async () => {
      await this.downloadFiles()
    }

    downloadFiles = async () => {
      const { path } = this.state
      const campaignConfig = await this.clientBFF.download({
        resourceName:
        selectedResourceName,
        storageContainer,
      }, { path })

      const categories = await this.clientBFF.download({
        resourceName: categoriesResourceName,
        storageContainer,
      }, { path })

      this.setState({
        preselectedCategories: campaignConfig.hotjarCategories.map(category => ({
          value: category,
          label: category,
        })),
        selectedCategories: campaignConfig.hotjarCategories.map(category => ({
          value: category,
          label: category,
        })),
        categories: categories.map(category => ({
          value: category.label,
          label: category.label,
        })),
        showLoader: false,
      })
    }

  hideCustomAlerts=() => {
    this.setState({ showErrorAlert: false, showSuccessAlert: false })
  }

  showErrorCustomAlert=() => {
    this.setState({ showErrorAlert: true }, () => {
      setTimeout(() => {
        this.hideCustomAlerts()
      }, 2000)
    })
  }

  showSuccessCustomAlert=() => {
    this.setState({ showSuccessAlert: true }, () => {
      setTimeout(() => {
        this.hideCustomAlerts()
      }, 2000)
    })
  }


  updateSelection = async () => {
    this.setState({ showLoader: true }, async () => {
      try {
        const { selectedCategories, path } = this.state

        const campaignConfig = await this.clientBFF.download({
          resourceName: selectedResourceName,
          storageContainer,
        }, { path })

        campaignConfig.hotjarCategories = selectedCategories.map(category => category.label)

        const file = JSONToFile(campaignConfig, 'campaignConfig.json')
        const formData = new FormData()
        formData.append('file', file)
        formData.append('storageContainer', 'landing/json')
        formData.append('keepOriginalName', 'true')
        await this.clientBFF.upload(formData)
        await this.downloadFiles()
        this.showSuccessCustomAlert()
      } catch (e) {
        this.showErrorCustomAlert()
      }
      this.setState({ showLoader: false })
    })
  }

  render() {
    const {
      preselectedCategories, categories, showSuccessAlert, showErrorAlert, showLoader,
    } = this.state
    return (
      <Fragment>
        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}
        {!showLoader && (
        <Fragment>
          <PageTitle>Categorías Hotjar</PageTitle>
          <Card>
            {showErrorAlert && (
            <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
              ¡Lo sentimos! Algo salió mal.
            </CustomAlert>
            )}
            {showSuccessAlert && (
            <CustomAlert type="success" onClick={this.hideCustomAlerts}>
              <b>¡Tus cambios han subido a producción correctamente! </b>
            </CustomAlert>
            )}
            <InnerTitle>Categorías seleccionadas</InnerTitle>
            {categories.length > 0 && (
            <Select
              defaultValue={preselectedCategories}
              isMulti
              name="categories"
              options={categories}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={(selection) => {
                this.setState({ selectedCategories: selection })
              }}
            />
            )}
            <ActionButton style={{ marginTop: '12px' }} type="primary" onClick={this.updateSelection}>Actualizar selección</ActionButton>
          </Card>
        </Fragment>

        )}
      </Fragment>
    )
  }
}

HotjarCategories.propTypes = {}

export default HotjarCategories
