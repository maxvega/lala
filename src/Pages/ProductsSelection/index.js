import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import XLSX from 'xlsx'
import * as FileSaver from 'file-saver'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '../../components/Card'
import ActionButton from '../../components/ActionButton'
import ClientBFF from '../../common/ClientBFF'
import FileInput from '../../components/FileInput'
import withFeedbackModal from '../../components/WithFeedbackModal'
import { JSONToFile, groupArrayByKey } from '../../Helper'

class ProductsSelection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      generateXLSXLoader: false,
      file: null,
      uploadToStagingLoader: false,
      uploadedToStaging: false,
      disableUploadToProductionButton: true,
      uploadToProductionLoader: false,
      homeConfigData: {},
      path: '/storage/download-parsed',
    }
    this.clientBFF = new ClientBFF()
  }

  componentDidMount = async () => {
    await this.getProductsSelections()
  }

  handleChange = (e) => {
    e.preventDefault()
    const { files } = e.target
    if (files && files[0]) {
      this.setState({ file: files[0] })
    }
  }

  handleUploadToProduction = async () => {
    const { generatedJSON } = this.state
    if (generatedJSON) {
      this.setState({
        uploadToProductionLoader: true,
      }, async () => {
        const file = JSONToFile(generatedJSON, 'homePageConfig.json')
        const formData = new FormData()
        formData.append('file', file)
        formData.append('storageContainer', 'landing/json')
        formData.append('keepOriginalName', 'true')
        await this.clientBFF.upload(formData)
        this.setState({ uploadToProductionLoader: false })
      })
    }
  }

  getProductsSelections = async () => {
    const { path } = this.state
    const productsSelection = await this.clientBFF.download({
      storageContainer: 'landing/json',
      resourceName: 'homePageConfig.json',
    }, { path })
    this.setState({ homeConfigData: productsSelection })
    return productsSelection
  }

  getProductsSelectionSize = selection => selection.reduce((prev, current) => ((prev.products && current.products && prev.products.length > current.products.length) ? prev.products : current.products)).length

  generateXLSXfromJSON = async () => {
    const { homeConfigData } = this.state
    const { productSelections } = homeConfigData
    const LIMIT = this.getProductsSelectionSize(productSelections)
    const data = []
    const xlsxColumnTitles = productSelections.map(selection => selection.tab)
    data.push(xlsxColumnTitles)
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i <= LIMIT; i++) {
      const xlsColumnContent = []
      productSelections.forEach((selection) => {
        if (selection.products && selection.products[i]) {
          xlsColumnContent.push(selection.products[i])
        } else {
          xlsColumnContent.push('')
        }
      })
      data.push(xlsColumnContent)
    }
    const wb = XLSX.utils.book_new()
    const ws = XLSX.utils.aoa_to_sheet(data)

    wb.SheetNames.push('productos')
    wb.Sheets.productos = ws

    return XLSX.write(wb, { type: 'array', bookType: 'xlsx', bookSST: false })
  }

  generateJSONfromXLSX = (data) => {
    const { homeConfigData } = this.state
    const { productSelections } = homeConfigData
    if (data && data.length > 0) {
      const keysFromXLSXFile = Object.keys(data[0])
      const flattenDataFromXLSX = keysFromXLSXFile.map(key => groupArrayByKey(data, key))
      productSelections.forEach((productSelection) => {
        productSelection.products = flattenDataFromXLSX.find(element => element.tab === productSelection.tab).products
      })
      return homeConfigData
    }
    return null
  }

  handleUploadToStaging = () => {
    const { file } = this.state
    const reader = new FileReader()
    reader.onload = async (e) => {
      const wb = XLSX.read(e.target.result, { type: 'binary', bookVBA: true })
      const ws = wb.Sheets[wb.SheetNames[0]]
      const data = XLSX.utils.sheet_to_json(ws)
      try {
        this.setState({ uploadToStagingLoader: true }, async () => {
          const generatedJSON = await this.generateJSONfromXLSX(data)
          if (generatedJSON) {
            const file = JSONToFile(generatedJSON, 'homePageConfig.json')
            const formData = new FormData()
            formData.append('file', file)
            formData.append('storageContainer', 'landing/json')
            formData.append('keepOriginalName', 'true')

            await this.clientBFF.upload(formData, { isProduction: false })
            this.setState({ uploadToStagingLoader: false, uploadedToStaging: true, generatedJSON })
            // window.open('https://buysmart-staging.lider.cl/catalogo', '_blank')
          } else {
            this.setState({ uploadToStagingLoader: false })
            this.handleError('Algo salió mal 😔, revisa el formato del archivo')
          }
        })
      } catch (e) {
        this.setState({ uploadToStagingLoader: false })
        this.handleError('Algo salió mal 😔, revisa el formato del archivo')
        // eslint-disable-next-line no-console
        console.log(e.message)
      }
    }
    reader.readAsBinaryString(file)
  }

  handleGenerateXLSXClick = async () => {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
    const fileExtension = '.xlsx'
    const { homeConfigData } = this.state
    try {
      this.setState({ generateXLSXLoader: true }, async () => {
        if (homeConfigData && homeConfigData.productSelections) {
          const newXLSX = await this.generateXLSXfromJSON(homeConfigData)
          const data = new Blob([newXLSX], { type: fileType })
          FileSaver.saveAs(data, `product-selection${fileExtension}`)
          this.setState({ generateXLSXLoader: false })
        } else {
          this.handleError('Error al descargar el archivo 😔')
          this.setState({ generateXLSXLoader: false })
        }
      })
    } catch (e) {
      this.handleError('Error al descargar el archivo 😔')
      this.setState({ generateXLSXLoader: false })
    }
  }

  handleError = (message) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: message,
    })
  }

  render() {
    const {
      generateXLSXLoader, file, uploadToStagingLoader, uploadedToStaging, disableUploadToProductionButton, uploadToProductionLoader,
    } = this.state
    return (
      <div className="row">
        <Card className="col-sm-12 left-card-margin">
          <Fragment>
            <div className="row mb-10">
              <div className="col-sm-12">
                <h3 style={{ lineHeight: '40px' }}>Administrador de Selección de Productos</h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 mb-20" style={{ borderRight: '1px solid black' }}>
                <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleGenerateXLSXClick}>
                  {generateXLSXLoader ? (
                    <CircularProgress
                      className="progress-primary"
                      size={26}
                      mode="determinate"
                      value={75}
                      style={{ color: '#fff', margin: 'auto' }}
                    />
                  ) : 'Descargar configuración de selección de productos'}
                </ActionButton>
              </div>
              <div className="col-md-6 mb-20">
                <div className="d-flex text-center mb-20">
                  <FileInput type="file" title="Subir archivo xlsx de selección de productos" accept=".xlsx" inputHandler={this.handleChange} />
                </div>
                {file && (
                  <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleUploadToStaging}>
                    {uploadToStagingLoader ? (
                      <CircularProgress
                        className="progress-primary"
                        size={26}
                        mode="determinate"
                        value={75}
                        style={{ color: '#fff', margin: 'auto' }}
                      />
                    ) : 'Subir categorías a staging'}
                  </ActionButton>
                )}
              </div>
            </div>
            {uploadedToStaging && (
              <div className="text-center">
                <div className="mb-20">
                  <input type="checkbox" onChange={() => this.setState({ disableUploadToProductionButton: !disableUploadToProductionButton })} />
                  {' He revisado en el sitio de pruebas el funcionamiento de la seccion de productos que subira a producción.'}
                </div>
                <ActionButton disabled={disableUploadToProductionButton} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleUploadToProduction}>
                  {uploadToProductionLoader ? (
                    <CircularProgress
                      className="progress-primary"
                      size={26}
                      mode="determinate"
                      value={75}
                      style={{ color: '#fff', margin: 'auto' }}
                    />
                  ) : 'Subir categorías a producción'}
                </ActionButton>
              </div>
            )}
          </Fragment>
        </Card>
      </div>
    )
  }
}

ProductsSelection.propTypes = {
  handleFeedback: PropTypes.func.isRequired,
}

export default withFeedbackModal(ProductsSelection)
