import React, { Fragment, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'

import PageTitle from '../../components/PageTitle'
import ClientBFF from '../../common/ClientBFF'
import Card from '../../components/Card'
import Label from '../../components/Label'
import Input from '../../components/Input'
import Toggle from '../../components/Toggle'
import ActionButton from '../../components/ActionButton'
import { JSONToFile, generateJSONFormData } from '../../Helper'

const storageContainer = 'landing/json'
const resourceName = 'campaignConfig.json'
const initialState = {
  loading: true,
  loadingStaging: false,
  loadingProduction: false,
  uploadedStaging: false,
  uploadedProduction: false,
  uploadProductionConfirmation: false,
  file: {},
  path: '/storage/download-parsed',
}

const HeaderAlertMessage = (props) => {
  const { title } = props
  const [state, setState] = useState(initialState)

  const bff = new ClientBFF(window.__ENV__)

  useEffect(() => {
    const fetchData = async () => {
      const { path } = state

      const data = await bff.download({
        storageContainer,
        resourceName,
      }, { path })
      setState(state => ({ ...state, loading: false, file: data }))
    }
    fetchData()
  }, [])

  const handleToggleEventChange = () => {
    const newState = { ...state, uploadedStaging: false, uploadProductionConfirmation: false }
    newState.file.headerAlertMessageConfig.enabled = !state.file.headerAlertMessageConfig.enabled
    setState(newState)
  }

  const handleChangeEventOnInput = (event) => {
    const newState = { ...state, uploadedStaging: false, uploadProductionConfirmation: false }
    if (event.target) {
      newState.file.headerAlertMessageConfig.content = event.target.value
    } else {
      newState.file.headerAlertMessageConfig.content = event.value
    }
    setState(newState)
  }

  const handleActionButtonUpdateStaging = async () => {
    setState(state => ({ ...state, loadingStaging: true }))
    const { path } = state

    bff.download({
      storageContainer,
      resourceName,
    }, { path })
      .then(async () => {
        const file = JSONToFile(state.file, resourceName)
        const formData = generateJSONFormData(file, storageContainer)
        await bff.upload(formData, { isProduction: false })
        setState(state => ({ ...state, loadingStaging: false, uploadedStaging: true }))
        window.open('https://buysmart-staging.lider.cl/catalogo', '_blank')
      })
  }

  const handleActionButtonUploadProduction = async () => {
    setState(state => ({ ...state, loadingProduction: true }))
    const { path } = state

    bff.download({
      storageContainer,
      resourceName,
    }, { path })
      .then(async () => {
        const file = JSONToFile(state.file, resourceName)
        const formData = generateJSONFormData(file, storageContainer)
        await bff.upload(formData)
        setState(state => ({ ...state, loadingProduction: false, uploadedProduction: true }))
        window.open('https://www.lider.cl/catalogo', '_blank')
      })
  }

  const getToggleLabelMessage = () => (state.file.headerAlertMessageConfig.enabled ? 'Desactivar Huincha de Contingencia' : 'Activar Huincha de Contingencia')

  const renderStagingActionButtonLabel = () => {
    if (state.loadingStaging) {
      return (
        <CircularProgress
          className="progress-primary"
          size={26}
          mode="determinate"
          value={75}
          style={{ color: '#fff', margin: 'auto' }}
        />
      )
    }
    return 'probar huincha en staging'
  }

  const renderConfirmationProductionComponents = () => {
    if (state.uploadedStaging) {
      return (
        <Fragment>
          <div className="text-center">
            <div className="mb-20">
              <input type="checkbox" id="confirm" name="confirm" onChange={() => setState(state => ({ ...state, uploadProductionConfirmation: !state.uploadProductionConfirmation }))} />
              {' Confirmo que he revisado los cambios realizados en Staging y estoy seguro que no hay errores.' }
              <br />
              <span style={{ 'font-size': '80%' }}>
                Importante: recuerda que para ver los cambios en producción, la huincha debe estar activada.
              </span>
            </div>
          </div>
          <div className="text-center">
            <ActionButton disabled={!state.uploadProductionConfirmation} className="text-center action-production" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={handleActionButtonUploadProduction}>
              {state.loadingProduction ? (
                <CircularProgress
                  className="progress-primary"
                  size={26}
                  mode="determinate"
                  value={75}
                  style={{ color: '#fff', margin: 'auto' }}
                />
              ) : 'pasar a producción'}
            </ActionButton>
          </div>
        </Fragment>
      )
    }
    return null
  }

  return (
    <Fragment>
      <PageTitle>{title}</PageTitle>
      {state.loading && (
        <div className="row">
          <CircularProgress
            className="progress-primary"
            size={26}
            mode="determinate"
            value={75}
            style={{ color: '#0071ce', margin: 'auto' }}
          />
        </div>
      )}
      {!state.loading && (
        <Fragment>
          <Card>
            <div className="mb-10">
              <Label>{getToggleLabelMessage()}</Label>
              <Toggle
                id="enabled"
                area="enabled"
                isActive={state.file.headerAlertMessageConfig.enabled}
                clickHandler={handleToggleEventChange}
                value={state.file.headerAlertMessageConfig.enabled}
              />
            </div>
            <div className="mb-10">
              <Label>Mensaje</Label>
              <Input
                id="content"
                name="content"
                className="bold"
                value={state.file.headerAlertMessageConfig.content}
                onChange={handleChangeEventOnInput}
              />
            </div>
            <div className="text-center">
              <div className="mb-20">
                <ActionButton className="action-staging" style={{ marginTop: '12px' }} type="primary" onClick={handleActionButtonUpdateStaging}>
                  {renderStagingActionButtonLabel()}
                </ActionButton>
              </div>
            </div>
            {renderConfirmationProductionComponents()}
          </Card>
        </Fragment>
      )}
    </Fragment>
  )
}

HeaderAlertMessage.propTypes = {
  title: PropTypes.string.isRequired,
}

export default HeaderAlertMessage
