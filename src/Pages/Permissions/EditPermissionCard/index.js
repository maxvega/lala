import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Scrollbars } from 'react-custom-scrollbars'
import CircularProgress from '@material-ui/core/CircularProgress'
import { AutoSizer } from 'react-virtualized'
import BackArrow from '../../../components/BackArrow'
import Card from '../../../components/Card'
import Input from '../../../components/Input'
import EditTitle from '../../../components/EditTitle'
import ClientAuth from '../../../common/ClientAuth'
import { getLocalToken } from '../../../Helper'
import ActionButton from '../../../components/ActionButton'
import Element from '../../../components/Card/Element'
import Label from '../../../components/Label'
import RoundIconWrapper from '../../../components/RoundIconWrapper'

class EditPermissionCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      permission: props.permission,
      permissionName: props.permission.name,
      permissionDescription: props.permission.description,
      roles: this.getPermissionsInRoles(props.permission, props.roles),
      showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }


  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.permission) !== JSON.stringify(nextProps.permission)) {
      this.setState({
        permission: nextProps.permission,
        permissionName: nextProps.permission.name,
        permissionDescription: nextProps.permission.description,
        roles: this.getPermissionsInRoles(nextProps.permission, nextProps.roles),
      })
    }
  }

  handleChangeName = (e) => {
    this.setState({ permissionName: e.target.value })
  }

  handleChangeDescription = (e) => {
    this.setState({ permissionDescription: e.target.value })
  }

  getPermissionsInRoles = (permission, roles) => {
    const newRoles = roles.map((role) => {
      const isPermissionInRole = role.permissions.find(permissionInRole => permissionInRole.id === permission.id)
      role.applied = !!isPermissionInRole
      return role
    })
    return newRoles
  }

  toggleRole=(roleId) => {
    const { roles } = this.state

    const newRoles = JSON.parse(JSON.stringify(roles))
    newRoles.forEach((role) => {
      if (role.id === roleId) {
        role.applied = !role.applied
      }
    })
    this.setState({ roles: newRoles })
  }

  updatePermission = () => {
    const {
      permission, permissionDescription, permissionName, roles,
    } = this.state
    const { getPermissionsAndRoles, handleFeedback, setSelectedPermission } = this.props
    const token = getLocalToken()
    const newPermission = Object.assign({}, permission)
    newPermission.name = permissionName
    newPermission.description = permissionDescription
    newPermission.roles = roles.filter(role => role.applied)
    newPermission.roles.forEach(role => delete role.applied)
    return this.clientAuth.updatePermission(token, newPermission).then(() => {
      handleFeedback({
        title: 'Permiso editado correctamente',
      })
    }).catch((err) => {
      handleFeedback({
        title: 'Error al actualizar',
        message: `Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: ${err?.response?.data?.message}`,
      })
    }).finally(() => {
      getPermissionsAndRoles()
      this.setState({ showLoader: false })
      setSelectedPermission(null)
    })
  }

  savePermission = async () => {
    this.setState({ showLoader: true }, this.updatePermission)
  }

  renderRolePermissions = () => {
    const { roles } = this.state
    return (
      <AutoSizer disableWidth>
        {({ height }) => (
          <Scrollbars
            className="rct-scroll"
            autoHeight
            autoHeightMax={height}
            autoHide
          >
            {roles.map(role => (
              <Element key={`role-${role.name}`} className="d-flex justify-content-between pointer" onClick={() => { this.toggleRole(role.id) }}>
                <div>
                  <div className="bold">{role.name}</div>
                  <div>{role.description}</div>
                </div>
                <div>
                  {role.applied ? (
                    <RoundIconWrapper>
                      <i className="zmdi zmdi-check" />
                    </RoundIconWrapper>
                  ) : (
                    <div
                      className="pointer"
                      style={{
                        width: '27px', height: '27px', border: '1px solid gray', borderRadius: '50%',
                      }}
                    />
                  )}
                </div>
              </Element>
            ))}
          </Scrollbars>
        )}
      </AutoSizer>
    )
  }

  render() {
    const { permissionName, permissionDescription, showLoader } = this.state
    const { setSelectedPermission } = this.props

    return (
      <Card className="col-md-5 col-sm-12 right-card-margin">
        <div className="d-flex justify-content-between mb-20">
          <div>
            <div className="d-flex">
              <BackArrow
                className="text-center"
                onClick={() => { setSelectedPermission(null) }}
              />
              <EditTitle>
                {permissionName}
              </EditTitle>
            </div>
          </div>
        </div>

        <div className="mb-10">
          <Label>Nombre</Label>
          <Input
            className="bold"
            value={permissionName}
            onChange={e => this.handleChangeName(e)}
          />
        </div>

        <div className="mb-10">
          <Label>Descripción</Label>
          <Input
            className="bold"
            value={permissionDescription}
            onChange={e => this.handleChangeDescription(e)}
          />
        </div>

        <div className="mb-20" style={{ height: 'calc(100vh - 450px)' }}>
          <Label>Roles que tienen este permiso</Label>
          {this.renderRolePermissions()}
        </div>

        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}

        {!showLoader && (
        <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.savePermission}>
          guardar cambios
        </ActionButton>
        )}
      </Card>
    )
  }
}

EditPermissionCard.propTypes = {
  roles: PropTypes.array.isRequired,
  permission: PropTypes.object.isRequired,
  setSelectedPermission: PropTypes.func.isRequired,
  getPermissionsAndRoles: PropTypes.func.isRequired,
  handleFeedback: PropTypes.func,
}

EditPermissionCard.defaultProps = {
  handleFeedback: () => {},
}

export default EditPermissionCard
