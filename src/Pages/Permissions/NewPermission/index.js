import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Scrollbars } from 'react-custom-scrollbars'
import { AutoSizer } from 'react-virtualized'
import ActionButton from '../../../components/ActionButton'
import Input from '../../../components/Input'
import Label from '../../../components/Label'
import SideModal from '../../../components/SideModal'
import { getLocalToken } from '../../../Helper'
import ClientAuth from '../../../common/ClientAuth'
import Element from '../../../components/Card/Element'
import RoundIconWrapper from '../../../components/RoundIconWrapper'

class NewPermission extends Component {
  constructor(props) {
    super(props)
    this.state = {
      permissionName: '',
      permissionDescription: '',
      roles: props.roles,
      showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }

  toggleRole=(roleId) => {
    const { roles } = this.state

    const newRoles = JSON.parse(JSON.stringify(roles))
    newRoles.forEach((role) => {
      if (role.id === roleId) {
        role.applied = !role.applied
      }
    })
    this.setState({ roles: newRoles })
  }

  handleChangeName = (e) => {
    this.setState({ permissionName: e.target.value })
  }

  handleChangeDescription = (e) => {
    this.setState({ permissionDescription: e.target.value })
  }

  renderRolePermissions = () => {
    const { roles } = this.state
    return (
      <AutoSizer disableWidth>
        {({ height }) => (
          <Scrollbars
            className="rct-scroll"
            autoHeight
            autoHeightMax={height}
            autoHide
          >
            {roles.map(role => (
              <Element key={`role-${role.name}`} className="d-flex justify-content-between pointer" onClick={() => { this.toggleRole(role.id) }}>
                <div>
                  <div className="bold">{role.name}</div>
                  <div>{role.description}</div>
                </div>
                <div>
                  {role.applied ? (
                    <RoundIconWrapper>
                      <i className="zmdi zmdi-check" />
                    </RoundIconWrapper>
                  ) : (
                    <div
                      className="pointer"
                      style={{
                        width: '27px', height: '27px', border: '1px solid gray', borderRadius: '50%',
                      }}
                    />
                  )}
                </div>
              </Element>
            ))}
          </Scrollbars>
        )}
      </AutoSizer>
    )
  }

  createPermission = () => {
    const {
      permissionDescription, permissionName, roles,
    } = this.state
    const { getPermissionsAndRoles, handleFeedback, closeSideNav } = this.props
    const newPermission = {}
    const token = getLocalToken()
    newPermission.name = permissionName
    newPermission.description = permissionDescription
    newPermission.roles = roles.filter(role => role.applied)
    newPermission.roles.forEach(role => delete role.applied)
    return this.clientAuth.createPermission(token, newPermission).then(() => {
      handleFeedback({
        title: 'Permiso creado correctamente',
      })
    }).catch((err) => {
      handleFeedback({
        title: 'Error al crear',
        message: `Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: ${err?.response?.data?.message}`,
      })
    }).finally(() => {
      getPermissionsAndRoles()
      this.setState({ showLoader: false })
      closeSideNav()
    })
  }

  savePermission = async () => {
    this.setState({ showLoader: true }, this.createPermission)
  }

  render() {
    const { permissionName, permissionDescription, showLoader } = this.state
    const { closeSideNav } = this.props
    return (
      <SideModal>
        <div
          role="button"
          style={{
            position: 'absolute', top: 10, right: 20, cursor: 'pointer',
          }}
          onClick={closeSideNav}
          tabIndex="-1"
        >
          <i style={{ marginLeft: ' 10px', fontSize: '30px' }} className="zmdi zmdi-close" />
        </div>
        <div>
          <div className="d-flex">
            <div className="d-flex justify-content-between mb-10" style={{ marginTop: '20px' }}>
              <div>
                <span style={{ fontSize: '24px' }}>Nuevo Permiso</span>
              </div>
            </div>

            <div
              className="bold"
              style={{
                margin: 0, marginLeft: '5px', fontSize: '20px', border: 0, padding: 0,
              }}
            >
              {permissionName}
            </div>
          </div>

        </div>

        <div className="mb-10">
          <Label>Nombre</Label>
          <Input
            className="bold"
            value={permissionName}
            onChange={e => this.handleChangeName(e)}
          />
        </div>

        <div className="mb-10">
          <Label>Descripción</Label>
          <Input
            className="bold"
            value={permissionDescription}
            onChange={e => this.handleChangeDescription(e)}
          />
        </div>

        <div className="mb-20" style={{ height: 'calc(100% - 340px)' }}>
          <Label>Roles que tienen este permiso</Label>
          {this.renderRolePermissions()}
        </div>

        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}

        {!showLoader && (
        <ActionButton id="savePermissionButton" className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.savePermission}>
          guardar cambios
        </ActionButton>
        )}
      </SideModal>
    )
  }
}

NewPermission.propTypes = {
  getPermissionsAndRoles: PropTypes.func,
  closeSideNav: PropTypes.func.isRequired,
  roles: PropTypes.array.isRequired,
  handleFeedback: PropTypes.func,
}

NewPermission.defaultProps = {
  handleFeedback: () => {},
  getPermissionsAndRoles: () => {},
}

export default NewPermission
