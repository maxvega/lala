import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Scrollbars } from 'react-custom-scrollbars'
import { AutoSizer } from 'react-virtualized'
import InnerTitle from '../../components/InnerTitle'
import ClientAuth from '../../common/ClientAuth'
import Card from '../../components/Card'
import Element from '../../components/Card/Element'
import ActionButton from '../../components/ActionButton'
import PageTitle from '../../components/PageTitle'
import RoundIconWrapper from '../../components/RoundIconWrapper'
import { getLocalToken } from '../../Helper'
import EditPermissionCard from './EditPermissionCard'
import NewPermission from './NewPermission'
import withFeedbackModal from '../../components/WithFeedbackModal'

export class Permissions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      permissions: [],
      roles: [],
      selectedPermission: null,
      showNewPermission: false,
      showLoader: false,
    }
    this.clientAuth = new ClientAuth()
  }

  componentDidMount = () => {
    this.getPermissionsAndRoles()
  }

  getPermissionsAndRoles = async () => {
    this.setState({ showLoader: true }, async () => {
      const token = getLocalToken()
      const permissions = await this.clientAuth.getPermissions(token || 'bananas')
      const roles = await this.clientAuth.getRoles(token || 'bananas')
      this.setState({ permissions, roles, showLoader: false })
    })
  }

  setSelectedPermission = (permission) => {
    this.setState({ selectedPermission: permission })
  }

  showNewPermission = () => {
    this.setState({ showNewPermission: true })
  }

  closeSideNav =() => {
    this.setState({ showNewPermission: false })
  }

  onClickRemove = (permission) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: `¿Estás seguro que quieres borrar el permiso '${permission.name}' ?`,
      confirmationHandler: this.handleDeletePermission,
      confirmationData: [permission],
    })
  }

  renderPermissions = permissions => (
    <AutoSizer disableWidth>
      {({ height }) => (
        <Scrollbars
          className="rct-scroll"
          autoHeight
          autoHeightMax={height}
          autoHide
        >
          {permissions.map(permission => (
            <Element key={`permission-${permission.name}`} className="d-flex justify-content-between">
              <div>{permission.name}</div>
              <div className="d-flex">
                <RoundIconWrapper tooltipText="Editar Permiso" id={`edit-${permission.id}`} style={{ marginRight: '5px' }} onClick={() => { this.setSelectedPermission(permission) }}>
                  <i className="zmdi zmdi-edit" />
                </RoundIconWrapper>

                <RoundIconWrapper tooltipText="Borrar Permiso" id={`delete-${permission.id}`} backgroundColor="#eaeaea" color="#000000" onClick={() => { this.onClickRemove(permission) }}>
                  <i className="zmdi zmdi-delete" />
                </RoundIconWrapper>

              </div>
            </Element>
          ))}
        </Scrollbars>
      )}
    </AutoSizer>
  )

  handleDeletePermission = async (permission) => {
    const token = getLocalToken()
    await this.clientAuth.deletePermission(token, permission)
    this.getPermissionsAndRoles()
  }

  render() {
    const {
      permissions, selectedPermission, showNewPermission, roles, showLoader,
    } = this.state

    const { handleFeedback, title } = this.props
    return (
      <Fragment>
        <PageTitle>{title}</PageTitle>
        <div className="row mb-20">
          <Card className={`${selectedPermission ? 'not-display-in-mobile col-md-7 ' : ''}col-sm-12 left-card-margin`}>

            <div className="row mb-10">
              <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
                <InnerTitle>Permisos creados</InnerTitle>
              </div>
              <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
                <div className="create-button-container">
                  <ActionButton type="info" onClick={this.showNewPermission} className="create-new-button">
                    <div className="d-flex">
                      <div>crear nuevo</div>
                      <div><i style={{ marginLeft: '5px', fontWeight: 'bolder' }} className="zmdi zmdi-plus" /></div>
                    </div>
                  </ActionButton>
                </div>
              </div>
            </div>
            {showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto' }}
              />
            </div>
            )}
            <div style={{ height: 'calc(100vh - 250px)' }}>
              {!showLoader && this.renderPermissions(permissions)}
            </div>
          </Card>
          {selectedPermission && selectedPermission.name && (
          <EditPermissionCard permission={selectedPermission} roles={roles} setSelectedPermission={this.setSelectedPermission} getPermissionsAndRoles={this.getPermissionsAndRoles} handleFeedback={handleFeedback} />
          )}
          <div
            className={showNewPermission ? 'overlay-right-modal' : ''}
            onClick={() => { this.closeSideNav(false) }}
          />
          {showNewPermission && <NewPermission closeSideNav={this.closeSideNav} roles={roles} getPermissionsAndRoles={this.getPermissionsAndRoles} handleFeedback={handleFeedback} />}
        </div>
      </Fragment>
    )
  }
}

export default withFeedbackModal(Permissions)

Permissions.propTypes = {
  handleFeedback: PropTypes.func,
  title: PropTypes.string.isRequired,
}

Permissions.defaultProps = {
  handleFeedback: () => {},
}
