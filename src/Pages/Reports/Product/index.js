import React, { Component } from 'react'
import * as FileSaver from 'file-saver'
import CircularProgress from '@material-ui/core/CircularProgress'
import ActionButton from '../../../components/ActionButton'
import Card from '../../../components/Card'
import ClientBFF from '../../../common/ClientBFF'

const configData = window.__ENV__

class Product extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showError: false,
      showLoader: false,
    }

    this.clientBFF = new ClientBFF(configData)
  }

  handleButtonClick=async () => {
    try {
      this.setState({ showLoader: true, showError: false }, async () => {
        const fileType = 'text/csv'
        const fileExtension = '.tsv'
        const report = await this.clientBFF.download({
          storageContainer: 'reports',
          resourceName: 'products-google.tsv',
        })
        if (report) {
          const data = new Blob([report.message], { type: fileType })
          FileSaver.saveAs(data, `products-google${fileExtension}`)
        }
        this.setState({ showLoader: false, showError: false })
      })
    } catch (e) {
      this.setState({ showLoader: false, showError: true })
    }
  }

  render() {
    const { showLoader, showError } = this.state

    return (
      <div>
        <Card>
          Descargar reporte de productos
          <ActionButton type="primary" className="text-center pt-10" onClick={this.handleButtonClick} style={{ margin: '20px auto', color: '#fff', width: '250px' }}>
            {showLoader ? (
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#fff', margin: 'auto' }}
              />
            ) : 'descargar reporte' }
          </ActionButton>
          {showError && <div>Algo salió mal :c</div>}
        </Card>
      </div>
    )
  }
}

export default Product
