import styled from 'styled-components'

const buttonBackgroundColors = {
  primary: '#0071ce',
  warning: '#ffc107',
  danger: '#dc3545',
  info: '#fff',
}

const buttonBorderColors = {
  primary: '#0071ce',
  warning: '#ffc107',
  danger: '#dc3545',
  info: '#0071ce',
}

const buttonTextColors = {
  primary: '#fff',
  warning: '#ffc107',
  danger: '#dc3545',
  info: '#0071ce',
}

const ActionButton = styled.button`
    border: solid 0.5px #979797;
    padding: 10px 15px;
    background-color: ${props => (props.type && buttonBackgroundColors[props.type] ? buttonBackgroundColors[props.type] : '#f7f7f7')};
    border: solid 1.5px ${props => (props.type && buttonBorderColors[props.type] ? buttonBorderColors[props.type] : '#efeded')};
    color: ${props => (props.type && buttonTextColors[props.type] ? buttonTextColors[props.type] : '#000')};
    pointer-events: ${props => (props.disabled ? 'none' : 'unset')};
    opacity: ${props => (props.disabled ? '0.5' : '1')};
    font-size: 16px;
    font-weight: bold;
    line-height: 22px;
    cursor: pointer;
    border-radius: 50px;
    max-width: 300px;
    @media (max-width: 768px) {
      font-size: 14px;
    }
    &:focus {
      outline: none;
    }
`
export default ActionButton
