import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import StyledModal from '../StyledModal'
import ActionButton from '../ActionButton'

class FeedbackModal extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
    }
  }

  handleAccept = () => {
    const {
      confirmationHandler, handleClose, confirmationData,
    } = this.props
    if (confirmationHandler && confirmationData.length >= 0) {
      this.setState({ showLoader: true }, () => {
        confirmationHandler(...confirmationData).then(() => {
          this.setState({ showLoader: false }, () => {
            handleClose()
          })
        })
      })
    } else {
      handleClose()
    }
  }

  render() {
    const {
      title, message, handleClose,
    } = this.props
    const { showLoader } = this.state
    const { modalIsOpen, confirmationHandler } = this.props
    const requiresConfirmation = !!confirmationHandler
    return (
      <StyledModal
        modalIsOpen={modalIsOpen}
        closeModalAction={() => {
          handleClose()
        }}
      >
        <div className="container" style={{ padding: '20px' }}>
          <div className="text-center mb-20">
            <h2 className="mb-10">{title}</h2>
            <div className="mb-20">
              {message}
            </div>

            {showLoader && (
              <div className="row">
                <CircularProgress
                  className="progress-primary"
                  size={26}
                  mode="determinate"
                  value={75}
                  style={{ color: '#0071ce', margin: 'auto' }}
                />
              </div>
            )}
            {!showLoader && (
              <Fragment>
                <div className="mb-10">
                  <ActionButton
                    type={requiresConfirmation ? 'danger' : 'primary'}
                    onClick={() => this.handleAccept()}
                    style={{ margin: 'auto', color: '#fff', width: '200px' }}
                  >
                    Aceptar
                  </ActionButton>
                </div>
                {requiresConfirmation && (
                <div className="mb-20">
                  <ActionButton
                    onClick={handleClose}
                    style={{
                      margin: 'auto',
                      backgroundColor: '#f6f6f6',
                      color: '#293152',
                      width: '200px',
                      borderColor: '#f6f6f6',
                    }}
                  >
                    Cancelar
                  </ActionButton>
                </div>
                )}
              </Fragment>
            )}
          </div>
        </div>
      </StyledModal>
    )
  }
}

FeedbackModal.propTypes = {
  modalIsOpen: PropTypes.bool.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  confirmationHandler: PropTypes.func,
  confirmationData: PropTypes.array,
}

FeedbackModal.defaultProps = {
  title: '',
  message: '',
  confirmationHandler: null,
  confirmationData: [],
}

export default FeedbackModal
