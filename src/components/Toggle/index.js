import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import { UncontrolledTooltip } from 'reactstrap'
import { ToggleBody, ToggleControl } from './style'

class Toggle extends Component {
  render() {
    const {
      isActive, clickHandler, tooltipText, area,
    } = this.props

    return (
      <ToggleBody id={`toggle-${area}`} isActive={isActive} onClick={clickHandler}>
        <ToggleControl isActive={isActive} />
        {tooltipText && (
        <UncontrolledTooltip placement="right" target={`toggle-${area}`}>
          {tooltipText}
        </UncontrolledTooltip>
        )}
      </ToggleBody>
    )
  }
}

Toggle.defaultProps = {
  isActive: false,
  area: undefined,
  tooltipText: undefined,
  clickHandler: () => {},
}

Toggle.propTypes = {
  isActive: PropTypes.bool,
  clickHandler: PropTypes.func,
  area: PropTypes.string,
  tooltipText: PropTypes.string,
}

export default Toggle
