import styled from 'styled-components'

export const ToggleBody = styled.div`
  width: 34px;
  height: 20px;
  border-radius: 10px;
  background-color: ${props => (props.isActive ? '#0071c3' : '#cbced9')};
  display: flex;
  padding: 3px;
  pointer: cursor;
  position: relative;
`

export const ToggleControl = styled.div`
  width: 14px;
  height: 14px;
  border-radius: 7px;
  background-color: #ffffff;
  
  position: absolute;
  transition: right .35s ease-in-out;
   ${props => (props.isActive ? 'right: 3px;' : 'right: 17px;')}
`
