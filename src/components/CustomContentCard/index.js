import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '../Card'
import ActionButton from '../ActionButton'
import withFeedbackModal from '../WithFeedbackModal'
import { InnerTitle, PageContent } from './styled'

export class CustomContentCard extends Component {
  render() {
    const {
      title, children, actionButtonTitle, actionButtonHandler, showActionButton, showLoader, className,
    } = this.props

    return (
      <Card className={className}>
        <PageContent>
          <div className="row mb-10">
            <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
              <InnerTitle>{title}</InnerTitle>
            </div>
            { showActionButton && (
            <div className="col-md-6 col-sm-12" style={{ padding: 0 }}>
              <div className="create-button-container">
                <ActionButton type="info" onClick={() => actionButtonHandler()} className="create-new-button">
                  <div className="d-flex">
                    <div>{actionButtonTitle}</div>
                    <div><i style={{ marginLeft: '5px', fontWeight: 'bolder' }} className="zmdi zmdi-plus" /></div>
                  </div>
                </ActionButton>
              </div>
            </div>
            )}
          </div>
        </PageContent>
        {
          showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto' }}
              />
            </div>
          )
        }
        { !showLoader && children }
      </Card>
    )
  }
}


export default withFeedbackModal(CustomContentCard)

CustomContentCard.propTypes = {
  title: PropTypes.string,
  children: PropTypes.object,
  actionButtonTitle: PropTypes.string,
  actionButtonHandler: PropTypes.func,
  showActionButton: PropTypes.bool,
  showLoader: PropTypes.bool,
  className: PropTypes.string,
}
CustomContentCard.defaultProps = {
  title: '',
  children: <div />,
  actionButtonTitle: '',
  actionButtonHandler: () => {},
  showActionButton: false,
  showLoader: false,
  className: '',
}
