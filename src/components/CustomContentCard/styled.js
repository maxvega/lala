import styled from 'styled-components'

export const PageTitle = styled.h1`
  font-size: 20px;
  color: #424242;
  margin-bottom: 16px;
`

export const InnerTitle = styled.h1`
  font-size: 16px;
  font-weight: bold;
  line-height: 40px;
  padding: 0;
`
export const PageContent = styled.div`
  //display: flex;
  margin-bottom: 10px;
`
