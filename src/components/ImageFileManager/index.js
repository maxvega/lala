import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import {
  isExtensionValid, maxBytesAllowed, generateImageFormData, fileNameEnhancer,
} from '../../Helper'
import ClientBFF from '../../common/ClientBFF'
import CustomAlert from '../CustomAlert'
import FileInput from '../FileInput'
import ImageGallery from '../ImageGallery'
import StyledModal from '../StyledModal'
import FileUploadModal from './fileUploadModal'
import FileDeleteModal from './fileDeleteModal'


class ImageFileManager extends Component {
  constructor(props) {
    super(props)
    this.state = {
      uploadedFile: null,
      fileToDelete: null,
      showModal: false,
      showErrorAlert: false,
      showSuccessAlert: false,
    }
    this.clientBFF = new ClientBFF()
  }

  inputHandler=(e) => {
    const { files } = e.target
    this.hideCustomAlerts()
    if (files && files[0]) {
      const imageIsValid = isExtensionValid(files[0].name) && files[0].size <= maxBytesAllowed
      files[0].imageIsValid = imageIsValid
      if (!imageIsValid) {
        this.showErrorCustomAlert()
      }
      this.setState({
        uploadedFile: files[0], showModal: true, fileToDelete: null,
      })
    }
  }

  hideCustomAlerts=() => {
    this.setState({ showErrorAlert: false, showSuccessAlert: false })
  }

  showErrorCustomAlert=() => {
    this.setState({ showErrorAlert: true }, () => { setTimeout(() => { this.hideCustomAlerts() }, 2000) })
  }

  showSuccessCustomAlert=() => {
    this.setState({ showSuccessAlert: true }, () => { setTimeout(() => { this.hideCustomAlerts() }, 2000) })
  }


  uploadFileToStorage = async () => {
    const {
      container, delimiter, device, updateImages,
    } = this.props
    const { uploadedFile } = this.state
    if (uploadedFile && uploadedFile.imageIsValid) {
      const formData = generateImageFormData(fileNameEnhancer(uploadedFile), `${container}/${delimiter}/${device}`)
      try {
        await this.clientBFF.upload(formData)
        await this.clientBFF.upload(formData, { isProduction: false })
        updateImages(device)
        this.closeImageFileModal()
        this.showSuccessCustomAlert()
      } catch (e) {
        this.closeImageFileModal()
      }
    }
  }

  closeImageFileModal=() => {
    this.setState({ showModal: false })
  }

  handleImageToDelete = async (e, image) => {
    e.stopPropagation()
    this.setState({ fileToDelete: image, uploadedFile: null, showModal: true })
  }

  deleteFile = async (image) => {
    try {
      const { container, updateImages, device } = this.props
      await this.clientBFF.delete({
        storageContainer: container,
        fileName: image,
      })
      this.setState({
        showModal: false, fileToDelete: null, uploadedFile: null,
      })
      updateImages(device)
    } catch (e) {
      // TODO, to be defined error message
      // eslint-disable-next-line no-console
      console.log(e)
    }
  }

  render() {
    const {
      showModal, uploadedFile, showErrorAlert, showSuccessAlert, fileToDelete,
    } = this.state
    const {
      images, selectedImageHandler, imageSection, device, selectedDesktopImage, selectedMobileImage,
    } = this.props

    return (
      <Fragment>
        {showErrorAlert && <CustomAlert type="danger" onClick={this.hideCustomAlerts}>¡Lo sentimos! La imagen no es válida. Intenta con otra.</CustomAlert>}
        {showSuccessAlert && <CustomAlert type="success" onClick={this.hideCustomAlerts}>La imagen ha cargado correctamente.</CustomAlert>}
        <div className="mb-10" style={{ display: 'flex', justifyContent: 'center' }}>
          <FileInput inputHandler={this.inputHandler} />
        </div>
        <p style={{
          fontSize: '10px', fontStyle: 'italic', textAlign: 'center', color: '#0071ce',
        }}
        >
          La imagen no debe exceder los 200Kb y debe estar en  formato .jpg .png o .gif
        </p>
        <ImageGallery
          handleImageToDelete={this.handleImageToDelete}
          images={images}
          device={device}
          title="Subidas recientes"
          selectedImageHandler={selectedImageHandler}
          selectedDesktopImage={selectedDesktopImage}
          selectedMobileImage={selectedMobileImage}
          imageSection={`${imageSection}-${device}`}
        />
        <StyledModal
          modalIsOpen={showModal}
          closeModalAction={() => {
            this.closeImageFileModal()
          }}
        >
          {
            uploadedFile && (
              <FileUploadModal
                uploadedFile={uploadedFile}
                closeModal={this.closeImageFileModal}
                actionButtonHandler={this.uploadFileToStorage}
              />
            )
          }
          {
            fileToDelete && (
              <FileDeleteModal
                fileToDelete={fileToDelete}
                closeModal={this.closeImageFileModal}
                actionButtonHandler={this.deleteFile}
              />
            )
          }
        </StyledModal>
      </Fragment>
    )
  }
}

ImageFileManager.propTypes = {
  imageSection: PropTypes.string,
  device: PropTypes.string,
  images: PropTypes.array,
  selectedDesktopImage: PropTypes.string,
  selectedMobileImage: PropTypes.string,
  container: PropTypes.string.isRequired,
  selectedImageHandler: PropTypes.func,
  delimiter: PropTypes.string,
  updateImages: PropTypes.func,
}

ImageFileManager.defaultProps = {
  imageSection: '',
  device: '',
  selectedDesktopImage: '',
  selectedMobileImage: '',
  images: [],
  delimiter: '',
  selectedImageHandler: () => {},
  updateImages: () => {},
}

export default ImageFileManager
