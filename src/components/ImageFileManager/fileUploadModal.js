import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import ActionButton from '../ActionButton'
import { formatBytes } from '../../Helper'
import CustomParagraph from '../CustomParagraph'
import { MainContainer, Container, Title } from './styled'

const FileUploadModal = ({ ...props }) => {
  const { uploadedFile, closeModal, actionButtonHandler } = props
  const { imageIsValid, name, size } = uploadedFile
  return (
    <Fragment>
      <MainContainer>
        <Title>Subir una imagen nueva</Title>
        <Title>
          {imageIsValid ? 'La imagen se ha cargado correctamente.' : '¡Lo sentimos! Esta imagen no es válida..' }
        </Title>
        <CustomParagraph
          paragraphTitle={`${name} (${formatBytes(size)})`}
          paragraphStyle={{ whiteSpace: 'nowrap', color: imageIsValid ? '#0071c3' : '#cd3434' }}
          iconCustomStyle={{ marginRight: '5px', backgroundColor: imageIsValid ? '#0071c3' : '#cd3434' }}
          parentCustomStyle={{ justifyContent: 'center' }}
          iconTooltipText="editar banner"
          iconTooltipId="edit-banner"
          icon={imageIsValid ? 'zmdi zmdi-check' : 'zmdi zmdi-close'}
        />
        {!imageIsValid && (
        <Title
          fontSize="10px"
          fontStyle="italic"
          style={{ color: '#cd3434' }}
        >
          La imagen no debe exceder los 200Kb y debe estar en  formato .jpg .png o .gif
        </Title>
        )}

        <Container>
          {imageIsValid ? (
            <ActionButton type="primary" onClick={() => actionButtonHandler()} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
              guardar cambios
            </ActionButton>
          ) : (
            <ActionButton type="primary" onClick={() => closeModal()} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
              intentar nuevamente
            </ActionButton>
          )}
        </Container>
        <Container>
          <ActionButton
            onClick={() => closeModal()}
            style={{
              margin: 'auto', backgroundColor: '#f6f6f6', color: '#293152', width: '200px', borderColor: '#f6f6f6',
            }}
          >
            Cancelar
          </ActionButton>
        </Container>
      </MainContainer>
    </Fragment>
  )
}

FileUploadModal.propTypes = {
  closeModal: PropTypes.func,
  actionButtonHandler: PropTypes.func,
  uploadedFile: PropTypes.object.isRequired,
}

FileUploadModal.defaultProps = {
  closeModal: () => {},
  actionButtonHandler: () => {},
}

export default FileUploadModal
