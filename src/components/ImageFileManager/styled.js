import styled from 'styled-components'

export const MainContainer = styled.div`
  padding: 20px;
  text-align: center;
`

export const Title = styled.div`
  font-size: ${props => (props.fontSize ? props.fontSize : '16px')};
  font-style: ${props => (props.fontStyle ? props.fontStyle : 'normal')};
  margin-bottom: 10px;
`

export const Container = styled.div`
  margin-bottom: 10px;
`

export const Img = styled.img`
  max-width: 100%;
  height: auto;
  vertical-align: middle;
`
