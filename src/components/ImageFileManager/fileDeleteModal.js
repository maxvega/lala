import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import ActionButton from '../ActionButton'
import {
  MainContainer, Container, Title, Img,
} from './styled'

const configData = window.__ENV__


const FileDeleteModal = ({ ...props }) => {
  const { closeModal, actionButtonHandler, fileToDelete } = props
  return (
    <Fragment>
      <MainContainer>
        <Title fontSize="25px">Seguro deseas eliminar esta imagen?</Title>
        <Container className="mb-20">
          <Img alt="logo" src={`${configData.storageBaseUrl}/${fileToDelete}`} />
        </Container>
        <Container>
          <ActionButton type="danger" onClick={() => actionButtonHandler(fileToDelete)} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
            Borrar
          </ActionButton>
        </Container>
        <Container>
          <ActionButton
            onClick={() => closeModal()}
            style={{
              margin: 'auto', backgroundColor: '#f6f6f6', color: '#293152', width: '200px', borderColor: '#f6f6f6',
            }}
          >
            Cancelar
          </ActionButton>
        </Container>
      </MainContainer>
    </Fragment>
  )
}

FileDeleteModal.propTypes = {
  closeModal: PropTypes.func,
  actionButtonHandler: PropTypes.func,
  fileToDelete: PropTypes.string.isRequired,
}

FileDeleteModal.defaultProps = {
  closeModal: () => {},
  actionButtonHandler: () => {},
}

export default FileDeleteModal
