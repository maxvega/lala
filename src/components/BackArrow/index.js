import styled from 'styled-components'
import React from 'react'

const BackArrowContainer = styled.div`
    font-size: 30px;
    cursor: pointer;
    line-height: 1px;
    margin-right: 10px;
    width: 30px;
    height: 30px;
    background: #0071c3;
    color: white;
    border-radius: 50%;
    min-width: 30px;
`
const BackArrow = ({ ...props }) => (
  <BackArrowContainer {...props}>
    <i data-testid="zmdi-chevron-left" className="zmdi zmdi-chevron-left" />
  </BackArrowContainer>
)

export default BackArrow
