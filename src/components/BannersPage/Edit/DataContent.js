import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Moment from 'moment'
import Label from '../../Label'
import Input from '../../Input'
import CustomSelect from '../../CustomSelect'
import DateAndTimePickers from '../../DateAndTimePickers'

const DataContent = ({ ...props }) => {
  const {
    banner, handleEditSelectedBanner, showUpdateField,
  } = props

  const {
    link, target, tag, name, startDate, endDate, counterBgColor, counterFontColor, updated, area, order,
  } = banner
  let targetValue = null
  if (target) {
    targetValue = { value: target.value, label: target.substring(1) }
  }


  return (
    <Fragment>
      <div className="mb-10">
        <Label>Nombre</Label>
        <Input
          data-testid="data-name"
          className="bold"
          value={name || ''}
          onChange={e => handleEditSelectedBanner(e, 'name')}
        />
      </div>
      <div className="mb-10">
        <Label>URL</Label>
        <Input
          data-testid="data-link"
          className="bold"
          value={decodeURI(link)}
          onChange={e => handleEditSelectedBanner(e, 'link')}
        />
      </div>
      {targetValue && (
      <div data-testid="data-select" className="mb-10">
        <Label>Target</Label>
        <CustomSelect
          value={targetValue}
          placeholder="Seleccionar target"
          onChange={e => handleEditSelectedBanner(e, 'target')}
          options={[{ value: '_blank', label: 'blank' }, { value: '_self', label: 'self' }]}
        />
      </div>
      )}
      <div className="mb-10">
        <Label>Orden</Label>
        <Input
          data-testid="data-order"
          className="bold"
          value={order}
          onChange={e => handleEditSelectedBanner(e, 'order')}
        />
      </div>
      <div className="mb-10">
        <Label>Tag Google Analytics</Label>
        <Input
          data-testid="data-tag"
          className="bold"
          value={tag}
          onChange={e => handleEditSelectedBanner(e, 'tag')}
        />
      </div>
      {startDate && (
        <div className="mb-10">
          <Label>Fecha de inicio</Label>
          <DateAndTimePickers
            name="startDate"
            key={`${startDate}${area}`}
            area={area}
            defaultDate={startDate}
            handleAction={handleEditSelectedBanner}
          />
        </div>
      )}
      {endDate && (
        <div className="mb-10">
          <Label>Fecha de fin</Label>
          <DateAndTimePickers
            name="endDate"
            key={`${endDate}${area}`}
            area={area}
            defaultDate={endDate}
            handleAction={handleEditSelectedBanner}
          />
        </div>
      )}
      {counterBgColor && (
        <div className="mb-10">
          <Label>Background color</Label>
          <Input
            data-testid="data-counterBgColor"
            className="bold"
            value={counterBgColor || ''}
            onChange={e => handleEditSelectedBanner(e, 'counterBgColor')}
          />
        </div>
      )}
      {counterFontColor && (
        <div className="mb-10">
          <Label>Font color</Label>
          <Input
            data-testid="data-counterFontColor"
            className="bold"
            value={counterFontColor || ''}
            onChange={e => handleEditSelectedBanner(e, 'counterFontColor')}
          />
        </div>
      )}
      {showUpdateField && (
        <div className="mb-10">
          <Label>Fecha de actualización</Label>
          <Input
            data-testid="data-update"
            className="bold"
            value={Moment(updated).format('DD-MM-YYYY') || ''}
            disabled
          />
        </div>
      )}
    </Fragment>
  )
}

DataContent.defaultProps = {
  showUpdateField: false,
}

DataContent.propTypes = {
  handleEditSelectedBanner: PropTypes.func.isRequired,
  banner: PropTypes.object.isRequired,
  showUpdateField: PropTypes.bool,
}

export default DataContent
