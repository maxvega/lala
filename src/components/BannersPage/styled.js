import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const HeaderBannerTitle = styled.span`
  margin-right: 10px;
  white-space: nowrap;
`

export const HeaderBannerTitleCR = styled.span`
  color: #0071c3;
  margin-right: 10px;
`

export const I = styled.i`
  color: #0071c3;
  height: 8px;
  margin-right: 10px;
`
