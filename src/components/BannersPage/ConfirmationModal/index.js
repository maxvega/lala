import React, { Component, Fragment } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import PropTypes from 'prop-types'
import { ModalContainer, ModalTitle } from '../../CustomEditCard/styled'
import ActionButton from '../../ActionButton'
import Modal from '../../StyledModal'

class ConfirmationModal extends Component {
  render() {
    const {
      showLoader, modalIsOpen, hideConfirmationModal, continueProcess, confirmationText, confirmationButton, hideCancelButtom, headerText,
    } = this.props
    return (
      <Modal
        modalIsOpen={modalIsOpen}
        closeModalAction={hideConfirmationModal}
        style={{
          content: {
            width: '470px',
          },
        }}
      >
        <ModalContainer>
          <ModalTitle>{headerText}</ModalTitle>
          <div className="d-flex">
            <span>{confirmationText}</span>
          </div>
          <div className="d-flex flex-column">
            {showLoader && (
            <div className="row">
              <CircularProgress
                className="progress-primary"
                size={26}
                mode="determinate"
                value={75}
                style={{ color: '#0071ce', margin: 'auto', marginTop: '28px' }}
              />
            </div>
            )}
            {!showLoader && (
            <Fragment>
              <ActionButton
                className="text-center"
                type="primary"
                style={{
                  width: '100%', color: '#fff', margin: 'auto', marginBottom: '10px', marginTop: '28px',
                }}
                onClick={continueProcess}
              >
                {confirmationButton}
              </ActionButton>
              {
                !hideCancelButtom && (
                <ActionButton
                  className="text-center"
                  type="secondary"
                  style={{ width: '100%', color: '#0071c3', margin: 'auto' }}
                  onClick={hideConfirmationModal}
                >
                  cancelar
                </ActionButton>
                )
              }
            </Fragment>
            )}
          </div>
        </ModalContainer>
      </Modal>
    )
  }
}

export default ConfirmationModal

ConfirmationModal.propTypes = {
  showLoader: PropTypes.bool,
  modalIsOpen: PropTypes.bool,
  hideConfirmationModal: PropTypes.func,
  continueProcess: PropTypes.func,
  confirmationText: PropTypes.string.isRequired,
  confirmationButton: PropTypes.string.isRequired,
  headerText: PropTypes.string,
  hideCancelButtom: PropTypes.bool,
}

ConfirmationModal.defaultProps = {
  showLoader: false,
  modalIsOpen: false,
  hideConfirmationModal: () => {},
  continueProcess: () => {},
  headerText: '¿Estás seguro?',
  hideCancelButtom: false,
}
