import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Moment from 'moment'
import CircularProgress from '@material-ui/core/CircularProgress'
import { JSONToFile, generateJSONFormData } from '../../../Helper'
import ClientBFF from '../../../common/ClientBFF'
import CustomAlert from '../../CustomAlert'
import { showParametersList } from '../Config'
import DataContentAdd from '../Add/DataContentAdd'
import BackArrow from '../../BackArrow'
import Card from '../../Card'
import ActionButton from '../../ActionButton'
import {
  Title, HeaderContainer,
} from './styled'


const storageContainer = 'landing'
const selectedResourceName = 'contentSettings/countdown_settings.json'


class AddNew extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      showErrorAlert: false,
      showConflictDatesErrorAlert: false,
      showSuccessAlert: false,
      newCountDown: {},
      path: '/storage/download-parsed',
    }
    this.clientBFF = new ClientBFF()
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        showLoader: false,
      })
    }
  }

  getNewCountDownInfo=(e, value) => {
    const newCountDownState = this.state.newCountDown

    if (e.target) {
      this.setState({
        newCountDown:
            {
              ...newCountDownState,
              [value]: e.target.value,
            },
      })
    } else {
      this.setState({
        newCountDown:
            {
              ...newCountDownState,
              [value]: e.value,
            },
      })
    }
  }


  hideCustomAlerts=() => {
    this.setState({ showErrorAlert: false, showSuccessAlert: false, showConflictDatesErrorAlert: false })
  }

  showErrorCustomAlert=() => {
    this.setState({ showErrorAlert: true }, () => { setTimeout(() => { this.hideCustomAlerts() }, 2000) })
  }

  showSuccessCustomAlert=() => {
    this.setState({ showSuccessAlert: true }, () => { setTimeout(() => { this.hideCustomAlerts() }, 2000) })
  }

  isConflictBetweenDates = (elements, newCountDown) => {
    const countdownList = Object.values(elements.countdownBanner)
    return countdownList.some(cr => Moment(newCountDown.startDate).isBetween(cr.startDate, cr.endDate) || Moment(newCountDown.endDate).isBetween(cr.startDate, cr.endDate))
  }

  uploadToStaging = async (newCountDown) => {
    const { retrieveData, setSelectedBannerIndex } = this.props
    try {
      this.setState({ showLoader: true })
      const { path } = this.state
      const elements = await this.clientBFF.download({ resourceName: selectedResourceName, storageContainer }, { path, isProduction: false })

      if (this.isConflictBetweenDates(elements, newCountDown)) {
        this.setState({ showLoader: false, showConflictDatesErrorAlert: true })
        return false
      }

      const elementSize = Object.keys(elements.countdownBanner).length
      newCountDown.area = elementSize + 1
      elements.countdownBanner = { ...elements.countdownBanner, [newCountDown.area]: newCountDown }

      const file = JSONToFile(elements, selectedResourceName)
      const formData = generateJSONFormData(file, storageContainer)
      await this.clientBFF.upload(formData, { isProduction: false })

      await retrieveData()

      setSelectedBannerIndex(null, newCountDown.area)

      this.setState({ showLoader: false })
      return true
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e.message)
      this.setState({ showLoader: false, showErrorAlert: true })
      return false
    }
  }

  createCountdown = () => {
    const {
      startDate, endDate, link, counterBgColor, counterFontColor, name, tag,
    } = this.state.newCountDown

    const newCountDownObj = {
      startDate,
      endDate,
      backgroundDesktop: '',
      backgroundMobile: '',
      link,
      counterBgColor,
      counterFontColor,
      name,
      tag,
      order: 1,
      updated: Moment().format(),
      type: 'countdown',
      internalName: name,
      active: false,
    }

    this.uploadToStaging(newCountDownObj)
  }

  saveNewCountDown = async () => {
    this.setState({ showLoader: true }, this.createCountdown())
  }

  render() {
    const {
      showLoader, showErrorAlert, showSuccessAlert, showConflictDatesErrorAlert,
    } = this.state
    const { title, backArrowHandler, className } = this.props

    return (
      <Card className={className} style={{ position: 'unset' }}>
        {showConflictDatesErrorAlert && (
        <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
          ¡Lo sentimos! No pudimos crear la nueva Cuenta Regresiva, ya que coincide con la fecha de inicio y fin de una Cuenta Regresiva anterior.
        </CustomAlert>
        )}
        {showErrorAlert && (
          <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
            ¡Lo sentimos! Algo salió mal.
          </CustomAlert>
        )}
        {showSuccessAlert && (
          <CustomAlert type="success" onClick={this.hideCustomAlerts}>
            <b>¡Tus cambios han subido a producción correctamente! </b>
          </CustomAlert>
        )}
        <HeaderContainer className="justify-content-between mb-10">
          <div className="d-flex">
            <BackArrow
              className="text-center"
              onClick={backArrowHandler}
            />
            <Title data-testid="header-container-title">
              {title}
            </Title>
          </div>
        </HeaderContainer>

        <DataContentAdd
          showParametersList={showParametersList}
          getNewElementInfo={this.getNewCountDownInfo}
        />

        {showLoader && (
        <div className="row">
          <CircularProgress
            className="progress-primary"
            size={26}
            mode="determinate"
            value={75}
            style={{ color: '#0071ce', margin: 'auto' }}
          />
        </div>
        )}

        {!showLoader && (
        <div style={{ alignItems: 'center' }} className="d-flex flex-column">
          <ActionButton data-testid="action-button-to-add-cr" id="saveCountdown" className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto 20px' }} onClick={this.saveNewCountDown}>
            crear cuenta regresiva
          </ActionButton>
        </div>

        )}

      </Card>
    )
  }
}

export default AddNew

AddNew.defaultProps = {
  backArrowHandler: () => {},
  className: '',
  title: '',
}
AddNew.propTypes = {
  title: PropTypes.string,
  backArrowHandler: PropTypes.func,
  className: PropTypes.string,
  setSelectedBannerIndex: PropTypes.func.isRequired,
  retrieveData: PropTypes.func.isRequired,
}
