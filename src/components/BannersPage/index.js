import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import Moment from 'moment'
import { AutoSizer } from 'react-virtualized'
import { Scrollbars } from 'react-custom-scrollbars'
import ClientBFF from '../../common/ClientBFF'
import { BannerContent } from '../BannerContent'
import ImageFileManager from '../ImageFileManager'
import AddNew from './AddNew'
import DataContent from './Edit/DataContent'
import BannerContentHeader from './BannerContentHeader'
import CustomContentCard from '../CustomContentCard'
import CustomEditCard from '../CustomEditCard'
import PageTitle from '../PageTitle'
import Element from '../Card/Element'
import { rootNamesToShowActionButtonList } from './Config'
import { HeaderBannerTitle, HeaderBannerTitleCR, I } from './styled'

class BannersPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      collection: [],
      selectedBannerImages: [],
      showLoader: false,
      showEditSection: false,
      showAddNewSection: false,
      selectedBannerIndex: null,
      createdBannerIndex: null,
      selectedBanner: null,
      imageDevice: null,
      editData: false,
      showNewCountdown: false,
      downloadParameters: {
        storageContainer: 'landing/contentSettings',
        resourceName: props.resourceName,
        contentToExtract: [props.rootName],
      },
      path: '/storage/download-parsed',
    }
    this.clientBFF = new ClientBFF()
  }

  componentDidMount=async () => {
    await this.retrieveData()
  }

  retrieveData = async () => {
    const { rootName } = this.props
    const { downloadParameters, path } = this.state

    this.setState({ showLoader: true }, async () => {
      const response = await this.clientBFF.download(downloadParameters, { path, isProduction: false })
      const prodResponse = await this.clientBFF.download(downloadParameters, { path })
      const collectionProd = prodResponse[rootName] || []
      const collection = response[rootName] || []

      const orderCollection = rootName === 'countdownBanner' ? Object.values(collection).sort((a, b) => Moment(a.startDate).valueOf() - Moment(b.startDate).valueOf())
        .filter(banner => new Date(banner.endDate).getTime() > new Date().getTime()) : Object.values(collection)

      await this.setState({ collectionProd: Object.values(collectionProd), collection: orderCollection, showLoader: false })
    })
  }

  hideEditSection = () => {
    this.setState({ showEditSection: false })
  }


  setSelectedBannerIndex = async (index, newCreatedIndex) => {
    await this.setState({ selectedBannerIndex: index, createdBannerIndex: newCreatedIndex })
    await this.editData()
  }

  /* handleToggleClick = (banner) => {
    this.setState({ showToggleConfirmationModal: true, toggledBanner: banner })
  } */

  getSelectedBannerImages = async (selectedBanner, device) => {
    const { rootName } = this.props
    const selectedBannerImages = await this.clientBFF.getBanners({
      storageContainer: 'landing',
      delimiter: `banners/${rootName}/${selectedBanner.area}/${device}`,
      prefix: `banners/${rootName}/${selectedBanner.area}/${device}`,
    })
    this.setState({ selectedBannerImages, showLoader: false })
  }

  handleEditSelectedBanner=(e, value) => {
    const { selectedBanner } = this.state
    if (e.target) {
      selectedBanner[value] = e.target.value
    } else {
      selectedBanner[value] = e.value
    }

    this.setState({ selectedBanner })
  }

  handleEditSelectedBannerToggle=() => {
    const { selectedBanner } = this.state
    if (selectedBanner && 'active' in selectedBanner) {
      selectedBanner.active = !selectedBanner.active
    }
    this.setState({ selectedBanner })
  }

  handleEditSelectedBannerImage=(image) => {
    const { selectedBanner, imageDevice } = this.state
    if (imageDevice === 'desktop') {
      selectedBanner.backgroundDesktop = image
    } else {
      selectedBanner.backgroundMobile = image
    }
    this.setState({ selectedBanner })
  }

  editData = async () => {
    const { collection, selectedBannerIndex, createdBannerIndex } = this.state

    const createdIndex = createdBannerIndex ? collection.findIndex(banner => banner.area === createdBannerIndex) : selectedBannerIndex

    this.setState({
      selectedBannerIndex: createdIndex, editData: true, imageDevice: null, showEditSection: true, selectedBanner: collection[createdIndex], showAddNewSection: false, showNewCountdown: false,
    })
  }

  editImageDevice = (device) => {
    const { collection, selectedBannerIndex } = this.state
    const selectedBanner = collection[selectedBannerIndex]
    this.setState({
      editData: false, imageDevice: device, showEditSection: true, selectedBanner, showAddNewSection: false, showNewCountdown: false,
    }, async () => {
      await this.getSelectedBannerImages(selectedBanner, device)
    })
  }


  showNewCountdown = () => {
    this.setState({ showNewCountdown: true, showAddNewSection: true, showEditSection: false })
  }

  closeSideNav =() => {
    this.setState({ showNewCountdown: false, showEditSection: false, showAddNewSection: false })
  }

  getHeaderBannerTitle = ({ startDate, name, internalName }) => {
    const { rootName } = this.props
    const isCountdownBanner = ['countdownBanner'].includes(rootName)

    return isCountdownBanner ? (
      <Fragment>
        <HeaderBannerTitleCR>Inicio: </HeaderBannerTitleCR>
        <HeaderBannerTitle>{Moment(startDate).format('DD-MM-YYYY - HH:mm:ss')}</HeaderBannerTitle>
        <div>
          <I className="zmdi zmdi-chevron-right" />
        </div>
        <HeaderBannerTitle>{name}</HeaderBannerTitle>
      </Fragment>
    )
      : (
        <Fragment>
          <span style={{ marginRight: '10px', whiteSpace: 'nowrap' }}>{internalName}</span>
          <div>
            <I className="zmdi zmdi-chevron-right" />
          </div>
          <HeaderBannerTitle>{name}</HeaderBannerTitle>
        </Fragment>
      )
  }

  renderCollection = (activeEdit, toggleTag) => {
    const { collection, collectionProd, selectedBannerIndex } = this.state
    const { rootName, showBannerInfoSection } = this.props

    const elementCollection = collection.map((banner, index) => {
      const prodBanner = collectionProd.find(prodBanner => prodBanner.area === banner.area) || {}

      const image = ['countdownBanner', 'bannersHome', 'bannersBCI', 'inspirationalBlocks'].includes(rootName) ? `${rootName}-1-desktop` : `${rootName}-${index + 1}-desktop`

      const bannerTitle = this.getHeaderBannerTitle(banner)
      const showUpdateField = (banner && banner.updated && rootName === 'countdownBanner') || false

      return (
        <Element key={banner.area} style={{ padding: '0 15px' }}>
          <BannerContentHeader
            onClick={() => { this.setSelectedBannerIndex(index) }}
            style={{ cursor: 'pointer', padding: '10px' }}
            image={image}
            actionButtonHandler={() => { this.setSelectedBannerIndex(index) }}
            bannerTitle={bannerTitle || ''}
            toggleConfig={{
              hasToggle: prodBanner.active !== undefined,
              isActive: prodBanner.active,
              isCountdownBanner: rootName === 'countdownBanner',
              toggleTag,
            }}
          />
          {selectedBannerIndex === index && (
            <BannerContent
              activeEdit={activeEdit}
              grid={banner}
              editDataHandler={this.editData}
              editDesktopHandler={() => { this.editImageDevice('desktop') }}
              editMobileHandler={() => { this.editImageDevice('mobile') }}
              showInfoSection={showBannerInfoSection}
              showUpdateField={showUpdateField}
            />
          )}
        </Element>
      )
    })

    return (
      <div style={{ height: 'calc(100vh - 250px)', position: 'relative' }}>
        <AutoSizer disableWidth>
          {({ height }) => (
            <Scrollbars
              className="rct-scroll"
              autoHeight
              autoHeightMax={height}
              autoHide
            >
              {elementCollection}
            </Scrollbars>
          )}
        </AutoSizer>
      </div>
    )
  }

  render() {
    const {
      showLoader,
      showEditSection,
      selectedBannerImages,
      editData,
      imageDevice,
      selectedBannerIndex,
      selectedBanner,
      downloadParameters,
      showNewCountdown,
      showAddNewSection,
    } = this.state

    const { title, rootName, showBannerInfoSection } = this.props


    let editCustomChild = null
    let activeEdit = ''
    if (editData && showBannerInfoSection) {
      const showUpdateField = (selectedBanner && selectedBanner.updated && rootName === 'countdownBanner') || false
      editCustomChild = (<DataContent showUpdateField={showUpdateField} banner={selectedBanner} handleEditSelectedBanner={this.handleEditSelectedBanner} />)
      activeEdit = 'data'
    } else if (selectedBanner) {
      const device = imageDevice || 'desktop'
      activeEdit = device
      const container = 'landing'
      const delimiter = `banners/${rootName}/${selectedBanner.area}`
      const imageSection = ['countdownBanner', 'bannersHome', 'bannersBCI', 'inspirationalBlocks'].includes(rootName) ? `${rootName}-1` : `${rootName}-${selectedBannerIndex + 1}`
      editCustomChild = (
        <ImageFileManager
          device={device}
          imageSection={imageSection}
          images={selectedBannerImages}
          container={container}
          delimiter={delimiter}
          selectedDesktopImage={selectedBanner.backgroundDesktop}
          selectedMobileImage={selectedBanner.backgroundMobile}
          selectedImageHandler={this.handleEditSelectedBannerImage}
          updateImages={this.editImageDevice}
        />
      )
    }

    const { name: rootNameComponent, buttonText: actionButtonTitle, toggleText } = rootNamesToShowActionButtonList.find(item => item?.name?.includes(rootName)) || []

    const toggleTag = rootName === 'countdownBanner' ? toggleText : 'banner'

    return (
      <div>
        <PageTitle>Banners</PageTitle>
        <div className="row">
          <CustomContentCard
            {...this.props}
            title={title}
            showLoader={showLoader}
            showEditSection={showEditSection}
            showAddNewSection={showAddNewSection}
            className={`${showEditSection || showAddNewSection ? 'not-display-in-mobile col-md-7 ' : 'col-md-12'} col-sm-12 left-card-margin`}
            actionButtonHandler={() => {
              this.showNewCountdown()
            }}
            showActionButton={!!rootNameComponent}
            actionButtonTitle={actionButtonTitle}
          >
            { this.renderCollection(activeEdit, toggleTag) }
          </CustomContentCard>

          {showEditSection && (
            <CustomEditCard
              selectedElement={selectedBanner}
              downloadParameters={downloadParameters}
              selectedElementKey={selectedBanner.area.toString()}
              className="col-md-5 col-sm-12 right-card-margin"
              title={imageDevice ? `Editar imagen ${imageDevice}` : `Editar ${toggleTag}`}
              backArrowHandler={this.hideEditSection}
              elementParent={rootName}
              setSelectedBannerIndex={this.setSelectedBannerIndex}
              toggleConfig={{
                hasToggle: selectedBanner.active !== undefined,
                isActive: selectedBanner.active,
                area: selectedBanner.area.toString(),
                tooltipText: selectedBanner.active ? `desactivar ${toggleTag}` : `activar ${toggleTag}`,
                toggleTag,
                toggleClickHandler: (e) => {
                  e.stopPropagation()
                  // this.handleToggleClick(selectedBanner)
                },
                handleEditSelectedBannerToggle: () => this.handleEditSelectedBannerToggle(),
                retrieveData: () => this.retrieveData(),
              }}
            >
              {editCustomChild}
            </CustomEditCard>
          )}

          {showNewCountdown && (
          <AddNew setSelectedBannerIndex={this.setSelectedBannerIndex} retrieveData={this.retrieveData} backArrowHandler={this.closeSideNav} className="col-md-5 col-sm-12 right-card-margin" title="Nueva cuenta regresiva" />
          )}

        </div>

      </div>
    )
  }
}

BannersPage.defaultProps = {
  showBannerInfoSection: true,
}

BannersPage.propTypes = {
  title: PropTypes.string.isRequired,
  rootName: PropTypes.string.isRequired,
  resourceName: PropTypes.string.isRequired,
  showBannerInfoSection: PropTypes.bool,
}

export default BannersPage
