import React from 'react'
import PropTypes from 'prop-types'
import Label from '../../Label'
import Input from '../../Input'
import DateAndTimePickers from '../../DateAndTimePickers'


const renderElements = (elements, getNewElementInfo) => elements.map((element) => {
  if (element.type === 'input') {
    return (
      <div key={element.name} className="mb-10">
        <Label>{element.label}</Label>
        <Input
          data-testid={element.name}
          name={element.name}
          className="bold"
          defaultValue={element.value}
          onChange={e => getNewElementInfo(e, element.name)}
        />
      </div>
    )
  }

  if (element.type === 'date') {
    return (
      <div key={element.name} className="mb-10">
        <Label>{element.label}</Label>
        <DateAndTimePickers
          area={element.area}
          name={element.name}
          handleAction={e => getNewElementInfo(e, element.name)}
        />
      </div>
    )
  }

  return null
})


const DataContentAdd = ({ ...props }) => {
  const {
    showParametersList, getNewElementInfo,
  } = props

  return (
    renderElements(showParametersList, getNewElementInfo)
  )
}

DataContentAdd.propTypes = {
  showParametersList: PropTypes.array.isRequired,
  getNewElementInfo: PropTypes.func.isRequired,
}

export default DataContentAdd
