import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  position: relative;
  align-items: center;
`

export const Title = styled.span`
  margin-right: 10px;
  white-space: nowrap;
`

export const BannerIconMap = styled.img`
  height: 30px;
  max-width: 100%;
`

export const BannerIconEditMap = styled.img`
  height: 50px;
  max-width: 100%;
`

export const CustomText = styled.div`
  display: flex;
  align-items: center;
  height: 20px;
  padding: 4px 8px;
  border-radius: 16px;
  border: ${props => (props.isActive ? 'solid 1px #02aa8f' : 'solid 0.5px #979797')};
  background-color: ${props => (props.isActive ? '#caf6ef' : '#f7f7f7')};
  text-align: center;
  font-size: 10px;
  font-weight: bold;
  color: ${props => (props.isActive ? '#02aa8f' : '#979797')};
`
