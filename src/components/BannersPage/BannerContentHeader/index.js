import React from 'react'
import PropTypes, { shape } from 'prop-types'
import RoundIconWrapper from '../../RoundIconWrapper'
import {
  Container, BannerIconMap, CustomText,
} from './styled'

const addDefaultSrc = (e) => {
  e.target.src = '/images/transparentSquare.png'
}

const BannerContentHeader = ({ ...props }) => {
  const {
    image, actionButtonHandler, bannerTitle, style, onClick,
    toggleConfig: {
      isActive, hasToggle, toggleTag, isCountdownBanner,
    },
  } = props


  return (
    <Container style={style} onClick={onClick}>
      {bannerTitle}
      <BannerIconMap alt={image} src={`/images/${image}.svg`} onError={(e) => { addDefaultSrc(e) }} />
      <div style={{
        position: 'absolute',
        right: '0',
        display: 'flex',
      }}
      >
        {(hasToggle || isCountdownBanner) && (
          <CustomText isActive={isActive}>
            {isActive ? `${toggleTag} activado` : `${toggleTag} desactivado`}
          </CustomText>
        )}
        <RoundIconWrapper tooltipText="editar banner" id="edit-banner" width="19px" height="19px" className="icon-resize-container" style={{ marginLeft: '6px' }}>
          <i className="zmdi zmdi-edit" onClick={() => { actionButtonHandler() }} />
        </RoundIconWrapper>
      </div>

    </Container>
  )
}

BannerContentHeader.propTypes = {
  image: PropTypes.string.isRequired,
  actionButtonHandler: PropTypes.func,
  bannerTitle: PropTypes.object,
  style: PropTypes.object,
  onClick: PropTypes.func,
  toggleConfig: shape({
    isActive: PropTypes.bool,
    hasToggle: PropTypes.bool,
  }),
}

BannerContentHeader.defaultProps = {
  actionButtonHandler: () => {},
  bannerTitle: {},
  style: {},
  onClick: () => {},
  toggleConfig: {
    isActive: false,
    hasToggle: false,
  },
}

export default BannerContentHeader
