import styled from 'styled-components'

const StyledSeparator = styled.div`
  border: solid 1px #eeeeee;
  height: 1px;
  width: 100%;
  margin-top: 5px;
  margin-bottom: 5px;
`
export default StyledSeparator
