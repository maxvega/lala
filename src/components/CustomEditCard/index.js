import React, { Component, Fragment } from 'react'
import PropTypes, { shape } from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import Moment from 'moment'
import { JSONToFile, generateJSONFormData } from '../../Helper'
import ClientBFF from '../../common/ClientBFF'
import Modal from '../StyledModal'
import CustomAlert from '../CustomAlert'
import Toggle from '../Toggle'
import ConfirmationModal from '../BannersPage/ConfirmationModal'
import BackArrow from '../BackArrow'
import Card from '../Card'
import ActionButton from '../ActionButton'
import {
  Title, ModalTitle, ModalContainer, Checkbox, HeaderContainer,
} from './styled'


class CustomEditCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      testedStaging: false,
      isUploadToProductionModalOpen: false,
      isProductionChecked: false,
      showErrorAlert: false,
      showConflictDatesErrorAlert: false,
      showSuccessAlert: false,
      isToggleModalOpen: false,
      originalToggleState: false,
      showErrorAlertImage: false,
      path: '/storage/download-parsed',
    }
    this.config = typeof window !== 'undefined' ? window.__ENV__ : { }
    this.clientBFF = new ClientBFF()
  }

  componentDidMount() {
    const { toggleConfig } = this.props
    const currentToggleState = toggleConfig.isActive || false
    this.setState({ originalToggleState: currentToggleState })
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.selectedElement) !== JSON.stringify(nextProps.selectedElement) && nextProps.toggleConfig) {
      const currentToggleState = nextProps.toggleConfig.isActive ? nextProps.toggleConfig.isActive : false
      this.setState({ originalToggleState: currentToggleState })
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        showLoader: false,
        testedStaging: false,
        showErrorAlertImage: false,
      })
    }
  }

  isConflictBetweenDates = (elements, newCountDown) => {
    try {
      const countdownList = Object.values(elements.countdownBanner)
      return countdownList.some(cr => Moment(newCountDown.startDate).isBetween(cr.startDate, cr.endDate) || Moment(newCountDown.endDate).isBetween(cr.startDate, cr.endDate))
    } catch (e) {
      return false
    }
  }

  isEmptyBannerImage = () => {
    const { selectedElement } = this.props
    return (selectedElement.backgroundDesktop === '' || selectedElement.backgroundMobile === '')
  }

  // eslint-disable-next-line consistent-return
  uploadToStaging = async () => {
    const {
      selectedElement, downloadParameters, selectedElementKey, elementParent, toggleConfig: { retrieveData, toggleTag }, setSelectedBannerIndex,
    } = this.props
    const { resourceName, storageContainer } = downloadParameters
    const { IS_STRATI = false } = this.config

    try {
      this.setState({ showLoader: true }, async () => {
        const { path } = this.state
        const elements = await this.clientBFF.download({
          resourceName,
          storageContainer,
        }, { path, isProduction: false })

        if (this.isEmptyBannerImage()) {
          this.setState({ showErrorAlertImage: true, showLoader: false })
          this.showToggleToggleModal()
          return false
        }

        if (this.isConflictBetweenDates(elements, selectedElement)) {
          this.setState({ showLoader: false, showConflictDatesErrorAlert: true })
          return false
        }

        elements[elementParent][selectedElementKey] = selectedElement
        const file = JSONToFile(elements, resourceName)
        const formData = generateJSONFormData(file, storageContainer)
        await this.clientBFF.upload(formData, { isProduction: false })
        this.setState({ showLoader: false, testedStaging: true })
        if (!IS_STRATI) window.open('https://buysmart-staging.lider.cl/catalogo', '_blank')

        if (toggleTag === 'cuenta regresiva') {
          await retrieveData()

          if (setSelectedBannerIndex) {
            await setSelectedBannerIndex(null, selectedElement.area)
          }
        }

        return true
      })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e.message)
      this.setState({ showLoader: false, testedStaging: false, showErrorAlert: true })
      return false
    }
  }

  uploadToProduction = async () => {
    const {
      selectedElement, downloadParameters, selectedElementKey, elementParent, toggleConfig: { retrieveData },
    } = this.props
    const { resourceName, storageContainer } = downloadParameters

    try {
      this.setState({ showLoader: true }, async () => {
        const { path } = this.state
        const elements = await this.clientBFF.download({
          resourceName,
          storageContainer,
        }, { path })

        if (this.isConflictBetweenDates(elements, selectedElement)) {
          this.setState({ showLoader: false, showConflictDatesErrorAlert: true })
          return false
        }

        elements[elementParent][selectedElementKey] = selectedElement
        const file = JSONToFile(elements, resourceName)
        const formData = generateJSONFormData(file, storageContainer)
        await this.clientBFF.upload(formData)

        await retrieveData()

        this.setState({
          showLoader: false, isUploadToProductionModalOpen: false, isProductionChecked: false, showSuccessAlert: true,
        })
        return true
      })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e.message)
      this.setState({ showLoader: false, testedStaging: false, showErrorAlert: true })
    }
  }

  showUploadToProductionConfirmationModal = () => {
    this.setState({ isUploadToProductionModalOpen: true })
  }

  hideUploadToProductionConfirmationModal = () => {
    this.setState({ isUploadToProductionModalOpen: false })
  }

  toggleProductionCheckbox = () => {
    this.setState(({ isProductionChecked }) => ({
      isProductionChecked: !isProductionChecked,
    }))
  }

  hideCustomAlerts=() => {
    this.setState({
      showErrorAlert: false, showSuccessAlert: false, showConflictDatesErrorAlert: false,
    })
  }

  showErrorCustomAlert=() => {
    this.setState({ showErrorAlert: true }, () => { setTimeout(() => { this.hideCustomAlerts() }, 2000) })
  }

  showSuccessCustomAlert=() => {
    this.setState({ showSuccessAlert: true }, () => { setTimeout(() => { this.hideCustomAlerts() }, 2000) })
  }

  showToggleToggleModal= () => {
    this.setState({ isToggleModalOpen: true })
  }

  hideToggleModal=() => {
    this.setState({ isToggleModalOpen: false })
  }

  hideToggleModalEmptyImage=() => {
    this.setState({ showErrorAlertImage: false, isToggleModalOpen: false })
  }

  uploadToggleToStorage = async () => {
    const {
      elementParent,
      selectedElement: banner,
      downloadParameters: { resourceName, storageContainer },
    } = this.props

    const { path } = this.state
    const [elementsFromStaging, elementsFromProd] = await Promise.all([
      this.clientBFF.download({
        resourceName,
        storageContainer,
      }, { path, isProduction: false }),

      this.clientBFF.download({
        resourceName,
        storageContainer,
      }, { path }),
    ])

    const selectedElementFromStaging = elementsFromStaging[elementParent][banner.area]
    const selectedElementFromProd = elementsFromProd[elementParent][banner.area]
    elementsFromStaging[elementParent][banner.area] = { ...selectedElementFromStaging, active: !banner.active }
    elementsFromProd[elementParent][banner.area] = { ...selectedElementFromProd, active: !banner.active }

    const fileForStaging = JSONToFile(elementsFromStaging, resourceName)
    const fileForProd = JSONToFile(elementsFromProd, resourceName)
    const formDataForStaging = generateJSONFormData(fileForStaging, storageContainer)
    const formDataForProd = generateJSONFormData(fileForProd, storageContainer)

    await Promise.all([
      this.clientBFF.upload(formDataForProd),
      this.clientBFF.upload(formDataForStaging, { isProduction: false }),
    ])
    return Promise.resolve()
  }

  updateToggle = () => {
    const { toggleConfig: { handleEditSelectedBannerToggle, retrieveData } } = this.props
    this.setState({ showLoader: true }, async () => {
      await this.uploadToggleToStorage()
      await retrieveData()
      handleEditSelectedBannerToggle()
      this.setState({ showLoader: false, isToggleModalOpen: false })
      return true
    })
  }

  closeToggleModalAndUpdateBanner = () => {
    const { toggleConfig: { handleEditSelectedBannerToggle } } = this.props
    handleEditSelectedBannerToggle()
    this.hideToggleModal()
  }

  shouldHandleToggleInformation=() => {
    const { originalToggleState } = this.state
    const { toggleConfig: { isActive } } = this.props
    const nextToggleState = !isActive
    return (originalToggleState !== nextToggleState)
  }

  returnToggleModal = () => {
    const { isToggleModalOpen, showLoader, showErrorAlertImage } = this.state
    const { toggleConfig: { isActive, toggleTag } } = this.props

    if (isActive && this.shouldHandleToggleInformation()) {
      return (
        <ConfirmationModal
          modalIsOpen={isToggleModalOpen}
          continueProcess={this.updateToggle}
          hideConfirmationModal={this.hideToggleModal}
          showLoader={showLoader}
          confirmationText={`${isActive ? `Al desactivar ${toggleTag} este  se dejará de ver en producción inmediatamente.` : `Al activar el ${toggleTag} este se verá producción inmediatamente.`} `}
          confirmationButton={isActive ? 'desactivar' : 'activar'}
        />
      )
    }
    if (!isActive && !showErrorAlertImage && this.isEmptyBannerImage()) {
      return (
        <ConfirmationModal
          modalIsOpen={isToggleModalOpen}
          continueProcess={this.hideToggleModal}
          hideConfirmationModal={this.hideToggleModal}
          confirmationText="Recuerda que para poder activar esta Cuenta Regresiva, primero debes agregar las imágenes en versión desktop y mobile."
          confirmationButton="aceptar"
          hideCancelButtom
          headerText={`Activar ${toggleTag}`}
        />
      )
    }
    if (showErrorAlertImage) {
      return (
        <ConfirmationModal
          modalIsOpen={isToggleModalOpen}
          continueProcess={this.hideToggleModalEmptyImage}
          hideConfirmationModal={this.hideToggleModalEmptyImage}
          confirmationText="Recuerda que para poder probar esta Cuenta Regresiva en staging, primero debes agregar las imágenes en versión desktop y mobile."
          confirmationButton="aceptar"
          hideCancelButtom
          headerText={`Probar ${toggleTag}`}
        />
      )
    }
    return (
      <ConfirmationModal
        modalIsOpen={isToggleModalOpen}
        continueProcess={this.closeToggleModalAndUpdateBanner}
        hideConfirmationModal={this.closeToggleModalAndUpdateBanner}
        confirmationText="Recuerda utilizar la opción de probar en staging para visualizar estos cambios."
        confirmationButton="aceptar"
        hideCancelButtom
        headerText={`Activar ${toggleTag}`}
      />
    )
  }

  render() {
    const {
      showLoader, testedStaging, isUploadToProductionModalOpen, isProductionChecked, showErrorAlert, showSuccessAlert, showConflictDatesErrorAlert,
    } = this.state
    const {
      title, backArrowHandler, children, className, selectedElement,
      toggleConfig: {
        isActive, hasToggle, area, tooltipText,
      },
    } = this.props

    return (
      <Card className={className} style={{ position: 'unset' }}>
        {showErrorAlert && (
        <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
          ¡Lo sentimos! Algo salió mal.
        </CustomAlert>
        )}
        {showSuccessAlert && (
        <CustomAlert type="success" onClick={this.hideCustomAlerts}>
          <b>¡Tus cambios han subido a producción correctamente! </b>
        </CustomAlert>
        )}
        {showConflictDatesErrorAlert && (
        <CustomAlert type="danger" onClick={this.hideCustomAlerts}>
          ¡Lo sentimos! Algo salió mal, conflicto de fechas.
        </CustomAlert>
        )}
        <HeaderContainer className="justify-content-between mb-10">
          <div className="d-flex">
            <BackArrow
              className="text-center"
              onClick={backArrowHandler}
            />
            <Title>
              {title}
            </Title>
          </div>
          {
            hasToggle && (
              <Toggle
                id="toggle-banner"
                isActive={isActive}
                clickHandler={(e) => { e.stopPropagation(); this.showToggleToggleModal() }}
                area={area}
                tooltipText={tooltipText}
              />
            )
          }
        </HeaderContainer>
        {children}

        {showLoader && (
          <div className="row">
            <CircularProgress
              className="progress-primary"
              size={26}
              mode="determinate"
              value={75}
              style={{ color: '#0071ce', margin: 'auto' }}
            />
          </div>
        )}
        {!showLoader && (
          <div className="d-flex flex-column">
            <ActionButton
              className="text-center"
              type="primary"
              style={{
                width: '100%', color: '#fff', margin: 'auto', marginBottom: '10px',
              }}
              onClick={this.uploadToStaging}
            >
              probar en staging
            </ActionButton>
            <ActionButton disabled={!testedStaging} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.showUploadToProductionConfirmationModal}>
              pasar a producción
            </ActionButton>
          </div>
        )}
        <Modal
          modalIsOpen={isUploadToProductionModalOpen}
          closeModalAction={this.hideUploadToProductionConfirmationModal}
          style={{
            content: {
              width: '470px',
            },
          }}
        >
          <ModalContainer>
            <ModalTitle>¿Listo para pasar a producción?</ModalTitle>
            <div className="d-flex">
              <Checkbox
                checked={isProductionChecked}
                onClick={this.toggleProductionCheckbox}
              >
                {isProductionChecked && <i className="zmdi zmdi-check" />}
              </Checkbox>

              <span>Confirmo que he revisado los cambios realizados en Staging y estoy seguro que no hay errores.</span>
            </div>
            {selectedElement.active !== undefined && (
              <span style={{
                fontSize: '12px',
                marginTop: '12px',
                textAlign: 'center',
              }}
              >
                Importante: recuerda que para ver los cambios en producción, el banner debe estar activado.
              </span>
            )}
            <div className="d-flex flex-column">
              {showLoader && (
                <div className="row">
                  <CircularProgress
                    className="progress-primary"
                    size={26}
                    mode="determinate"
                    value={75}
                    style={{ color: '#0071ce', margin: 'auto', marginTop: '28px' }}
                  />
                </div>
              )}
              {!showLoader && (
                <Fragment>
                  <ActionButton
                    className="text-center"
                    type="primary"
                    style={{
                      width: '100%', color: '#fff', margin: 'auto', marginBottom: '10px', marginTop: '28px',
                    }}
                    disabled={!isProductionChecked}
                    onClick={this.uploadToProduction}
                  >
                    pasar a producción
                  </ActionButton>
                  <ActionButton
                    className="text-center"
                    type="secondary"
                    style={{ width: '100%', color: '#0071c3', margin: 'auto' }}
                    onClick={this.hideUploadToProductionConfirmationModal}
                  >
                    cancelar
                  </ActionButton>
                </Fragment>
              )}
            </div>
          </ModalContainer>
        </Modal>
        { this.returnToggleModal() }
      </Card>
    )
  }
}

export default CustomEditCard

CustomEditCard.defaultProps = {
  backArrowHandler: () => {},
  className: '',
  children: <div />,
  title: '',
  downloadParameters: {
    resourceName: '',
    storageContainer: '',
    contentToExtract: [],
  },
  toggleConfig: {
    isActive: false,
    hasToggle: false,
    toggleClickHandler: () => {},
    area: undefined,
    tooltipText: undefined,
    handleEditSelectedBannerToggle: () => {},
  },
  setSelectedBannerIndex: null,
}
CustomEditCard.propTypes = {
  title: PropTypes.string,
  backArrowHandler: PropTypes.func,
  children: PropTypes.object,
  className: PropTypes.string,
  selectedElementKey: PropTypes.string.isRequired,
  selectedElement: PropTypes.object.isRequired,
  elementParent: PropTypes.string.isRequired,
  setSelectedBannerIndex: PropTypes.func,
  downloadParameters: PropTypes.shape({
    resourceName: PropTypes.string.isRequired,
    storageContainer: PropTypes.string.isRequired,
    contentToExtract: PropTypes.array,
  }),
  toggleConfig: shape({
    isActive: PropTypes.bool,
    hasToggle: PropTypes.bool,
    toggleClickHandler: PropTypes.func,
    tooltipText: PropTypes.string,
    area: PropTypes.string,
  }),
}
