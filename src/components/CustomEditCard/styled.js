import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Title = styled.div`
    margin: 0; 
    margin-left: 5px;
    font-size: 20px;
    border: 0; padding: 0;
    font-weight: bold;
`

export const ModalTitle = styled.span`
 font-size: 20px;
 font-weight: bold;
 text-align: center;
 color: #414042;
 margin-bottom: 20px;
`

export const ModalContainer = styled.div`
  margin: 45px 45px 65px 45px;
  color: #414042;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  display: flex;
  flex-direction: column;
`

export const Checkbox = styled.div`
  width: 20px;
  height: 20px;
  margin-right: 10px;
  margin-top: 2px;
  border-radius: 4px;
  border: solid 1px #0071c3;
  background-color: ${props => (props.checked ? '#0071c3' : 'white')};
  flex: none;
  cursor: pointer;
  color: white;
  font-weight: bold;
  
  i {
      margin: 0 0 0 3px;
  }
}
`

export const HeaderContainer = styled.div`
  display: flex; 
  width: 100%; 
  align-items: center; 
  justify-content: space-between;
  margin-bottom: 10px;
`
