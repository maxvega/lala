import React from 'react'
import Select from 'react-select'

const customStyles = {
  control: base => ({
    ...base,
    borderRadius: '30px',
    border: '1px solid #0071c3',
    '&:hover': {
      border: '1px solid #0071c3',
    },
  }),
  menu: base => ({
    ...base,
    borderRadius: 0,
    marginTop: 0,
  }),
  menuList: base => ({
    ...base,
    padding: 0,
  }),
}
const CustomSelect = ({ ...props }) => (<Select styles={customStyles} {...props} isSearchable={false} />)

export default CustomSelect
