import styled from 'styled-components'

const Input = styled.input`
    border: solid 1px #979797;
    border-radius: 6px;
    background-color: #ffffff;
    padding: 10px;
    color: #424242;
    margin: 10px 0 10px 0;
    font-size: 14px;
    width: 100%;
    
   :focus {
      outline: none;
    }
`
export default Input
