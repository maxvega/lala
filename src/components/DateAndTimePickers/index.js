import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import './styles.css'
import PropTypes from 'prop-types'

// eslint-disable-next-line no-unused-vars
const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    fontSize: '14px',
    marginTop: '10px',
  },
  textField: {
    width: '100%',
    color: '#fffff',
    borderStyle: 'none',
    marginTop: '0px',
    marginBottom: '0px',
  },

}))

export default function DateAndTimePickers(props) {
  const classes = useStyles()

  const {
    defaultDate, handleAction, name, area,
  } = props


  const [selectedDate, setSelectedDate] = React.useState(defaultDate)

  const handleDateChange = (event) => {
    const { target: { value } } = event
    setSelectedDate(value)
    handleAction(event.target, event.target.name)
  }

  return (
    <form className={classes.container} noValidate>
      <TextField
        data-testid={name}
        key={`datetime-local-${name}-${area}`}
        name={name}
        id={`datetime-local-${name}-${area}`}
        locale="es"
        type="datetime-local"
        defaultValue={selectedDate}
        className={classes.textField}
        variant="outlined"
        InputLabelProps={{
          shrink: false,
          locale: 'es',
        }}
        onBlur={e => handleDateChange(e)}
      />
    </form>
  )
}

DateAndTimePickers.defaultProps = {
  defaultDate: null,
  area: null,
}
DateAndTimePickers.propTypes = {
  name: PropTypes.string.isRequired,
  area: PropTypes.number,
  handleAction: PropTypes.func.isRequired,
  defaultDate: PropTypes.string,
}
