import styled from 'styled-components'
import React from 'react'
import PropTypes from 'prop-types'
import { UncontrolledTooltip } from 'reactstrap'

const RoundIconDiv = styled.div`
    width: ${props => (props.width)};
    height: ${props => (props.height)};
    text-align: center !important;
    background-color: ${props => (props.backgroundColor)};
    color: ${props => (props.color)};
    border-radius: 20px;
    padding: 3px 0px;
    cursor: pointer;
`

const RoundIconWrapper = ({ ...props }) => {
  const width = window.innerWidth
  return (
    <RoundIconDiv {...props}>
      {props.children}
      {
      props && props.id && props.tooltipText && width > 767 && (
        <UncontrolledTooltip placement="right" target={props.id}>
          {props.tooltipText}
        </UncontrolledTooltip>
      )
    }
    </RoundIconDiv>
  )
}
export default RoundIconWrapper

RoundIconWrapper.defaultProps = {
  backgroundColor: '#0071ce',
  color: '#ffffff',
  tooltipText: '',
  children: <div />,
  id: '',
  width: '27px',
  height: '27px',
}

RoundIconWrapper.propTypes = {
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  id: PropTypes.string,
  tooltipText: PropTypes.string,
  children: PropTypes.object,
  width: PropTypes.string,
  height: PropTypes.string,
}
