import styled from 'styled-components'

export const customModalStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    padding: '0',
    display: 'flex',
    flexDirection: 'column',
    height: 'auto',
    maxWidth: '760px',
    width: '760px',
  },
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    zIndex: '10',
  },
}

export const SearchInput = styled.input`
    padding: 0.3rem 2.2rem;
    width: 100%;
    position: relative;
    background-color: #fff;
    border: 1px solid #0071c3;
    border-radius: 21px;
  `
export const SearchTitle = styled.h2`
    font-weight: bold;
    margin-bottom: 15px;
    color: #414042;
    font-size: 24px;
  `

export const SearchContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: left;
    height: 100%;
    padding: 20px;
    padding-top: 12px;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    width: 100%;
    padding-bottom: 60px;

    &.clearfix:after {visibility: hidden;}

    :after {
      content:"";
      position:absolute;
      bottom:0;
      left:0;
      height:66px;
      width:100%;
      background: -moz-linear-gradient(top,  rgba(255,255,255,0) 0%, rgba(255,255,255,0.83) 83%, rgba(255,255,255,1) 100%); /* FF3.6-15 */
      background: -webkit-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.83) 83%,rgba(255,255,255,1) 100%); /* Chrome10-25,Safari5.1-6 */
      background: linear-gradient(to bottom,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.83) 83%,rgba(255,255,255,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
    }
  `

export const CloseModalButton = styled.button`
    background: none!important;
    font-size: 14px;
    border: none;
    cursor: pointer;
    padding: 0;
    color: slategray;
    font-size: 22px;
  `

export const NoElementsFound = styled.div`
  display: flex;
  flex-direction: column;
  flex: 0 0 70.66667%;
  justify-content: center;
  `

export const ModalHeader = styled.div`
  display: flex;
  flex-direction: column;
  flex-flow: wrap;
  align-items: flex-start;
  width: 100%;
  min-height: 110px;
  padding: 20px;
  box-shadow: 0 4px 6px -1px rgba(220,213,213,0.5);
`
export const ModalHeaderControlsContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  margin: 6px;
  padding-right: 12px;
  margin-bottom: 2px;
`

export const ModalHeaderContentContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-flow: wrap;
  align-items: flex-end;
`

export const SingleElementFoundMessage = styled.span`
  display: flex;
  align-items: flex-start;
  margin-top: 16px;
  padding: 14px;
  border-radius: 4px;
  background-color: rgba(0, 113, 195, 0.1);
  word-wrap:break-word;
  white-space: normal;
  font-family: Bogle;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  color: #0071c3;
`
