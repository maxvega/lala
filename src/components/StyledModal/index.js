import React, { PureComponent } from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'
import { customModalStyles, CloseModalButton, ModalHeaderControlsContainer } from './styled'

import './styles.css'

Modal.setAppElement('#root')

class StyledModal extends PureComponent {
  render() {
    const {
      modalIsOpen, children, closeModalAction, style,
    } = this.props

    const styles = {
      content: { ...customModalStyles.content, ...style.content },
      overlay: { ...customModalStyles.overlay, ...style.overlay },
    }

    return (
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModalAction}
        style={styles}
        shouldCloseOnEsc
        shouldCloseOnOverlayClick
        shouldFocusAfterRender
        closeTimeoutMS={200}
      >
        <ModalHeaderControlsContainer>
          <CloseModalButton
            type="button"
            onClick={closeModalAction}
            className="zmdi zmdi-close"
          />
        </ModalHeaderControlsContainer>
        {children}
      </Modal>
    )
  }
}

export default StyledModal

StyledModal.propTypes = {
  children: PropTypes.node.isRequired,
  modalIsOpen: PropTypes.bool.isRequired,
  closeModalAction: PropTypes.func.isRequired,
  style: PropTypes.object,
}

StyledModal.defaultProps = {
  style: {},
}
