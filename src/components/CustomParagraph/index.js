import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RoundIconWrapper from '../RoundIconWrapper'

class CustomParagraph extends Component {
  render() {
    const {
      paragraphTitle, clickHandler, iconCustomStyle, parentCustomStyle, paragraphStyle, iconTooltipText, iconTooltipId, icon,
    } = this.props
    return (
      <div className="d-flex" style={parentCustomStyle}>
        <p className="banner-paragraph" style={paragraphStyle}>{paragraphTitle}</p>
        <RoundIconWrapper tooltipText={iconTooltipText} id={iconTooltipId} width="19px" height="19px" className="icon-resize-container" style={iconCustomStyle}>
          <i className={icon} onClick={() => clickHandler()} />
        </RoundIconWrapper>
      </div>
    )
  }
}

CustomParagraph.propTypes = {
  icon: PropTypes.string.isRequired,
  paragraphTitle: PropTypes.string,
  iconTooltipText: PropTypes.string,
  iconTooltipId: PropTypes.string,
  clickHandler: PropTypes.func,
  iconCustomStyle: PropTypes.object,
  parentCustomStyle: PropTypes.object,
  paragraphStyle: PropTypes.object,
}

CustomParagraph.defaultProps = {
  paragraphTitle: '',
  iconTooltipText: '',
  iconTooltipId: '',
  clickHandler: () => {},
  iconCustomStyle: {},
  parentCustomStyle: {},
  paragraphStyle: {},
}

export default CustomParagraph
