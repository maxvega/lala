import styled from 'styled-components'
import React from 'react'
import PropTypes from 'prop-types'

const backgroundColors = {
  primary: '#0071ce',
  warning: '#ffc107',
  danger: '#ffe6e6',
  info: '#fff',
  success: '#caf6ef',
}

const borderColors = {
  primary: '#0071ce',
  warning: '#ffc107',
  danger: '#cd3434',
  info: '#0071ce',
  success: '#02aa8f',
}

const textColors = {
  primary: '#0071ce',
  warning: '#ffc107',
  danger: '#cd3434',
  info: '#0071ce',
  success: '#02aa8f',
}

const CustomAlertDiv = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    top: 20px;
    width: ${props => (props.width)};
    height: ${props => (props.height)};
    border: solid 1.5px ${props => (props.type && borderColors[props.type] ? borderColors[props.type] : '#0071ce')};
    background-color: ${props => (props.type && backgroundColors[props.type] ? backgroundColors[props.type] : '#0071ce')};
    color: ${props => (props.type && textColors[props.type] ? textColors[props.type] : '#000')};
    font-size: 16px;
    font-weight: bold;
    line-height: 22px;
    cursor: pointer;
    border-radius: 50px;
    padding: 3px 11px;
    margin: auto;
    @media (max-width: 768px) {
      font-size: 14px;
      right: 0;
      top: -13vh;
    }
`

const CustomAlert = ({ ...props }) => (
  <CustomAlertDiv {...props}>
    <div className="d-flex">
      <div>{props.children}</div>
      <div className="pull-right" style={{ alignSelf: 'center' }}>
        <i style={{ marginLeft: '5px' }} className={props.icon} />
      </div>
    </div>

  </CustomAlertDiv>
)
export default CustomAlert

CustomAlert.defaultProps = {
  backgroundColor: '#0071ce',
  color: '#ffffff',
  width: 'fit-content',
  text: '',
  icon: 'zmdi zmdi-close',
  onClick: () => {},
  children: {},
}

CustomAlert.propTypes = {
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  width: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.object,
  onClick: PropTypes.func,
}
