import styled from 'styled-components'

const SideModal = styled.div`
    position: fixed;
    background: #fff;
    top: 0;
    right: 0px;
    bottom: 0;
    width: 450px;
    height: 100%;
    z-index: 11;
    padding: 20px;
    overlay: 1;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
    transform: translate3d(0, 0, 0);
    transition: opacity 250ms ease-out, transform 250ms ease-out;
    overflow: auto;
    @media (max-width: 768px) {
      width: 100%;
      z-index:1000;
    }
`
export default SideModal
