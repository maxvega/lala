import React, { Component, Fragment } from 'react'
import FeedbackModal from '../FeedbackModal'

function withFeedbackModal(WrappedComponent) {
  return class extends Component {
    constructor(props) {
      super(props)
      this.state = {
        isFeedbackModalOpen: false,
        feedbackData: {
          title: '',
          message: '',
        },
      }
    }

    toggleFeedbackModal = () => {
      const { isFeedbackModalOpen } = this.state
      this.setState({ isFeedbackModalOpen: !isFeedbackModalOpen })
    }

    handleFeedback = async (feedbackData) => {
      const { isFeedbackModalOpen } = this.state
      return this.setState({ feedbackData, isFeedbackModalOpen: !isFeedbackModalOpen })
    }

    render() {
      const { feedbackData, isFeedbackModalOpen } = this.state
      return (
        <Fragment>
          <WrappedComponent
            handleFeedback={this.handleFeedback}
            {...this.props}
          />
          <FeedbackModal
            title={feedbackData.title}
            message={feedbackData.message}
            modalIsOpen={isFeedbackModalOpen}
            handleClose={this.toggleFeedbackModal}
            confirmationHandler={feedbackData.confirmationHandler}
            confirmationData={feedbackData.confirmationData}
          />
        </Fragment>
      )
    }
  }
}

export default withFeedbackModal
