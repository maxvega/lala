import styled from 'styled-components'

const PageTitle = styled.h1`
    font-size: 20px;
    color: #424242;
    margin-bottom: 16px;
`
export default PageTitle
