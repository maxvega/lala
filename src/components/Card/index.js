import styled from 'styled-components'

const Card = styled.div`
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    padding: 20px;
    background-color: #ffffff;
    @media (max-width: 768px) {
      padding: 20px 8px;
    }
`
export default Card
