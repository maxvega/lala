import styled from 'styled-components'

const Element = styled.div`
    border: solid 0.5px #979797;
    background-color: #ffffff;
    border-radius: 6px;
    padding: 10px 15px;
    color: #424242;
    margin: 10px 0 10px 0;
    font-size: 14px;
`
export default Element
