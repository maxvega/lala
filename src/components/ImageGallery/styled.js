import styled from 'styled-components'

export const Title = styled.div`
  width: 130px;
  height: 20px;
  font-size: 14px;
  font-weight: bold;
  color: #424242;
`

export const EditMapSection = styled.div`
  border-radius: 4px;
  background-color: #f9f9f9;
  display: flex;
  place-content: center;
  padding: 0 20px;
  margin-bottom: 10px;
`


export const ConditionTitle = styled.div`
  font-size: 12px;
  font-weight: bold;
  line-height: 11px;
  color: #0071c3;
  margin-bottom: 4px;
`

export const ConditionText = styled.div`
  font-size: 10px;
  font-style: italic;
  line-height: 11px;
  color: #4c4c4c;
`

export const ConditionContainer = styled.div`
  padding: 20px 0;
`

export const MapImageContainer = styled.div`
  align-self: center;
  margin-right: 10px;
`
