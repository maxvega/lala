import React, { Component, Fragment } from 'react'
import { AutoSizer } from 'react-virtualized'
import { Scrollbars } from 'react-custom-scrollbars'
import PropTypes from 'prop-types'
import ImageContainer from '../ImageContainer'
import {
  Title, EditMapSection, ConditionTitle, ConditionText, ConditionContainer, MapImageContainer,
} from './styled'
import './index.css'

class ImageGallery extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentImage: '',
    }
  }

  addDefaultSrc = (e) => {
    e.target.src = '/images/transparentSquare.png'
  }

  handleImageSelectedFromGallery = (image) => {
    const { selectedImageHandler } = this.props
    const { currentImage } = this.state
    const newImage = currentImage === image ? '' : image
    this.setState({ currentImage: newImage })
    selectedImageHandler(image)
  }

  render() {
    const {
      images, title, handleImageToDelete, imageSection, device, selectedMobileImage, selectedDesktopImage,
    } = this.props
    const { currentImage } = this.state
    const isMobile = device === 'mobile'

    let selectedImage = currentImage
    if (!selectedImage) {
      selectedImage = isMobile ? selectedMobileImage : selectedDesktopImage
    }

    return (
      <Fragment>
        <EditMapSection>
          <MapImageContainer>
            <img style={{ height: isMobile ? '70px' : '50px' }} className="img-fluid" alt={imageSection} src={`/images/${imageSection}.svg`} onError={(e) => { this.addDefaultSrc(e) }} />
          </MapImageContainer>
          <ConditionContainer>
            <ConditionTitle>Condiciones de subida:</ConditionTitle>
            <ConditionText>
              {'- No debe exceder los '}
              <b>200Kb</b>
            </ConditionText>
            <ConditionText>
              {'- Formato '}
              <b>.jpg .png o .gif</b>
            </ConditionText>
            <ConditionText>
              {'- '}
              <b>Proporción según se muestra.</b>
            </ConditionText>
          </ConditionContainer>
        </EditMapSection>
        <Title>{title}</Title>
        <div className="mb-20" style={{ height: 'calc(100vh - 450px)' }}>
          <AutoSizer disableWidth>
            {({ height }) => (
              <Scrollbars
                className="rct-scroll"
                autoHeight
                autoHeightMax={height}
                autoHide
              >
                <div className="gallery-container-grid">
                  {images.map((image, index) => (
                    <ImageContainer
                      key={`image-${image}-${index}`}
                      handleImageToDelete={handleImageToDelete}
                      image={image}
                      selectedImageHandler={this.handleImageSelectedFromGallery}
                      isSelected={selectedImage === image}
                    />
                  ))}
                </div>
              </Scrollbars>
            )}
          </AutoSizer>
        </div>
      </Fragment>
    )
  }
}

ImageGallery.defaultProps = {
  images: [],
  title: '',
  device: '',
  selectedMobileImage: '',
  selectedDesktopImage: '',
  imageSection: '',
  selectedImageHandler: () => {},
  handleImageToDelete: () => {},
}
ImageGallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string,
  imageSection: PropTypes.string,
  device: PropTypes.string,
  selectedMobileImage: PropTypes.string,
  selectedDesktopImage: PropTypes.string,
  selectedImageHandler: PropTypes.func,
  handleImageToDelete: PropTypes.func,
}

export default ImageGallery
