import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DataTable from 'react-data-table-component'

class DynamicTable extends Component {
  render() {
    const { title, columns, data } = this.props
    return (
      <div>
        <DataTable
          id="dinamic-data-table"
          title={title}
          columns={columns}
          data={data}
          sortable
          pagination
          progressPending={data.length === 0}
        />
      </div>
    )
  }
}

export default DynamicTable

DynamicTable.defaultProps = {
  data: [],
  title: '',
  columns: [],
}

DynamicTable.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string,
  columns: PropTypes.array,
}
