import styled from 'styled-components'

const MainContainer = styled.main`
    position: relative;
    transition: all .15s;
    padding: 40px 20px 0px 20px;
    margin-left: ${props => (props.expanded ? '240' : '50')}px;
    @media (max-width: 768px) {
      margin-left: 50px;
      padding: 40px 4px 0px 12px;      
    }
`
export default MainContainer
