import styled from 'styled-components'

export const Container = styled.div`
    border: solid 0.5px #979797;
    background-color: #ffffff;
    margin: 10px 0 10px 0;
    height: 90px;
    width: 10vw;
    padding: 3px 2px;
    border-radius: 5px;
    flex-direction: column;
    position: relative;
     @media (max-width: 768px) {
      width: 25vw;
    }
`

export const LowerRightContainer = styled.div`
  display: flex; 
  position: absolute; 
  bottom: 0;
  right: 0;
`

export const UpperRightContainer = styled.div`
  position: absolute; 
  top: 5px; 
  right: 5px;
  max-height: 19px;
`
