import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  setModalImageToDisplay,
  setModalImageOpen,
} from '../../Redux/actions/setModalImage'
import RoundIconWrapper from '../RoundIconWrapper'
import '../../index.css'
import {
  Container,
  LowerRightContainer,
  UpperRightContainer,
} from './styled'

const configData = window.__ENV__

class ImageContainer extends Component {
  setImageToDisplayInModal = (e, image) => {
    e.stopPropagation()
    const { setModalImageToDisplay, setModalImageOpen } = this.props
    setModalImageToDisplay(image)
    setModalImageOpen()
  }

  render() {
    const {
      image, isSelected, selectedImageHandler, handleImageToDelete,
    } = this.props
    return (
      <Fragment>
        <Container className="d-flex pointer" onClick={() => { selectedImageHandler(image) }}>
          <UpperRightContainer>
            {isSelected ? (
              <RoundIconWrapper className="icon-resize-container" width="16px" height="16px">
                <i className="zmdi zmdi-check" />
              </RoundIconWrapper>
            ) : (
              <div
                style={{
                  width: '15px', height: '15px', border: '1px solid gray', borderRadius: '50%', background: '#fff',
                }}
              />
            )}
          </UpperRightContainer>
          <div
            style={{
              maxHeight: '100%',
              display: 'flex',
              justifyContent: 'center',
            }}

          >
            <img alt={image} style={{ maxHeight: '100%' }} className="img-fluid" src={`${configData.storageBaseUrl}/${image}`} />
          </div>
          <LowerRightContainer>
            <RoundIconWrapper width="19px" height="19px" backgroundColor="#041e42" style={{ marginRight: '5px' }} className="icon-resize-container" onClick={(e) => { this.setImageToDisplayInModal(e, image) }}>
              <i className="zmdi zmdi-search" />
            </RoundIconWrapper>
            <RoundIconWrapper width="19px" height="19px" backgroundColor="#eaeaea" color="#000000" className="icon-resize-container" onClick={(e) => { handleImageToDelete(e, image) }}>
              <i className="zmdi zmdi-delete" />
            </RoundIconWrapper>
          </LowerRightContainer>
        </Container>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setModalImageToDisplay: image => dispatch(setModalImageToDisplay(image)),
  setModalImageOpen: () => dispatch(setModalImageOpen()),
})

ImageContainer.propTypes = {
  image: PropTypes.string.isRequired,
  setModalImageToDisplay: PropTypes.func.isRequired,
  setModalImageOpen: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
  selectedImageHandler: PropTypes.func.isRequired,
  handleImageToDelete: PropTypes.func,
}

ImageContainer.defaultProps = {
  handleImageToDelete: () => {},
}

export default connect(null, mapDispatchToProps)(ImageContainer)
