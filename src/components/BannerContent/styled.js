import styled from 'styled-components'

export const ParagraphContainer = styled.div`
  display: flex; 
  position: relative;
  margin-bottom: 12px;
`

export const ParagraphText = styled.span`
  font-size: 14px;
  font-weight: normal;
  font-weight: ${props => (props.fontWeight ? props.fontWeight : 'normal')};
  color: #0071c3;
  margin-right: 3px;
  overflow: ${props => (props.addEllipsis ? 'hidden' : 'none')}; 
  text-overflow: ${props => (props.addEllipsis ? 'ellipsis' : 'none')};
  max-width: 100%;
  white-space: nowrap;

  @media (max-width: 850px) {
    font-size: 12px;
  }
`

export const ParagraphButton = styled.div`
  right: 0; 
  position: absolute;
  display: flex;
  justify-content: center;
  padding: 0px 8px 0px;
  background-color: ${props => (props.backgroundColor ? props.backgroundColor : '#ffff')};
  color: #0071c3;;
  border-radius: 9.5px;
  border: ${props => (props.border ? props.border : 'unset')};
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;

  @media (max-width: 850px) {
    font-size: 10px;
  }
`
