import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Moment from 'moment'
import {
  setModalImageToDisplay,
  setModalImageOpen,
} from '../../Redux/actions/setModalImage'
import RoundIconWrapper from '../RoundIconWrapper'
import StyledSeparator from '../StyledSeparator'
import { ParagraphContainer, ParagraphText, ParagraphButton } from './styled'


export const ContentRow = ({ ...props }) => {
  const {
    keyName, value, showActionButton, actionButtonHandler, actionButtonTitle, actionButtonStyles, decode, className,
  } = props
  return (
    <div className={className || ''} style={{ padding: 'unset' }}>
      <ParagraphContainer>
        <ParagraphText>
          {keyName}
        </ParagraphText>
        <ParagraphText fontWeight="bold" addEllipsis>
          {decode ? decodeURI(value) : value}
        </ParagraphText>
        {
          showActionButton && <ParagraphButton {...actionButtonStyles} onClick={actionButtonHandler}>{actionButtonTitle}</ParagraphButton>
        }
      </ParagraphContainer>
    </div>
  )
}

const BannerContentComponent = ({ ...props }) => {
  const {
    editDataHandler, editDesktopHandler, editMobileHandler, grid, setModalImageToDisplay, setModalImageOpen, activeEdit, showInfoSection, showUpdateField,
  } = props

  const {
    name, target, link, tag: gtm, backgroundDesktop: desktopImage, backgroundMobile: mobileImage, startDate, endDate, counterBgColor, counterFontColor, updated, order,
  } = grid

  return (
    <Fragment>
      {
        showInfoSection && (
          <Fragment>
            <StyledSeparator style={{ marginBottom: '10px' }} />
            <ContentRow keyName="Nombre:" value={name} showActionButton actionButtonHandler={editDataHandler} actionButtonTitle="editar datos" actionButtonStyles={{ backgroundColor: activeEdit === 'data' ? '#d9efff' : '#fff', border: activeEdit === 'data' ? 'none' : 'solid 0.5px #0071c3' }} />
            {target && <ContentRow keyName="Target:" value={target} />}
            {link && <ContentRow keyName="URL:" value={link} decode />}
            {order && <ContentRow keyName="Orden:" value={order} />}
            {gtm && <ContentRow keyName="Tag Google Analytics:" value={gtm} />}
            {startDate && <ContentRow keyName="Fecha de inicio:" value={Moment(startDate).format('DD-MM-YYYY - HH:mm:ss')} />}
            {endDate && <ContentRow keyName="Fecha de fin:" value={Moment(endDate).format('DD-MM-YYYY - HH:mm:ss')} />}
            {counterBgColor && <ContentRow keyName="Background color:" value={counterBgColor} />}
            {counterFontColor && <ContentRow keyName="Font color:" value={counterFontColor} />}
            {showUpdateField && <ContentRow keyName="Fecha de actualización:" value={Moment(updated).format('DD-MM-YYYY')} />}
            <StyledSeparator style={{ marginBottom: '10px' }} />
          </Fragment>
        )
      }
      <div style={{ display: 'flex', position: 'relative' }}>
        <ContentRow className="col-11" keyName="Imagen Desktop:" value={desktopImage} showActionButton actionButtonHandler={editDesktopHandler} actionButtonTitle={desktopImage === '' ? 'agregar imagen' : 'editar desktop'} actionButtonParameter="desktop" actionButtonStyles={{ backgroundColor: activeEdit === 'desktop' ? '#d9efff' : '#fff', border: activeEdit === 'desktop' ? 'none' : 'solid 0.5px #0071c3', width: '107px' }} />
        <RoundIconWrapper
          className="col-1"
          backgroundColor="#041e42"
          style={{
            marginRight: '5px', position: 'absolute', right: '0', display: 'flex', justifyContent: 'center', height: '19px', width: '19px',
          }}
          onClick={() => { setModalImageToDisplay(desktopImage); setModalImageOpen() }}
        >
          <i className="zmdi zmdi-search" />
        </RoundIconWrapper>
      </div>
      <div style={{ display: 'flex', position: 'relative' }}>
        <ContentRow className="col-11" keyName="Imagen Mobile:" value={mobileImage} showActionButton actionButtonHandler={editMobileHandler} actionButtonTitle={mobileImage === '' ? 'agregar imagen' : 'editar mobile'} actionButtonParameter="mobile" actionButtonStyles={{ backgroundColor: activeEdit === 'mobile' ? '#d9efff' : '#fff', border: activeEdit === 'mobile' ? 'none' : 'solid 0.5px #0071c3', width: '107px' }} />
        <RoundIconWrapper
          className="col-1"
          backgroundColor="#041e42"
          style={{
            marginRight: '5px', position: 'absolute', right: '0', display: 'flex', justifyContent: 'center', height: '19px', width: '19px',
          }}
          onClick={() => { setModalImageToDisplay(mobileImage); setModalImageOpen() }}
        >
          <i className="zmdi zmdi-search" />
        </RoundIconWrapper>
      </div>
    </Fragment>
  )
}

const mapDispatchToProps = dispatch => ({
  setModalImageToDisplay: image => dispatch(setModalImageToDisplay(image)),
  setModalImageOpen: () => dispatch(setModalImageOpen()),
})

export const BannerContent = connect(null, mapDispatchToProps)(BannerContentComponent)

BannerContentComponent.propTypes = {
  editDataHandler: PropTypes.func,
  editDesktopHandler: PropTypes.func,
  editMobileHandler: PropTypes.func,
  grid: PropTypes.object.isRequired,
  setModalImageToDisplay: PropTypes.func.isRequired,
  setModalImageOpen: PropTypes.func.isRequired,
  activeEdit: PropTypes.string,
  showInfoSection: PropTypes.bool.isRequired,
  showUpdateField: PropTypes.bool,
}
BannerContentComponent.defaultProps = {
  activeEdit: '',
  editDataHandler: () => {},
  editDesktopHandler: () => {},
  editMobileHandler: () => {},
  showUpdateField: false,
}

ContentRow.propTypes = {
  keyName: PropTypes.string,
  value: PropTypes.string,
  showActionButton: PropTypes.bool,
  actionButtonHandler: PropTypes.func,
  actionButtonTitle: PropTypes.string,
  decode: PropTypes.bool,
  className: PropTypes.string,
  actionButtonStyles: PropTypes.object,
}

ContentRow.defaultProps = {
  keyName: '',
  value: '',
  showActionButton: false,
  actionButtonTitle: '',
  actionButtonHandler: () => {},
  decode: false,
  className: '',
  actionButtonStyles: {},
}
