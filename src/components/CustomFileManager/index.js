import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import withFeedbackModal from '../WithFeedbackModal'
import Card from '../Card'
import ActionButton from '../ActionButton'
import FileInput from '../FileInput'
import CustomAlert from '../CustomAlert'

export class CustomFileManager extends Component {
  constructor(props) {
    super(props)
    this.state = {
      generateXLSXLoader: false,
      file: null,
      uploadToStagingLoader: false,
      uploadedToStaging: false,
      disableUploadToProductionButton: true,
      uploadToProductionLoader: false,
      showSuccessCustomAlert: false,
    }
  }

  handleChange = (e) => {
    e.preventDefault()
    const { files } = e.target
    if (files && files[0]) {
      this.setState({ file: files[0] })
    }
  }

  handleError = (message) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: message,
    })
  }

  handleLocalGenerateXLSXClick = async () => {
    const { handleGenerateXLSXClick } = this.props
    try {
      this.setState({ generateXLSXLoader: true }, async () => {
        try {
          await handleGenerateXLSXClick()
          this.setState({ generateXLSXLoader: false })
        } catch (e) {
          this.handleError('Error al descargar bundles 😔')
          this.setState({ generateXLSXLoader: false })
        }
      })
    } catch (e) {
      this.handleError('Error al descargar bundles 😔')
      this.setState({ generateXLSXLoader: false })
    }
  }

  handleLocalUploadToStaging = async () => {
    const { file } = this.state
    const { handleUploadToStaging } = this.props
    const reader = new FileReader()
    reader.onload = async (e) => {
      this.setState({ uploadToStagingLoader: true }, async () => {
        try {
          await handleUploadToStaging(e)
          this.setState({ uploadToStagingLoader: false, uploadedToStaging: true })
        } catch (e) {
          this.setState({ uploadToStagingLoader: false })
          this.handleError('Algo salió mal 😔, revisa el formato del archivo')
        }
      })
    }
    reader.readAsBinaryString(file)
  }

  handleLocalUploadToProduction = async () => {
    const { handleUploadToProduction } = this.props
    this.setState({
      uploadToProductionLoader: true,
    }, async () => {
      try {
        await handleUploadToProduction()
        this.setState({
          generateXLSXLoader: false,
          file: null,
          uploadToStagingLoader: false,
          uploadedToStaging: false,
          disableUploadToProductionButton: true,
          uploadToProductionLoader: false,
          showSuccessCustomAlert: false,
        })
        this.showSuccessCustomAlert()
      } catch (e) {
        this.setState({
          generateXLSXLoader: false,
          file: null,
          uploadToStagingLoader: false,
          uploadedToStaging: false,
          disableUploadToProductionButton: true,
          uploadToProductionLoader: false,
          showSuccessCustomAlert: false,
        })
        this.handleError('Algo salió mal en la actualización de bundles a producción 😔')
      }
    })
  }

  showSuccessCustomAlert = () => {
    this.setState({ showSuccessCustomAlert: true }, () => { setTimeout(() => { this.setState({ showSuccessCustomAlert: false }) }, 5000) })
  }

  renderSuccessAlert = () => {
    const { alertSucessTitle } = this.props.titlesConfig
    return (
      <CustomAlert type="success" onClick={this.hideCustomAlerts} style={{ marginBottom: '10px' }}>
        {alertSucessTitle}
      </CustomAlert>
    )
  }

  render() {
    const { titlesConfig } = this.props
    const {
      generateXLSXLoader, file, uploadToStagingLoader, uploadedToStaging, disableUploadToProductionButton, uploadToProductionLoader, showSuccessCustomAlert,
    } = this.state

    return (
      <div className="row">
        {showSuccessCustomAlert && this.renderSuccessAlert()}
        <Card className="col-sm-12 left-card-margin">
          <Fragment>
            <div className="row mb-10">
              <div className="col-sm-12">
                <h3 style={{ lineHeight: '40px' }}>{titlesConfig.headTitle}</h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 mb-20" style={{ borderRight: '1px solid black' }}>
                <div className="d-flex">
                  <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleLocalGenerateXLSXClick}>
                    {generateXLSXLoader ? (
                      <CircularProgress
                        className="progress-primary"
                        size={26}
                        mode="determinate"
                        value={75}
                        style={{ color: '#fff', margin: 'auto' }}
                      />
                    ) : titlesConfig.donwloadButtonText}
                  </ActionButton>

                </div>
              </div>
              <div className="col-md-6 mb-20">
                <div className="d-flex text-center mb-20">
                  <FileInput type="file" title="Subir archivo xlsx de bundles" accept=".xlsx" inputHandler={this.handleChange} />
                </div>
                {file && (
                  <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleLocalUploadToStaging}>
                    {uploadToStagingLoader ? (
                      <CircularProgress
                        className="progress-primary"
                        size={26}
                        mode="determinate"
                        value={75}
                        style={{ color: '#fff', margin: 'auto' }}
                      />
                    ) : titlesConfig.uploadStagingButtonText}
                  </ActionButton>
                )}
              </div>
            </div>
            {uploadedToStaging && (
              <div className="text-center">
                <div className="mb-20">
                  <input type="checkbox" onChange={() => this.setState({ disableUploadToProductionButton: !disableUploadToProductionButton })} />
                  {titlesConfig.checkInfoText}
                </div>
                <ActionButton disabled={disableUploadToProductionButton} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleLocalUploadToProduction}>
                  {uploadToProductionLoader ? (
                    <CircularProgress
                      className="progress-primary"
                      size={26}
                      mode="determinate"
                      value={75}
                      style={{ color: '#fff', margin: 'auto' }}
                    />
                  ) : titlesConfig.uploadProductionButtonText}
                </ActionButton>
              </div>
            )}
          </Fragment>
        </Card>
      </div>
    )
  }
}

CustomFileManager.propTypes = {
  handleUploadToProduction: PropTypes.func.isRequired,
  handleUploadToStaging: PropTypes.func.isRequired,
  handleGenerateXLSXClick: PropTypes.func.isRequired,
  handleFeedback: PropTypes.func.isRequired,
  titlesConfig: PropTypes.shape({
    alertSucessTitle: PropTypes.string.isRequired,
    headTitle: PropTypes.string.isRequired,
    donwloadButtonText: PropTypes.string.isRequired,
    uploadStagingButtonText: PropTypes.string.isRequired,
    checkInfoText: PropTypes.string.isRequired,
    uploadProductionButtonText: PropTypes.string.isRequired,
  }).isRequired,

}

export default withFeedbackModal(CustomFileManager)
