import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '../Card'
import ActionButton from '../ActionButton'
import FileInput from '../FileInput'
import ClientBFF from '../../common/ClientBFF'
import withFeedbackModal from '../WithFeedbackModal'

export class XLSXUploadHandler extends Component {
  constructor(props) {
    super(props)
    this.state = {
      file: null,
      uploadedToStaging: false,
      disableUploadToProductionButton: true,
      loader: { staging: false, production: false },
      loading: false,
    }
    this.clientBFF = new ClientBFF()
    this.config = typeof window !== 'undefined' ? window.__ENV__ : { }
  }

  handleChange = (e) => {
    e.preventDefault()
    const { files } = e.target
    if (files && files[0]) {
      this.setState({ file: files[0] })
    }
  }

  handleUpload = async (environment) => {
    const { file, loader } = this.state
    const {
      successMessage,
      failureMessage,
      UrlToOpenInProduction,
      openURLAfterUpload,
      UrlToOpenInStaging,
      uploadToStagingFirst,
      stagingSuccessMessage,
      bffRouteToUploadFile,
      tenant,
    } = this.props
    const isProduction = environment === 'production'

    const toRequest = {
      path: bffRouteToUploadFile,
      isProduction,
      fromXlsxUploadHandle: true,
      tenant,
    }

    const URLToOpen = isProduction ? UrlToOpenInProduction : UrlToOpenInStaging
    const feedbackSuccessMessage = isProduction ? successMessage : stagingSuccessMessage
    loader[environment] = true
    const states = { loader }

    this.setState({ loader }, async () => {
      const formData = new FormData()
      formData.append('file', file)
      this.clientBFF.upload(formData, toRequest).then(() => {
        this.handleMessage(feedbackSuccessMessage)
        if (openURLAfterUpload) {
          window.open(URLToOpen, '_blank')
        }

        if (uploadToStagingFirst && !isProduction) {
          states.uploadedToStaging = true
        }
      }).catch(() => {
        this.handleMessage(failureMessage)
      }).finally(() => {
        loader[environment] = false
        this.setState(states)
      })
    })
  }

  handleMessage = (message) => {
    const { handleFeedback } = this.props
    handleFeedback({
      title: message,
    })
  }

  showLoader = () => (
    <CircularProgress
      className="progress-primary"
      size={16}
      mode="determinate"
      value={75}
      style={{ color: '#fff', margin: 'auto' }}
    />
  )

  handleButtonClick= async () => {
    const {
      bffRouteToDownloadFile, fileName, failureMessage, tenant,
    } = this.props

    try {
      this.setState({ loading: true }, async () => {
        const toRequest = {
          path: bffRouteToDownloadFile,
          method: 'get',
          responseType: 'blob',
          fileName,
          tenant,
        }
        await this.clientBFF.download({}, toRequest)
        this.setState({ loading: false })
      })
    } catch (e) {
      this.handleMessage(failureMessage)
      this.setState({ loading: false })
    }
  }

  render() {
    const {
      title, downloadButtonText, uploadButtonText, uploadToStagingFirst, showDownloadButton,
    } = this.props
    const {
      file, loader, uploadedToStaging, disableUploadToProductionButton, loading,
    } = this.state
    const stagingLoader = loader.staging
    const productionLoader = loader.production
    const environment = uploadToStagingFirst ? 'staging' : 'production'

    return (
      <div className="row">
        <Card className="col-sm-12 left-card-margin">
          <Fragment>
            <div className="row mb-20">
              <div className="col-sm-12">
                <h3 style={{ lineHeight: '40px' }}>{title}</h3>
              </div>
            </div>
            <div className="row">
              {showDownloadButton && (
              <div className="col-md-6 mb-20 text-center" style={{ borderRight: '1px solid black' }}>

                <ActionButton className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={this.handleButtonClick}>
                  {
                    loading ? (
                      this.showLoader()
                    ) : downloadButtonText }
                </ActionButton>

              </div>
              )}
              <div className={`col-md-${showDownloadButton ? '6' : '12'} mb-20 text-center`}>
                <div className="d-flex text-center mb-20">
                  <FileInput type="file" title="Subir archivo xlsx" accept=".xlsx" inputHandler={this.handleChange} />
                </div>

                {file && (
                  <ActionButton className="text-center mb-20" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={() => { this.handleUpload(environment) }}>
                    {(stagingLoader || (productionLoader && !uploadedToStaging)) ? this.showLoader() : uploadButtonText}
                  </ActionButton>
                )}

                { uploadToStagingFirst && uploadedToStaging && (
                  <div className="text-center w-100">
                    <div className="mb-10">
                      <input type="checkbox" onChange={() => this.setState({ disableUploadToProductionButton: !disableUploadToProductionButton })} />
                      {' He revisado lo que voy a subir a producción.'}
                    </div>
                    <ActionButton disabled={disableUploadToProductionButton} className="text-center" type="primary" style={{ width: '100%', color: '#fff', margin: 'auto' }} onClick={() => { this.handleUpload('production') }}>
                      {productionLoader ? this.showLoader() : `${uploadButtonText} a producción`}
                    </ActionButton>

                  </div>
                )}
              </div>

            </div>
          </Fragment>
        </Card>
      </div>
    )
  }
}

XLSXUploadHandler.defaultProps = {
  downloadButtonText: 'Descargar',
  uploadButtonText: 'Actualizar',
  successMessage: 'Éxito subiendo el archivo a producción!',
  stagingSuccessMessage: 'Éxito subiendo el archivo a staging!',
  failureMessage: 'Algo salió mal :/',
  uploadToStagingFirst: true,
  showDownloadButton: true,
  openURLAfterUpload: false,
  UrlToOpenInStaging: '',
  UrlToOpenInProduction: '',
  fileName: 'file.xlsx',
  tenant: 'catalogo',
}


XLSXUploadHandler.propTypes = {
  title: PropTypes.string.isRequired,
  bffRouteToDownloadFile: PropTypes.string.isRequired,
  bffRouteToUploadFile: PropTypes.string.isRequired,
  fileName: PropTypes.string,
  handleFeedback: PropTypes.func.isRequired,
  downloadButtonText: PropTypes.string,
  uploadButtonText: PropTypes.string,
  successMessage: PropTypes.string,
  stagingSuccessMessage: PropTypes.string,
  failureMessage: PropTypes.string,
  uploadToStagingFirst: PropTypes.bool,
  openURLAfterUpload: PropTypes.bool,
  UrlToOpenInStaging: PropTypes.string,
  UrlToOpenInProduction: PropTypes.string,
  showDownloadButton: PropTypes.bool,
  tenant: PropTypes.oneOf(['catalogo', 'supermercado']),
}

export default withFeedbackModal(XLSXUploadHandler)
