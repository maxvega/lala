import styled from 'styled-components'

const InnerTitle = styled.h1`
    font-size: 16px;
    font-weight: bold;
    line-height: 40px;
`
export default InnerTitle
