import styled from 'styled-components'

const TextButton = styled.div`
    color: #0071ce;
    font-weight: bold;
    font-size: 16px;
    cursor: pointer;
`
export default TextButton
