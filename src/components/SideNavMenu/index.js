import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import SideNav, {
  Nav, NavItem, NavIcon, NavText,
} from '@trendmicro/react-sidenav'
import Loadable from 'loadable-components'
import { Route } from 'react-router-dom'
import { setDestroySession } from '../../Redux/actions/loginActions'
import ActionButton from '../ActionButton'
import StyledModal from '../StyledModal'
import navigationList from '../../navigationList'
import MainContainer from '../MainContainer'
import {
  // eslint-disable-next-line no-unused-vars
  calculateMenuToDisplay, cleanUserAndLogout, getAuthData,
} from '../../Helper'

import RoundIconWrapper from '../RoundIconWrapper'
import './index.css'

const HomeLoader = Loadable(() => import('../../RoutesPages/Home'))
const BundleCreatorLoader = Loadable(() => import('../../RoutesPages/BundleCreator'))
const UsersLoader = Loadable(() => import('../../RoutesPages/Users'))
const RolesLoader = Loadable(() => import('../../RoutesPages/Roles'))
const PermissionsLoader = Loadable(() => import('../../RoutesPages/Permissions'))
const ProfileLoader = Loadable(() => import('../../RoutesPages/Profile'))
const BciBannerLoader = Loadable(() => import('../../RoutesPages/BciBanner'))
const GridBannerLoader = Loadable(() => import('../../RoutesPages/GridBanner'))
const CountDownBannerLoader = Loadable(() => import('../../RoutesPages/CountDownBanner'))
const ProductsSelectionLoader = Loadable(() => import('../../RoutesPages/ProductsSelection'))
const ProductsReportLoader = Loadable(() => import('../../RoutesPages/ProductsReport'))
const RandomGeneratorLoader = Loadable(() => import('../../RoutesPages/RandomGenerator'))
const LowerBlocksLoader = Loadable(() => import('../../RoutesPages/LowerBlocks'))
const LineBreakersLoader = Loadable(() => import('../../RoutesPages/LineBreakers'))
const InspirationalBlocksLoader = Loadable(() => import('../../RoutesPages/InspirationalBlocks'))
const BannersHomeLoader = Loadable(() => import('../../RoutesPages/BannersHome'))
const HotjarCategoriesLoader = Loadable(() => import('../../RoutesPages/HotjarCategories'))
const LegalInfoLoader = Loadable(() => import('../../RoutesPages/LegalInfo'))
const XLSXUploadHandlerLoader = Loadable(() => import('../../RoutesPages/XLSXUploadHandler'))
const HeaderAlertMessageLoader = Loadable(() => import('../../RoutesPages/HeaderAlertMessage'))
const FreeShippingPromotionLoader = Loadable(() => import('../../RoutesPages/FreeShippingPromotion'))

class SideNavMenu extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      expanded: true,
      modalIsOpen: false,
    }
    this.config = typeof window !== 'undefined' ? window.__ENV__ : { }
  }

  onSelect = (selected) => {
    const { history, location } = this.props
    const to = `/${selected}`

    if (location.pathname !== to) {
      history.push(to)
    }
  }

  toggleModal = () => {
    const { modalIsOpen } = this.state
    this.setState({ modalIsOpen: !modalIsOpen })
  }

  onToggle = () => {
    const { expanded } = this.state
    this.setState({ expanded: !expanded })
  }

  handleLogOut = () => {
    const { history, setDestroySession } = this.props
    setDestroySession()
    cleanUserAndLogout(history)
  }

  renderLogo = () => (
    <Fragment>
      <div className="d-flex justify-content-between align-items-center">
        <div>
          <img width={130} className="img-fluid" alt="logo" src="/images/buysmart-logo.svg" />
        </div>
        <div>
          <RoundIconWrapper style={{ fontSize: '30px', width: '37px', height: '37px' }}>
            <i className="zmdi zmdi-chevron-left" />
          </RoundIconWrapper>
        </div>
      </div>
    </Fragment>
  )

  renderNavItemHome = pathname => (
    <NavItem eventKey="" active={pathname === '/'}>
      <NavIcon>
        <i style={{ fontSize: '20px' }} className="zmdi zmdi-home" />
      </NavIcon>
      <NavText>
        Inicio
      </NavText>
    </NavItem>
  )

  renderNavItemLogOut = () => (
    <div id="logout" role="presentation" className="sidenav---sidenav-navitem---uwIJ-" onClick={this.toggleModal}>
      <div className="sidenav---navitem---9uL5T" role="menuitem" tabIndex="-1">
        <div className="sidenav---navicon---3gCRo">
          <i
            className="zmdi zmdi-power"
            style={{ fontSize: '20px' }}
          />
        </div>
        <div className="sidenav---navtext---1AE_f">Cerrar sesión</div>
      </div>
    </div>
  )

  renderBurgerIcon = () => (
    <div>
      <RoundIconWrapper style={{ fontSize: '30px', width: '37px', height: '37px' }}>
        <i className="zmdi zmdi-chevron-right" />
      </RoundIconWrapper>
    </div>
  )

  renderNavigationItemsAccordingToPermissions = (pathname, isStrati) => {
    const permissions = this.getPermissions(isStrati)

    if (permissions && permissions.length > 0) {
      const menuToDisplay = calculateMenuToDisplay(permissions, navigationList)
      return (
        <Nav defaultSelected="home">
          {this.renderNavItemHome(pathname)}
          {menuToDisplay.map((menu, ix) => {
            const {
              eventKey, text, icon, children,
            } = menu
            return (
              <NavItem key={`navItem-${ix}`} eventKey={eventKey} active={pathname === `/${eventKey}`}>
                <NavIcon>
                  <i style={{ fontSize: '20px' }} className={`zmdi zmdi-${icon}`} />
                </NavIcon>
                <NavText>
                  {text}
                </NavText>
                {children && children.map((childData, ij) => {
                  const {
                    eventKey, text,
                  } = childData
                  return (
                    <NavItem key={`navChild-${ij}`} eventKey={eventKey} active={pathname === `/${eventKey}`}>
                      <NavText>
                        {text}
                      </NavText>
                    </NavItem>
                  )
                })}
              </NavItem>
            )
          })}
          {this.renderNavItemLogOut(pathname)}
        </Nav>
      )
    }
    return (
      <Nav defaultSelected="home">
        {this.renderNavItemHome(pathname)}
        {this.renderNavItemLogOut(pathname)}
      </Nav>
    )
  }


  getPermissions = (isStrati) => {
    const auth = getAuthData()

    if (!isStrati) {
      const { role = { permissions: [] } } = auth
      const { permissions } = role
      return permissions
    }

    const { user = {} } = auth || {}
    const roles = user.roles ? user.roles : []
    const flatPermissions = roles.map(role => role.permissions).flat()
    const setPermissions = new Set(flatPermissions.map(JSON.stringify))
    return Array.from(setPermissions).map(JSON.parse)
  }


  renderRoutesAccordingToPermissions = (expanded, isStrati) => {
    const permissions = this.getPermissions(isStrati)

    const filteredPermissions = permissions.map(permission => permission.name)

    return (
      <Fragment>
        <div className={expanded ? 'overlay-left-menu' : ''} />
        <MainContainer expanded={expanded}>
          <Route
            path="/app"
            exact
            render={props => (
              <HomeLoader {...props} />
            )}
          />
          <Route
            path="/app/products/bundle"
            exact
            render={(props) => {
              if (filteredPermissions.includes('bundles.create')) {
                return (<BundleCreatorLoader {...props} title="Crear Bundle" type="unit" />)
              } return null
            }}
          />
          <Route
            path="/app/products/bundles"
            exact
            render={(props) => {
              if (filteredPermissions.includes('bundles.create')) {
                return (<BundleCreatorLoader {...props} title="Carga Masiva de Bundles" />)
              } return null
            }}
          />
          {
                !isStrati
                && (
                <Fragment>
                  <Route
                    path="/app/users"
                    exact
                    render={(props) => {
                      if (filteredPermissions.includes('users.create')) {
                        return (<UsersLoader {...props} title="Usuarios" />)
                      }
                      return null
                    }}
                  />
                  <Route
                    path="/app/roles"
                    exact
                    render={(props) => {
                      if (filteredPermissions.includes('roles.create')) {
                        return (<RolesLoader {...props} title="Roles" />)
                      } return null
                    }}
                  />
                  <Route
                    path="/app/permissions"
                    exact
                    render={(props) => {
                      if (filteredPermissions.includes('permissions.create')) {
                        return (<PermissionsLoader {...props} title="Permisos" />)
                      } return null
                    }}
                  />

                </Fragment>
                )
            }

          <Route
            path="/app/profile"
            exact
            render={props => (<ProfileLoader {...props} title="Perfil" />)}
          />
          <Route
            path="/app/banners/banners-home"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<BannersHomeLoader {...props} title="Banners home" rootName="bannersHome" resourceName="banners_settings.json" />)
              } return null
            }}
          />
          <Route
            path="/app/banners/bci"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<BciBannerLoader {...props} title="Banners Lider BCI" rootName="bannersBCI" resourceName="banners_settings.json" showBannerInfoSection={false} />)
              } return null
            }}
          />
          <Route
            path="/app/banners/grid"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<GridBannerLoader {...props} title="Grilla descuentos imperdibles" rootName="grids" resourceName="grid_settings.json" />)
              } return null
            }}
          />
          <Route
            path="/app/banners/countdown"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<CountDownBannerLoader {...props} title="Cuentas regresivas" rootName="countdownBanner" resourceName="countdown_settings.json" />)
              } return null
            }}
          />
          <Route
            path="/app/banners/lower-blocks"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<LowerBlocksLoader {...props} title="Calugas Inferiores" rootName="lowerBlocks" resourceName="banners_settings.json" />)
              } return null
            }}
          />
          <Route
            path="/app/banners/line-breakers"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<LineBreakersLoader {...props} title="Rompefilas" rootName="lineBreakers" resourceName="banners_settings.json" />)
              } return null
            }}
          />
          <Route
            path="/app/banners/inspirational-blocks"
            exact
            render={(props) => {
              if (filteredPermissions.includes('banners.create')) {
                return (<InspirationalBlocksLoader {...props} title="Destacados Mundo Lider" rootName="inspirationalBlocks" resourceName="banners_settings.json" />)
              } return null
            }}
          />
          <Route
            path="/app/categories"
            exact
            render={(props) => {
              if (filteredPermissions.includes('categories.create')) {
                return (
                  <XLSXUploadHandlerLoader
                    {...props}
                    uploadButtonText="Subir archivo xslx de categorías"
                    downloadButtonText="Descargar categorías activas"
                    bffRouteToDownloadFile="/categories/download"
                    bffRouteToUploadFile="/categories/upload"
                    fileName="menuCategories.xlsx"
                    stagingSuccessMessage="Se han actualizado las categorías en Staging!"
                    successMessage="Se han actualizado las categorías correctamente!"
                    title="Administrar Categorías"
                    uploadToStagingFirst
                    openURLAfterUpload={false}
                  />
                )
              } return null
            }}
          />
          <Route
            path="/app/categories-sod"
            exact
            render={(props) => {
              if (filteredPermissions.includes('categoriesSod.create')) {
                return (
                  <XLSXUploadHandlerLoader
                    {...props}
                    uploadButtonText="Subir archivo xslx de categorías SOD"
                    downloadButtonText="Descargar categorías de SOD activas"
                    bffRouteToDownloadFile="/categories/download"
                    bffRouteToUploadFile="/categories/upload"
                    fileName="menuCategoriesSod.xlsx"
                    stagingSuccessMessage="Se han actualizado las categorías de SOD en Staging!"
                    successMessage="Se han actualizado las categorías de SOD correctamente!"
                    title="Administrar Categorías Supermercado"
                    uploadToStagingFirst
                    openURLAfterUpload={false}
                    tenant="supermercado"
                  />
                )
              } return null
            }}
          />
          <Route
            path="/app/hotjar/categories"
            exact
            render={(props) => {
              if (filteredPermissions.includes('categories.create')) {
                return (<HotjarCategoriesLoader {...props} title="Administrar categorías hotjar" />)
              } return null
            }}
          />
          <Route
            path="/app/walstore/stores"
            exact
            render={(props) => {
              if (filteredPermissions.includes('walstore.stores.create')) {
                return (
                  <XLSXUploadHandlerLoader
                    {...props}
                    uploadButtonText="Actualizar tiendas"
                    downloadButtonText="Descargar tiendas"
                    bffRouteToDownloadFile="/walstore/stores/download"
                    bffRouteToUploadFile="/walstore/stores/upload"
                    fileName="walstoreStores.xlsx"
                    stagingSuccessMessage="Se han actualizado las tiendas en Staging!"
                    successMessage="Se han actualizado las tiendas correctamente!"
                    title="Administrar Tiendas de Walstore"
                    uploadToStagingFirst
                    openURLAfterUpload={false}
                  />
                )
              } return null
            }}
          />
          <Route
            path="/app/products/selection"
            exact
            render={(props) => {
              if (filteredPermissions.includes('products-selection.create')) {
                return (<ProductsSelectionLoader {...props} title="Administrar seleccion de productos" />)
              } return null
            }}
          />
          <Route
            path="/app/products/initial-stock"
            exact
            render={(props) => {
              if (filteredPermissions.includes('products-initial-stock.create')) {
                return (
                  <XLSXUploadHandlerLoader
                    {...props}
                    uploadButtonText="Actualizar stock inicial"
                    downloadButtonText="Descargar stock inicial"
                    bffRouteToDownloadFile="/product/initial-stock/download"
                    bffRouteToUploadFile="/product/initial-stock/upload"
                    fileName="productsInitialStock.xlsx"
                    successMessage="Se han actualizado los stock iniciales correctamente!"
                    title="Administrar stock inicial"
                    uploadToStagingFirst={false}
                    openURLAfterUpload={false}
                  />
                )
              } return null
            }}
          />
          <Route
            path="/app/reports/product"
            exact
            render={(props) => {
              if (filteredPermissions.includes('products-report.read')) {
                return (<ProductsReportLoader {...props} title="Descargar Reporte de Productos" />)
              } return null
            }}
          />
          <Route
            path="/app/random"
            exact
            render={(props) => {
              if (filteredPermissions.includes('users.create')) {
                return (<RandomGeneratorLoader {...props} title="Random Generator" />)
              } return null
            }}
          />
          <Route
            path="/app/legal-info"
            exact
            render={(props) => {
              if (filteredPermissions.includes('legal.create')) {
                return (<LegalInfoLoader {...props} title="Información Legal" />)
              } return null
            }}
          />
          <Route
            path="/app/stores-status"
            exact
            render={(props) => {
              if (filteredPermissions.includes('stores.create')) {
                return (
                  <XLSXUploadHandlerLoader
                    {...props}
                    uploadButtonText="Actualizar tiendas"
                    bffRouteToDownloadFile="/stores-status/download"
                    bffRouteToUploadFile="/store-status/upload"
                    fileName="storesStatus.xlsx"
                    successMessage="Se actualizaron las tiendas correctamente!"
                    title="Administrar Estado de Tiendas"
                    uploadToStagingFirst={false}
                    showDownloadButton={false}
                    openURLAfterUpload
                    UrlToOpenInProduction="https://www.lider.cl/tiendas"
                  />
                )
              } return null
            }}
          />
          <Route
            path="/app/header-alert-message"
            exact
            render={(props) => {
              if (filteredPermissions.includes('users.create')) {
                return (<HeaderAlertMessageLoader {...props} title="Huincha de Contingencia" />)
              } return null
            }}
          />
          <Route
            path="/app/checkout/free-shipping-promotion"
            exact
            render={(props) => {
              if (filteredPermissions.includes('checkout.write')) {
                return (<FreeShippingPromotionLoader {...props} title="Promoción despacho gratis" />)
              } return null
            }}
          />
        </MainContainer>
      </Fragment>
    )
  }

  render() {
    const { expanded, modalIsOpen } = this.state
    const { location } = this.props

    const { pathname } = location
    const { IS_STRATI = false } = this.config

    return (
      <Fragment>
        <SideNav
          expanded={expanded}
          style={{ position: 'fixed', backgroundColor: '#041e42' }}
          onSelect={this.onSelect}
          onToggle={this.onToggle}
        >
          <div
            id="logo"
            role="button"
            tabIndex="0"
            onKeyPress={this.onToggle}
            onClick={this.onToggle}
            style={{ padding: '10px', color: 'white', cursor: 'pointer' }}
          >
            {expanded ? (this.renderLogo()) : (this.renderBurgerIcon()) }
          </div>
          {this.renderNavigationItemsAccordingToPermissions(pathname, IS_STRATI)}
        </SideNav>
        {this.renderRoutesAccordingToPermissions(expanded, IS_STRATI)}
        <StyledModal
          modalIsOpen={modalIsOpen}
          closeModalAction={() => {
            this.toggleModal()
          }}
        >
          <Fragment>
            <div className="container" style={{ padding: '20px' }}>
              <div className="text-center mb-20">
                <h2 className="mb-10">Cerrar sesión</h2>
                <div className="mb-20">
                  ¿Estás seguro que quieres cerrar sesión?
                </div>

                <div className="mb-10">
                  <ActionButton type="primary" onClick={() => this.handleLogOut()} style={{ margin: 'auto', color: '#fff', width: '200px' }}>
                    Aceptar
                  </ActionButton>
                </div>

                <div className="mb-20">
                  <ActionButton
                    onClick={() => this.toggleModal()}
                    style={{
                      margin: 'auto', backgroundColor: '#f6f6f6', color: '#293152', width: '200px', borderColor: '#f6f6f6',
                    }}
                  >
                    Cancelar
                  </ActionButton>
                </div>
              </div>

            </div>
          </Fragment>
        </StyledModal>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setDestroySession: () => dispatch(setDestroySession()),
})

const mapStateToProps = state => ({
  userData: state.userDataReducer,
})

export default connect(mapStateToProps, mapDispatchToProps)(SideNavMenu)

SideNavMenu.defaultProps = {
  location: {},
  history: {},
  userData: {},
}

SideNavMenu.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object,
  userData: PropTypes.object,
  setDestroySession: PropTypes.func.isRequired,
}
