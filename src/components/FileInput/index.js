import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ActionButton from '../ActionButton'

class FileInput extends Component {
  constructor(props) {
    super(props)
    this.inputReference = React.createRef()
  }

  fileUploadAction = () => this.inputReference.current.click()

  handlerInputChange = (e) => {
    const { inputHandler, type } = this.props
    inputHandler(e, type)
    e.target.value = ''
  }

  render() {
    const { title, accept } = this.props
    return (
      <ActionButton
        className="text-center"
        style={{
          width: '100%', color: '#0071c3', border: 'solid 1.5px #0071c3', margin: 'auto',
        }}
        onClick={this.fileUploadAction}
      >

        <div className="d-flex" style={{ justifyContent: 'center' }}>
          <div>{title}</div>
          <div><i style={{ marginLeft: '5px', fontWeight: 'bolder' }} className="zmdi zmdi-plus" /></div>
          <input
            accept={accept}
            type="file"
            hidden
            ref={this.inputReference}
            onChange={e => this.handlerInputChange(e)}
          />
        </div>
      </ActionButton>
    )
  }
}

FileInput.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  accept: PropTypes.string,
  inputHandler: PropTypes.func,
}

FileInput.defaultProps = {
  title: 'subir una imagen nueva',
  type: '',
  accept: 'image/*',
  inputHandler: () => {},
}

export default FileInput
