import styled from 'styled-components'

const EditTitle = styled.div`
    margin: 0; 
    margin-left: 5px;
    font-size: 20px;
    border: 0; padding: 0;
    font-weight: bold;
`
export default EditTitle
