import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom'
import PropTypes from 'prop-types'
import { getAuthData } from 'Src/Helper'
import Loadable from 'loadable-components'
import SideNavMenu from './components/SideNavMenu'
import ScrollToTop from './ScrollToTop'
import LogIn from './Pages/LogIn'
import setWindowSize from './Redux/actions/setWindowSize'
import { setModalImageClose } from './Redux/actions/setModalImage'
import StyledModal from './components/StyledModal'

const configData = window.__ENV__

const HomeLoader = Loadable(() => import('./components/SideNavMenu'), {
  fallback: <div>Loading...</div>,
})

const PrivateRoute = ({ component: Component, ...rest }) => {
  const auth = getAuthData()
  const isLogedIn = !!auth

  return (
    <Route {...rest} render={props => (isLogedIn === true ? <Component {...props} /> : <Redirect to="/login" />)} />
  )
}

export class Routes extends Component {
  constructor(props) {
    super(props)
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
  }

  componentDidMount = async () => {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

   Auth = ({ history }) => {
     // const auth = getAuthDataField('initials')
     const auth = getAuthData()
     const isLogedIn = !!auth
     const isInLoginPage = history.location.pathname.match(/\/login.*!/) !== null

     if (!isLogedIn && !isInLoginPage) {
       setTimeout(() => {
         history.replace('/login')
       }, 1)
     }
     return <Redirect to="/app" />
   }

   updateWindowDimensions() {
     const { setWindowSize } = this.props
     setWindowSize({ width: window.innerWidth, height: window.innerHeight })
   }

   render() {
     const {
       isModalImageOpen, imageToDisplay, setModalImageClose,
     } = this.props

     return (
       <div className="wrapper">
         <Router>
           <ScrollToTop>
             <Switch>
               <PrivateRoute path="/app" component={HomeLoader} />
               <Route path="/health">
                 <h3>Hey There!!!</h3>
               </Route>
               <Route
                 path="/login"
                 render={({ history }) => (
                   <LogIn history={history} />
                 )}
               />
               <Route path="/" component={this.Auth} />
               <Route exact path="">
                 <Redirect to="/app" />
               </Route>
               <Route render={({ location, history }) => (
                 <SideNavMenu location={location} history={history} />
               )}
               />
             </Switch>
           </ScrollToTop>
         </Router>
         <StyledModal
           modalIsOpen={isModalImageOpen}
           closeModalAction={() => {
             setModalImageClose()
           }}
         >
           <Fragment>
             <div className="container" style={{ padding: '20px' }}>
               <div className="text-center mb-20">
                 <div className="mb-20">
                   <img style={{ maxHeight: '100%' }} className="img-fluid" alt="logo" src={`${configData.storageBaseUrl}/${imageToDisplay}`} />
                 </div>
               </div>
             </div>
           </Fragment>
         </StyledModal>
       </div>
     )
   }
}

const mapStateToProps = state => ({
  isModalImageOpen: state.modalImageReducer.isModalImageOpen,
  imageToDisplay: state.modalImageReducer.imageToDisplay,
})

const mapDispatchToProps = dispatch => ({
  setModalImageClose: () => dispatch(setModalImageClose()),
  setWindowSize: windowSize => dispatch(setWindowSize(windowSize)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Routes)

PrivateRoute.propTypes = {
  component: PropTypes.node.isRequired,
}

Routes.defaultProps = {
  isModalImageOpen: false,
  imageToDisplay: '',
}

Routes.propTypes = {
  setModalImageClose: PropTypes.func.isRequired,
  setWindowSize: PropTypes.func.isRequired,
  isModalImageOpen: PropTypes.bool,
  imageToDisplay: PropTypes.string,
}
