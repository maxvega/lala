var moduleExport = false
if (typeof window === 'undefined' ) {
    var window = {}
    moduleExport = true
}
window.__ENV__ = {
    appId: "529CV9H7MW",
    baseURLBFF: "http://localhost:3002",
    baseURLStagingBFF: "http://localhost:3002",
    analyticsApiKey: "",
    timeout : 5000,
    storageBaseUrl: "",
    baseLandingURLbff: "",
    REDIS_FLUSH_TOKEN: "",
    PINGFED: "",
    LOCAL_AUTH_KEY: ""
}

if (moduleExport) {
    module.exports = window
}
