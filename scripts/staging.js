// configuring env variables
process.env.BABEL_ENV = 'production'
process.env.NODE_ENV = 'development'

const { spawn } = require('child_process');
const webpack = require('webpack')
const dll = require('../configs/dll.config.js')

// clearConsole()
console.log('Start building dll...')
webpack(dll, (err, stats) => {
  if (err !== null) {
    console.error(err);
    return;
  }
  console.info('dll builded successfull');
  console.info('Start building bundles...');
  spawn('webpack', ['--config', 'configs/webpack.staging.config.js', '--mode', 'development'], {
    stdio: ['inherit', 'inherit', 'inherit'],
  });

  ['SIGINT', 'SIGTERM'].forEach((sig) => {
    process.on(sig, () => {
      process.exit();
    });
  });
});
