// configuring env variables
process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'test'

const { spawn } = require('child_process')
const webpack = require('webpack')
const clearConsole = require('react-dev-utils/clearConsole')
const { choosePort } = require('react-dev-utils/WebpackDevServerUtils')
const dll = require('../configs/dll.dev.config')

const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000
const HOST = process.env.HOST || '0.0.0.0'

choosePort(HOST, DEFAULT_PORT)
  .then((port) => {
    if (port == null) {
      // We have not found a port.
      return
    }

    // clearConsole();
    webpack(dll, (err) => {
      if (err !== null) {
        console.error(err)
        return
      }

      spawn(
        'webpack-dev-server',
        ['--config', 'configs/webpack.test.config.js', '--hot', '--hot-only', '--port', port],
        {
          stdio: ['inherit', 'inherit', 'inherit'],
        },
      )
    });

    ['SIGINT', 'SIGTERM'].forEach((sig) => {
      process.on(sig, () => {
        process.exit()
      })
    })
  })
  .catch((err) => {
    if (err && err.message) {
      console.log(err.message)
    }
    process.exit(1)
  })
