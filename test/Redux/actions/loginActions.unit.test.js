/**
 * @jest-environment jsdom
 */
import React from 'react'
import sinon from 'sinon'
import { userValidateAction } from '../../../src/Redux/actions/loginActions'
import ClientBFF from '../../../src/common/ClientBFF'
import {
  AUTH_SUCCESS, AUTH_PENDING,
  AUTH_ERROR,
} from '../../../src/Redux/types'

const { userRoles } = require('../../Mocks/userRoles')

window.__ENV__ = { LOCAL_AUTH_KEY: 'wm-blackcyber-auth-key' }


describe('Redux: login Actions', () => {
  let sandbox
  let dispatch

  beforeEach(() => {
    sandbox = sinon.createSandbox()
    // mock the dispatch functions from Redux thunk.
    dispatch = jest.fn()
  })

  afterEach(() => {
    sandbox.restore()
    dispatch.mockReset()
  })

  test('Should dispatch AUTH_SUCCESS when environment from backend is development and no code is received', async () => {
    const authStub = sandbox.stub(ClientBFF.prototype, 'auth').returns(Promise.resolve({ stageName: 'dev' }))
    const expected = {
      type: AUTH_SUCCESS,
    }

    // execute
    await userValidateAction()(dispatch)

    sinon.assert.calledOnce(authStub)
    expect(dispatch.mock.calls[0][0].type).toEqual(expected.type)
    expect(dispatch.mock.calls[0][0].payload.loginInfo).toEqual(userRoles)
  })

  test('Should dispatch AUTH_SUCCESS when a valid code is received', async () => {
    const authStub = sandbox.stub(ClientBFF.prototype, 'auth').returns(Promise.resolve({ ...userRoles }))
    const expected = {
      type: AUTH_SUCCESS,
    }

    // execute
    await userValidateAction('a1213r930m391a70nd992o')(dispatch)

    sinon.assert.calledOnce(authStub)
    expect(dispatch.mock.calls[0][0].type).toEqual(expected.type)
    expect(dispatch.mock.calls[0][0].payload.loginInfo).toEqual(userRoles)
  })

  test('Should dispatch AUTH_PENDING when backend is not dev and code is missing', async () => {
    const authStub = sandbox.stub(ClientBFF.prototype, 'auth').returns(Promise.resolve({ stageName: 'stage' }))
    const expected = {
      type: AUTH_PENDING,
    }

    // execute
    await userValidateAction()(dispatch)

    sinon.assert.calledOnce(authStub)
    expect(dispatch.mock.calls[0][0].type).toEqual(expected.type)
    expect(dispatch.mock.calls[0][0].payload.loginInfo).toEqual({})
  })

  test('Should dispatch AUTH_ERROR when some error is detected with ClientBFF', async () => {
    const authStub = sandbox.stub(ClientBFF.prototype, 'auth').returns(Promise.reject())
    const expected = {
      type: AUTH_ERROR,
    }

    // execute
    await userValidateAction('a1213r930m391a70nd992o')(dispatch)

    sinon.assert.calledOnce(authStub)
    expect(dispatch.mock.calls[0][0].type).toEqual(expected.type)
    expect(dispatch.mock.calls[0][0].payload.loginInfo).toEqual({})
  })
})
