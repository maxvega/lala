import sinon from 'sinon'
import bunyan from 'browser-bunyan'
import logger from '../../src/common/Logger'

describe('common: Logger Test', () => {
  let sandbox
  window.__ENV__ = { newBff: 'localhost:3000/', timeout: 5000 }
  let infoSpy
  beforeEach(() => {
    sandbox = sinon.createSandbox()
    infoSpy = jest.fn()
    sandbox.stub(bunyan, 'createLogger').returns({infoSpy})
  })

  afterEach(() => {
    sandbox.restore()
  })
  test('Logger should return instances of object', async () => {
    const config = window.__ENV__
    const loggerInstance = logger.getLogInstance(config)
    expect(Object.keys(loggerInstance).length).toBe(2)
  })
})
