/**
 * @jest-environment jsdom
 */
import sinon from 'sinon'
import ClientAlgolia from '../../src/common/ClientAlgolia'
import logger from '../../src/common/Logger'

describe('common: ClientApi Test', () => {
  let sandbox
  window.__ENV__ = { newBff: 'localhost:3000/', timeout: 5000 }
  let infoSpy
  let clientAPI
  beforeEach(() => {
    sandbox = sinon.createSandbox()
    infoSpy = jest.fn()
    sandbox.stub(logger, 'getLogInstance').returns({ info: () => {}, error: infoSpy })
    clientAPI = new ClientAlgolia()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('AlgoliaInstance  should return instances of Axios', async () => {
    const client = clientAPI.algoliaInstance()
    expect(client).toBeInstanceOf(Function)
  })
  test('getTopSearches should return an object', async () => {
    sandbox.stub(clientAPI, 'algoliaInstance').returns({ request: () => Promise.resolve({ data: { sku: '123' } }) })
    const product = await clientAPI.getTopSearches('123', '123')
    expect(product).toBeInstanceOf(Object)
  })
})
