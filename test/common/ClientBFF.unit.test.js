import ClientBFF from '../../src/common/ClientBFF'
import logger from '../../src/common/Logger'

describe('common: ClientBFF Tests', () => {
  let clientBff
  let infoSpy
  window.__ENV__ = { newBff: 'localhost:3000/', timeout: 5000, LOCAL_AUTH_KEY: 'wm-blackcyber-auth-key' }

  beforeEach(() => {
    clientBff = new ClientBFF()
    infoSpy = jest.fn()
    jest.spyOn(logger, 'getLogInstance').mockReturnValue({ info: () => {}, error: infoSpy })
    jest.resetAllMocks()
  })
  test('bffInstance should return an axios instance', async () => {
    const client = clientBff.bffInstance()
    expect(client).toBeInstanceOf(Function)
  })
  test('bffInstance should return an axios instance for staging', async () => {
    const client = clientBff.bffStagingInstance()
    expect(client).toBeInstanceOf(Function)
  })
  test('bffInstance should return an axios instance for landing', async () => {
    const client = clientBff.landingBffInstance()
    expect(client).toBeInstanceOf(Function)
  })
  describe('get banners method', () => {
    test('should return banners', async () => {
      const data = {
        data: {
          banners: [],
        },
      }
      const expectedData = {
        banners: [],
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })

      const response = await clientBff.getBanners({})
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.getBanners({})
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('upload method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          upload: 'OK',
        },
      }
      const expectedData = {
        upload: 'OK',
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })

      const response = await clientBff.upload({}, {})
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should response ok for staging', async () => {
      const data = {
        data: {
          upload: 'OK',
        },
      }
      const expectedData = {
        upload: 'OK',
      }
      const bffStagingInstanceSpy = jest.spyOn(clientBff, 'bffStagingInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.upload({}, { isProduction: false })
      expect(response).toEqual(expectedData)
      expect(bffStagingInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.upload({}, {})
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('download method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          download: 'OK',
        },
      }
      const expectedData = {
        download: 'OK',
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.download({}, {})
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should response ok get', async () => {
      window.URL.createObjectURL = jest.fn()
      const data = {
        data: {
          download: 'OK',
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.download({}, { method: 'get' })
      expect(response).toEqual({})
      expect(bffInstanceSpy).toHaveBeenCalled()
      window.URL.createObjectURL.mockReset()
    })
    test('should response ok for staging', async () => {
      const data = {
        data: {
          download: 'OK',
        },
      }
      const expectedData = {
        download: 'OK',
      }
      const bffStagingInstanceSpy = jest.spyOn(clientBff, 'bffStagingInstance').mockReturnValue({ request: () => Promise.resolve(data) })

      const response = await clientBff.download({}, { isProduction: false })
      expect(response).toEqual(expectedData)
      expect(bffStagingInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.download({}, {})
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('delete method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          delete: 'OK',
        },
      }
      const expectedData = {
        delete: 'OK',
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.delete({})
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.delete({})
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('getBundles method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          bundles: 'OK',
        },
      }
      const expectedData = {
        bundles: 'OK',
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.getBundles()
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.getBundles()
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('updateProductsToStaging method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          updateProductsToStaging: 'OK',
        },
      }
      const expectedData = {
        updateProductsToStaging: 'OK',
      }
      const bffStagingInstanceSpy = jest.spyOn(clientBff, 'bffStagingInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.updateProductsToStaging({})
      expect(response).toEqual(expectedData)
      expect(bffStagingInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffStagingInstanceSpy = jest.spyOn(clientBff, 'bffStagingInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.updateProductsToStaging({})
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffStagingInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('updateProductsToProduction method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          updateProductsToProduction: 'OK',
        },
      }
      const expectedData = {
        updateProductsToProduction: 'OK',
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.updateProductsToProduction({})
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.updateProductsToProduction({})
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('clearBFFCache method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          clearBFFCache: 'OK',
        },
      }
      const expectedData = {
        clearBFFCache: 'OK',
      }
      const landingBffInstanceSpy = jest.spyOn(clientBff, 'landingBffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.clearBFFCache()
      expect(response).toEqual(expectedData)
      expect(landingBffInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const landingBffInstanceSpy = jest.spyOn(clientBff, 'landingBffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.clearBFFCache()
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(landingBffInstanceSpy).toHaveBeenCalled()
    })
  })
  describe('auth method', () => {
    test('should response ok', async () => {
      const data = {
        data: {
          clearBFFCache: 'OK',
        },
      }
      const expectedData = {
        clearBFFCache: 'OK',
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.resolve(data) })
      const response = await clientBff.auth()
      expect(response).toEqual(expectedData)
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
    test('should return with error', async () => {
      const error = {
        message: 'failed to reach service',
        response: {
          status: 500,
        },
      }
      const bffInstanceSpy = jest.spyOn(clientBff, 'bffInstance').mockReturnValue({ request: () => Promise.reject(error) })

      try {
        await clientBff.auth()
      } catch (error) {
        expect(error.message).toEqual('failed to reach service')
      }
      expect(bffInstanceSpy).toHaveBeenCalled()
    })
  })
})
