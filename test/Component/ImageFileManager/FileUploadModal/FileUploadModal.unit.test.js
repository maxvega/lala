import React from 'react'
import { shallow } from 'enzyme'
import FileUploadModal from '../../../../src/components/ImageFileManager/fileUploadModal'

describe('FileUploadModal', () => {
  test('should show only components correct with imageIsValid true', () => {
    const actionButtonHandler = jest.fn()
    const closeModal = jest.fn()
    const uploadedFile = { imageIsValid: true, name: 'name', size: '100' }

    const wrapper = shallow(
      <FileUploadModal
        uploadedFile={uploadedFile}
        closeModal={closeModal}
        actionButtonHandler={actionButtonHandler}
      />,
    )

    expect(wrapper.text().includes('La imagen se ha cargado correctamente.')).toBeTruthy()
    expect(wrapper.text().includes('guardar cambios')).toBeTruthy()
  })

  test('should show only components correct with imageIsValid false', () => {
    const actionButtonHandler = jest.fn()
    const closeModal = jest.fn()
    const uploadedFile = { imageIsValid: false, name: 'name', size: '100' }

    const wrapper = shallow(
      <FileUploadModal
        uploadedFile={uploadedFile}
        closeModal={closeModal}
        actionButtonHandler={actionButtonHandler}
      />,
    )

    expect(wrapper.text().includes('¡Lo sentimos! Esta imagen no es válida..')).toBeTruthy()
    expect(wrapper.text().includes('intentar nuevamente')).toBeTruthy()
  })

  test('should call actionButtonHandler when Click on guardar cambios button and imageIsValid is true', () => {
    const actionButtonHandler = jest.fn()
    const closeModal = jest.fn()
    const uploadedFile = { imageIsValid: true, name: 'name', size: '100' }

    const wrapper = shallow(
      <FileUploadModal
        uploadedFile={uploadedFile}
        closeModal={closeModal}
        actionButtonHandler={actionButtonHandler}
      />,
    )

    wrapper.find('ActionButton').at(0).simulate('click')

    expect(actionButtonHandler.mock.calls).toEqual([[]])
  })

  test('should call closeModal when Click on intentar nuevamente button and imageIsValid is false', () => {
    const actionButtonHandler = jest.fn()
    const closeModal = jest.fn()
    const uploadedFile = { imageIsValid: false, name: 'name', size: '100' }

    const wrapper = shallow(
      <FileUploadModal
        uploadedFile={uploadedFile}
        closeModal={closeModal}
        actionButtonHandler={actionButtonHandler}
      />,
    )

    wrapper.find('ActionButton').at(0).simulate('click')

    expect(closeModal.mock.calls).toEqual([[]])
  })

  test('should call closeModal when Click on cancelar button', () => {
    const actionButtonHandler = jest.fn()
    const closeModal = jest.fn()
    const uploadedFile = { imageIsValid: true, name: 'name', size: '100' }

    const wrapper = shallow(
      <FileUploadModal
        uploadedFile={uploadedFile}
        closeModal={closeModal}
        actionButtonHandler={actionButtonHandler}
      />,
    )

    wrapper.find('ActionButton').at(1).simulate('click')

    expect(closeModal.mock.calls).toEqual([[]])
  })
})
