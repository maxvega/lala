import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import ImageFileManager from '../../../src/components/ImageFileManager'
import ClientBFF from '../../../src/common/ClientBFF'

window.__ENV__ = { baseURL: 'http://localhost:3000', timeout: 5000 }
window.alert = () => true

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

describe('ImageFileManager', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  describe('inputHandler', () => {
    test('should show modal', () => {
      const wrapper = shallow(
        <ImageFileManager
          container="landing"
          uploadedFile
        />,
      )

      wrapper.instance().inputHandler({ target: { files: [{ name: 'file', size: '1' }] } })

      expect(wrapper.state().showModal).toBeTruthy()
    })

    test('should not show modal', () => {
      const wrapper = shallow(
        <ImageFileManager
          uploadedFile
          container="landing"
        />,
      )

      wrapper.instance().inputHandler({ target: { files: [] } })

      expect(wrapper.state().showModal).toBeFalsy()
    })
  })

  describe('uploadFileToStorage', () => {
    test('should updateImages when image is valid', async () => {
      const uploadStub = sandbox.stub(ClientBFF.prototype, 'upload')
      uploadStub.onCall(0).returns(Promise.resolve())
      uploadStub.onCall(1).returns(Promise.resolve())

      const updateImages = jest.fn()

      const wrapper = shallow(
        <ImageFileManager
          container="landing"
          uploadedFile
          updateImages={updateImages}
        />,
      )

      wrapper.setState({
        uploadedFile: { name: 'randomImage.jpg', imageIsValid: true, size: 123 },
      })

      await wrapper.instance().uploadFileToStorage({ target: { files: [] } })
      expect(updateImages).toHaveBeenCalled()
    })

    test('should updateImages when image is invalid', async () => {
      sandbox.stub(ClientBFF.prototype, 'upload').returns(Promise.resolve())
      const updateImages = jest.fn()

      const wrapper = shallow(
        <ImageFileManager
          container="landing"
          uploadedFile
          updateImages={updateImages}
        />,
      )

      wrapper.setState({
        uploadedFile: { imageIsValid: false },
      })
      await wrapper.instance().uploadFileToStorage({ target: { files: [] } })
      expect(updateImages.mock.calls).toEqual(
        [],
      )
    })

    test('should not updateImages and enter in catch', async () => {
      sandbox.stub(ClientBFF.prototype, 'upload').returns(Promise.reject())
      const updateImages = jest.fn()

      const wrapper = shallow(
        <ImageFileManager
          container="landing"
          uploadedFile
          updateImages={updateImages}
        />,
      )

      wrapper.setState({
        uploadedFile: { imageIsValid: true, name: 'file.jpg' },
      })
      await wrapper.instance().uploadFileToStorage({ target: { files: [] } })
      expect(updateImages.mock.calls).toEqual(
        [],
      )
    })
  })

  describe('deleteFile', () => {
    test('should delete file and run updateImages fuctions', async () => {
      sandbox.stub(ClientBFF.prototype, 'delete').returns(Promise.resolve())
      const updateImages = jest.fn()

      const wrapper = shallow(
        <ImageFileManager
          container="landing"
          updateImages={updateImages}
        />,
      )
      await wrapper.instance().deleteFile('image')
      expect(updateImages.mock.calls).toEqual(
        [['']],
      )
    })

    test('should not delete file and not run updateImages fuctions, enter in catch', async () => {
      sandbox.stub(ClientBFF.prototype, 'delete').returns(Promise.reject())
      const updateImages = jest.fn()

      const wrapper = shallow(
        <ImageFileManager
          container="landing"
          updateImages={updateImages}
        />,
      )
      await wrapper.instance().deleteFile('image')
      expect(updateImages.mock.calls).toEqual(
        [],
      )
    })
  })
})
