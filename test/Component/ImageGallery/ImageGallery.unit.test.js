/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import ImageGallery from 'Src/components/ImageGallery'

describe('component: ImageGallery', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render the correct number of ImageContainers', async () => {
    const spy = jest.fn()
    const ImageGalleryComponent = enzyme.shallow(
      <ImageGallery
        handleImageToDelete={jest.fn()}
        images={['images/1600103360172-09_14_cal1.jpg']}
        title="Subidas recientes"
        selectedImageHandler={spy}
      />,
    )
    expect(ImageGalleryComponent.instance().props.images).toHaveLength(1)
  })

  test('should call selectedImageHandler with the correct image and set the correct currentImage state', async () => {
    const spy = jest.fn()
    const ImageGalleryComponent = enzyme.shallow(
      <ImageGallery
        handleImageToDelete={jest.fn()}
        images={[]}
        title="Subidas recientes"
        selectedImageHandler={spy}
      />,
    )
    ImageGalleryComponent.instance().handleImageSelectedFromGallery('images/1600103360172-09_14_cal1.jpg')
    expect(ImageGalleryComponent.instance().state.currentImage).toBe('images/1600103360172-09_14_cal1.jpg')
    expect(spy).toHaveBeenCalledWith('images/1600103360172-09_14_cal1.jpg')
    ImageGalleryComponent.instance().handleImageSelectedFromGallery('images/1600103360172-09_14_cal1.jpg')
    expect(ImageGalleryComponent.instance().state.currentImage).toBe('')
  })
})
