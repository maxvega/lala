/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import CustomEditCard from 'Src/components/CustomEditCard'
import ClientBFF from 'Src/common/ClientBFF'
import * as Helper from 'Src/Helper'

window.__ENV__ = { baseURLStagingBFF: 'http://localhost:3000', timeout: 5000 }

window.open = () => true

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))


describe('component: CustomEditCard', () => {
  let sandbox
  const downloadParameters = {
    resourceName: 'archivo.json',
    storageContainer: 'storage',
  }
  const selectedElement = {
    area: 'grid1',
    backgroundDesktop: 'images/area1.jpg',
    backgroundMobile: 'images/area1-mob.jpg',
    link: '/Videojuegos',
    name: 'VideoJuegos campaña',
    order: 1,
    tag: 'gm_videojuego',
    target: '_blank',
    type: 'grid',
    updated: '09-08-2020',
  }

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should uploadToStaging the correct selected element', async () => {
    sandbox.stub(ClientBFF.prototype, 'download').returns(Promise.resolve({ grids: { grid1: selectedElement } }))

    const uploadStub = sandbox.stub(ClientBFF.prototype, 'upload')
    uploadStub.onCall(0).returns(Promise.resolve())
    uploadStub.onCall(1).returns(Promise.resolve())

    const jsonToFileStub = sandbox.stub(Helper, 'JSONToFile').returns({ file: 'file' })
    const generateJSONFromDataStub = sandbox.stub(Helper, 'generateJSONFormData').returns({})

    const CustomEditCardComponent = enzyme.shallow(
      <CustomEditCard
        selectedElement={selectedElement}
        downloadParameters={downloadParameters}
        selectedElementKey="grid1"
        title="Editar Area"
        backArrowHandler={jest.fn()}
        elementParent="grids"
      />,
    )
    await CustomEditCardComponent.instance().uploadToStaging()
    sinon.assert.calledWithExactly(jsonToFileStub, { grids: { grid1: selectedElement } }, 'archivo.json')

    sinon.assert.calledWithExactly(generateJSONFromDataStub, { file: 'file' }, 'storage')
  })

  test('should turn on or off a complete banner setting via toggle', async () => {
    const grid = {
      area: 'grid',
      backgroundDesktop: 'images/area1.jpg',
      backgroundMobile: 'images/area1-mob.jpg',
      link: '/Videojuegos',
      name: 'VideoJuegos campaña',
      order: 1,
      tag: 'gm_videojuego',
      target: '_blank',
      type: 'grid',
      updated: '09-08-2020',
      active: true,
    }

    const wrapper = enzyme.shallow(
      <CustomEditCard
        selectedElement={grid}
        downloadParameters={downloadParameters}
        selectedElementKey="grid1"
        title="Editar Area"
        backArrowHandler={jest.fn()}
        elementParent="grids"
        toggleConfig={{
          retrieveData: jest.fn(),
          handleEditSelectedBannerToggle: jest.fn(),
        }}
      />,
    )

    const downloadStub = sandbox.stub(ClientBFF.prototype, 'download')
    downloadStub.onCall(0).returns(Promise.resolve({ grids: { grid } }))
    downloadStub.onCall(1).returns(Promise.resolve({ grids: { grid } }))

    const uploadStub = sandbox.stub(ClientBFF.prototype, 'upload')
    uploadStub.onCall(0).returns(Promise.resolve())
    uploadStub.onCall(1).returns(Promise.resolve())


    const JSONToFileStub = sandbox.stub(Helper, 'JSONToFile').returns({ file: 'file' })
    const generateJSONFormDataStub = sandbox.stub(Helper, 'generateJSONFormData').returns({})

    await wrapper.instance().uploadToggleToStorage()
    grid.active = false

    sinon.assert.calledWith(JSONToFileStub, { grids: { grid } }, 'archivo.json')
    sinon.assert.calledWith(generateJSONFormDataStub, { file: 'file' }, 'storage')
    sinon.assert.calledTwice(downloadStub)
    sinon.assert.calledTwice(uploadStub)
  })

  test('should show and hide UploadToProductionConfirmationModal', async () => {
    const CustomEditCardComponent = enzyme.shallow(
      <CustomEditCard
        selectedElement={selectedElement}
        downloadParameters={downloadParameters}
        selectedElementKey="grid1"
        title="Editar Area"
        backArrowHandler={jest.fn()}
        elementParent="grids"
      />,
    )

    CustomEditCardComponent.setState({ isUploadToProductionModalOpen: false })
    CustomEditCardComponent.instance().showUploadToProductionConfirmationModal()
    expect(CustomEditCardComponent.state().isUploadToProductionModalOpen).toBeTruthy()

    CustomEditCardComponent.instance().hideUploadToProductionConfirmationModal()
    expect(CustomEditCardComponent.state().isUploadToProductionModalOpen).toBeFalsy()
  })

  test('should show and hide all the custom alerts', async () => {
    const CustomEditCardComponent = enzyme.shallow(
      <CustomEditCard
        selectedElement={selectedElement}
        downloadParameters={downloadParameters}
        selectedElementKey="grid1"
        title="Editar Area"
        backArrowHandler={jest.fn()}
        elementParent="grids"
      />,
    )

    CustomEditCardComponent.setState({ showErrorAlert: false, showSuccessAlert: false })
    CustomEditCardComponent.instance().showErrorCustomAlert()
    expect(CustomEditCardComponent.state().showErrorAlert).toBeTruthy()

    CustomEditCardComponent.instance().showSuccessCustomAlert()
    expect(CustomEditCardComponent.state().showSuccessAlert).toBeTruthy()

    CustomEditCardComponent.instance().hideCustomAlerts()
    expect(CustomEditCardComponent.state().showSuccessAlert).toBeFalsy()
    expect(CustomEditCardComponent.state().showErrorAlert).toBeFalsy()
  })
})
