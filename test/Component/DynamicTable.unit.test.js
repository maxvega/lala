/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme, { configure } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import DynamicTable from '../../src/components/DynamicTable'

configure({ adapter: new Adapter() })

const PRODUCTS_SEARCHS = [
  { search: 'galaxy z flip', count: 5993, nbHits: 2 },
  { search: 'galaxy watch', count: 5994, nbHits: 3 },
  { search: 'galaxy buds', count: 5995, nbHits: 4 },
]
const columns = [
  { name: 'Búsqueda', selector: 'search', sortable: true },
  { name: 'Cantidad', selector: 'count', sortable: true },
  { name: 'Resultados', selector: 'nbHits', sortable: true },
]

describe('component: DynamicTable', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('DynamicTable should have the correct props', async () => {
    const DinamicTableComponent = enzyme.shallow(
      <DynamicTable columns={columns} data={PRODUCTS_SEARCHS} title="Búquedas en los últimos 7 días" />,
    )
    expect(DinamicTableComponent.instance().props.data).toHaveLength(3)
  })

  test('DynamicTable should render 3 rows in DataTable', async () => {
    const DinamicTableComponent = enzyme.mount(
      <DynamicTable columns={columns} data={PRODUCTS_SEARCHS} title="Búquedas en los últimos 7 días" />,
    )
    const DataTable = DinamicTableComponent.find('#dinamic-data-table')
    expect(DataTable.length).toBe(1)
    expect(DataTable.find('#row-0').exists()).toBeTruthy()
    expect(DataTable.find('#row-1').exists()).toBeTruthy()
    expect(DataTable.find('#row-2').exists()).toBeTruthy()
    expect(DataTable.find('#row-3').exists()).toBeFalsy()
  })

  test('DynamicTable should not render DataTable', async () => {
    const EMPTY_PRODUCTS = []
    const DinamicTableComponent = enzyme.mount(
      <DynamicTable columns={columns} data={EMPTY_PRODUCTS} title="Búquedas en los últimos 7 días" />,
    )
    const DataTable = DinamicTableComponent.find('#dinamic-data-table')
    expect(DataTable.length).toBe(1)
    expect(DataTable.find('#row-0').exists()).toBeFalsy()
  })
})
