import React from 'react'
import { shallow } from 'enzyme'
import FileInput from '../../../src/components/FileInput'

describe('FileInput', () => {
  test('should update input with file selected', () => {
    const spy = jest.fn()
    const inputHandler = spy

    const wrapper = shallow(<FileInput inputHandler={inputHandler} type="file" />)
    wrapper.find('input').simulate('change', { target: { files: 'Hello files' } })

    expect(spy).toHaveBeenCalledTimes(1)
  })
})
