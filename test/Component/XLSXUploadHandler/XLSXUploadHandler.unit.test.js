/**
 * @jest-environment jsdom
 */
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapater from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import XLSXUploadHandlerWithModal, { XLSXUploadHandler } from '../../../src/components/XLSXUploadHandler'
import ClientBFF from '../../../src/common/ClientBFF'

configure({ adapter: new Adapater() })

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

describe('XLSXUploadHandler component unit test', () => {
  window.__ENV__ = { baseURLBFF: 'bffURL' }
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
    jest.resetAllMocks()
  })

  test('should show download button', () => {
    const XLSXUploadHandlerComponent = shallow(
      <XLSXUploadHandlerWithModal
        showDownloadButton
        title="Administrar Tiendas de Walstore"
        bffRouteToDownloadFile="walstore/stores/download"
        methodToUploadToProduction="uploadWalstoreStores"
        methodToUploadToStaging="uploadWalstoreStores"
        handleFeedback={() => {}}
      />,
    )
    const XLSXUploadHandlerWithoutModal = XLSXUploadHandlerComponent.find(XLSXUploadHandler).dive()
    expect(XLSXUploadHandlerWithoutModal.find('h3').length).toEqual(1)
    expect(XLSXUploadHandlerWithoutModal.find('.col-md-6').length).toEqual(2)
  })


  test('should not show download button', () => {
    const XLSXUploadHandlerComponent = shallow(
      <XLSXUploadHandlerWithModal
        showDownloadButton={false}
        title="Administrar Tiendas de Walstore"
        bffRouteToDownloadFile="walstore/stores/download"
        methodToUploadToProduction="uploadWalstoreStores"
        methodToUploadToStaging="uploadWalstoreStores"
        handleFeedback={() => {}}
      />,
    )
    const XLSXUploadHandlerWithoutModal = XLSXUploadHandlerComponent.find(XLSXUploadHandler).dive()
    expect(XLSXUploadHandlerWithoutModal.find('h3').length).toEqual(1)
    expect(XLSXUploadHandlerWithoutModal.find('.col-md-6').length).toEqual(0)
  })

  test('should get success message with default tenant (catalogo)', async () => {
    jest.spyOn(ClientBFF.prototype, 'upload').mockReturnValue(Promise.resolve())

    const XLSXUploadHandlerComponent = shallow(
      <XLSXUploadHandlerWithModal
        showDownloadButton
        title="Administrar Tiendas de Walstore"
        bffRouteToDownloadFile="walstore/stores/download"
        methodToUploadToProduction="uploadWalstoreStores"
        methodToUploadToStaging="uploadWalstoreStores"
        handleFeedback={() => {}}
        successMessage="Éxito"
      />,
    )
    const XLSXUploadHandlerWithoutModal = XLSXUploadHandlerComponent.find(XLSXUploadHandler).dive()
    const handleMessageSpy = jest.spyOn(XLSXUploadHandlerWithoutModal.instance(), 'handleMessage')
    await XLSXUploadHandlerWithoutModal.instance().handleUpload('production')
    expect(handleMessageSpy).toHaveBeenCalledWith('Éxito')
  })

  test('should show input message', async () => {
    const XLSXUploadHandlerComponent = shallow(
      <XLSXUploadHandlerWithModal
        showDownloadButton
        title="Administrar Tiendas de Walstore"
        bffRouteToDownloadFile="walstore/stores/download"
        methodToUploadToProduction="uploadWalstoreStores"
        methodToUploadToStaging="uploadWalstoreStores"
        handleFeedback={() => {}}
        successMessage="Éxito"
        uploadToStagingFirst
      />,
    )
    const XLSXUploadHandlerWithoutModal = XLSXUploadHandlerComponent.find(XLSXUploadHandler).dive()

    expect(XLSXUploadHandlerWithoutModal.find('input').length).toEqual(0)
    XLSXUploadHandlerWithoutModal.instance().setState({ uploadedToStaging: true })
    expect(XLSXUploadHandlerWithoutModal.find('input').length).toEqual(1)
  })

  test('should upload with default tenant', async () => {
    let tenant
    jest.spyOn(ClientBFF.prototype, 'upload').mockImplementationOnce((formData, toRequest) => {
      ({ tenant } = toRequest)
      return Promise.resolve()
    })

    const XLSXUploadHandlerComponent = shallow(
      <XLSXUploadHandlerWithModal
        showDownloadButton
        title="title"
        bffRouteToDownloadFile="download/Route"
      />,
    )
    const XLSXUploadHandlerWithoutModal = XLSXUploadHandlerComponent.find(XLSXUploadHandler).dive()
    await XLSXUploadHandlerWithoutModal.instance().handleUpload('production')
    expect(tenant).toEqual('catalogo')
  })

  test('should upload with different tenant', async () => {
    let tenant
    jest.spyOn(ClientBFF.prototype, 'upload').mockImplementationOnce((formData, toRequest) => {
      ({ tenant } = toRequest)
      return Promise.resolve()
    })

    const XLSXUploadHandlerComponent = shallow(
      <XLSXUploadHandlerWithModal
        showDownloadButton
        title="title"
        bffRouteToDownloadFile="download/Route"
        tenant="supermercado"
      />,
    )
    const XLSXUploadHandlerWithoutModal = XLSXUploadHandlerComponent.find(XLSXUploadHandler).dive()
    await XLSXUploadHandlerWithoutModal.instance().handleUpload('production')
    expect(tenant).toEqual('supermercado')
  })
})
