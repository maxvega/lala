/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme, { configure } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import BundleCreator from '../../src/Pages/Products/BundleCreator/UnitBundle'

configure({ adapter: new Adapter() })

const FORM_DATA = {
  formData: {
    ID: 'PROD_12',
    appId: 'BuySmart',
    available: false,
    brand: 'Combo',
    campaignId: 'blackcyber',
    color: '',
    destacado: false,
    discount: 0,
    displayName: '',
    etiqueta: 'EXCLUSIVO INTERNET',
    gtin13: 1231231,
    associatedProducts: [
      {
        sku: '123',
        price: {
          BasePriceReference: 20,
          BasePriceSales: 10,
        },
        quantity: 1,
      },
    ],
  },
}

describe('component: BundleCreator', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should set the correct state when handleOnChange is called', async () => {
    const BundleCreatorComponent = enzyme.shallow(
      <BundleCreator
        title="title"
      />,
    )
    BundleCreatorComponent.instance().handleOnChange(FORM_DATA)
    expect(BundleCreatorComponent.state().schema.properties.ID.default).toEqual('PROD_') // TODO, modificar este test ¿setState asincrono?
    expect(BundleCreatorComponent.state().schema.properties.associatedProducts.default.length).toBe(1)
  })
})
