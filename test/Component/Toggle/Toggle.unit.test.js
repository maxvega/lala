import React from 'react'
import Toggle from 'Src/components/Toggle'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import { UncontrolledTooltip } from 'reactstrap'

describe('Toggle component unit test', () => {
  test('should create a Toggle instance', () => {
    const wrapper = shallow(<Toggle isActive />)
    expect(wrapper.instance()).toBeInstanceOf(Toggle)
  })

  test('should create a Toggle instance', () => {
    const wrapper = shallow(<Toggle isActive={false} />)
    expect(wrapper.instance()).toBeInstanceOf(Toggle)
  })

  test('should call handler on click', () => {
    const clickspy = sinon.spy()
    const wrapper = shallow(<Toggle clickHandler={clickspy} />)
    wrapper.simulate('click')
    sinon.assert.calledOnce(clickspy)
  })

  test('should show tooltip', () => {
    const wrapper = shallow(<Toggle area="area" tooltipText="texto del tooltip" />)
    wrapper.simulate('hover')
    expect(wrapper.find(UncontrolledTooltip).length).toEqual(1)
  })
})
