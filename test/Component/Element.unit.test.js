/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Element from '../../src/components/Card/Element'

describe('component: Element', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render children', async () => {
    const ElementComponent = enzyme.mount(
      <Element><p>body</p></Element>,
    )
    expect(ElementComponent.find('p').length).toEqual(1)
  })
})
