import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import CustomContentCardWithModal, { CustomContentCard } from '../../../src/components/CustomContentCard'

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

describe('CustomContentCard', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })
  test('should run actionButtonHandler when click', () => {
    const actionButtonHandler = jest.fn()
    const CustomContentCardWithModalWrapper = shallow(
      <CustomContentCardWithModal>
        <CustomContentCard showActionButton actionButtonHandler={actionButtonHandler} />
      </CustomContentCardWithModal>,
    )

    const CustomContentCardWrapper = CustomContentCardWithModalWrapper.find('CustomContentCard').at(1).dive()

    CustomContentCardWrapper.find('ActionButton').at(0).simulate('click')

    expect(actionButtonHandler.mock.calls).toEqual([[]])
  })

  test('should show CircularProgress components if showLoader is true', () => {
    const actionButtonHandler = jest.fn()
    const CustomContentCardWithModalWrapper = shallow(
      <CustomContentCardWithModal>
        <CustomContentCard showActionButton actionButtonHandler={actionButtonHandler} showLoader />
      </CustomContentCardWithModal>,
    )

    const CustomContentCardWrapper = CustomContentCardWithModalWrapper.find('CustomContentCard').at(1).dive()

    expect(CustomContentCardWrapper.find('.progress-primary').length === 1).toBeTruthy()
  })

  test('should not show CircularProgress components if showLoader is false', () => {
    const actionButtonHandler = jest.fn()
    const CustomContentCardWithModalWrapper = shallow(
      <CustomContentCardWithModal>
        <CustomContentCard showActionButton actionButtonHandler={actionButtonHandler} showLoader={false} />
      </CustomContentCardWithModal>,
    )

    const CustomContentCardWrapper = CustomContentCardWithModalWrapper.find('CustomContentCard').at(1).dive()

    expect(CustomContentCardWrapper.find('.progress-primary').length === 0).toBeTruthy()
  })
})
