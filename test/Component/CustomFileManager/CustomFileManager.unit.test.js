/**
 * @jest-environment jsdom
 */
import React from 'react'
import {
  shallow, configure,
} from 'enzyme'
import Adapater from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import { CustomFileManager } from '../../../src/components/CustomFileManager'
import ActionButton from '../../../src/components/ActionButton'


configure({ adapter: new Adapater() })

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

describe('component: CustomFileManager', () => {
  let sandbox
  const titlesConfig = {
    alertSucessTitle: 'Los bundles se han creado correctamente en producción',
    headTitle: 'Carga Masiva de Bundles',
    donwloadButtonText: 'Descargar Bundles',
    uploadStagingButtonText: 'Subir bundles a staging',
    uploadProductionButtonText: 'Subir bundles a producción',
    checkInfoText: ' He revisado en el sitio de pruebas que los bundles posean la información correcta antes de subir a producción.',
  }

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('run handleLocalGenerateXLSXClick when click', async () => {
    const handleGenerateXLSXClick = jest.fn()
    const handleUploadToStaging = jest.fn()
    const handleUploadToProduction = jest.fn()

    const wrapper = shallow(
      <CustomFileManager
        titlesConfig={titlesConfig}
        handleGenerateXLSXClick={handleGenerateXLSXClick}
        handleUploadToStaging={handleUploadToStaging}
        handleUploadToProduction={handleUploadToProduction}
      />,
    )
    const handleGenerateXLSXClickSpy = jest.spyOn(wrapper.instance().props, 'handleGenerateXLSXClick')
    expect(wrapper.state().generateXLSXLoader).toBeFalsy()
    expect(handleGenerateXLSXClickSpy).toHaveBeenCalledTimes(0)
    wrapper.find(ActionButton).at(0).simulate('click')
    expect(wrapper.state().generateXLSXLoader).toBeTruthy()
    expect(handleGenerateXLSXClickSpy).toHaveBeenCalledTimes(1)
  })

  test('should catch error when handleGenerateXLSXClick fail', async () => {
    const handleGenerateXLSXClick = jest.fn(Promise.reject(new Error('generate error')))
    const handleUploadToStaging = jest.fn()
    const handleUploadToProduction = jest.fn()

    const wrapper = shallow(
      <CustomFileManager
        titlesConfig={titlesConfig}
        handleGenerateXLSXClick={handleGenerateXLSXClick}
        handleUploadToStaging={handleUploadToStaging}
        handleUploadToProduction={handleUploadToProduction}
      />,
    )
    const spy = jest.spyOn(wrapper.instance(), 'handleError')
    wrapper.instance().handleLocalGenerateXLSXClick()
    expect(spy).toHaveBeenCalledTimes(1)
  })

  test('run handleLocalUploadToProduction when click', async () => {
    const handleGenerateXLSXClick = jest.fn()
    const handleUploadToStaging = jest.fn()
    const handleUploadToProduction = jest.fn()

    const wrapper = shallow(
      <CustomFileManager
        titlesConfig={titlesConfig}
        handleGenerateXLSXClick={handleGenerateXLSXClick}
        handleUploadToStaging={handleUploadToStaging}
        handleUploadToProduction={handleUploadToProduction}
      />,
    )
    const spy = jest.spyOn(wrapper.instance().props, 'handleUploadToProduction')
    wrapper.instance().handleLocalUploadToProduction()
    expect(spy).toHaveBeenCalledTimes(1)
  })

  test('should catch error when handleUploadToProduction fail', async () => {
    const handleGenerateXLSXClick = jest.fn()
    const handleUploadToStaging = jest.fn()
    const handleUploadToProduction = jest.fn(Promise.reject(new Error('upload error')))

    const wrapper = shallow(
      <CustomFileManager
        titlesConfig={titlesConfig}
        handleGenerateXLSXClick={handleGenerateXLSXClick}
        handleUploadToStaging={handleUploadToStaging}
        handleUploadToProduction={handleUploadToProduction}
      />,
    )
    const spy = jest.spyOn(wrapper.instance(), 'handleError')
    wrapper.instance().handleLocalUploadToProduction()
    expect(spy).toHaveBeenCalledTimes(1)
  })
})
