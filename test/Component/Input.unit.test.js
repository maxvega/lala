/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Input from '../../src/components/Input'

describe('component: Input', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should get props', async () => {
    const InputComponent = enzyme.mount(
      <Input value="oli" />,
    )
    expect(InputComponent.props().value).toEqual('oli')
  })
})
