/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import ActionButton from '../../src/components/ActionButton'

describe('component: ActionButton', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render children', async () => {
    const ActionButtonComponent = enzyme.mount(
      <ActionButton>Click Aquí</ActionButton>,
    )
    expect(ActionButtonComponent.children().text()).toEqual('Click Aquí')
  })
})
