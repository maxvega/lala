import React from 'react'
import { shallow } from 'enzyme'
import CustomParagraph from '../../../src/components/CustomParagraph'

describe('CustomParagraph', () => {
  test('should selected grid index state', () => {
    const clickHandler = jest.fn()
    const wrapper = shallow(<CustomParagraph icon="icon" clickHandler={clickHandler} />)

    wrapper.find('i').at(0).simulate('click')

    expect(clickHandler.mock.calls).toEqual([[]])
  })
})
