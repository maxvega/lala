/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import TextButton from '../../src/components/TextButton'

describe('component: TextButton', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render children', async () => {
    const TextButtonComponent = enzyme.mount(
      <TextButton><p>body</p></TextButton>,
    )
    expect(TextButtonComponent.find('p').length).toEqual(1)
  })
})
