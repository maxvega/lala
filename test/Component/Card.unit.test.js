/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Card from '../../src/components/Card'

describe('component: Card', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render children', async () => {
    const CardComponent = enzyme.mount(
      <Card><p>body</p></Card>,
    )
    expect(CardComponent.find('p').length).toEqual(1)
  })
})
