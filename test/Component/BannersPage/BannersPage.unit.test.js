/**
 * @jest-environment jsdom
 */

import React from 'react'
import { configure, shallow } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import ClientBFF from '../../../src/common/ClientBFF'
import BannersPage from '../../../src/components/BannersPage'
import { gridResponse, countdownResponse } from '../../Mocks/bannersPage'

configure({ adapter: new Adapter() })

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

window.__ENV__ = { baseURLStagingBFF: 'http://localhost:3000', timeout: 5000 }

describe('BannersPage', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should retrieveData and set the correct states', async () => {
    const wrapper = shallow(
      <BannersPage
        title="Banners Lider BCI"
        rootName="bannersBCI"
        resourceName="banners_settings.json"
        showBannerInfoSection
      />,
    )

    const downloadStub = sandbox.stub(ClientBFF.prototype, 'download')
    downloadStub.onCall(0).returns(Promise.resolve({ grids: { grid: gridResponse } }))
    downloadStub.onCall(1).returns(Promise.resolve({ grids: { grid: gridResponse } }))
    await wrapper.instance().retrieveData()

    sinon.assert.calledTwice(downloadStub)
  })

  test('should hide the editSection', () => {
    const wrapper = shallow(
      <BannersPage
        title="Banners Lider BCI"
        rootName="bannersBCI"
        resourceName="banners_settings.json"
        showBannerInfoSection
      />,
    )
    wrapper.setState({
      showEditSection: true,
      selectedBanner: {
        area: 'HuinchaSemana', backgroundDesktop: 'banners/bannersBCI/HuinchaSemana/desktop/10_29_20_12CI-1604607738397.png', backgroundMobile: 'banners/liderbci/mobile/Cyber_12cuotasImbatibles_12CI-CYBER-MOB.png', link: 'category/Precios_Bajos/Tecno/Televisión', tag: 'gm_11_05_2020_slider1_Loquieroya_Televisión', order: 1, updated: '24-09-2020', target: '_self', internalName: 'HuinchaSemana', name: 'Banner Lo quiero ya Televisión',
      },
      selectedBannerImage: ['banners/bannersBCI/HuinchaSemana/mobile/10_29_20_12CI_mob-1604607688199.png'],
    })

    expect(wrapper.instance().state.showEditSection).toBeTruthy()
    wrapper.instance().hideEditSection()
    expect(wrapper.instance().state.showEditSection).toBeFalsy()
  })

  test('should set the correct image in the state', () => {
    const wrapper = shallow(
      <BannersPage
        title="Banners Lider BCI"
        rootName="bannersBCI"
        resourceName="banners_settings.json"
        showBannerInfoSection
      />,
    )
    wrapper.setState({
      selectedBanner: {
        area: 'HuinchaSemana', backgroundDesktop: 'banners/bannersBCI/HuinchaSemana/desktop/10_29_20_12CI-1604607738397.png', backgroundMobile: 'banners/liderbci/mobile/Cyber_12cuotasImbatibles_12CI-CYBER-MOB.png', link: 'category/Precios_Bajos/Tecno/Televisión', tag: 'gm_11_05_2020_slider1_Loquieroya_Televisión', order: 1, updated: '24-09-2020', target: '_self', internalName: 'HuinchaSemana', name: 'Banner Lo quiero ya Televisión',
      },
      selectedBannerImage: ['banners/bannersBCI/HuinchaSemana/mobile/10_29_20_12CI_mob-1604607688199.png'],
    })
    wrapper.instance().handleEditSelectedBannerImage('newMobileImage')
    expect(wrapper.instance().state.selectedBanner.backgroundMobile).toBe('newMobileImage')
    wrapper.setState({ imageDevice: 'desktop' })
    wrapper.instance().handleEditSelectedBannerImage('newDesktopImage')
    expect(wrapper.instance().state.selectedBanner.backgroundDesktop).toBe('newDesktopImage')
  })

  test('should show countdown banners', async () => {
    const title = 'Cuentas regresivas'

    const wrapper = shallow(
      <BannersPage
        title={title}
        rootName="countdownBanner"
        resourceName="countdown_settings.json"
        showBannerInfoSection
      />,
    )

    const downloadStub = sandbox.stub(ClientBFF.prototype, 'download').returns(Promise.resolve(countdownResponse))
    await wrapper.instance().retrieveData()

    expect(wrapper.props().children[1].props.children[0].props.title).toBe(title)
    expect(wrapper.props().children[1].props.children[0].props.actionButtonTitle).toBe('crear nuevo')

    sinon.assert.calledTwice(downloadStub)
  })
})
