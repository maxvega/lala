import React from 'react'
import {
  render, screen, cleanup,
} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import DataContent from '../../../../src/components/BannersPage/Edit/DataContent'

describe('<DataContent/>', () => {
  afterEach(() => {
    cleanup()
  })

  test('<DataContent/> Should show all field correctly when is not countdown page', () => {
    const handleEditSelectedBanner = jest.fn()
    const showUpdateField = false
    const banner = {
      link: 'https://www.lider.cl/juntosnoscuidamos/',
      target: '_blank',
      tag: 'gm_04_13_2021_informativo_esenciales_rompefila',
      name: 'Informativo esenciales',
      startDate: '',
      endDate: '',
      counterBgColor: '#FFFFF',
      counterFontColor: '#FFFFF',
      updated: '17-09-2020',
      area: 'bottom',
    }

    render(<DataContent
      handleEditSelectedBanner={handleEditSelectedBanner}
      banner={banner}
      showUpdateField={showUpdateField}
    />)

    expect(screen.getByTestId('data-link')).toBeInTheDocument()
    expect(screen.getByTestId('data-link').tagName).toBe('INPUT')
    expect(screen.queryByTestId('data-select')).toBeInTheDocument()
    expect(screen.getByTestId('data-select').tagName).toBe('DIV')
    expect(screen.getByTestId('data-tag')).toBeInTheDocument()
    expect(screen.getByTestId('data-tag').tagName).toBe('INPUT')
    expect(screen.getByTestId('data-name')).toBeInTheDocument()
    expect(screen.getByTestId('data-name').tagName).toBe('INPUT')
    expect(screen.queryByTestId('startDate')).not.toBeInTheDocument()
    expect(screen.queryByTestId('endDate')).not.toBeInTheDocument()
    expect(screen.getByTestId('data-counterBgColor')).toBeInTheDocument()
    expect(screen.getByTestId('data-counterBgColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('data-counterFontColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('data-counterFontColor')).toBeInTheDocument()
    expect(screen.queryByTestId('data-updated')).not.toBeInTheDocument()
  })

  test('<DataContent/> Should show all field correctly when countdown page is', () => {
    const handleEditSelectedBanner = jest.fn()
    const showUpdateField = true
    const banner = {
      link: 'https://www.lider.cl/juntosnoscuidamos/',
      target: '_blank',
      tag: 'gm_04_13_2021_informativo_esenciales_rompefila',
      name: 'Informativo esenciales',
      startDate: '2021-05-17T12:48',
      endDate: '2021-05-17T13:48',
      counterBgColor: '#FFFFF',
      counterFontColor: '#FFFFF',
      updated: '17-09-2020',
      area: 1,
    }

    render(<DataContent
      handleEditSelectedBanner={handleEditSelectedBanner}
      banner={banner}
      showUpdateField={showUpdateField}
    />)

    expect(screen.getByTestId('data-link')).toBeInTheDocument()
    expect(screen.getByTestId('data-link').tagName).toBe('INPUT')
    expect(screen.queryByTestId('data-select')).toBeInTheDocument()
    expect(screen.getByTestId('data-select').tagName).toBe('DIV')
    expect(screen.getByTestId('data-tag')).toBeInTheDocument()
    expect(screen.getByTestId('data-tag').tagName).toBe('INPUT')
    expect(screen.getByTestId('data-name')).toBeInTheDocument()
    expect(screen.getByTestId('data-name').tagName).toBe('INPUT')
    expect(screen.queryByTestId('startDate')).toBeInTheDocument()
    expect(screen.queryByTestId('endDate')).toBeInTheDocument()
    expect(screen.getByTestId('data-counterBgColor')).toBeInTheDocument()
    expect(screen.getByTestId('data-counterBgColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('data-counterFontColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('data-counterFontColor')).toBeInTheDocument()
  })
})
