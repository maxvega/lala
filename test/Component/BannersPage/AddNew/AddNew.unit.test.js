import React from 'react'
import {
  render, screen,
} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import ClientBFF from '../../../../src/common/ClientBFF'
import AddNew from '../../../../src/components/BannersPage/AddNew'
import { countdownResponse } from '../../../Mocks/bannersPage'


configure({ adapter: new Adapter() })

window.__ENV__ = { baseURLStagingBFF: 'http://localhost:3000', timeout: 5000 }

const setSelectedBannerIndex = jest.fn()
const retrieveData = jest.fn()

describe('<AddNew/>', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('<AddNew/> Should show all field correctly', () => {
    render(<AddNew
      setSelectedBannerIndex={setSelectedBannerIndex}
      retrieveData={retrieveData}
    />)

    expect(screen.getByTestId('header-container-title')).toBeInTheDocument()
    expect(screen.getByTestId('zmdi-chevron-left')).toBeInTheDocument()
    expect(screen.getByTestId('name').tagName).toBe('INPUT')
    expect(screen.getByTestId('link').tagName).toBe('INPUT')
    expect(screen.getByTestId('tag').tagName).toBe('INPUT')
    expect(screen.getByTestId('startDate').tagName).toBe('DIV')
    expect(screen.getByTestId('endDate').tagName).toBe('DIV')
    expect(screen.getByTestId('counterBgColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('counterFontColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('action-button-to-add-cr')).toBeInTheDocument()
    expect(screen.getByTestId('action-button-to-add-cr').textContent).toBe('crear cuenta regresiva')
  })

  test('<AddNew/> should add a new countdown correctly', async () => {
    sandbox.stub(ClientBFF.prototype, 'download').returns(Promise.resolve(countdownResponse))
    const uploadToStagingStub = sandbox.stub(ClientBFF.prototype, 'upload').returns(Promise.resolve(true))

    const wrapper = shallow(<AddNew
      setSelectedBannerIndex={setSelectedBannerIndex}
      retrieveData={retrieveData}
    />)

    const newCountDown = {
      startDate: '2020-01-01T08:00',
      endDate: '2020-01-01T16:00',
      link: 'ok',
      counterBgColor: '#ffff',
      counterFontColor: '#ffff',
      name: 'CR TEST',
      tag: 'TAG',
    }

    expect(wrapper.state().showConflictDatesErrorAlert).toBeFalsy()

    wrapper.setState({
      newCountDown,
    })

    await wrapper.instance().createCountdown()
    expect(wrapper.state().showConflictDatesErrorAlert).toBeFalsy()
    sinon.assert.calledOnce(uploadToStagingStub)
  })

  test('<AddNew/> should not add a new countdown if there conflict between dates', async () => {
    sandbox.stub(ClientBFF.prototype, 'download').returns(Promise.resolve(countdownResponse))
    const uploadToStagingStub = sandbox.stub(ClientBFF.prototype, 'upload').returns(Promise.resolve())

    const wrapper = shallow(<AddNew
      setSelectedBannerIndex={setSelectedBannerIndex}
      retrieveData={retrieveData}
    />)

    const newCountDown = {
      startDate: '2021-05-13T08:00',
      endDate: '2021-05-15T08:00',
      link: 'ok',
      counterBgColor: '#ffff',
      counterFontColor: '#ffff',
      name: 'CR TEST',
      tag: 'TAG',
    }

    expect(wrapper.state().showConflictDatesErrorAlert).toBeFalsy()

    wrapper.setState({
      newCountDown,
    })

    await wrapper.instance().createCountdown()

    expect(wrapper.state().showConflictDatesErrorAlert).toBeTruthy()
    sinon.assert.notCalled(uploadToStagingStub)
  })
})
