import React from 'react'
import {
  render, screen,
} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import DataContentAdd from '../../../../src/components/BannersPage/Add/DataContentAdd'
import showParametersList from '../../../Mocks/configDataContentAdd'

describe('<DataContentAdd/>', () => {
  test('<DataContentAdd/> Should show all field correctly', () => {
    const getNewElementInfo = jest.fn()
    render(<DataContentAdd
      showParametersList={showParametersList}
      getNewElementInfo={getNewElementInfo}
    />)

    expect(screen.getByTestId('name')).toBeInTheDocument()
    expect(screen.getByTestId('name').tagName).toBe('INPUT')
    expect(screen.getByTestId('link')).toBeInTheDocument()
    expect(screen.getByTestId('link').tagName).toBe('INPUT')
    expect(screen.getByTestId('tag')).toBeInTheDocument()
    expect(screen.getByTestId('tag').tagName).toBe('INPUT')
    expect(screen.getByTestId('startDate')).toBeInTheDocument()
    expect(screen.getByTestId('startDate').tagName).toBe('DIV')
    expect(screen.getByTestId('endDate')).toBeInTheDocument()
    expect(screen.getByTestId('endDate').tagName).toBe('DIV')
    expect(screen.getByTestId('counterBgColor')).toBeInTheDocument()
    expect(screen.getByTestId('counterBgColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('counterFontColor').tagName).toBe('INPUT')
    expect(screen.getByTestId('counterFontColor')).toBeInTheDocument()
  })
})
