/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import RoundIconWrapper from '../../src/components/RoundIconWrapper'

describe('component: RoundIconWrapper', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render children', async () => {
    const RoundIconWrapperComponent = enzyme.mount(
      <RoundIconWrapper><p>body</p></RoundIconWrapper>,
    )
    expect(RoundIconWrapperComponent.find('p').length).toEqual(1)
  })
})
