/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme, { configure } from 'enzyme'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import Adapter from 'enzyme-adapter-react-16'
import { MemoryRouter as Router, Route } from 'react-router-dom'
import * as Helper from '../../src/Helper'
import SideNavMenu from '../../src/components/SideNavMenu'

const { userRoles } = require('../Mocks/userRoles')
const { menuToDisplay } = require('../Mocks/menuToDisplay')
const { permissions } = require('../Mocks/permissions')

configure({ adapter: new Adapter() })

const mockStore = configureStore([thunk])
window.__ENV__ = { LOCAL_AUTH_KEY: 'wm-blackcyber-auth-key', IS_STRATI: true }
let wrapper

const store = mockStore({
  userDataReducer: {
  },
})

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

beforeEach(() => {
  jest.clearAllMocks()
  Helper.getAuthData.mockReturnValue({ user: {} })
  wrapper = enzyme.shallow(
    <Router>
      <SideNavMenu store={store} />
    </Router>,
  ).dive().dive().dive()
    .dive()
    .dive()
    .dive()
})

beforeAll(() => {
  Helper.getAuthData = jest.fn()
  Helper.calculateMenuToDisplay = jest.fn()
  Helper.cleanUserAndLogout = jest.fn(() => null)
})

describe('Component: SideNavMenuComponent', () => {
  test('should change expanded state on toggle', () => {

    expect(wrapper.state().expanded).toBeTruthy()
    wrapper.instance().onToggle()
    expect(wrapper.state().expanded).toBeFalsy()
    wrapper.instance().onToggle()
    expect(wrapper.state().expanded).toBeTruthy()
  })

  test('Should show logo when expanded', async () => {
    expect(wrapper.state().expanded).toBeTruthy()
    expect(wrapper.find('#logo').html()).toEqual(expect.stringContaining('buysmart-logo.svg'))
  })

  test('Should NOT show logo when not expanded', async () => {
    wrapper.instance().onToggle()
    expect(wrapper.state().expanded).toBeFalsy()
    expect(wrapper.find('#logo').html()).toEqual(expect.not.stringContaining('buysmart-logo.svg'))
  })

  test('should change modalIsOpen state on toggle', () => {
    expect(wrapper.state().modalIsOpen).toBeFalsy()
    wrapper.instance().toggleModal()
    expect(wrapper.state().modalIsOpen).toBeTruthy()
    wrapper.instance().toggleModal()
    expect(wrapper.state().modalIsOpen).toBeFalsy()
  })

  test('should render modal on click Cerrar Sesión', () => {
    const toggleSpy = jest.spyOn(wrapper.instance(), 'toggleModal')
    wrapper.instance().forceUpdate()

    wrapper.find('#logout').simulate('click')
    expect(wrapper.state().modalIsOpen).toBeTruthy()
    expect(toggleSpy).toHaveBeenCalledTimes(1)
  })

  test('should close modal on click ActionButton Cancelar', () => {
    // set modalIsOpen = true
    wrapper.instance().toggleModal()
    const toggleSpy = jest.spyOn(wrapper.instance(), 'toggleModal')

    // Cancelar is the second ActionButton
    wrapper.find('ActionButton').at(1).simulate('click')
    expect(wrapper.state().modalIsOpen).toBeFalsy()
    expect(toggleSpy).toHaveBeenCalledTimes(1)
  })

  test('should close modal when click close modal button', () => {
    // set modalIsOpen = true
    wrapper.instance().toggleModal()
    const toggleSpy = jest.spyOn(wrapper.instance(), 'toggleModal')

    const modalElement = wrapper.find('StyledModal').dive()
    modalElement.find('styled__CloseModalButton').simulate('click')
    expect(wrapper.state().modalIsOpen).toBeFalsy()
    expect(toggleSpy).toHaveBeenCalledTimes(1)
  })

  test('should logout on click ActionButton Aceptar', () => {
    const logoutSpy = jest.spyOn(wrapper.instance(), 'handleLogOut')
    // Aceptar is the first ActionButton
    wrapper.find('ActionButton').at(0).simulate('click')
    expect(logoutSpy).toHaveBeenCalled()
  })

  test('Should change history onSelect', async () => {
    Helper.getAuthData.mockReturnValue(userRoles)
    Helper.calculateMenuToDisplay.mockReturnValue(menuToDisplay)
    const wrapper = enzyme.shallow(
      <Router initialEntries={['/app']}>
        <Route render={({ location, history }) => (
          <SideNavMenu store={store} location={location} history={history} />
        )}
        />
      </Router>,
    ).dive().dive().dive()
      .dive()
      .dive()
      .dive()
      .dive()
      .dive()
      .dive()

    expect(wrapper.instance().props.history).toHaveLength(1)
    expect(wrapper.instance().props.history.location.pathname).toEqual('/app')
    wrapper.instance().onSelect('app/products/bundle')
    expect(wrapper.instance().props.history).toHaveLength(2)
    expect(wrapper.instance().props.history.location.pathname).toEqual('/app/products/bundle')
  })

  test('Should NOT change history onSelect', async () => {
    Helper.getAuthData.mockReturnValue(userRoles)
    Helper.calculateMenuToDisplay.mockReturnValue(menuToDisplay)
    const wrapper = enzyme.shallow(
      <Router initialEntries={['/app/products/bundle']}>
        <Route render={({ location, history }) => (
          <SideNavMenu store={store} location={location} history={history} />
        )}
        />
      </Router>,
    ).dive().dive().dive()
      .dive()
      .dive()
      .dive()
      .dive()
      .dive()
      .dive()

    expect(wrapper.instance().props.history).toHaveLength(1)
    expect(wrapper.instance().props.history.location.pathname).toEqual('/app/products/bundle')
    wrapper.instance().onSelect('app/products/bundle')
    expect(wrapper.instance().props.history).toHaveLength(1)
  })

  test('should return empty permissions array', () => {
    Helper.getAuthData.mockReturnValue(undefined)
    expect(wrapper.instance().getPermissions(true)).toHaveLength(0)
  })

  test('should return permissions array', () => {
    Helper.getAuthData.mockReturnValue(userRoles)
    wrapper = enzyme.shallow(
      <Router>
        <SideNavMenu store={store} />
      </Router>,
    ).dive().dive().dive()
      .dive()
      .dive()
      .dive()
    expect(wrapper.instance().getPermissions(true)).toEqual(permissions)
  })

  test('should display Navigation Items with permissions', () => {

    Helper.getAuthData.mockReturnValue(userRoles)
    Helper.calculateMenuToDisplay.mockReturnValue(menuToDisplay)
    wrapper = enzyme.shallow(
      <Router>
        <SideNavMenu store={store} />
      </Router>,
    ).dive().dive().dive()
      .dive()
      .dive()
      .dive()

    // Menu Items
    let navItems = 0
    menuToDisplay.forEach((item) => {
      expect(wrapper.findWhere(n => n.props().eventKey === item.eventKey)).toHaveLength(1)
      navItems += 1
      if (item.children) {
        item.children.forEach((child) => {
          expect(wrapper.findWhere(n => n.props().eventKey === child.eventKey)).toHaveLength(1)
          navItems += 1
        })
      }
    })
    // Nav Items Total + home
    expect(wrapper.find('NavItem')).toHaveLength(navItems + 1)
  })

  test('should display home whithout permissions', () => {
    const spyHomeNavItem = jest.spyOn(wrapper.instance(), 'renderNavItemHome')
    wrapper.instance().forceUpdate()

    expect(spyHomeNavItem).toHaveBeenCalled()
    expect(wrapper.find('NavItem').find('.zmdi-home')).toHaveLength(1)
    // Nav Items Total
    expect(wrapper.find('NavItem')).toHaveLength(1)
  })

  test('should display logout whithout permissions', () => {
    const spyLogoutItem = jest.spyOn(wrapper.instance(), 'renderNavItemLogOut')
    wrapper.instance().forceUpdate()

    expect(spyLogoutItem).toHaveBeenCalled()
    expect(wrapper.find('#logout')).toHaveLength(1)
  })

  const routeRenderValidator = (routePath, validLength, checkLocation = false) => {
    const wrapper = enzyme.shallow(
      <Router initialEntries={[`/${routePath}`]}>
        <SideNavMenu store={store} />
      </Router>,
    ).dive().dive().dive()
      .dive()
      .dive()
      .dive()

    const routeElement = wrapper.findWhere(n => n.props().path === `/${routePath}`).dive().dive()
    expect(routeElement.find('LoadableComponent')).toHaveLength(validLength)
    if (checkLocation) {
      expect(routeElement.find('LoadableComponent').props().location.pathname).toEqual(`/${routePath}`)
    }
  }

  test('should render Routes with permissions', () => {
    Helper.getAuthData.mockReturnValue(userRoles)
    const validLength = 1
    const checkLocation = true
    menuToDisplay.forEach((item) => {
      if (item.eventKey.startsWith('app')) {
        routeRenderValidator(item.eventKey, validLength, checkLocation)
      }
      if (item.children) {
        item.children.forEach(
          child => routeRenderValidator(child.eventKey, validLength, checkLocation),
        )
      }
    })
  })

  test('should NOT render Route without permissions', () => {
    const validLength = 0
    menuToDisplay.forEach((item) => {
      if (item.eventKey.startsWith('app')) {
        routeRenderValidator(item.eventKey, validLength)
      }
      if (item.children) {
        item.children.forEach(child => routeRenderValidator(child.eventKey, validLength))
      }
    })
  })
  test('render home without permissions', () => {
    const routePath = 'app'
    const validLength = 1
    const checkLocation = true
    routeRenderValidator(routePath, validLength, checkLocation)
  })

  test('render profile page without permissions', () => {
    const routePath = 'app/profile'
    const validLength = 1
    const checkLocation = true
    routeRenderValidator(routePath, validLength, checkLocation)
  })
})
