import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import BannerContentHeader from '../../../src/components/BannersPage/BannerContentHeader'

configure({ adapter: new Adapter() })

describe('BannerContentHeader', () => {
  test('should run actionButtonHandler when icon click', () => {
    const actionButtonHandler = jest.fn()
    const wrapper = shallow(
      <BannerContentHeader
        image="image.jpeg"
        actionButtonHandler={actionButtonHandler}
        bannerTitle="titulo A"
      />,
    )

    wrapper.find('i').at(0).simulate('click')

    expect(actionButtonHandler).toHaveBeenCalledTimes(1)
  })
})
