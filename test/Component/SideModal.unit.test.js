/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import SideModal from '../../src/components/SideModal'

describe('component: SideModal', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render children', async () => {
    const SideModalComponent = enzyme.mount(
      <SideModal><p>body</p></SideModal>,
    )
    expect(SideModalComponent.find('p').length).toEqual(1)
  })
})
