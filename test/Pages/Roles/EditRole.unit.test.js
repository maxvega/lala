/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Element from '../../../src/components/Card/Element'
import EditRoleCard from '../../../src/Pages/Roles/EditRoleCard'
import ClientAuth from '../../../src/common/ClientAuth'

describe('Component: EditRoleCard', () => {
  let sandbox
  const PERMISSIONS = [
    {
      id: '562511529933111302',
      name: 'users.create',
      description: 'can create users',
      enabled: true,
      revision: '0',
    },
  ]
  const ROLE = {
    id: '1',
    name: 'Admin',
    description: 'Administrador del sitio',
    enabled: true,
    isCore: true,
    revision: '0',
    permissions: [{
      id: '562511529933111302',
      name: 'users.create',
      description: 'can create users',
      enabled: true,
      revision: '0',
      RolePermission: {
        id: '562511530610720774',
        PermissionId: '562511529933111302',
        RoleId: '562511530276388870',
      },
    }],
  }
  const getRolesAndPermissionsSpy = jest.fn()
  const closeSideNavSpy = jest.fn()
  const handleFeedbackSpy = jest.fn()
  const setSelectedRoleSpy = jest.fn()

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })
  test('should render the correct number of permissions', async () => {
    const EditRoleCardComponent = enzyme.mount(
      <EditRoleCard permissions={PERMISSIONS} getRolesAndPermissions={getRolesAndPermissionsSpy} role={ROLE} />,
    )
    expect(EditRoleCardComponent.find(Element).length).toEqual(1)
  })

  test('should toggle permission', async () => {
    const EditRoleCardComponent = enzyme.mount(
      <EditRoleCard permissions={PERMISSIONS} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} role={ROLE} />,
    )
    EditRoleCardComponent.instance().togglePermission(PERMISSIONS[0].id)
    expect(EditRoleCardComponent.instance().state.permissions[0].applied).toBeFalsy()
    EditRoleCardComponent.instance().togglePermission(PERMISSIONS[0].id)
    expect(EditRoleCardComponent.instance().state.permissions[0].applied).toBeTruthy()
  })

  test('should show the modal with update role completed', async () => {
    sandbox.stub(ClientAuth.prototype, 'updateRole').returns(Promise.resolve())
    const EditRoleCardComponent = enzyme.shallow(
      <EditRoleCard setSelectedRole={setSelectedRoleSpy} role={ROLE} permissions={PERMISSIONS} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} />,
    )
    await EditRoleCardComponent.instance().updateRole()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Rol editado correctamente',
    })
  })

  test('should show the modal with error when updateRole fails', async () => {
    sandbox.stub(ClientAuth.prototype, 'updateRole').returns(Promise.reject({ response: { data: { message: 'error' } } }))
    const EditRoleCardComponent = enzyme.shallow(
      <EditRoleCard setSelectedRole={setSelectedRoleSpy} role={ROLE} permissions={PERMISSIONS} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} />,
    )
    await EditRoleCardComponent.instance().updateRole()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Error al editar',
      message: 'Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: error',
    })
  })
})
