/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import RolesWithModal, { Roles } from '../../../src/Pages/Roles'

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

describe('Page: Roles', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render overlay class when newRole active', async () => {
    const RolesComponent = enzyme.shallow(
      <RolesWithModal
        title="title"
      />,
    )
    const RolesWithoutModal = RolesComponent.find(Roles).dive()
    RolesWithoutModal.setState({ showNewRole: true })
    expect(RolesWithoutModal.find('.overlay-right-modal').length).toEqual(1)
  })

  test('should render overlay class when newPermission active', async () => {
    const RolesComponent = enzyme.shallow(
      <RolesWithModal
        title="title"
      />,
    )
    const RolesWithoutModal = RolesComponent.find(Roles).dive()
    RolesWithoutModal.setState({ showNewRole: true })
    expect(RolesWithoutModal.find('.overlay-right-modal').length).toEqual(1)
  })

  test('should not render overlay class', async () => {
    const RolesComponent = enzyme.shallow(
      <RolesWithModal
        title="title"
      />,
    )
    const RolesWithoutModal = RolesComponent.find(Roles).dive()
    expect(RolesWithoutModal.find('.overlay-right-modal').length).toEqual(0)
  })
})
