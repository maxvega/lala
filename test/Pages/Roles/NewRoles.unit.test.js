/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Element from '../../../src/components/Card/Element'
import NewRole from '../../../src/Pages/Roles/NewRole'
import ClientAuth from '../../../src/common/ClientAuth'

describe('Component: NewRole', () => {
  let sandbox
  const PERMISSIONS = [
    {
      id: '562511529933111302',
      name: 'users.create',
      description: 'can create users',
      enabled: true,
      revision: '0',
    },
  ]
  const handleFeedbackSpy = jest.fn()
  const closeSideNavSpy = jest.fn()
  const getRolesAndPermissionsSpy = jest.fn()

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render the correct number of permissions', async () => {
    const NewRoleComponent = enzyme.mount(
      <NewRole permissions={PERMISSIONS} />,
    )
    expect(NewRoleComponent.find(Element).length).toEqual(1)
  })

  test('should show the modal with create role completed', async () => {
    sandbox.stub(ClientAuth.prototype, 'createRole').returns(Promise.resolve())
    const NewRoleComponent = enzyme.shallow(
      <NewRole permissions={PERMISSIONS} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} />,
    )
    await NewRoleComponent.instance().createRole()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Creación exitosa',
      message: 'El rol ha sido creado correctamente',
    })
  })

  test('should show the modal with error when createRole fails', async () => {
    sandbox.stub(ClientAuth.prototype, 'createRole').returns(Promise.reject({ response: { data: { message: 'error' } } }))
    const NewRoleComponent = enzyme.shallow(
      <NewRole permissions={PERMISSIONS} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} />,
    )
    await NewRoleComponent.instance().createRole()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Error al crear',
      message: 'Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: error',
    })
  })

  test('should show the modal with error when parameters are missing on save role', async () => {
    const NewRoleComponent = enzyme.shallow(
      <NewRole permissions={PERMISSIONS} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} />,
    )
    await NewRoleComponent.instance().saveRole()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Faltan Campos',
      message: 'Por favor revisa los campos solicitados',
    })
  })

  test('should toggle permission', async () => {
    const NewRoleComponent = enzyme.mount(
      <NewRole permissions={PERMISSIONS} closeSideNav={closeSideNavSpy} getRolesAndPermissions={getRolesAndPermissionsSpy} />,
    )
    NewRoleComponent.instance().togglePermission(PERMISSIONS[0].id)
    expect(NewRoleComponent.instance().state.permissions[0].applied).toBeTruthy()
    NewRoleComponent.instance().togglePermission(PERMISSIONS[0].id)
    expect(NewRoleComponent.instance().state.permissions[0].applied).toBeFalsy()
  })
})
