/**
 * @jest-environment jsdom
 */
import React from 'react'
import { act } from 'react-dom/test-utils'
import enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import ClientBFF from '../../../src/common/ClientBFF'
import * as Helper from '../../../src/Helper'
import HeaderAlertMessage from '../../../src/Pages/HeaderAlertMessage'

enzyme.configure({ adapter: new Adapter() })

const mock = {
  headerAlertMessageConfig: {
    enabled: true,
    content: 'Mensaje de Pruebas',
  },
}

// hack to force jest to wait until the component was fully rendered
const waitForComponentToPaint = async (wrapper) => {
  await act(async () => {
    await new Promise(resolve => setTimeout(resolve, 0))
    wrapper.update()
  })
}

describe('Page: HeaderAlertMessage', () => {
  let sandbox
  let component

  beforeEach(async () => {
    sandbox = sinon.createSandbox()
    sandbox.stub(ClientBFF.prototype, 'download').resolves(mock)
    sandbox.stub(ClientBFF.prototype, 'bffInstance').resolves({})
    sandbox.stub(ClientBFF.prototype, 'upload').resolves({})
    sandbox.stub(Helper, 'getAuthData').returns(undefined)
    sandbox.stub(window, 'open').returns(jest.fn())
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should be defined', async () => {
    const component = enzyme.shallow(<HeaderAlertMessage title="Huincha de Alertas" />)
    expect(component).toBeDefined()
  })

  test('should have a toggle button', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    expect(component.find('div#toggle-enabled').length).toEqual(1)
  })

  test('should have a input text', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    expect(component.find('input#content').length).toEqual(1)
  })

  test('should have a input text with the default text', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    const result = component.find('input#content')
    expect(result.length).toEqual(1)
    expect(result.get(0).props.value).toBe('Mensaje de Pruebas')
  })

  test('should have a action button to update staging', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    const button = component.find('button.action-staging')
    expect(button).toBeDefined()
    expect(button.props().children).toBe('probar huincha en staging')
  })

  test('should open new window when action button to update staging is clicked', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    const button = component.find('button.action-staging')
    expect(button).toBeDefined()
    expect(button.props().children).toBe('probar huincha en staging')
    button.simulate('click')
    await waitForComponentToPaint(component)
    expect(window.open.calledOnce).toBe(true)
  })

  test('should not have a checkbox button to confirm update production', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    const checkbox = component.find({ type: 'checkbox' })
    expect(checkbox.length).toBe(0)
  })

  test('should render a checkbox button to confirm update production after click in the action button', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    component.find('button.action-staging').simulate('click')

    await waitForComponentToPaint(component)
    const checkbox = component.find({ type: 'checkbox' })
    expect(checkbox.length).toBe(1)
  })

  test('should render production button as disabled', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    component.find('button.action-staging').simulate('click')

    await waitForComponentToPaint(component)
    expect(component.find('button.action-production').props().disabled).toBe(true)
  })

  test('should activate production button when the checkbox is checked', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    component.find('button.action-staging').simulate('click')

    await waitForComponentToPaint(component)
    component.find({ type: 'checkbox' }).simulate('change')

    await waitForComponentToPaint(component)
    expect(component.find('button.action-production').props().disabled).toBe(false)
  })

  test('should open new window when production button is clicked', async () => {
    component = await enzyme.mount(
      <HeaderAlertMessage title="Huincha de Alertas" />,
    )
    await waitForComponentToPaint(component)
    component.find('button.action-staging').simulate('click')

    await waitForComponentToPaint(component)
    component.find({ type: 'checkbox' }).simulate('change')

    await waitForComponentToPaint(component)
    expect(component.find('button.action-production').props().disabled).toBe(false)
    component.find('button.action-production').simulate('click')

    await waitForComponentToPaint(component)
    expect(window.open.calledTwice).toBe(true)
  })
})
