/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Loader from 'Src/Pages/LogIn/Loader'

describe('Page: Loader', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render Loader', async () => {
    const LoaderComponent = enzyme.shallow(
      <Loader />,
    )
    expect(LoaderComponent.find('img').length).toEqual(2)
  })
})
