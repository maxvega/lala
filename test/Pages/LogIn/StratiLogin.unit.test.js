/**
 * @jest-environment jsdom
 */
import React from 'react'
import { act } from 'react-dom/test-utils'

import { configure, mount } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import StratiLogin from '../../../src/Pages/LogIn/StratiLogin'

const { userRoles } = require('../../Mocks/userRoles')

const mockStore = configureStore([thunk])
window.__ENV__ = { LOCAL_AUTH_KEY: 'wm-blackcyber-auth-key' }


describe('Page: StratiLogin', () => {
  configure({ adapter: new Adapter() })
  const userValidateAction = jest.fn()
  const history = { push: jest.fn() }

  const defineUrl = (url) => {
    Object.defineProperty(window, 'location', {
      value: {
        href: url,
        search: '',
      },
      writable: true,
    })
  }

  let sandbox

  beforeEach(() => {
    sandbox = sinon.createSandbox()
    jest.useFakeTimers()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('Should show error message when error in auth', () => {
    const store = mockStore({
      loginReducer: {
        loginInfo: {},
        result: 'error',
      },
    })


    const wrapper = mount(<StratiLogin userValidateAction={userValidateAction} history={history} store={store} />)

    act(() => {
      jest.runAllTimers()
      wrapper.update()
    })

    expect(wrapper.find('#showError').text()).toEqual('Algo salió mal :c')
  })

  test('Should not show error message when auth is pending and and redux state is in initialState', () => {
    const store = mockStore({
      loginReducer: {
        loginInfo: {},
        result: '',
      },
    })

    const wrapper = mount(<StratiLogin userValidateAction={userValidateAction} history={history} store={store} />)

    act(() => {
      jest.runAllTimers()
      wrapper.update()
    })
    expect(wrapper.find('#showError').exists()).toBe(false)
  })

  test('Should show loader and redirect when auth is success', async () => {
    global.window = Object.create(window)
    defineUrl('https://blackcyber-bcm-landing.cl-buysmart.dev.k8s.walmart.net/login?code=a1213r930m391a70nd992o')

    const store = mockStore({
      loginReducer: {
        loginInfo: userRoles,
        result: 'success',
      },
    })

    const wrapper = mount(<StratiLogin userValidateAction={userValidateAction} history={history} store={store} />)
    const loader = wrapper.find('Loader')
    expect(loader.exists()).toBe(true)

    act(() => {
      jest.runAllTimers()
      wrapper.update()
    })

    const actionButton = wrapper.find('ActionButton')
    expect(actionButton.exists()).toBe(false)
    expect(history.push).toBeCalled()
  })

  test('Should show ActionButton (Walmart Login) when auth is pending', async () => {
    global.window = Object.create(window)
    defineUrl('https://blackcyber-bcm-landing.cl-buysmart.dev.k8s.walmart.net/login')
    const store = mockStore({
      loginReducer: {
        loginInfo: {},
        result: 'pending',
      },
    })

    const wrapper = mount(<StratiLogin userValidateAction={userValidateAction} history={history} store={store} />)
    const loader = wrapper.find('Loader')
    expect(loader.exists()).toBe(true)

    act(() => {
      jest.runAllTimers()
      wrapper.update()
    })

    const actionButton = wrapper.find('ActionButton')
    expect(actionButton.exists()).toBe(true)
    expect(actionButton.text()).toEqual('Walmart Login')
  })
})
