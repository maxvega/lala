/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import OriginalLogin from '../../../src/Pages/LogIn/OriginalLogin'
import ActionButton from '../../../src/components/ActionButton'
import ClientAuth from '../../../src/common/ClientAuth'
import * as loginActions from '../../../src/Redux/actions/loginActions'

describe('Page: OriginalLogin', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should change email input', async () => {
    const userInfo = { result: '', loginInfo: {} }
    const values = { email: 'newEmail', password: 'email' }

    const wrapper = enzyme.mount(<OriginalLogin authUserCL={jest.fn()} userInfo={userInfo} history={jest.fn()} handleChange={jest.fn()} form={values} />)
    const input = wrapper.find('input')
    expect(input.get(0).props.value).toEqual('newEmail')
  })

  test('should change password input', async () => {
    const userInfo = { result: '', loginInfo: {} }
    const values = { email: 'newEmail', password: 'superSecurePassword123' }
    const wrapper = enzyme.mount(<OriginalLogin authUserCL={jest.fn()} userInfo={userInfo} history={jest.fn()} handleChange={jest.fn()} form={values} />)
    const input = wrapper.find('input')

    expect(input.get(1).props.value).toEqual('superSecurePassword123')
  })

  xtest('should handle submit', async () => {
    const userInfo = { result: '', loginInfo: {} }
    const values = { email: 'newEmail', password: 'superSecurePassword123' }
    const wrapper = enzyme.mount(<OriginalLogin authUserCL={jest.fn()} userInfo={userInfo} history={jest.fn()} handleChange={jest.fn()} form={values} />)
    const form = wrapper.find('form')

    const actionStub = sandbox.stub(loginActions, 'authUserCL')

    form.simulate('submit', {
      preventDefault: () => {
      },
      target: [values],
    })

    sinon.assert.calledOnce(actionStub)
  })
})
