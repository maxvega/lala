/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import EditUserCardCard from '../../../src/Pages/Users/EditUserCard'
import ClientAuth from '../../../src/common/ClientAuth'


describe('Component: EditUser', () => {
  const ROLES = [
    {
      id: '1',
      name: 'Admin',
      description: 'Administrador del sitio',
      enabled: true,
      isCore: true,
      revision: '0',
      permissions: [{
        id: '562511529933111302',
        name: 'users.create',
        description: 'can create users',
        enabled: true,
        revision: '0',
        RolePermission: {
          id: '562511530610720774',
          PermissionId: '562511529933111302',
          RoleId: '562511530276388870',
        },
      }],
    },
  ]
  const USER = {
    id: '1',
    firstName: 'User',
    LastName: 'Admin',
    email: 'admin@email.com',
    enabled: true,
    isCore: true,
    created_at: '2020-06-09T19:19:42.775Z',
    updated_at: '2020-06-09T19:19:42.778Z',
    RoleId: null,
    roleId: '562495431457734657',
    revision: '1',
    role: {
      id: '562495431457734657',
      name: 'Admin',
      description: 'Administrador del sitio',
      enabled: true,
      isCore: true,
      revision: '0',
      permissions: [
        {
          id: '562495431415201793',
          name: 'bundles.delete',
          description: 'can delete bundle products',
          enabled: true,
          revision: '0',
          RolePermission: {
            id: '562495431519305729',
            PermissionId: '562495431415201793',
            RoleId: '562495431457734657',
          },
        },
      ],
    },
  }
  const setSelectedUserSpy = jest.fn()
  const getUsersAndRolesSpy = jest.fn()
  const handleFeedbackSpy = jest.fn()
  let sandbox

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should show the modal with update user completed', async () => {
    sandbox.stub(ClientAuth.prototype, 'updateUser').returns(Promise.resolve())
    const EditUserCardCardComponent = enzyme.shallow(
      <EditUserCardCard roles={ROLES} user={USER} setSelectedUser={setSelectedUserSpy} getUsersAndRoles={getUsersAndRolesSpy} handleFeedback={handleFeedbackSpy} />,
    )
    await EditUserCardCardComponent.instance().saveUser()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Usuario editado correctamente',
    })
  })

  test('should show the modal with error when updateUser fails', async () => {
    sandbox.stub(ClientAuth.prototype, 'updateUser').returns(Promise.reject({ response: { data: { message: 'error' } } }))
    const EditUserCardCardComponent = enzyme.shallow(
      <EditUserCardCard roles={ROLES} user={USER} setSelectedUser={setSelectedUserSpy} getUsersAndRoles={getUsersAndRolesSpy} handleFeedback={handleFeedbackSpy} />,
    )
    await EditUserCardCardComponent.instance().updateUser()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Usuario editado correctamente',
      message: 'Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: error',
    })
  })
})
