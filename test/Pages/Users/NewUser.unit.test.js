/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Element from '../../../src/components/Card/Element'
import NewUser from '../../../src/Pages/Users/NewUser'
import ClientAuth from '../../../src/common/ClientAuth'

describe('Component: NewUser', () => {
  const ROLES = [
    {
      id: '1',
      name: 'Admin',
      description: 'Administrador del sitio',
      enabled: true,
      isCore: true,
      revision: '0',
      permissions: [{
        id: '562511529933111302',
        name: 'users.create',
        description: 'can create users',
        enabled: true,
        revision: '0',
        RolePermission: {
          id: '562511530610720774',
          PermissionId: '562511529933111302',
          RoleId: '562511530276388870',
        },
      }],
    },
  ]
  const handleFeedbackSpy = jest.fn()
  const closeSideNavSpy = jest.fn()
  const getUsersAndRolesSpy = jest.fn()

  let sandbox

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should show the modal with error when parameters are missing on save user', async () => {
    const NewUserComponent = enzyme.shallow(
      <NewUser handleFeedback={handleFeedbackSpy} roles={ROLES} />,
    )
    await NewUserComponent.instance().saveUser()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Campos incompletos',
      message: 'Por favor revisa los campos solicitados',
    })
  })

  test('should show the modal with create user completed', async () => {
    const handleFeedbackSpy = jest.fn()
    sandbox.stub(ClientAuth.prototype, 'createUser').returns(Promise.resolve())
    const NewUserComponent = enzyme.shallow(
      <NewUser roles={ROLES} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getUsersAndRoles={getUsersAndRolesSpy} />,
    )
    NewUserComponent.setState({
      firstName: 'New', lastName: 'User', email: 'email@email.com', password: '123', selectedRole: { value: '1234', label: 'Role' },
    })
    await NewUserComponent.instance().saveUser()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Usuario creado correctamente',
      message: 'El usuario ha sido creado correctamente',
    })
  })

  test('should show the modal with error when createUser fails', async () => {
    const handleFeedbackSpy = jest.fn()
    sandbox.stub(ClientAuth.prototype, 'createUser').returns(Promise.reject({ response: { data: { message: 'error' } } }))
    const NewUserComponent = enzyme.shallow(
      <NewUser roles={ROLES} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} getUsersAndRoles={getUsersAndRolesSpy} />,
    )
    NewUserComponent.setState({
      firstName: 'New', lastName: 'User', email: 'email@email.com', password: '123', selectedRole: { value: '1234', label: 'Role' },
    })
    await NewUserComponent.instance().createUser()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Error al crear usuario',
      message: 'Por favor enviar la siguiente información a #black-cyber-squad en Slack: error',
    })
  })
})
