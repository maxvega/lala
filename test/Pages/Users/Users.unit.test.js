/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import UsersWithModal, { Users } from '../../../src/Pages/Users'

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))
describe('Page: Users', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render overlay class', async () => {
    const UsersComponent = enzyme.shallow(
      <UsersWithModal
        title="title"
      />,
    )
    const UsersWithoutModal = UsersComponent.find(Users).dive()
    UsersWithoutModal.setState({ showNewUser: true })
    expect(UsersWithoutModal.find('.overlay-right-modal').length).toEqual(1)
  })

  test('should not render overlay class', async () => {
    const UsersComponent = enzyme.shallow(
      <UsersWithModal
        title="title"
      />,
    )
    const UsersWithoutModal = UsersComponent.find(Users).dive()
    expect(UsersWithoutModal.find('.overlay-right-modal').length).toEqual(0)
  })
})
