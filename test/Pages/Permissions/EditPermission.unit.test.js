/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Element from '../../../src/components/Card/Element'
import EditPermissionCard from '../../../src/Pages/Permissions/EditPermissionCard'
import ClientAuth from '../../../src/common/ClientAuth'

describe('Component: EditPermissionCard', () => {
  let sandbox
  const PERMISSIONS = {
    id: '562511529933111302',
    name: 'users.create',
    description: 'can create users',
    enabled: true,
    revision: '0',
  }
  const ROLE = [{
    id: '1',
    name: 'Admin',
    description: 'Administrador del sitio',
    enabled: true,
    isCore: true,
    revision: '0',
    permissions: [{
      id: '562511529933111302',
      name: 'users.create',
      description: 'can create users',
      enabled: true,
      revision: '0',
      RolePermission: {
        id: '562511530610720774',
        PermissionId: '562511529933111302',
        RoleId: '562511530276388870',
      },
    }],
  }]
  const getPermissionsAndRolesSpy = jest.fn()
  const closeSideNavSpy = jest.fn()
  const handleFeedbackSpy = jest.fn()
  const setSelectedPermissionSpy = jest.fn()

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render the correct number of roles', async () => {
    const EditPermissionCardComponent = enzyme.mount(
      <EditPermissionCard permission={PERMISSIONS} roles={ROLE} />,
    )
    expect(EditPermissionCardComponent.find(Element).length).toEqual(1)
  })

  test('should toggle roles', async () => {
    const EditPermissionCardComponent = enzyme.mount(
      <EditPermissionCard permission={PERMISSIONS} roles={ROLE} />,
    )
    EditPermissionCardComponent.instance().toggleRole(ROLE[0].id)
    expect(EditPermissionCardComponent.instance().state.roles[0].applied).toBeFalsy()
    EditPermissionCardComponent.instance().toggleRole(ROLE[0].id)
    expect(EditPermissionCardComponent.instance().state.roles[0].applied).toBeTruthy()
  })

  test('should show the modal with update permission completed', async () => {
    sandbox.stub(ClientAuth.prototype, 'updatePermission').returns(Promise.resolve())
    const EditPermissionCardComponent = enzyme.shallow(
      <EditPermissionCard handleFeedback={handleFeedbackSpy} roles={ROLE} permission={PERMISSIONS} getPermissionsAndRoles={getPermissionsAndRolesSpy} setSelectedPermission={setSelectedPermissionSpy} />,
    )
    await EditPermissionCardComponent.instance().updatePermission()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Permiso editado correctamente',
    })
  })

  test('should show the modal with error when updatePermission fails', async () => {
    sandbox.stub(ClientAuth.prototype, 'updatePermission').returns(Promise.reject({ response: { data: { message: 'error' } } }))
    const EditPermissionCardComponent = enzyme.shallow(
      <EditPermissionCard handleFeedback={handleFeedbackSpy} roles={ROLE} permission={PERMISSIONS} getPermissionsAndRoles={getPermissionsAndRolesSpy} setSelectedPermission={setSelectedPermissionSpy} />,
    )
    await EditPermissionCardComponent.instance().updatePermission()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Error al actualizar',
      message: 'Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: error',
    })
  })
})
