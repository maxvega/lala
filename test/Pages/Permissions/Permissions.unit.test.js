/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import PermissionsWithModal, { Permissions } from '../../../src/Pages/Permissions'

jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))
describe('Page: Permissions', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('should render overlay class', async () => {
    const PermissionsComponent = enzyme.shallow(
      <PermissionsWithModal
        title="title"
      />,
    )
    const PermissionsWithoutModal = PermissionsComponent.find(Permissions).dive()
    PermissionsWithoutModal.setState({ showNewPermission: true })
    expect(PermissionsWithoutModal.find('.overlay-right-modal').length).toEqual(1)
  })

  test('should not render overlay class', async () => {
    const PermissionsComponent = enzyme.shallow(
      <PermissionsWithModal
        title="title"
      />,
    )
    const PermissionsWithoutModal = PermissionsComponent.find(Permissions).dive()
    expect(PermissionsWithoutModal.find('.overlay-right-modal').length).toEqual(0)
  })
})
