/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme from 'enzyme'
import sinon from 'sinon'
import Element from '../../../src/components/Card/Element'
import NewPermission from '../../../src/Pages/Permissions/NewPermission'
import ClientAuth from '../../../src/common/ClientAuth'

describe('Component: NewPermissions', () => {
  const ROLES = [
    {
      id: '1',
      name: 'Admin',
      description: 'Administrador del sitio',
      enabled: true,
      isCore: true,
      revision: '0',
      permissions: [{
        id: '562511529933111302',
        name: 'users.create',
        description: 'can create users',
        enabled: true,
        revision: '0',
        RolePermission: {
          id: '562511530610720774',
          PermissionId: '562511529933111302',
          RoleId: '562511530276388870',
        },
      }],
    },
  ]
  let sandbox

  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })
  test('should render the correct number of roles', async () => {
    const NewPermissionComponent = enzyme.mount(
      <NewPermission roles={ROLES} />,
    )
    expect(NewPermissionComponent.find(Element).length).toEqual(1)
  })

  test('should show the modal with create permission completed', async () => {
    const handleFeedbackSpy = jest.fn()
    const closeSideNavSpy = jest.fn()
    sandbox.stub(ClientAuth.prototype, 'createPermission').returns(Promise.resolve())
    const NewPermissionComponent = enzyme.shallow(
      <NewPermission roles={ROLES} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} />,
    )
    await NewPermissionComponent.instance().createPermission()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({ title: 'Permiso creado correctamente' })
  })

  test('should show the modal with error when createPermission fails', async () => {
    const handleFeedbackSpy = jest.fn()
    const closeSideNavSpy = jest.fn()
    sandbox.stub(ClientAuth.prototype, 'createPermission').returns(Promise.reject({ response: { data: { message: 'error' } } }))
    const NewPermissionComponent = enzyme.shallow(
      <NewPermission roles={ROLES} handleFeedback={handleFeedbackSpy} closeSideNav={closeSideNavSpy} />,
    )
    await NewPermissionComponent.instance().createPermission()
    expect(handleFeedbackSpy).toHaveBeenCalledWith({
      title: 'Error al crear',
      message: 'Por favor contactarse con el equipo #black-cyber-squad en Slack con la siguiente información: error',
    })
  })

  test('should toggle role', async () => {
    const NewPermissionComponent = enzyme.mount(
      <NewPermission roles={ROLES} />,
    )
    NewPermissionComponent.instance().toggleRole(ROLES[0].id)
    expect(NewPermissionComponent.instance().state.roles[0].applied).toBeTruthy()
    NewPermissionComponent.instance().toggleRole(ROLES[0].id)
    expect(NewPermissionComponent.instance().state.roles[0].applied).toBeFalsy()
  })
})
