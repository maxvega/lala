/**
 * @jest-environment jsdom
 */
import React from 'react'
import enzyme, { configure } from 'enzyme'
import sinon from 'sinon'
import Adapter from 'enzyme-adapter-react-16'
import Home from '../../../src/Pages/Home'
import ClientAlgolia from '../../../src/common/ClientAlgolia'

configure({ adapter: new Adapter() })

window.__ENV__ = { LOCAL_AUTH_KEY: 'wm-blackcyber-auth-key' }

describe('Pages: Home', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('Should set algoliaSearches state', async () => {
    sandbox.stub(ClientAlgolia.prototype, 'getTopSearches').returns(new Promise((resolve) => {
      resolve({ searches: [{}, {}] })
    }))
    const HomeComponent = enzyme.shallow(
      <Home />,
    )
    await HomeComponent.instance().getSearches()
    expect(HomeComponent.state().algoliaSearches.length).toBe(2)
  })
})
