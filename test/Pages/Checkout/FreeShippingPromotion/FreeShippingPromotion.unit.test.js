/**
 * @jest-environment jsdom
 */
import { render, screen, waitFor } from '@testing-library/react'
import React from 'react'
import sinon from 'sinon'
import FreeShippingPromotion from '../../../../src/Pages/Checkout/FreeShippingPromotion'
import '@testing-library/jest-dom/extend-expect'
import ClientBFF from '../../../../src/common/ClientBFF'

describe('Page: FreeShippingPromotion', () => {
  afterEach(() => { sinon.restore() })

  test('should show loader at first', () => {
    sinon.stub(ClientBFF.prototype, 'checkoutGetFreeShippingPromotionConfig').resolves({ freeShippingPromotionConfig: { enabled: true, value: 1500000 } })
    render(<FreeShippingPromotion />)
    expect(screen.getByTestId('loader-container')).toBeInTheDocument()
  })

  test('should show deactivate button when promotion is active', async () => {
    const spy = sinon.stub(ClientBFF.prototype, 'checkoutGetFreeShippingPromotionConfig').resolves({ freeShippingPromotionConfig: { enabled: true, value: 500000 } })
    render(<FreeShippingPromotion />)

    await waitFor(() => {
      sinon.assert.called(spy)
      const input = screen.getByTestId('free-shipping-value')
      expect(input).toBeInTheDocument()
      expect(input.value).toBe('500000')
      expect(screen.queryByText('Desactivado')).toBeNull()
      expect(screen.getByText('Activado')).toBeInTheDocument()
      expect(screen.getByTestId('deactivate-button')).toBeInTheDocument()
    })
  })

  test('should show "Actualizar monto mínimo" button when promotion is active', async () => {
    const spy = sinon.stub(ClientBFF.prototype, 'checkoutGetFreeShippingPromotionConfig').resolves({ freeShippingPromotionConfig: { enabled: true, value: 1500000 } })
    render(<FreeShippingPromotion />)

    await waitFor(() => {
      sinon.assert.called(spy)
      const input = screen.getByTestId('free-shipping-value')
      expect(input).toBeInTheDocument()
      expect(input.value).toBe('1500000')
      expect(screen.getByTestId('activate-button')).toBeInTheDocument()
      expect(screen.getByText('Actualizar monto mínimo')).toBeInTheDocument()
    })
  })

  test('should show only "Activar" button when promotion is not active', async () => {
    const spy = sinon.stub(ClientBFF.prototype, 'checkoutGetFreeShippingPromotionConfig').resolves({ freeShippingPromotionConfig: { enabled: false, value: 1000000 } })
    render(<FreeShippingPromotion />)

    await waitFor(() => {
      sinon.assert.called(spy)
      const input = screen.getByTestId('free-shipping-value')
      expect(input).toBeInTheDocument()
      expect(input.value).toBe('1000000')
      expect(screen.getByTestId('activate-button')).toBeInTheDocument()
      expect(screen.getByText('Activar')).toBeInTheDocument()
    })
  })
})
