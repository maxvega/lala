const showParametersList = [
  {
    name: 'name',
    label: 'Nombre',
    isShow: true,
    value: '',
    type: 'input',
  },
  {
    name: 'link',
    label: 'URL',
    isShow: true,
    value: '',
    type: 'input',
  },
  {
    name: 'tag',
    label: 'Tag Google Analytics',
    isShow: true,
    value: '',
    type: 'input',
  },
  {
    name: 'startDate',
    label: 'Fecha de inicio',
    isShow: true,
    value: '',
    type: 'date',
  },
  {
    name: 'endDate',
    label: 'Fecha de fin',
    isShow: true,
    value: '',
    type: 'date',
  },
  {
    name: 'counterBgColor',
    label: 'Background color',
    isShow: true,
    value: '#',
    type: 'input',
  },
  {
    name: 'counterFontColor',
    label: 'Font color',
    isShow: true,
    value: '#',
    type: 'input',
  },
]

export default showParametersList
