/**
 * @jest-environment jsdom
 */
import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapater from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import * as FileSaver from 'file-saver'
import XLSX from 'xlsx'
import BulksBundles from '../../../../src/Pages/Products/BundleCreator/BulksBundles'
import ClientBFF from '../../../../src/common/ClientBFF'
import * as Helper from '../../../../src/Helper'

const {
  bundlesListDBMock, dataBundlesMock, dataBundleAssociatedsMock, expectBundlesListToDBMock, dataBundleAssociatedsWithoutPackMock, sheetMock,
} = require('../../../Mocks/bulksBundles')

configure({ adapter: new Adapater() })

jest.mock('file-saver', () => ({ saveAs: jest.fn() }))


jest.mock('react-modal', () => ({
  setAppElement: jest.fn(),
}))

window.__ENV__ = { baseURL: 'http://localhost:3000', timeout: 5000 }
window.open = () => true

describe('Page: BulksBundles', () => {
  let sandbox
  beforeEach(() => {
    sandbox = sinon.createSandbox()
  })

  afterEach(() => {
    sandbox.restore()
  })

  test('handleGenerateXLSXClick: should generate xlsx file from jsonDB', async () => {
    const wrapper = shallow(<BulksBundles />)

    wrapper.setState({ bundlesListDB: bundlesListDBMock })
    await wrapper.instance().handleGenerateXLSXClick()
    expect(FileSaver.saveAs).toHaveBeenCalled()
  })


  test('handleUploadToStaging: should upload bundles to staging', async () => {
    sandbox.stub(XLSX, 'read').returns(sheetMock)
    sandbox.stub(XLSX.utils, 'sheet_to_json').returns([])
    sandbox.stub(Helper, 'generateChunksOfArray').returns([expectBundlesListToDBMock])

    const wrapper = shallow(<BulksBundles />)

    const setProductsToStaging = sandbox.stub(ClientBFF.prototype, 'updateProductsToStaging').returns(Promise.resolve([[], []]))

    await wrapper.instance().handleUploadToStaging({ target: { result: 'data' } })

    sandbox.assert.calledOnce(setProductsToStaging)
    expect(wrapper.state().generatedJSON).toEqual([expectBundlesListToDBMock])
  })

  test('handleUploadToProduction: should upload bundles to production', async () => {
    const wrapper = shallow(<BulksBundles />)
    const setProductsToProduction = sandbox.stub(ClientBFF.prototype, 'updateProductsToProduction').returns(Promise.resolve([[], []]))

    wrapper.setState({ generatedJSON: bundlesListDBMock })
    await wrapper.instance().handleUploadToProduction()
    sandbox.assert.calledOnce(setProductsToProduction)
  })

  test('generateJSONfromXLSX: should return correct data for bundles', async () => {
    const wrapper = shallow(<BulksBundles />)

    const generatedJSON = wrapper.instance().generateJSONfromXLSX(dataBundlesMock, dataBundleAssociatedsMock)

    expect(generatedJSON.length).toEqual(expectBundlesListToDBMock.length)
    expect(generatedJSON).toEqual(expectBundlesListToDBMock)
    expect(generatedJSON[0].length).toEqual(expectBundlesListToDBMock[0].length)
    expect(generatedJSON[0].price).toEqual(expectBundlesListToDBMock[0].price)
    expect(generatedJSON[1].associatedProducts.length).toEqual(expectBundlesListToDBMock[1].associatedProducts.length)
    expect(generatedJSON[1].price).toEqual(expectBundlesListToDBMock[1].price)
  })

  test('generateJSONfromXLSX: should ignore bundles without associatedProducts', async () => {
    const wrapper = shallow(<BulksBundles />)

    const generatedJSON = wrapper.instance().generateJSONfromXLSX(dataBundlesMock, dataBundleAssociatedsWithoutPackMock)

    expect(generatedJSON.length).toBe(1)
  })

  test('getBundlesProducts: should return bundles list from DB', async () => {
    sandbox.stub(ClientBFF.prototype, 'getBundles').returns(Promise.resolve(bundlesListDBMock))

    const wrapper = shallow(<BulksBundles />)

    expect(wrapper.state().bundlesListDB.length).toBe(0)
    await wrapper.instance().getBundlesProducts()
    expect(wrapper.state().bundlesListDB.length).toBe(3)
  })
})
