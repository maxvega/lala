/**
 * @jest-environment jsdom
 */
import { isExtensionValid, groupArrayByKey, cleanString } from '../../src/Helper'


describe('helper: test', () => {
  test('Should validate an allowed file extension', async () => {
    const response = isExtensionValid('file.png')
    expect(response).toBeTruthy()
  })

  test('Should return false when file extention is not allowed', async () => {
    const response = isExtensionValid('file.txt')
    expect(response).toBeFalsy()
  })
  test('Should group an array of objects by key', async () => {
    const originalData = [
      {
        'Mundo tecno': 1, 'Mundo casa': 2, 'Mundo fitness': 3, 'Mundo entretenido': 4,
      },
      {
        'Mundo tecno': 5, 'Mundo casa': 6, 'Mundo fitness': 7, 'Mundo entretenido': 8,
      },
      {
        'Mundo tecno': 9, 'Mundo casa': 10, 'Mundo fitness': 11, 'Mundo entretenido': 12,
      },
      {
        'Mundo tecno': 13, 'Mundo casa': 14, 'Mundo fitness': 15, 'Mundo entretenido': 16,
      },
      {
        'Mundo tecno': '', 'Mundo casa': 17, 'Mundo fitness': '', 'Mundo entretenido': 18,
      },
    ]
    const expectedObject = [
      { tab: 'Mundo tecno', products: [1, 5, 9, 13] },
      { tab: 'Mundo casa', products: [2, 6, 10, 14, 17] },
      { tab: 'Mundo fitness', products: [3, 7, 11, 15] },
      { tab: 'Mundo entretenido', products: [4, 8, 12, 16, 18] },
    ]
    const objectKeys = Object.keys(originalData[0])
    const response = objectKeys.map(key => groupArrayByKey(originalData, key))
    expect(response).toEqual(expectedObject)
  })
  test('Should leave only alphanumeric values in a string', async () => {
    const notAlphanumericString = 'Str"ing wi\'th not "alphanumer"ic va\'lu"e"s'
    expect(cleanString(notAlphanumericString)).toEqual('String with not alphanumeric values')
  })
  test('Should not clean string when empty or nullable string is provide it', async () => {
    expect(cleanString(null)).toEqual('')
    expect(cleanString('')).toEqual('')
    expect(cleanString(undefined)).toEqual('')
  })
})
