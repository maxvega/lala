FROM node:12-alpine

WORKDIR /workspace
COPY configs /workspace/configs
COPY docker /workspace/docker
COPY node_modules /workspace/node_modules
COPY public /workspace/public
COPY scripts /workspace/scripts
COPY src /workspace/src
COPY .babelrc /workspace
COPY package.json /workspace

RUN apk add --update make python g++ nginx ca-certificates

ARG CONSUL_TEMPLATE_VERSION=0.19.4
RUN wget "https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz"
RUN tar zxfv consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz

RUN yarn production

COPY configs/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY configs/nginx/nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT docker/entrypoint.sh
