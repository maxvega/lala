# BuySmart Content Manager

[![Quality Gate Status](https://kitt-badges.k8s.walmart.com/kittbadge?org=Catalogo-Extendido&repo=blackcyber-bcm-landing)](https://concord.prod.walmart.com/#/org/strati/project/pipeline/process?meta.repoMetadata=Catalogo-Extendido%2Fblackcyber-bcm-landing&limit=20)

[![Build Status](https://ci.wcnp.walmart.com/buildStatus/icon?job=catalogo-extendido/blackcyber-bcm-landing/master)](https://ci.wcnp.walmart.com/job/catalogo-extendido/job/blackcyber-bcm-landing)

[![Quality Gate Status](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=alert_status)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Coverage](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=coverage)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Reliability Rating](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=reliability_rating)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Security Rating](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=security_rating)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Maintainability Rating](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=sqale_rating)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Technical Debt](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=sqale_index)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Bugs](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=bugs)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)
[![Vulnerabilities](https://sonar.looper.prod.walmartlabs.com/api/project_badges/measure?project=blackcyber.bcm.landing&metric=vulnerabilities)](https://sonar.looper.prod.walmartlabs.com/dashboard?id=blackcyber.bcm.landing)


## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:3000
yarn start

# build for production with minification
yarn production
```

#####Routes
In ```src/components/SideNavMenu/index.js``` you can find the routes enabled.
In ```src/navigationList.js``` you can find the routes that are shown in the menu and their permissions.

If you need to add a new one, both files should be updated. 
In other words, you should add the routes in ```SideNavMenu.js``` 
and the route configuration in ```navigationList.js```.
 

For a detailed explanation on how things work, ask BlackCyberSquad 👊
