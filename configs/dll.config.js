const path = require('path');
const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const sourcePath = path.join(__dirname, '../src');
const modulePath = path.resolve(__dirname, '../node_modules');
const libraryPath = path.resolve(__dirname, '../src/dll');
const outputPath = path.resolve(__dirname, '../dist/js/dll');
const dllJSONPath = path.resolve(__dirname, '../dist/js/dll/[name].json');

module.exports = {
  mode: 'production',

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [modulePath, sourcePath],
  },

  entry: {
    // static core libs, almost never changed
    dll: [libraryPath],
  },

  output: {
    filename: '[name].js',
    path: outputPath,
    library: '[name]',
  },

  module: {
    rules: [
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
    ],
  },

  plugins: [
    new webpack.DllPlugin({
      name: '[name]',
      path: dllJSONPath,
    }),
    new CompressionPlugin({
      test: /[.js|.css]/,
    }),
    new UglifyJsPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
};
