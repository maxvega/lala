const path = require('path')
const webpack = require('webpack')

const sourcePath = path.join(__dirname, '../src')

module.exports = {
  mode: 'production',

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [path.resolve(__dirname, '../node_modules'), sourcePath],
  },

  entry: {
    // static core libs, almost never changed
    library: ['./src/dll'],
  },

  output: {
    filename: '[name].js',
    path: path.join(__dirname, '../dist/js/dll'),
    library: '[name]',
  },

  plugins: [
    new webpack.DllPlugin({
      name: '[name]',
      path: './dist/js/dll/[name].json',
    }),
  ],
}
