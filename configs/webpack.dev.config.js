const path = require('path')
const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin')
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware')
const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const dll = require('../dist/js/dll/library.json')

const sourcePath = path.join(__dirname, '../')

const ts = new Date().getTime()

module.exports = {
  entry: {
    main: path.resolve(__dirname, `${sourcePath}/src/`),
  },

  mode: 'development',

  devtool: 'eval-source-map',

  output: {
    publicPath: '/',
    path: path.join(__dirname, '../dist'),
    filename: `js/[id].js?ts=${ts}`,
    chunkFilename: `js/[name].js?ts=${ts}`,
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.(jsx|js)?$/,
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
        options: {
          // This is a feature of `babel-loader` for webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
          cacheDirectory: true,
          plugins: ['react-hot-loader/babel'],
        },
      },
      {
        test: /\.css?$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=./font/[name].[ext]',
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [path.resolve(__dirname, '../node_modules'), sourcePath],
    alias: {
      Src: path.resolve(`${sourcePath}/src`),
    },
  },

  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
    },
  },

  devServer: {
    contentBase: path.join(__dirname, '../dist'),
    inline: true,
    historyApiFallback: {
      index: '/',
    },
    overlay: false,
    before(app) {
      // This lets us open files from the runtime error overlay.
      app.use(errorOverlayMiddleware())
      app.use(noopServiceWorkerMiddleware())
    },
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: `css/[id].css?ts=${ts}`,
      chunkFilename: `css/[id].css?ts=${ts}`,
    }),

    new webpack.DllReferencePlugin({
      context: path.join(__dirname, '../dist'),
      manifest: dll,
    }),

    new ManifestPlugin({ writeToFileEmit: true }),

    new webpack.DefinePlugin({
      __PROD__: false,
    }),

    new HtmlWebPackPlugin({
      filename: './index.html',
      template: './src/template/index.html',
      inject: true,
      hash: true,
      excludeAssets: [/home\..*/],
    }),

    new HtmlWebpackExcludeAssetsPlugin(),
    new CopyWebpackPlugin([
      { from: 'public/images', to: 'images' },
      { from: 'public/fonts', to: 'fonts' },
      { from: 'public/favicon.ico' },
      { from: 'public/environment.js' },
      { from: 'public/styles', to: 'styles' },
    ]),
  ],
}
