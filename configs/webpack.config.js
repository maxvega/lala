const path = require('path')
const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin')
const ResourceHintWebpackPlugin = require('resource-hints-webpack-plugin')
const ConfigWebpackPlugin = require('config-webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { InjectManifest } = require('workbox-webpack-plugin')

const sourcePath = path.join(__dirname, '../')

const ts = new Date().getTime()

module.exports = {
  entry: {
    main: path.resolve(__dirname, `${sourcePath}/src/`),
  },

  mode: 'production',

  devtool: 'none',

  output: {
    publicPath: '/',
    path: path.join(__dirname, '../dist'),
    filename: `js/[id].js?ts=${ts}`,
    chunkFilename: `js/[id].js?ts=${ts}`,
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.(jsx|js)?$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.css?$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=./font/[name].[ext]',
      },
      {
        test: /\.(jpg|jpeg|png)$/,
        loader: 'file-loader?name=./images/[name].[ext]',
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
    ],
  },

  resolve: {
    extensions: ['.mjs', '.js', '.jsx'],
    modules: [path.resolve(__dirname, '../node_modules'), sourcePath],
    alias: {
      Src: path.resolve(`${sourcePath}/src`),
    },
  },

  optimization: {
    namedModules: false,
    minimize: true,
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
    },
  },

  plugins: [
    new ConfigWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: `css/[id].css?ts=${ts}`,
      chunkFilename: `css/[id].css?ts=${ts}`,
    }),
    new CompressionPlugin({
      test: /[.js|.css]/,
    }),
    new webpack.DefinePlugin({
      __PROD__: true,
    }),
    new webpack.DllReferencePlugin({
      context: path.resolve(__dirname, '../'),
      manifest: require('../dist/js/dll/dll.json'),
    }),

    new HtmlWebPackPlugin({
      filename: './index.html',
      template: './src/template/index.html',
      inject: true,
      hash: false,
      excludeAssets: [/home\..*/],
      prefetch: ['**/*.*'],
      preload: ['**/*.*'],
    }),
    new HtmlWebpackExcludeAssetsPlugin(),
    new ResourceHintWebpackPlugin(),
    // new InjectManifest({swSrc: 'src/sw.js'}),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new CopyWebpackPlugin([
      { from: 'public/images', to: 'images' },
      { from: 'public/fonts', to: 'fonts' },
      { from: 'public/styles', to: 'styles' },
      // { from: 'src/sw.js', to: 'sw.js' },
      { from: 'src/manifest.json', to: 'manifest.json' },
      { from: 'public/favicon.ico' },
      { from: 'public/environment.js' },
    ]),
    // new BundleAnalyzerPlugin(),
  ],

}
