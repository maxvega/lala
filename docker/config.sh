#!/bin/bash
#
# Add assignment to dist
#

echo "var moduleExport = false
if (typeof window === 'undefined' ) {
    var window = {}
    moduleExport = true
}" >/workspace/dist/environment.js

configJson=`cat /etc/secrets/config.txt`

echo "window.__ENV__ = $configJson" >>/workspace/dist/environment.js

echo "if (moduleExport) {
    module.exports = window
}" >>/workspace/dist/environment.js

#
# Add assignment to public
#
echo "var moduleExport = false
if (typeof window === 'undefined' ) {
    var window = {}
    moduleExport = true
}" >/workspace/public/environment.js

configJson=`cat /etc/secrets/config.txt`

echo "window.__ENV__ = $configJson" >>/workspace/public/environment.js

echo "if (moduleExport) {
    module.exports = window
}" >>/workspace/public/environment.js
