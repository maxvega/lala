#!/bin/sh

set -e

echo "Validating required params..."

if [ -z $NODE_ENV ]; then
  echo "NODE_ENV is missing"
  exit 1
fi

if [ -z $CONSUL_HTTP_ADDR ]; then
  echo "CONSUL_HTTP_ADDR is missing"
  exit 2
fi

if [ -z $CONSUL_HTTP_TOKEN ]; then
  echo "CONSUL_HTTP_TOKEN is missing"
  exit 3
fi

echo "Getting configuration from consul and BOOT..."

./consul-template -config ./docker/config.hcl -once -log-level trace

exec nginx -g 'daemon off;'

