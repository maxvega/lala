#!/bin/sh

FILE=/etc/secrets/config.txt
set -e

echo "Validating required params..."

if [ -z $NODE_ENV ]; then
  echo "NODE_ENV is missing"
  exit 1
fi


if [ -f "$FILE" ]; then
  echo "Getting configuration from vault..."
  sh ./docker/config.sh
fi

echo "BOOTING..."

nginx -g 'daemon off;'

