var moduleExport = false
if (typeof window === 'undefined' ) {
    var window = {}
    moduleExport = true
}

window.__ENV__ = {
    appId: "{{key (print "buysmart/landing/" (env "NODE_ENV") "/algolia/appId")}}",
    baseURLBFF: "{{key (print "buysmart/bcm/" (env "NODE_ENV") "/BASE_URL_BFF")}}",
    baseURLStagingBFF: "{{key (print "buysmart/bcm/staging/BASE_URL_BFF")}}",
    baseURLAuth: "{{key (print "buysmart/bcm/" (env "NODE_ENV") "/BASE_URL_AUTH_API")}}",
    analyticsApiKey: "{{key (print "buysmart/landing/" (env "NODE_ENV") "/algolia/analyticsApiKey")}}",
    storageBaseUrl: "{{key (print "buysmart/landing/" (env "NODE_ENV") "/storageBaseUrl")}}",
    baseLandingURLbff: "{{key (print "buysmart/landing/" (env "NODE_ENV") "/baseURLbff")}}",
    REDIS_FLUSH_TOKEN: "{{key (print "buysmart/bff/" (env "NODE_ENV") "/REDIS_FLUSH_TOKEN")}}",
}

if (moduleExport) {
    module.exports = window
}
